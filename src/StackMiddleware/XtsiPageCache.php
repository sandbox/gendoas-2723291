<?php

namespace Drupal\sxt_slogitem\StackMiddleware;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\XtUserData;
use Drupal\page_cache\StackMiddleware\PageCache;
use Symfony\Component\HttpFoundation\Request;

/**
 * Prevent to display cached page for pages that are to be redirected.
 */
class XtsiPageCache extends PageCache {

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = TRUE) {
//    return parent::handle($request, $type, $catch);
    if ($type !== self::MASTER_REQUEST) {
      return parent::handle($request, $type, $catch);
    }
    
//    //Note: current user is not set yet !!!
//    $account = \Drupal::currentUser();
    
    $path_info = $request->getPathInfo();
//todo::current::   XtsiRoutes: one route for all user redirections:: 'entity.user.canonical' ???
    if (preg_match('/^\/user\//', $path_info) && !preg_match('/^\/user\/logout$/', $path_info)) {
      $request->attributes->set('core_redirect_user', TRUE);
      return parent::pass($request, $type, $catch);
    }

    $basePath = trim(SlogXt::getBasePath('sxt_slogitem'), '/');
    $patterns = [
      "/^\/$basePath\/tid\/\d+$/",
      "/^\/$basePath\/sid\/\d+$/",
    ];
    foreach ($patterns as $pattern) {
      if (preg_match($pattern, $path_info)) {
        return parent::pass($request, $type, $catch);
      }        
    }
    
    return parent::handle($request, $type, $catch);
  }

}
