<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Theme\XtsiNegotiator.
 */

namespace Drupal\sxt_slogitem\Theme;

use Drupal\slogxt\SlogxtMin;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Negotiates the theme for the ... pages.
 */
class XtsiNegotiator implements ThemeNegotiatorInterface {
  
  static $sjqlout_required = FALSE;
  
  public static function isThemeSlogJqLoutRequired() {
    return self::$sjqlout_required;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return SlogxtMin::SJQLOUT_THEME;
  }
  
  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    if ($route_object = $route_match->getRouteObject()) {
      $required = $route_object->getOption(SlogxtMin::SJQLOUT_THEME_ROUTE);
      self::$sjqlout_required = (isset($required) && $required);
    }
    return self::$sjqlout_required;
  }

}
