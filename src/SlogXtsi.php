<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\SlogXtsi.
 */

namespace Drupal\sxt_slogitem;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\XtUserData;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\node\Entity\Node;
use Drupal\block\Entity\Block;
use Drupal\user\RoleInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Crypt;

/**
 * Static helper functions and ...
 */
class SlogXtsi extends SlogXt {

  /**
   * The id of the promoted vocabulary within the _sys toolbar.
   */
  const SYS_VOCAB_ID_PROMOTED = 'promoted';

  /**
   * The id of the surprise vocabulary within the _sys toolbar.
   */
  const SYS_VOCAB_ID_SURPRISE = 'surprise';

  /**
   * The id of the bookmark vocabulary within the _sys toolbar.
   */
  const SYS_VOCAB_ID_BMSTORAGE = 'bmstorage';

  /**
   * Cache object for table 'cache_slogapp'
   *
   * @var Drupal\Core\Cache\CacheBackendInterface 
   */
  static $cache;
  static $tbsubmenuConst;
  static $js_autorun = [];
  static $cachedRankByVocabulary = NULL;
  static $cachedIsDebugMode = NULL;
  protected static $storage = NULL;
  protected static $config = NULL;

  public static function getConfig() {
    if (empty(self::$config)) {
      self::$config = \Drupal::config('sxt_slogitem.settings');
    }
    return self::$config;
  }

  public static function getJsAutorun() {
    return self::$js_autorun;
  }

  public static function setJsAutorun($value) {
    if (is_array($value)) {
      self::$js_autorun += $value;
    }
    return self::$js_autorun;
  }

  /**
   * Returns a list of editable toolbars for bundle.
   */
  public static function getJsTbEditable($bundle) {
    $result = [
      SlogTx::TOOLBAR_ID_SYS => FALSE,
      SlogTx::TOOLBAR_ID_NODE_SUBSYS => FALSE,
    ];
    $xt_perms = SlogXt::getTxPermissions();
    $toolbars = SlogTb::getToolbarsVisible();
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $has_perm = FALSE;
      switch ($toolbar_id) {
        case 'user':
          $has_perm = TRUE;
          break;
        case 'role':
          $role_perms = SlogXt::getSlogxtGroupPermHandler()->getPermissionsCurrent();
          $has_perm = in_array('admin sxtrole-menu', $role_perms);
          if (!$has_perm) {
            $for_role_perms = SlogXt::pluginManager('edit')->getForRolePermissions($bundle);
            $intersect = array_intersect($role_perms, $for_role_perms);
            $has_perm = !empty($intersect);
          }
          break;
        default:
          $has_perm = !empty($xt_perms['administer slog taxonomy']);
          if (!$has_perm) {
            $perm = "administer toolbar $toolbar_id";
            $has_perm = !empty($xt_perms[$perm]);
          }
          break;
      }

      $result[$toolbar_id] = (boolean) $has_perm;
    }

    return $result;
  }

  public static function rankByVocabulary() {
    if (!isset(self::$cachedRankByVocabulary)) {
      self::$cachedRankByVocabulary = (boolean) self::getConfig()->get('rank_by_vocabulary');
    }
    return self::$cachedRankByVocabulary;
  }

  public static function hasBasepagesRedirect() {
    return (boolean) self::getConfig()->get('basepages_redirect');
  }

  public static function isDebugMode() {
    if (!isset(self::$cachedIsDebugMode)) {
      self::$cachedIsDebugMode = (boolean) self::getConfig()->get('debug_mode');
    }
    return self::$cachedIsDebugMode;
  }

  /**
   * 
   */
//  public static function getTbSubmenuHiddenBlock($vocabulary) {
//    $entity_type = $vocabulary->getTargetEnityTypeId(); 
  public static function getTbSubmenuHiddenBlock($entity_type) {
//todo::current:: TxRootTermGetPlugin - getTbSubmenuHiddenBlock
//todo::current:: TxRootTermGetPlugin - getSubmenuVocabulary
    $block = FALSE;
    $vocabulary = static::getSubmenuVocabulary($entity_type);
    $separator = PluginBase::DERIVATIVE_SEPARATOR;
    $toolbar = $vocabulary ? $vocabulary->getToolbar() : false;
    $toolbar_id = $toolbar ? $toolbar->id() : '';
    $block_plugin_id = SlogTb::STB_ID_CONTENT . $separator . $toolbar_id;
    if ($vocabulary && $toolbar && SlogTb::hasSlogtbBlock($block_plugin_id, FALSE)) {
      // originally there is no sys_submenu block created,
      // we create a helper block once for handling hidden blocks
      // note: this block is not to get by SlogTb::getBlockByPluginId()
      $entity_id = "xtsi_sys_submenu_tb_{$entity_type}";
      if (!$block = Block::load($entity_id)) {
        $plugin_id = SlogTb::STB_ID_TOOLBAR . $separator . $toolbar_id;
        $values = [
          'id' => $entity_id,
          'plugin' => $plugin_id,
          'theme' => \Drupal::theme()->getActiveTheme()->getName(),
        ];
        \Drupal::entityTypeManager()->getStorage('block')->create($values)->save();
        $block = Block::load($entity_id);
      }
    }

    return $block;
  }

//            public static function prepareSysNodeDefaultRootTerm($vid, $node_id, $description = '') {
//              $node = Node::load($node_id);
//              if ($node) {
//                $data = [
//                  'description' => t('Submenu for this node'),
//                  'target_entity_plugin_id' => 'node',
//                  'provider' => 'sxt_slogitem',
//                ];
//                $vocabulary = SlogTx::ensureVocabulary($vid, $data, TRUE);
//              }
//
//              if ($vocabulary && $vocabulary->isNodeTargetEntity()) {
//                $plugin = $vocabulary->getTargetEntityPlugin();
//                if ($plugin) {
//                  $target_name = $plugin->buildTargetTermName($node_id);
//                  $values = [
//                    'vid' => $vid,
//                    'name' => $target_name,
//                    'description' => $description,
//                    'parent' => 0,
//                    'langcode' => $vocabulary->get('langcode'),
//                    'status' => TRUE,
//                    'locked' => TRUE,
//                  ];
//                  $vocabulary->addTargetTerm($values);
//                }
//              }
//
//              return self::getSysNodeDefaultRootTerm($vid, $node_id);
//todo::current:: TxRootTermGetPlugin - new::deprecated
//            }

  public static function getSubmenuVocabulary($entity_type) {
//todo::current:: TxRootTermGetPlugin - getSubmenuVocabulary
    $separator = SlogTx::SLOG_NAME_DELIMITER;
    $submenu = SlogTx::SYS_VOCAB_ID_SUBMENU;
    $vid = "_{$entity_type}{$separator}{$submenu}";
    return SlogTx::getVocabulary($vid);
  }

//            public static function getNodeSubmenuVid() {
//todo::current:: TxRootTermGetPlugin - new::deprecated
//              return SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_NODE_SUBSYS, SlogTx::SYS_VOCAB_ID_SUBMENU]);
//            }

//            public static function prepareNodeSubmenuRootTerm($node_id) {
//todo::current:: TxRootTermGetPlugin - new::deprecated
//              return SlogTx::getSysNodeRootTermSubmenu($node_id, FALSE);
//          //              $vid = self::getNodeSubmenuVid();
//          //              $description = t('System target term for node @nid', ['@nid' => $node_id]);
//          //              return self::prepareSysNodeDefaultRootTerm($vid, $node_id, $description);
//            }

//                public static function prepareSysSurpriseRootTerm($extra, $user_role) {
//todo::current:: TxRootTermGetPlugin - new::deprecated
//                  $vid = self::getNodeSubmenuVid();
//                  $args = [
//                    '@entity_label' => $user_role->label(),
//                    '@extra' => $extra,
//                  ];
//                  $description = t('@entity_label: TbTab for @extra', $args);
//                  return SlogTx::prepareTbtabRootTerm($vid, $user_role, NULL, $description, 'sxt_slogitem');
//                }

//              public static function getSysNodeDefaultRootTerm($vid, $node_id, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
//todo::current:: TxRootTermGetPlugin - new::deprecated
//                $rootterm = FALSE;
//                $node = Node::load($node_id);
//                $vocabulary = SlogTx::getVocabulary($vid);
//
//                if ($node && $vocabulary && $vocabulary->isNodeTargetEntity()) {
//                  if (($plugin = $vocabulary->getTargetEntityPlugin()) && $plugin->isValid()) {
//                    $target_name = $plugin->buildTargetTermName($node_id);
//                    $target_term = $vocabulary->getTargetTermByName($target_name, $reset = FALSE, $do_create = TRUE);
//                    if ($target_term && $target_term->label() === $target_name) {
//                      $rootterm = $vocabulary->getRootTermByName(SlogTx::ROOTTERM_DEFAULT, $target_term->id());
//                    }
//                    if ($rootterm && $rt_name !== SlogTx::ROOTTERM_DEFAULT) {
//                      $rootterm = $rootterm->getSiblingRootTerm($rt_name);
//                    }
//                  }
//                }
//                return $rootterm;
//              }

  /**
   * Return the root term for a node if exists.
   * 
   * @param integer $node_id
   * @return \Drupal\slogtx\Entity\RootTerm or NULL
   */
//  public static function getNodeSubmenuRootTerm($node_id, $fallback = FALSE) {
//              public static function getNodeSubmenuRootTerm($node_id, $do_create = FALSE, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
//todo::current:: TxRootTermGetPlugin - new::deprecated
//                return SlogTx::getSysNodeRootTermSubmenu($node_id, $do_create, $rt_name);
//            //                    $options = [
//            //                      'plugin_id' => SlogTx::SYS_VOCAB_ID_SUBMENU,
//            //                      'entity' => Node::load($node_id),
//            //                      'do_create' => $do_create, // ?? create target term
//            //                    ];
//            //                    $rtget_plugin = SlogTx::pluginManager('rtget_sys_node')->getInstance($options);
//            //                    $rootterm = $rtget_plugin->getRootTermByName($rt_name, $do_create);  // ?? create root term
//            //
//            //                    return ($rootterm ?? FALSE);
//            //todo::current:: TxRootTermGetPlugin - new
//            //    $vid = self::getNodeSubmenuVid();
//            //    $rootterm = self::getSysNodeDefaultRootTerm($vid, $node_id);
//            //    if ($fallback) {
//            //      return $rootterm;
//            //    }
//            //
//            //    // do not return fallback (i.e. target term==SlogTx::TARGETTERM_DEFAULT)
//            //    if ($rootterm && self::nodeHasSubmenuRootTerm($node_id, $rootterm)) {
//            //      return $rootterm;
//            //    }
//            //    return NULL;
//              }

  /**
   * Whether the submenu for this node is prepared.
   * 
   * Notice: there may be a root term but with the wrong target term 
   * (fallback to SlogTx::TARGETTERM_DEFAULT)
   * 
   * @param integer $node_id
   * @param \Drupal\slogtx\Entity\RootTerm $rootterm (optional)
   * @return boolean
   * 
   * 
   * not used
   */
//              public static function nodeHasSubmenuRootTerm($node_id, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
//                $rootterm = self::getNodeSubmenuRootTerm($node_id, FALSE, $rt_name);
//                return (!empty($rootterm));
//todo::current:: TxRootTermGetPlugin - new::deprecated
//
//
//            //    $rootterm = $rootterm ?: self::getNodeSubmenuRootTerm($node_id, TRUE);
//            //    if ($vocabulary = $rootterm ? $rootterm->getVocabulary() : FALSE) {
//            //      $target_term = $rootterm->getTargetTerm();
//            //      $test_name = $vocabulary->getTargetEntityPlugin()->buildTargetTermName($node_id);
//            //      return ($test_name === $target_term->label());
//            //    }
//            //    return FALSE;
//              }

  public static function getPromotedRootTerm($user_role, $do_create = FALSE) {
//              $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, self::SYS_VOCAB_ID_PROMOTED]);
//              return SlogTx::getTbtabRootTerm($vid, $user_role);
//todo::current:: TxRootTermGetPlugin - new::promote
    
    return SlogTx::getSysSysRootTerm(self::SYS_VOCAB_ID_PROMOTED, $user_role, $do_create);
  }

  public static function getSurpriseRootTerm($user_role, $do_create = FALSE) {
//              $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, self::SYS_VOCAB_ID_SURPRISE]);
//              return SlogTx::getTbtabRootTerm($vid, $user_role);
//todo::current:: TxRootTermGetPlugin - new
    
    return SlogTx::getSysSysRootTerm(self::SYS_VOCAB_ID_SURPRISE, $user_role, $do_create);
  }

  public static function getBookmarkRootTerm($bookmark_type, $user, $do_create = FALSE) {
    $plugin_id = self::SYS_VOCAB_ID_BMSTORAGE . '_' . $bookmark_type;
    return SlogTx::getSysSysRootTerm($plugin_id, $user, $do_create);
    
    
//            $toolbartab = self::SYS_VOCAB_ID_BMSTORAGE . '_' . $bookmark_type;
//            $vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, $toolbartab]);
//            return SlogTx::getTbtabRootTerm($vid, $user);
  }

  public static function getBookmarkRootTerms($user) {
    $root_terms = [];
    foreach (['content', 'list'] as $type) {
      $root_term = self::getBookmarkRootTerm($type, $user);
      if ($root_term && $root_term->isRootTerm() && $root_term->hasChildren()) {
        $root_terms[$type] = $root_term;
      }
    }

    return $root_terms;
  }

//                public static function getCollaborateNodeVid() {
///todo::current:: TxRootTermGetPlugin - new::deprecated
//                  return SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_NODE_SUBSYS, SlogTx::SYS_VOCAB_ID_COLLABORATE]);
//                }

//              public static function prepareNodeCollaborateRootTerm($node_id) {
///todo::current:: TxRootTermGetPlugin - new::deprecated
//                $vid = self::getCollaborateNodeVid();
//                $description = t('Collaboration target term for node @nid', ['@nid' => $node_id]);
//                return self::prepareSysNodeDefaultRootTerm($vid, $node_id, $description);
//              }

//              public static function getSysNodeCollaborateRootTerm($node_id, $rt_name = SlogTx::ROOTTERM_DEFAULT) {
///todo::current:: TxRootTermGetPlugin - new::deprecated
//                $rootterm = FALSE;
//                $vid = self::getCollaborateNodeVid();
//                $node = Node::load($node_id);
//                $vocabulary = SlogTx::getVocabulary($vid);
//
//                if ($node && $vocabulary && $vocabulary->isNodeTargetEntity()) {
//                  if (($plugin = $vocabulary->getTargetEntityPlugin()) && $plugin->isValid()) {
//                    $target_name = $plugin->buildTargetTermName($node_id);
//                    $target_term = $vocabulary->getTargetTermByName($target_name, $reset = FALSE, $do_create = TRUE);
//                    if ($target_term && $target_term->label() === $target_name) {
//                      $rootterm = $vocabulary->getRootTermByName(SlogTx::ROOTTERM_DEFAULT, $target_term->id());
//                    }
//                    if ($rootterm && $rt_name !== SlogTx::ROOTTERM_DEFAULT) {
//                      $rootterm = $rootterm->getSiblingRootTerm($rt_name);
//                    }
//                  }
//                }
//                return $rootterm;
//              }

  /**
   * Return the root term for a node if exists.
   * 
   * @param integer $node_id
   * @return \Drupal\slogtx\Entity\RootTerm or NULL
   */
//              public static function getCollaborateNodeRootTerm($node_id, $fallback = FALSE) {
///todo::current:: TxRootTermGetPlugin - new::deprecated
//                $vid = self::getCollaborateNodeVid();
//                $rootterm = self::getSysNodeDefaultRootTerm($vid, $node_id);
//                if ($fallback) {
//                  return $rootterm;
//                }
//
//                // do not return fallback (i.e. target term==SlogTx::TARGETTERM_DEFAULT)
//                if ($rootterm && self::nodeHasCollaborateRootTerm($node_id, $rootterm)) {
//                  return $rootterm;
//                }
//                return NULL;
//              }

  /**
   * Whether the submenu for this node is prepared.
   * 
   * Notice: there may be a root term but with the wrong target term 
   * (fallback to SlogTx::TARGETTERM_DEFAULT)
   * 
   * @param integer $node_id
   * @param \Drupal\slogtx\Entity\RootTerm $rootterm (optional)
   * @return boolean
   */
//              public static function nodeHasCollaborateRootTerm($node_id, $rootterm = NULL) {
///todo::current:: TxRootTermGetPlugin - new::deprecated
//                $rootterm = $rootterm ?: self::getCollaborateNodeRootTerm($node_id, TRUE);
//                if ($vocabulary = $rootterm ? $rootterm->getVocabulary() : FALSE) {
//                  $target_term = $rootterm->getTargetTerm();
//                  $test_name = $vocabulary->getTargetEntityPlugin()->buildTargetTermName($node_id);
//                  return ($test_name === $target_term->label());
//                }
//                return FALSE;
//              }

  /**
   * Return menu term object for collection if exists.
   * 
   * @param integer $node_id
   * @param string $menu_label
   * @param boolean $do_create
   *  Do create menu term if not exists
   * @return Drupal\slogtx\Entity\MenuTerm
   *  return menu term object or FALSE
   */
  public static function getMwCollectBaseDraftMenuTerm($node_id, $menu_label, $do_create = FALSE) {

    $rootterm = SlogTx::getSysNodeRootTermCollaborate($node_id, $do_create, SlogTx::ROOTTERM_DRAFT);
///todo::current:: TxRootTermGetPlugin - new
    
//                $rootterm = self::getSysNodeCollaborateRootTerm($node_id, SlogTx::ROOTTERM_DRAFT);
//                if (!$rootterm && $do_create) {
//                  self::prepareNodeCollaborateRootTerm($node_id);
//                  $rootterm = self::getSysNodeCollaborateRootTerm($node_id, SlogTx::ROOTTERM_DRAFT);
//                }

    if ($rootterm) {
      if ($rootterm->hasMenuTermByName($menu_label)) {
        $menu_term = $rootterm->getMenuTermByName($menu_label);
      }
      elseif ($do_create) {
        $args = [
          '@node_id' => $node_id,
          '@label' => Node::load($node_id)->label(),
        ];
        $description = t('Container for collect content: (@node_id) @label', $args);
        $menu_term = $rootterm->createMenuTermByName($menu_label, $description);
      }
    }

    return ($menu_term ?? FALSE);
  }

  public static function getHistoryMaxItems() {
    $max_items = (integer) self::getConfig()->get('max_items_history');
    if ($max_items < 20 || $max_items > 50) {
      $max_items = 30;
    }
    return $max_items;
  }

  public static function getBookmarkMaxItems() {
    $max_items = (integer) self::getConfig()->get('max_items_bookmark');
    if ($max_items < 30 || $max_items > 80) {
      $max_items = 40;
    }
    return $max_items;
  }

  public static function getTbnodeMaxDepth() {
    $max_depth = (integer) self::getConfig()->get('max_depth_tbnode');
    if ($max_depth < 1 || $max_depth > 5) {
      $max_depth = 2;
    }
    return $max_depth;
  }

  public static function getPromotedSlogitemId($account = NULL) {
    $promoted_data = self::getConfig()->get('promoted_sid');
    $account = $account ?: \Drupal::currentUser();
    $default_role = XtUserData::getDefaultRole($account);
    if (is_array($promoted_data) && !empty($promoted_data[$default_role])) {
      return $promoted_data[$default_role];
    }

    return FALSE;
  }

  public static function getSurpriseTermId($account = NULL) {
    $surprise_data = self::getConfig()->get('surprise_tid');
    $account = $account ?: \Drupal::currentUser();
    $default_role = XtUserData::getDefaultRole($account);
    if (is_array($surprise_data) && !empty($surprise_data[$default_role])) {
      return $surprise_data[$default_role];
    }

    return FALSE;
  }

  public static function getSurpriseData($account = NULL) {
    return self::getSlogitemAttachDataByTid(self::getSurpriseTermId($account));
  }

  public static function getPromotedData($account = NULL) {
    return self::getSlogitemAttachData(self::getPromotedSlogitemId($account));
  }

  public static function getSlogitemAttachDataByItems(array $slogitems) {
    if (!empty($slogitems)) {
      $result_data = [];
      foreach ($slogitems as $item) {
        if ($data = $item->getAttachData()) {
          $result_data[] = $data;
        }
      }
      return $result_data;
    }

    return false;
  }

  public static function getStorage() {
    if (empty(self::$storage)) {
      self::$storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    }
    return self::$storage;
  }

  public static function getSlogitemAttachDataBySids(array $sids) {
    $slogitems = self::getStorage()->loadMultiple($sids);
    return self::getSlogitemAttachDataByItems($slogitems);
  }

  public static function getSlogitemAttachDataByTid($tid) {
    $slogitems = self::loadSlogitems($tid);
    return self::getSlogitemAttachDataByItems($slogitems);
  }

  public static function getSlogitemHashesByTid($tid) {
    $result_data = [];
    $slogitems = self::loadSlogitems($tid);
    if (!empty($slogitems)) {
      foreach ($slogitems as $item) {
        $result_data[] = $item->getTargetEntityHash(TRUE); // TRUE: from cache only
      }
    }

    return $result_data;
  }

  public static function getSlogitemAttachData($slogitem_id) {
    if ($slogitem_id && ($slogitem = SlogItem::load($slogitem_id))) {
      return $slogitem->getAttachData();
    }
    return false;
  }

  public static function getNidsByTid($tid, $deep = FALSE) {
    $items = self::getStorage()->loadByTid($tid);
    $nids = [];
    foreach ($items as $item) {
      $args = [
        '@id' => $item->id(),
        '@label' => $item->label(),
      ];
      if ($deep && $item->isRedirectItem()) {
        $item = self::loadSlogitem($item->getTargetEntityId());
      }
      if (!$item) {
        $msg = t('Redirect item not found: (@id) @label.', $args);
        SlogXt::logger('sxt_slogitem')->error($msg);
      }
      elseif ($item->isNodeEntity()) {
        $id = $item->getTargetEntityId();
        $nids[$id] = TRUE;
      }
    }

    return array_keys($nids);
  }

  public static function hasSlogitems($term_id, &$multiple = NULL) {
    $sids = self::getStorage()->getSidsFromTidsSorted([$term_id], 2);
    $multiple = count($sids) > 1;
    return count($sids) > 0;
  }

  public static function getTidsByEntity($entity, $eid) {
    return self::getStorage()->getTidsByEntity($entity, $eid);
  }

  public static function getSidsFromTids(array $tids) {
    return self::getStorage()->getSidsFromTids($tids);
  }

  public static function getNodeIdsBySids(array $sids) {
    return self::getStorage()->getNodeIdsBySids($sids);
  }

  public static function getSidsFromTidsSorted(array $tids, $limit = 100) {
    return self::getStorage()->getSidsFromTidsSorted($tids, $limit);
  }

  public static function getSidsByEntity($entity, $eid, $limit = 100) {
    return self::getStorage()->getSidsByEntity($entity, $eid, $limit);
  }

  public static function loadSlogitems($term_id, $limit = 100) {
    return self::getStorage()->loadByTidSorted($term_id, $limit);
  }

  public static function getSlogitem($sid) {
    return self::loadSlogitem($sid);
  }

  public static function loadSlogitem($sid) {
    return self::getStorage()->load($sid);
  }

  public static function loadByTidAndEntity($tid, $entity, $eid) {
    return self::getStorage()->loadByTidAndEntity($tid, $entity, $eid);
  }

  public static function hasSidWithTidAndEntity($tid, $entity, $eid) {
    return self::getStorage()->hasSidWithTidAndEntity($tid, $entity, $eid);
  }

  public static function getSlogitemFromTidAndTitle($tid, $name) {
    return self::getStorage()->loadSlogitemFromTidAndTitle($tid, $name);
  }

  /**
   * Return xtsi target entity plugin objects.
   * 
   * @see \Drupal\slogtx\TargetEntityPluginManager::getTargetEntityPlugins()
   */
  public static function getTargetEntityPlugins($all = FALSE, $status = TRUE) {
    return SlogXt::pluginManager('target_entity', 'sxt_slogitem')
            ->getTargetEntityPlugins($all, $status);
  }

  public static function getTargetEntityPlugin($id) {
    $plugins = self::getTargetEntityPlugins();
    return $plugins[$id];
  }

  public static function bmMenuSelectAlter($options, $isSave = FALSE) {
    foreach ($options as $key => &$item) {
      if (!empty((integer) $item['entityid'])) {
        $item_data = unserialize($item['liDescription']);
        $count = !empty($item_data['items']) ? count(explode(',', $item_data['items'])) : 0;
        if ($count > 0) {
          $args = [
            '%count' => $count,
            '%items' => $item_data['items'],
          ];
          $item['liDescription'] = t('List contains %count bookmarks (%items)', $args);
          $item['tids'] = $item_data['items'];
        }
        elseif ($isSave) {
          $item['liDescription'] = '>>> ' . t('There are no bookmarks in the list' . ' <<<');
        }
        else {
          unset($options[$key]);
        }
      }
    }

    return array_values($options);
  }

  /**
   * Check if there is a not empty node dependent toolbar
   * 
   * @param integer $node_id
   *  The id of the node to check
   * @return boolean
   *  TRUE if a not empty toolbar exists
   */
  public static function nodeHasTbnode($node_id) {
//        SlogXt::logger('sxt_slogitem')->error('SlogXtsi::nodeHasTbnode not working !!!');    
//    if ($rootterm = self::getNodeSubmenuRootTerm($node_id, FALSE)) {
//todo::current:: TxRootTermGetPlugin - new
    if ($rootterm = SlogTx::getSysNodeRootTermSubmenu($node_id)) {
      $tree_service = SlogTb::getMenuLinkTreeService();
      return $tree_service->hasMenuItems($rootterm);
    }
    
    return FALSE;
  }

  public static function getTbnodeHash($node, $cached_only = FALSE) {
    $hash = FALSE;
    $entity_type = 'node';
    $nid = $node->id();
    $vocabulary = self::getSubmenuVocabulary($entity_type);
//todo::current:: TxRootTermGetPlugin - getSubmenuVocabulary
    $te_plugin = $vocabulary ? $vocabulary->getTargetEntityPlugin() : FALSE;
    $block = $vocabulary ? self::getTbSubmenuHiddenBlock($entity_type) : FALSE;
//todo::current:: TxRootTermGetPlugin - getTbSubmenuHiddenBlock
    if ($te_plugin && $block) {
      $te_plugin->setEntity($node);
      $rootterm_name = SlogTx::ROOTTERM_DEFAULT;
      $cur_rootterm = $vocabulary->setCurrentRootTermName($rootterm_name);
      if ($cur_rootterm === $rootterm_name) {
        $build = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block, 'block');
        if ($cached_only) {
          $root_term = $vocabulary->getCurrentRootTerm();
          $root_term->addRootTermCaches();
          $xt_render_cache = \Drupal::service('slogxt.render_cache');
          $xt_render_cache->addCacheableDependency($build, $root_term);
          $xt_render_cache->setRequiredCacheContexts($build);
          $data = $xt_render_cache->get($build);
          $html = (string) $data['content'];
        }
        else {
          $html = (string) \Drupal::service('renderer')->renderRoot($build);
        }
        if (!empty($html)) {
          // remove unique block id
          $testid = 'block-' . Html::cleanCssIdentifier('xtsi_sys_submenu_tb');
          $pattern = '/id="' . $testid . '.*"/';
          $replace = 'id="hash-node-' . $nid . '"';
          $html = preg_replace($pattern, $replace, $html);
          $rootterm = $vocabulary->getCurrentRootTerm();
          $checksum = \Drupal::service('cache_tags.invalidator.checksum')->getCurrentChecksum(['slogtx_rt:' . $rootterm->id()]);
          $base_tb_id = XtsiNodeTypeData::getPermBaseTbIdByNode($node);
          $hash = Crypt::hashBase64($html . $checksum . $base_tb_id);
        }
      }
    }

    return $hash;
  }

  public static function getListAttachDataByMenuTids(array $menu_tids) {
    $items = [];
    $list_terms = SlogTx::entityStorage('slogtx_mt')
        ->loadMultiple($menu_tids);
    if (!empty($list_terms)) {
      $slogtb_handler = SlogTb::getSlogtbHandler();
      foreach ($list_terms as $key => $term) {
        $vocabulary = $term->getVocabulary();
        $entity = $term->setRequiredEntity();
        $tid = $term->id();
        list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
        $path = $slogtb_handler->getPathPrefix() . $tid;

        if ($toolbar !== 'role' || ($entity && $entity instanceof RoleInterface)) {
          $items[$key] = [
            'data' => [
              'path' => $path,
              'tid' => $tid,
              'toolbar' => $toolbar,
              'toolbartab' => $toolbartab,
            ],
            'path' => $path,
            'mainLabel' => $term->label(),
            'pathLabel' => $vocabulary->headerPathLabel(),
            'tbline' => '',
            'tblineIndex' => '',
            'type' => 'list',
          ];
          if ($toolbar === 'role') {
            $items[$key]['pdRole'] = $entity->id();
          }
        }
      }
    }
    return $items;
  }

  public static function getContentAttachDataBySids(array $sids) {
    $items = [];
    $content_data = self::getSlogitemAttachDataBySids($sids);
    if (!empty($content_data)) {
      foreach ($content_data as $item) {
        $cData = $item['content'];
        $key = $cData['sid'];
        $items[$key] = [];
        $items[$key]['data'] = $cData;
        $items[$key]['path'] = $cData['path'];
        $items[$key]['mainLabel'] = $cData['mainLabel'];
        $items[$key]['pathLabel'] = $cData['pathLabel']
            . '/' . $cData['listLabel'] . ' - ' . $cData['label'];
        $items[$key]['type'] = 'content';
        $items[$key]['tbline'] = $items[$key]['tblineIndex'] = '';
      }
    }
    return $items;
  }

}
