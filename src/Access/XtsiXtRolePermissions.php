<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Access\XtsiXtRolePermissions.
 */

namespace Drupal\sxt_slogitem\Access;

/**
 * Provides permissions for Xt-Roles.
 */
class XtsiXtRolePermissions {

  /**
   * Returns an array of Xtsi Xt-Role permissions.
   *
   * @return array
   *   The Xt-Role permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getXtRolePermissions() {
    $perms = [
      'admin sxtrole-content' => [
        'title' => 'Administer Xt-Role content',
        'description' => 'Permission for each of the Xt-Role content actions (new, edit, move, rearange).',
        'warning' => 'Warning: This can be rather technical and should only be granted to power users.',
      ],
      'newnode sxtrole-content' => [
        'title' => 'Add new Xt-Role content',
        'description' => 'Permission for adding new Xt-Role content.',
      ],
      'newlinks sxtrole-content' => [
        'title' => 'Add new Xt-Role links',
        'description' => 'Permission for adding new links to Xt-Role contents.',
      ],
      'edit sxtrole-content' => [
        'title' => 'Edit Xt-Role content',
        'description' => 'Permission for editing Xt-Role content.',
      ],
      'move sxtrole-content' => [
        'title' => 'Move Xt-Role content',
        'description' => 'Permission for moving Xt-Role content.',
      ],
      'rearrange sxtrole-content' => [
        'title' => 'Rearrange Xt-Role content',
        'description' => 'Permission for rearranging Xt-Role content.',
      ],
//*************************************************
      'admin xtsi-promoted' => [
        'title' => 'Administer Xt-Role Promoted',
        'description' => 'Administer Xt-Role Promoted, i.e. preparation and selection of promoted content for a specific role.',
        'warning' => 'Warning: This can be rather technical and should only be granted to power users.',
      ],
      'admin xtsi-surprise' => [
        'title' => 'Administer Xt-Role Surprise',
        'description' => 'Administer Xt-Role Surprise, i.e. preparation and selection of surprise content for a specific role.',
        'restrict access' => TRUE,
        'warning' => 'Warning: This can be rather technical and should only be granted to power users.',
      ],
    ];

    return $perms;
  }

}
