<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\XtsiBreadcrumbBuilder.
 */

namespace Drupal\sxt_slogitem;

use Drupal\slogtx\SlogTx;
use Drupal\system\PathBasedBreadcrumbBuilder;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a custom breadcrumb builder that uses the tx term hierarchy.
 */
class XtsiBreadcrumbBuilder extends PathBasedBreadcrumbBuilder {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $routeName = $route_match->getRouteName();
    return in_array($route_match->getRouteName(), ['entity.slogtx_mt.overview']);
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = parent::build($route_match);

    switch ($route_match->getRouteName()) {
      case 'entity.slogtx_mt.overview':
        $this->alterBreadcrumbRootTermOverview($route_match, $breadcrumb);
        break;
    }

    return $breadcrumb;
  }

  // RootTerm Overview
  private function alterBreadcrumbRootTermOverview(RouteMatchInterface $route_match, Breadcrumb $breadcrumb) {
    $menu_term = $this->getInstanceByRouteParam($route_match, 'slogtx_mt');
    $root_term = $menu_term->getRootTerm();
    
    // add overview link for the vocabulary (root term collection)
    $title = t('%item Menu terms', ['%item' => $root_term->pathLabel()]);
    $url = $root_term->toUrl('overview-form');
    $breadcrumb->addLink(new Link($title, $url));
  }

  /**
   * Wrapper for SlogTx::entityInstanceByRoute().
   */
  private function getInstanceByRouteParam(RouteMatchInterface $route_match, $entity_type_id) {
    return SlogTx::entityInstanceByRoute($route_match, $entity_type_id);
  }


}
