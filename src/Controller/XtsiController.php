<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Controller\XtsiController.
 */

namespace Drupal\sxt_slogitem\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\Interfaces\TxTermInterface;
use Drupal\node\Entity\Node;

/**
 */
class XtsiController extends ControllerBase {

  //dummy
  public function dummy($slogitem_id = 0) {
    return ['#markup' => 'XtsiController.dummy::slogitem_id = '];
  }

  public function nodeMenues(Node $node = NULL) {
    \Drupal::request()->query->remove('destination');
//    $rootterm = SlogXtsi::getNodeSubmenuRootTerm($node->id(), FALSE);
    $rootterm = SlogTx::getSysNodeRootTermSubmenu($node->id());
//todo::current:: TxRootTermGetPlugin - new
    if ($rootterm) {
      $route_name = 'entity.slogtx_rt.overview_form';
      $route_parameters = ['slogtx_rt' => $rootterm->id()];
    } else {
      $route_name = 'sxt_slogitem.node.prepare_confirm';
      $route_parameters = ['node' => $node->id(), 'target' => 'submenu'];
    }
    return $this->redirect($route_name, $route_parameters);
  }

  /**
   * The _title_callback for the entity.slogtx_mt.overview route.
   *
   * @param \Drupal\slogtx\Interfaces\TxTermInterface $slogtx_mt
   *   The current slog menu term.
   *
   * @return string
   *   The slogitems list title for a special menu term.
   */
  public static function slogitemsTitle(TxTermInterface $slogtx_mt = NULL) {
    return t('Slogitems for menu: %title', ['%title' => $slogtx_mt->label()]);
  }

}
