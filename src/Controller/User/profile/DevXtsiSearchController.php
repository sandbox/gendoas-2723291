<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Controller\User\profile\DevXtsiSearchController.
 */

namespace Drupal\sxt_slogitem\Controller\User\profile;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtConfirmControllerBase;

/**
 * Defines a controller for deleting user's collaboration request.
 */
class DevXtsiSearchController extends XtConfirmControllerBase {

  protected $entity_key;
  protected $entity_id;
  protected $entity_data;

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Search');
  }

  protected function getSubmitLabel() {
    return t('Search');
  }

  protected function getEntityKeys() {
    $options = [
      'nid' => t('Node'),
      'sid' => t('Slogitem'),
      'tid' => t('Menu item'),
    ];
    return $options;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    parent::hookFormAlter($form, $form_state, $form_id);

    $options = $this->getEntityKeys();
    $form['entity_key'] = [
      '#type' => 'radios',
      '#title' => t('Entity'),
      '#options' => $options,
      '#description' => t('Select the entity type to search.'),
      '#default_value' => 'nid',
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper(TRUE);


    $form['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity id'),
      '#description' => t('The ID of the entity instance to search.'),
      '#default_value' => '',
      '#size' => 10,
      '#required' => TRUE,
        ] + $this->getInputFieldWrapper();
  }

  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    if ($this->opSave && !$form_state->hasAnyErrors()) {
      $slogxtData = & $form_state->get('slogxtData');
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {

    if (!$this->opSave || $form_state->hasAnyErrors()) {
      $this->setPreFormMessage('Select entity type and enter entity id for search.', $form_state);
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $entity_key = $form_state->getValue('entity_key');
    $entity_id = $form_state->getValue('entity_id');
    $finish_data = $slogitem = $toolbar = FALSE;

    if ($entity_key === 'nid') {
      if ($node = Node::load($entity_id)) {
        $slogitem = XtsiNodeTypeData::nodeGetBaseSlogitem($node);
      }
      if ($slogitem && $slogitem instanceof SlogItem) {
        $entity_key = 'sid';
        $entity_id = $slogitem->id();
        $data = $slogitem->getAttachData();
        $toolbar = $data['list']['toolbar'];
        $finish_data = $data['content'];
      }
    }
    elseif ($entity_key === 'sid') {
      $slogitem = SlogXtsi::loadSlogitem($entity_id);
      if ($slogitem && $slogitem instanceof SlogItem) {
        $data = $slogitem->getAttachData();
        $toolbar = $data['list']['toolbar'];
        $finish_data = $data['content'];
      }
    }
    elseif ($entity_key === 'tid') {
      $menu_term = SlogTx::getMenuTerm($entity_id);
      if ($menu_term && $menu_term->isMenuTerm()) {
        list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($menu_term->bundle());
        $slogtb_handler = SlogTb::getSlogtbHandler();
        $list_path = $slogtb_handler->getPathPrefix() . $entity_id;
        $finish_data = [
          'path' => $list_path,
          'tid' => $entity_id,
          'toolbar' => $toolbar,
          'toolbartab' => $toolbartab,
        ];
      }
    }

    if (empty($finish_data) || empty($toolbar)) {
      $keys = $calledObject->getEntityKeys();
      $label = $keys[$entity_key];
      $what = empty($finish_data) ? 'Entity' : 'Toolbar';
      $msg = "$what not found: $label / $entity_id";
      $form_state->setErrorByName('entity_id', $msg);
    }
    else {
      $calledObject->toolbar = $toolbar;
      $calledObject->entity_key = $entity_key;
      $calledObject->entity_id = $entity_id;
      $calledObject->finish_data = $finish_data;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    //nothing
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $this->addFinalMoreMessage(t('... search has been initialized.'));
    $this->setFinalMoreMessages();

    return [
      'command' => 'sxt_slogitem::devEntitySearch',
      'args' => [
        'toolbar' => $this->toolbar,
        'entityKey' => $this->entity_key,
        'entityId' => $this->entity_id,
        'data' => $this->finish_data,
      ],
    ];
  }

}
