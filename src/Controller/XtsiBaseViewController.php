<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Controller\XtsiBaseViewController.
 */

namespace Drupal\sxt_slogitem\Controller;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Controller\ControllerBase;

/**
 */
class XtsiBaseViewController extends ControllerBase {
  // return no contents
  public function handle() {
    return [];
  }

//  public function itemAutoload($slogitem) {
//    $autorun = ['fromPath' => $slogitem->getAttachData()];
//    SlogXtsi::setJsAutorun(['fromPath' => $slogitem->getAttachData()]);
//    return ['#markup' => ''];  //dummy
//  }

}
