<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Other\MwCommentEditActions.
 */

namespace Drupal\sxt_slogitem\Handler\Other;

use Drupal\sxt_mediawiki\SxtMediawiki;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\comment\Entity\Comment;
use Drupal\node\Entity\Node;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class MwCommentEditActions extends XtEditControllerBase {

  protected $mwc_action;
  protected $discussion_node_id;
  protected $discussion_node;
  protected $comment_id;
  protected $comment = FALSE;
  protected $parentComment = FALSE;
  protected $deleteGotoComment = FALSE;
  protected $isCommentEdit = FALSE;
  protected $isCommentNew = FALSE;
  protected $isDelete = FALSE;
  protected $isReply = FALSE;
  protected $opPreview = FALSE;
  protected $opSave = FALSE;
  protected $subject = '';

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function prepareFormObjectArg() {
    $request = \Drupal::request();
    $this->mwc_action = $request->get('mwc_action');
    $this->discussion_node_id = (integer) $request->get('node_id');
    $this->discussion_node = Node::load($this->discussion_node_id);
    $this->comment_id = (integer) $request->get('comment_id');
    if ($this->comment_id) {
      $this->comment = Comment::load($this->comment_id);
    }
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->prepareFormObjectArg();
    
    $this->isDelete = ($this->mwc_action === 'delete');
    $this->isReply = ($this->mwc_action === 'reply');
    if ($this->isReply) {
      $this->parentComment = $this->comment;
      $this->comment = FALSE;
    }

    switch ($this->mwc_action) {
      case 'delete':
        return $this->getEntityFormObject('comment.delete', $this->comment);

      case 'edit':
        $this->isCommentEdit = TRUE;
        return $this->getEntityFormObject('comment.default', $this->comment);

      case 'addnew':
      case 'reply':
        $this->isCommentNew = TRUE;
        $values = [
          'comment_type' => 'mwcomment',
          'field_name' => 'mwnodecomment',
          'entity_type' => 'node',
          'entity_id' => $this->discussion_node_id,
          'pid' => $this->comment_id ?? NULL,
        ];
        $this->comment = $this
            ->entityTypeManager()
            ->getStorage('comment')
            ->create($values);
        return $this->getEntityFormObject('comment.default', $this->comment);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $title = '???:' . $this->mwc_action;

    if ($this->isDelete) {
      $title = t('Delete: ') . $this->comment->label();
    }
    elseif ($this->opPreview && !$this->hasErrors) {
      $title = t('Preview');
    }
    elseif ($this->isCommentEdit) {
      $title = t('Edit: ') . $this->comment->label();
    }
    elseif ($this->isReply) {
      $title = t('Reply: ') . $this->parentComment->label();
    }
    elseif ($this->isCommentNew) {
      $title = t('Add new: ') . $this->discussion_node->label();
    }

    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    $label = '???:' . $this->mwc_action;
    if ($this->isDelete) {
      $label = t('Delete');
    }
    elseif ($this->opPreview && !$this->hasErrors) {
      if ($this->isCommentEdit) {
        $label = t('Update');
      }
      elseif ($this->isReply) {
        $label = t('Add Reply');
      }
      elseif ($this->isCommentNew) {
        $label = t('Add Comment');
      }
    }
    else {
      $label = t('Preview');
    }

    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    $request = \Drupal::request();
    $op = $request->get('op', FALSE);
    if ($op) {
      $this->opPreview = ($op === (string) t('Preview'));
      $this->opSave = ($op === (string) t('Save'));
    }

    if ($this->isDelete && ($op === (string) t('Delete'))) {
      $this->deleteGotoComment = $this->getMwCommentPrevNext($this->discussion_node, $this->comment_id);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->isDelete) {
      $controllerClass = get_class($this);
      $form['#validate'][] = "$controllerClass::formValidate";
      $allowed = ['submit', 'preview'];
      $actions = &$form['actions'];
      foreach (Element::children($actions) as $key) {
        if (!in_array($key, $allowed)) {
          unset($actions[$key]);
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function _buildContentResultDelete(&$form, FormStateInterface $form_state) {
    if (!$form_state->isSubmitted() || $form_state->hasAnyErrors()) {
      unset($form['description']);
      $msg = t('Deleting comment: @label', ['@label' => $this->comment->getSubject()]);
      $msg .= $this->htmlHrPlus() . t('You are about to delete the comment. This action cannot be undone.');
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['message'] = ['msg' => $msg, 'type' => 'warning'];
    }

    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->isDelete) {
      return $this->_buildContentResultDelete($form, $form_state);
    }

    // delete/edit/reply/addnew
    $request = \Drupal::request();
    $form['#action'] = $request->getBaseUrl() . $request->getPathInfo();
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::appendAttachments();
   */
  protected function appendAttachments() {
    return TRUE;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    if (!$this->isDelete) {
      foreach (Element::children($form) as $field_name) {
        $field = & $form[$field_name];
        $field_type = $field['#type'] ?? FALSE;
        if (!empty($field['#group']) || $field_type === 'vertical_tabs') {
          $field['#access'] = false;
        }
        elseif ($field_type === 'container' && isset($field['#access']) && $field['#access']) {
          $field['#attributes']['class'][] = 'slogxt-input-field';
        }

        if ($field_type === 'container' && isset($field['#access']) && $field['#access']) {
          $format = & $field['widget'][0]['format'];
          if ($format && $format['help']) {
            unset($format['help']);
          }
        }

        if (in_array($field_name, ['subject', 'mwcomment_body'])) {
          $field['#required'] = TRUE;
        }
      }

      // enable submit button
      //todo::incorrect::disables access mechanism
      $submit = & $form['actions']['submit'];
      is_array($submit) && $submit['#access'] = true;

      // Set error/broken if required button is missing
      $required = ['preview' => 'Preview', 'submit' => 'Submit'];
      foreach ($required as $key => $label) {
        $action = isset($form['actions'][$key]) ? $form['actions'][$key] : false;
        if (empty($action) || empty($action['#access']) || !$action['#access']) {
          if ($key === 'preview' && !empty($form['actions']['submit'])) {
            $form['actions']['submit']['#access'] = FALSE;
          }
          $this->formBroken = TRUE;
          drupal_set_message('Button not available/broken: ' . $label, 'error');
          break;
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::renderFormPlus();
   */
  protected function renderFormPlus(&$form, FormStateInterface $form_state) {
    $slogxtData = & $form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    if ($this->isDelete) {
      parent::renderFormPlus($form, $form_state);
      if ($this->submitted && !$this->hasErrors) {
        $this->formResult = 'saved';
        $this->htmlContent = '';
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $comment_preview = $form['comment_preview'] ?? FALSE;
      $has_comment_preview = (boolean) $comment_preview;
      foreach (['comment_preview', 'comment_output_below', 'footer', 'author'] as $field_name) {
        if (isset($form[$field_name])) {
          unset($form[$field_name]);
        }
      }

      // real form, assume not saved
      if ($this->submitted && !$this->hasErrors && !$this->opPreview) {
        $this->formResult = 'saved';
        $this->htmlContent = '';
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
      elseif (!$has_comment_preview || $this->hasErrors) {
        if ($this->submitted && !$this->hasErrors) {
          $this->formResult = 'saved';
          $this->htmlContent = '';
        }
        else {
          $this->formResult = 'edit';
          $this->htmlContent = $this->renderer->renderRoot($form);
        }
      }
      elseif ($this->opPreview) {
        $this->formResult = 'preview';
        $comment = $form_state->getFormObject()->getEntity();
        $title = '<h2 class="slogxt-preview-title">' . $comment->label() . '</h2>';
        $comment_preview['#prefix'] = '<div class="preview xtsi-mwcomment-preview">' . $title;
        $comment_preview['#suffix'] = '</div>';

        $this->htmlContent = $this->renderer->renderRoot($form);
        $html_preview = $this->renderer->renderRoot($comment_preview);
        $html_preview = preg_replace('#<footer class.*</footer>#s', '', $html_preview);
        $html_preview = preg_replace('#<h3.*</h3>#s', '', $html_preview);
        $html_preview = str_replace('comment__content', '', $html_preview);

        $this->htmlPreview = $html_preview;
      }
      else {
        $slogxtData['wizardFinalize'] = true;
      }
    }
  }

  /**
   * Add submit handler.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    if ($on_edit = $calledObject->isCommentEdit) {
      $comment = $calledObject->comment;
    }
    $has_changes = FALSE;
    foreach (['subject', 'mwcomment_body'] as $field_name) {
      $value = $form_state->getValue([$field_name, 0, 'value']);
      $trimmed = trim($value);
      if ($trimmed !== $value) {
        $value = $form_state->setValue([$field_name, 0, 'value'], $trimmed)->getValue([$field_name, 0, 'value']);
      }
      
      if (empty($value)) {
        $args = ['@label' => (string) $form[$field_name]['widget']['#title']];
        $form_state->setErrorByName($field_name, t('Required field: @label', $args));
      }
      elseif ($field_name === 'subject') {
        $calledObject->subject = $value;
      }

      if ($on_edit && $value !== $comment->get($field_name)->value) {
        $has_changes = TRUE;
      }
    }

    if ($on_edit && !$has_changes) {
      $form_state->setErrorByName('', t('No changes have been done.'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    if ($this->isCommentEdit) {
      $this->setSavedMessage();
    }
    else {
      drupal_get_messages();  // clear messages
      if ($this->isDelete) {
        $msg = t('Comment has been deleted: @label', ['@label' => $this->comment->label()]);
      }
      elseif ($this->isReply) {
        $msg = t('Reply has been created: @label', ['@label' => $this->parentComment->label()]);
      }
      elseif ($this->isCommentNew) {
        $msg = t('New comment has been created: @label', ['@label' => $this->discussion_node->label()]);
      }
      drupal_set_message($msg);
    }

    if ($this->isDelete) {
      $anchor = $this->deleteGotoComment ? $this->deleteGotoComment->getSubject() : FALSE;
    }
    else {
      $anchor = SxtMediawiki::getMwHeaderAnchor($this->subject);
    }

    return [
      'command' => 'sxt_slogitem::finishedNodeEdit',
      'args' => [
        'etype' => 'node',
        'eid' => $this->discussion_node_id,
        'goto' => ($anchor ?? FALSE),
        'closeDelay' => 2000,
      ],
    ];
  }

  protected function getMwCommentPrevNext($discussion_node, $current_cid) {
    $field_name = 'mwnodecomment';
    $mode = \Drupal\comment\CommentManagerInterface::COMMENT_MODE_THREADED;
    $comment_storage = \Drupal::service('entity_type.manager')->getStorage('comment');
    $comments = $comment_storage->loadThread($discussion_node, $field_name, $mode);

    $comment_found = FALSE;
    if (!empty($comments) && count($comments) > 1) {
      $cid_found = FALSE;
      $cid_last = FALSE;
      foreach ($comments as $cid => $comment) {
        if ($cid === $current_cid) {
          if (!empty($cid_last)) {
            $cid_found = $cid_last;
            break;
          }
        }
        elseif ($cid_last === $current_cid) {
          $cid_found = $cid;
          break;
        }

        $cid_last = $cid;
      }

      $comment_found = $comments[$cid_found] ?? FALSE;
    }

    return $comment_found;
  }

}
