<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Bmstorage\BmstorageSave.
 */

namespace Drupal\sxt_slogitem\Handler\Bmstorage;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\User;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\sxt_slogitem\Handler\Bmstorage\BmstorageBase;
use Drupal\user\RoleInterface;

class BmstorageSave extends BmstorageBase {

  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $request = \Drupal::request();
    $this->bmType = $bmtype = $request->get('bookmark_type');
    $menu_tid = (integer) $request->get('menu_tid');
    $user = User::load(\Drupal::currentUser()->id());
    $this->rootterm = $rootterm = SlogXtsi::getBookmarkRootTerm($bmtype, $user);
    $rootterm_id = (integer) $rootterm->id();
    $this->rootterm_has_children = $rootterm->hasChildren();
    $this->vocabulary = $vocabulary = $rootterm->getVocabulary();

    if (empty($menu_tid) && !$this->rootterm_has_children) {
      $menu_tid = $rootterm_id;
    }

    if (empty($menu_tid)) {
      $this->doTbMenuSelect = TRUE;
      $request->attributes->set('root_term', $rootterm);
      $request->attributes->set('menuSelectAlter', '\Drupal\sxt_slogitem\SlogXtsi::bmMenuSelectAlter');
      $request->attributes->set('menuSelectAlterIsSave', TRUE);
      return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
    }

    $this->create_new = ($menu_tid === $rootterm_id);
    if ($this->create_new) {
      $weight = 0;
      if ($this->rootterm_has_children) {
        $first_tid = $rootterm->getFirstChildTid();
        $weight = SlogTx::getMenuTerm($first_tid)->getWeight() - 1;
      }

      $values = [
        'vid' => $vocabulary->id(),
        'name' => '__' . t('New'),
        'parent' => $rootterm_id,
        'weight' => $weight,
      ];
      $this->menuTerm = MenuTerm::create($values);
    }
    else {
      $this->menuTerm = SlogTx::getMenuTerm($menu_tid);
    }
    $this->menuTid = $menu_tid;
    return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
  }

  protected function getFormTitle() {
    $title = t('Save');
    if (!$this->doTbMenuSelect) {
      $name = $this->create_new ? t('New') : $this->menuTerm->label();
      $title .= ": $name";
    }
    return $title;
  }

  protected function getSubmitLabel() {
    return ($this->doTbMenuSelect ? t('Next') : t('Save'));
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect) {
      $input = $form_state->getUserInput();
      $value = isset($input['ids_selected']) ? $input['ids_selected'] : '';
      $form['ids_selected'] = [
        '#type' => 'hidden',
        '#value' => $value,
      ];

      if (is_array($form['name'])) {
        $element = &$form['name']['widget'][0];
        $element['#title'] = $element['value']['#title'] = t('Title');
        $element['#description'] = $element['value']['#description'] = t('The title for a bookmark list.');
      }
      if (is_array($form['description'])) {
        $element = &$form['description']['widget'][0];
        $element['#title'] = t('Raw data');
        $element['#attributes']['disabled'] = 'disabled';
        $form['description']['#attributes']['class'][] = 'visually-hidden';
      }

      if (!$this->create_new) {
        $options = [
          SlogXt::XTOPTS_TARGETS_SELF_PREPEND => t('Prepend bookmarks to selected list'),
          SlogXt::XTOPTS_TARGETS_SELF_REPLACE => t('Replace selected list'),
        ];

        $value = isset($input['target_position']) ? $input['target_position'] : SlogXt::XTOPTS_TARGETS_BEFORE;
        $build = [
          '#type' => 'select',
          '#options' => $options,
          '#title' => t('Action'),
          '#value' => $value,
          '#parents' => ['target_position'],
          '#prefix' => '<div class="slogxt-input-field js-form-wrapper form-wrapper">',
          '#suffix' => '</div>',
        ];
        $form['target_position'] = \Drupal::formBuilder()->doBuildForm($form_id, $build, $form_state);
      }

      //
      $controllerClass = get_class($this);
      $form['#validate'][] = "$controllerClass::formValidate";
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    $this->preRenderFormBase($form, $form_state);

    if ($this->doTbMenuSelect) {
      $slogxtData = & $form_state->get('slogxtData');
      $slogxtData['infoMsg'] = t('Select where to save bookmarks');

      $request = \Drupal::request();
      $path_info = urldecode($request->getPathInfo());
      $replace_key = $request->get('pathReplaceKey', '{next_entity_id}');
      $rootterm_id = $this->rootterm->id();
      $new_option = [
        'entityid' => $rootterm_id,
        'liTitle' => t('New'),
        'liDescription' => t('Create a new bookmark list'),
        'path' => str_replace($replace_key, $rootterm_id, $path_info),
      ];
      array_unshift($slogxtData['items'], $new_option);

      return;
    }

    $allowed = ['name', 'description', 'actions', 'wrapper_id', 'form_build_id', 'form_id', 'form_token'];
    $allowed += ['ids_selected', 'target_position'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!in_array($field_name, $allowed)) {
        $form[$field_name]['#access'] = FALSE;
      }
      elseif ($field['#type'] === 'container' && isset($field['#access']) && $field['#access']) {
        $field['#attributes']['class'][] = 'slogxt-input-field';
      }
    }

    $form['ids_selected']['#access'] = TRUE;
    $form['target_position']['#access'] = TRUE;
    if (isset($form['actions']['delete'])) {
      unset($form['actions']['delete']);
    }

    $slogxtData = & $form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitBmstorageSave';
      $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * Add submit handler.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $new_value = $form_state->getValue('ids_selected');
    $new_items = explode(',', $new_value);
    $testid = (integer) $new_items[0];
    if (empty($testid)) {
      $msg = t('Not a valid term id: %testid', ['%testid' => $new_items[0]]);
      $form_state->setErrorByName('ids_selected', $msg);
      return;
    }
    
    $key = ['description', 0, 'value'];
    $pos = $form_state->getValue('target_position');
    $calledObject->msg_items = [
      'new' => $new_value,
      'old' => FALSE,
    ];

    if ($pos != SlogXt::XTOPTS_TARGETS_SELF_REPLACE) {
      $old_data = unserialize($form_state->getValue($key));
      if ($old_data && is_array($old_data) && isset($old_data['items'])) {
        $old_items = explode(',', $old_data['items']);
        $diff = array_diff($old_items, $new_items);
        $new_items = array_merge($new_items, $diff);
        if (count($diff) > 0) {
          $calledObject->msg_items['old'] = implode(',', $diff);
        }
      }
    }

    $max = SlogXtsi::getBookmarkMaxItems();
    $count = count($new_items);
    if ($count > $max) {
      $args = [
        '%max' => $max,
        '%count' => $count,
      ];
      $msg = t('Too many bookmarks: %count (maximal=%max)', $args);
      $form_state->setErrorByName('ids_selected', $msg);
    }
    else {
      if ($calledObject->bmType === 'list') {
        $new_items = self::listValidate($new_items);
      }
      $new_value = implode(',', $new_items);
      $items = [
        'type' => $calledObject->bmType,
        'items' => $new_value,
      ];
      $form_state->setValue($key, serialize($items));
    }
  }

  private static function listValidate($list) {
    $result = [];
    $bmTerms = SlogTx::entityStorage('slogtx_mt')
        ->loadMultiple($list);
    if (!empty($bmTerms)) {
      foreach ($bmTerms as $key => $term) {
        $toolbar_id = $term->getVocabulary()->getToolbarId();
        if ($toolbar_id === 'role') {
          $entity = $term->setRequiredEntity();
          if ($entity && $entity instanceof RoleInterface) {
            $result[] = $term->id();
          }
        }
        else {
          $result[] = $term->id();
        }
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if ($form_state->isSubmitted()) {
      $this->menuTerm = SlogTx::getMenuTerm($this->menuTid, TRUE); // reload !!
    }
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    // prepare $this->bmItems for parent::getOnWizardFinished()
    $bmdata = unserialize($this->menuTerm->getDescription());
    $this->bmItems = explode(',', $bmdata['items']);

    $result = parent::getOnWizardFinished();
    $result['command'] = 'sxt_slogitem::finishedBookmarksSaved';
    $args = & $result['args'];
    $args['action'] = 'bmstorage:save';
    $args['menuTid'] = $this->menuTerm->id();
    $args['menuTitle'] = $this->menuTerm->label();
    $args['ids'] = implode(',', $args['ids']);

    drupal_set_message(t('Bookmarks have been saved.'));
    drupal_set_message(t('Saved bookmarks: %new', ['%new' => $this->msg_items['new']]));
    if (!empty($this->msg_items['old'])) {
      drupal_set_message(t('Prepended to: %old', ['%old' => $this->msg_items['old']]));
    }
    return $result;
  }

}
