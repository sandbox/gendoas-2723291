<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Bmstorage\BmstorageBase.
 */
/**
 * 404,1054,1055  - Administrator
 * 859,385,386    - 
 * 
 * tbmenu.role (problem)
 *  - multiple vocabs for same role (see myrole1)
 *  - bmstorage for not current role -> xt-tbtab-_sys (???)
 * 
 * 
 * //todo::cache
 * 
 */

namespace Drupal\sxt_slogitem\Handler\Bmstorage;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;

class BmstorageBase extends XtEditControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->menuSelectAlter = FALSE;
    $request = \Drupal::request();
    $menu_tid = (integer) $request->get('menu_tid');
    $this->bmType = $bmtype = $request->get('bookmark_type');
    if (empty($menu_tid)) {
      $this->doTbMenuSelect = TRUE;
      $base_entity_id = 'bmstorage' . SlogXt::XTURL_DELIMITER . $bmtype;
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $rootterm = $result['value'];

      if (!$rootterm || !$rootterm->isRootTerm()) {
        $message = t("Root term not found for bookmark type %bmtype", ['%bmtype' => $bmtype]);
        throw new \LogicException($message);
      }

      $request->attributes->set('root_term', $rootterm);
      if (!empty($result['menuSelectAlter'])) {
        $request->attributes->set('menuSelectAlter', $result['menuSelectAlter']);
        $request->attributes->set('menuSelectAlterIsSave', FALSE);
      }
      $this->vocabulary = $rootterm->getVocabulary();
      return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
    }

    $this->menuTerm = $menu_term = SlogTx::getMenuTerm($menu_tid);
    $this->vocabulary = $vocabulary = $menu_term->getVocabulary();
    $this->bmData = $bmdata = unserialize($menu_term->getDescription());
    if (!$bmdata || !$bmdata['type'] || $bmdata['type'] !== $bmtype) {
      $message = t("Unvalid bookmark data for: %menuid", ['%menuid' => $menu_tid]);
      throw new \LogicException($message);
    }

    $vid = $vocabulary->id();
    $toolbartab = SlogXtsi::SYS_VOCAB_ID_BMSTORAGE . '_' . $bmtype;
    $test_vid = SlogTx::getVocabularyIdFromParts([SlogTx::TOOLBAR_ID_SYS, $toolbartab]);
    if ($test_vid !== $vid) {
      $message = t("Bookmark vocabulary missmatch: %vid / %tvid", ['%vid' => $menu_tid, '%tvid' => $test_vid]);
      throw new \LogicException($message);
    }

    $this->bmItems = explode(',', $bmdata['items']);
    return 'Drupal\slogxt\Form\EmptyCheckboxesForm';
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $slogxtData = & $form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      $slogxtData['hideRadios'] = TRUE;
    }
    else {
      $this->form_title = ($this->bmType === 'list') ? t('List') : t('Content');
      $this->form_title .= ': ' . $this->menuTerm->label();

      $slogxtData['runCommand'] = 'doFinish';
      unset($slogxtData['items']);
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $tids = $this->bmItems;
    $items = ($this->bmType === 'list') //
        ? SlogXtsi::getListAttachDataByMenuTids($tids) //
        : SlogXtsi::getContentAttachDataBySids($tids);
    return [
      'command' => 'sxt_slogitem::finishedBookmarksLoaded',
      'args' => [
        'type' => $this->bmType,
        'items' => array_values($items),
        'ids' => array_keys($items),
      ],
    ];
  }

}
