<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Bmstorage\BmstorageOpen.
 */

namespace Drupal\sxt_slogitem\Handler\Bmstorage;

use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogtx\SlogTx;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_slogitem\Handler\Bmstorage\BmstorageBase;

class BmstorageOpen extends BmstorageBase {

  /**
   * {@inheritdoc}
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->menuSelectAlter = FALSE;
    $request = \Drupal::request();
    $menu_tid = (integer) $request->get('menu_tid');
    $this->bmType = $bmtype = $request->get('bookmark_type');
    if (empty($menu_tid)) {
      $this->doTbMenuSelect = TRUE;
      $base_entity_id = 'bmstorage' . SlogXt::XTURL_DELIMITER . $bmtype;
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $rootterm = $result['value'];

      if (!$rootterm || !$rootterm->isRootTerm()) {
        $message = t("Root term not found for bookmark type %bmtype", ['%bmtype' => $bmtype]);
        throw new \LogicException($message);
      }

      $request->attributes->set('root_term', $rootterm);
      if (!empty($result['menuSelectAlter'])) {
        $request->attributes->set('menuSelectAlter', $result['menuSelectAlter']);
        $request->attributes->set('menuSelectAlterIsSave', FALSE);
      }
      $this->vocabulary = $rootterm->getVocabulary();
      return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
    }

    // 
    $this->menuTerm = $menu_term = SlogTx::getMenuTerm($menu_tid);
    $bm_data = unserialize($menu_term->getDescription());
    if ($bm_data['type'] !== $bmtype) {
      $message = t("Unvalid menu term id: @tid.", ['@tid' => $tid]);
      throw new \LogicException($message);
    }
    if (!isset($bm_data['items'])) {
      $message = t("There are no bookmarks set: @tid.", ['@tid' => $tid]);
      throw new \LogicException($message);
    }

    $this->bmItems = explode(',', $bm_data['items']);
    return 'Drupal\slogxt\Form\EmptyCheckboxesForm';
  }

  protected function getFormTitle() {
    $title = t('Open');
    if (!$this->doTbMenuSelect) {
      $title .= t(': %name', ['%name' => $this->menuTerm->label()]);
    }
    return $title;
  }

  protected function getSubmitLabel() {
    return (t('Open'));
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if ($this->doTbMenuSelect) {
      $slogxtData = & $form_state->get('slogxtData');
      $slogxtData['hideRadios'] = FALSE;
      $slogxtData['infoMsg'] = t('Select the bookmark list');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $result = parent::getOnWizardFinished();
    drupal_get_messages();  // clear messages
    drupal_set_message(t('Bookmarks have been loaded.'));
    return $result;
  }

}
