<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\OnRetrieveAttachdata.
 */

namespace Drupal\sxt_slogitem\Handler;

use Drupal\slogxt\Controller\AjaxDefaultControllerBase;
//use Drupal\sxt_slogitem\Entity\SlogItem;

/**
 * Defines a controller ...
 */
class OnRetrieveAttachdata extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    $request = \Drupal::request();
    $sids = explode(';', $request->get('slogitem_ids', ''));
    $slogitems = $storage->loadMultiple($sids);
    $attachdata = [];
    foreach ($slogitems as $sid => $slogitem) {
      $attachdata[$sid] = $slogitem->getAttachData();
    }
        
    return $attachdata;
  }
}
