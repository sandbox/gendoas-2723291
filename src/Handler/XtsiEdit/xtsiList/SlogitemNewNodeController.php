<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\SlogitemNewNodeController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\sxt_slogitem\Form\xtsiFormUtils;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent\NodeEditControllerBase;

/**
 * Defines a controller ....
 */
class SlogitemNewNodeController extends NodeEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $base_entity_id = $request->get('base_entity_id');
    $sid = $request->get('next_entity_id');
    $node_type = str_replace('{node_type}', '', $request->get('node_type', ''));
    if (empty($node_type)) {
      $this->opPreview = false;
      return 'Drupal\sxt_slogitem\Form\XtsiEdit\SelectNodeTypeForm';
    }

    $this->isNodeEdit = true;
    $this->createNew = true;
    $this->menuTid = $base_entity_id;
    $this->SlogitemId = $sid;
    $this->node_type = $node_type;
    $this->node = $this
        ->entityTypeManager()
        ->getStorage('node')
        ->create(['type' => $node_type]);
    return $this->getEntityFormObject('node.default', $this->node);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->isNodeEdit) {
      if ($this->opPreview) {
        $title = t('Preview');
      }
      elseif ($this->opSave) {
        $title = t('Created');
      }
      else {
        $title = t('Create new content for %type', ['%type' => $this->node_type]);
      }
    }
    else {

      $title = t('New content: Select content type');
    }
    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->isNodeEdit) {
      $slogxtData['runCommand'] = 'wizardFormEdit';
    }
    else {
      $slogxtData['runCommand'] = 'radios';
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getLabels();
   */
  protected function getLabels() {
    $labels = parent::getLabels();
    if (!$this->isNodeEdit) {
      $labels['submitLabel'] = t('Next');
    }
    return $labels;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::save();
   */
  public static function save(array $form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    $nid = $form_state->getValue('nid');

    $params = [
      'raw_title' => '__.@node',
      'tid' => $calledObject->menuTid,
      'pos_sid' => $calledObject->SlogitemId,
      'insert_after' => TRUE,
      'is_new_content' => TRUE,
      'new_type' => 'node',
      'new_eid' => $nid,
    ];
    $success = xtsiFormUtils::insertNewSlogitem($params);

    // return success
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['submit_ok'] = $success['done'];
    $slogxt_data['submit_err'] = $success['not_done'];
    $slogxt_data['done_sids'] = $success['done_sids'];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $data = $this->slogxt_data_final;
    if (!empty($data['submit_ok'])) {
      $list = implode("; ", $data['submit_ok']);
      drupal_set_message(t('Created new content: %list', ['%list' => $list]));
    }
    if (!empty($data['submit_err'])) {
      $list = implode("; ", $data['submit_err']);
      drupal_set_message(t('Could not be created: %list', ['%list' => $list]), 'error');
    }

    return [
      'command' => 'sxt_slogitem::finishedSlogitemNewItems',
      'args' => $data['done_sids'],
    ];
  }

}
