<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\SlogitemNewLinkController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class SlogitemNewLinkController extends SlogitemEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return ('\Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiNewLinkForm');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $request = \Drupal::request();
    $tid = (integer) $request->get('base_entity_id');
    $pos_sid = (integer) $request->get('next_entity_id');
    $slogitem = $pos_sid ? SlogXtsi::loadSlogitem($pos_sid) : FALSE;
    $menu_term = $slogitem ? $slogitem->getTerm() : SlogTx::getMenuTerm($tid);
    $args = ['%menu' => isset($menu_term) ? $menu_term->label() : '???'];
    return t('Create links in %menu', $args);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return t('Create');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitLinkSids';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    drupal_get_messages();  // clear messages
    $data = $this->slogxt_data_final;
    if (!empty($data['submit_ok'])) {
      $list = implode("; ", $data['submit_ok']);
      drupal_set_message(t('Created link(s) to: %list', ['%list' => $list]));
    }
    if (!empty($data['submit_err'])) {
      $list = implode("; ", $data['submit_err']);
      drupal_set_message(t('Could not be saved: %list', ['%list' => $list]), 'error');
    }

    return [
      'command' => 'sxt_slogitem::finishedSlogitemNewItems',
      'args' => $data['done_sids'],
    ];
  }

}
