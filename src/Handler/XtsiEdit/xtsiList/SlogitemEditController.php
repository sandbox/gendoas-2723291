<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\SlogitemEditController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class SlogitemEditController extends SlogitemEditControllerBase {

  protected $slogitem;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->createNew = false;
    $this->slogitem = SlogItem::load(\Drupal::request()->get('next_entity_id'));
    $this->label_old = $this->slogitem->label();
    
    return $this->getEntityFormObject('slogitem.edit', $this->slogitem);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Edit %title', ['%title' => $this->slogitem->label()]);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $slogitem = SlogXtsi::loadSlogitem($this->slogitem->id());
    $this->setSavedMessage($slogitem->label());
    
    $vocabulary = $slogitem->getTerm()->getVocabulary();
    list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());

    return [
        'command' => 'sxt_slogitem::finishedSlogitemEdit',
        'args' => [
          'isNew' => $this->createNew,
          'sid' => $slogitem->id(),
          'tid' => $slogitem->get('tid')->value,
          'toolbar' => $toolbar,
          'labelOld' => $this->label_old,
          'labelNew' => $slogitem->label(),
        ],
    ];
  }

}
