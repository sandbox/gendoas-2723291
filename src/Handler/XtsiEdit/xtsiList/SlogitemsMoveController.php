<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\SlogitemsMoveController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class SlogitemsMoveController extends SlogitemEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $this->hasTargetMenuTid = FALSE;
    $this->isSourceSelect = FALSE;
    $request = \Drupal::request();
    $this->baseEntityId = $request->get('base_entity_id');
    $next_entity_id = $request->get('next_entity_id');
    $link_entity_id = $request->get('link_entity_id');
    $target_eid = (integer) $next_entity_id;
    $source_eid = (integer) $link_entity_id;
    $this->formResult = FALSE;

    $target_menu_tid = $target_eid ?: (integer) substr($next_entity_id, 2, -2);  // see maskItemId
    $source_menu_tid = $source_eid ?: (integer) substr($link_entity_id, 2, -2);  // see maskItemId
    if (!$source_eid) {
      $this->doTbMenuSelect = TRUE;
      $this->hasTargetMenuTid = (boolean) $target_menu_tid;

      if ($source_menu_tid) {
        $menu_term = SlogTx::getMenuTerm($source_menu_tid);
        $name = $menu_term->label();
        $headerPath = $menu_term->getVocabulary()->headerPathLabel();
        $info_data = "<strong><br />$headerPath<br />$name</strong>";

        $this->isSourceSelect = TRUE;
        $this->client_resolve = [
          'resolve' => [
            'key' => $link_entity_id,
            'func' => 'sxt_slogitem::resolvePathSlogitemId',
            'provider' => 'sxt_slogitem',
          ],
          'args' => [
            'sysInMenuTid' => $source_menu_tid,
            'xtTitle' => t('..??'),
            'xtInfo' => t('Select contents from the selected list:') . $info_data,
            'isTarget' => FALSE,
            'isSource' => TRUE,
            'multiple' => TRUE,
          ],
        ];
        return SlogXt::arrangeUrgentMessageForm();
      }
      else {
        $replaceKey = $target_menu_tid ? $link_entity_id : $next_entity_id;
        list($target, $toolbar_id) = explode(SlogXt::XTURL_DELIMITER, $replaceKey . SlogXt::XTURL_DELIMITER);
        
        if ($toolbar = SlogTx::getToolbar($toolbar_id)) {
          $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($target);   //$target: 'archive', 'trash'
          $entity = SlogXt::getTbDeafultTargetEntity($toolbar_id);
          $rootterm = SlogTx::getSysSysRootTerm($plugin_id, $entity);
        }
        if (empty($rootterm)) {
          return SlogXt::arrangeUrgentUnexpectedErrorForm();
        }

        if ($target_eid && $slogitem = SlogXtsi::loadSlogitem($target_eid)) {
          $menuTid = $slogitem->getTermId();
          $request->attributes->set('excludeMenuTids', [$menuTid]);
        }




//todo::current:: SYS_VOCAB_ID_ARCHIVE/SYS_VOCAB_ID_TRASH
//                    if (in_array($target, [SlogXt::SYS_VOCAB_ID_ARCHIVE, SlogXt::SYS_VOCAB_ID_TRASH])) {  //'archive', 'trash'
//                      $entity = NULL;
//                      if ($toolbar_id === SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
//                        return SlogXt::arrangeUrgentMessageForm(t('Not implemented: move items for _node and Archive/Trash.'), 'error');
//            //                    if (!empty($target_eid)) {
//            //                      $slogitem = SlogXtsi::loadSlogitem($target_eid);
//            //                      $target_menu_term = $slogitem->getTerm();
//            //                      if ($slogitem && $vocab = $target_menu_term->getVocabulary()) {
//            //                        $term_name = $target_menu_term->getTargetTerm()->label();
//            //                        $entity = $vocab->getEntityFromTargetTermName($term_name);
//            //                      }  
//            //
//            //                      if (!$entity) {
//            //                        $message = t("Target node not found for sid: %sid.", ['%sid' => $target_eid]);
//            //                        throw new \LogicException($message);
//            //                      }
//            //                    } else {
//            //                      return SlogXt::arrangeUrgentMessageForm(t('Not implemented: A/T -> XX.'), 'error');
//            //                    }
//                      }
//
//                      if ($target === SlogXt::SYS_VOCAB_ID_ARCHIVE) {
//                        $rootterm = SlogXt::getArchiveRootTerm($toolbar_id, $entity);
//                      }
//                      else {
//                        $rootterm = SlogXt::getTrashRootTerm($toolbar_id, $entity);
//                      }
//                      if (empty($rootterm)) {
//                        return SlogXt::arrangeUrgentUnexpectedErrorForm();
//                      }
//
//                      if ($target_eid && $slogitem = SlogXtsi::loadSlogitem($target_eid)) {
//                        $menuTid = $slogitem->getTermId();
//                        $request->attributes->set('excludeMenuTids', [$menuTid]);
//                      }
//                    }

        $request->attributes->set('maskItemId', '{_%id%_}');
        $request->attributes->set('pathReplaceKey', $replaceKey);
        $request->attributes->set('root_term', $rootterm);
        return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
      }
    }

    if ($target_menu_tid && !$target_eid) {
      $request->attributes->set('next_entity_id', "-$target_menu_tid");
    }

    return ('\Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiMoveForm');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $this->slogitem = false;
    $request = \Drupal::request();
    $pos_sid = (integer) $request->get('next_entity_id');
    if ($pos_sid < 0) {
      // move menu tid (as negative id) to base_entity_id
      $menu_tid = -$pos_sid;
      $request->attributes->set('base_entity_id', $menu_tid);
      $request->attributes->set('next_entity_id', 0);
      $this->menu_term = SlogTx::getMenuTerm($menu_tid);
      $sids = SlogXtsi::getSidsFromTidsSorted([$menu_tid], 1);
      if (!empty($sids)) {
        $request->attributes->set('next_entity_id', $sids[0]);
        $request->attributes->set('forceInsertBefore', TRUE);
      }
    }
    elseif ($pos_sid > 0) {
      $this->slogitem = SlogXtsi::loadSlogitem($pos_sid);
      $this->menu_term = $this->slogitem->getTerm();
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $slogxtData = &$form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      $slogxtData['wizardFinalize'] = FALSE;
      $slogxtData['runCommand'] = 'radios';
      $slogxtData['attachCommand'] = 'attachSysTbmenuList';
      if (!$this->hasTargetMenuTid) {
        $slogxtData['serverResolved'] = TRUE;
      }
    }
    else {
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitLinkSids';
      if (!$this->slogitem) {
        $args = ['%menu' => $this->menu_term->label()];
        $msg = t('You are about to move contents to %menu', $args);
        $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      if ($this->isSourceSelect) {
        return t('Select contents to move');
      }
      else {
        return t('Select where to move contents');
      }
    }
    else {
      $args = ['%menu' => $this->menu_term->label()];
      return t('Move contents to %menu', $args);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return t('Next');
    }
    else {
      return t('Move');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    if ($this->doTbMenuSelect) {
      return;
    }

    drupal_get_messages();  // clear messages
    $data = $this->slogxt_data_final;
    if (!empty($data['submit_ok'])) {
      $list = implode("; ", $data['submit_ok']);
      drupal_set_message(t('Moved contents: %list', ['%list' => $list]));
    }
    if (!empty($data['submit_warn'])) {
      $list = implode("; ", $data['submit_warn']);
      drupal_set_message(t('Not moved since already present: %list', ['%list' => $list]), 'warning');
    }
    if (!empty($data['submit_err'])) {
      $list = implode("; ", $data['submit_err']);
      drupal_set_message(t('Could not be moved: %list', ['%list' => $list]), 'error');
    }

    return [
      'command' => 'sxt_slogitem::finishedSlogitemsMove',
      'args' => $data['done_sids'],
    ];
  }

}
