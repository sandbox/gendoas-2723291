<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\ListRearrangeController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class ListRearrangeController extends XtEditControllerBase {

  protected $menuTerm;
  protected $opSave = false;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->doTbMenuSelect = FALSE;
    $request = \Drupal::request();
    $this->baseEntityId = $request->get('base_entity_id');
    $this->menuTerm = $this->baseEntityId;
    $next_entity_id = $request->get('next_entity_id');
    $target_eid = (integer) $next_entity_id;
    $target_menu_tid = $target_eid ?: (integer) substr($next_entity_id, 2, -2);  // see maskItemId
    if (!empty($next_entity_id) && !$target_eid) {
      if ($target_menu_tid) {
        $this->menuTerm = SlogTx::getMenuTerm($target_menu_tid);
        $request->attributes->set('slogtx_mt', $this->menuTerm);
      }
      else {
        $this->doTbMenuSelect = TRUE;
        list($target, $toolbar_id) = explode(SlogXt::XTURL_DELIMITER, $next_entity_id . SlogXt::XTURL_DELIMITER);
//        $sys_toolbar = SlogTx::getToolbar(SlogTx::TOOLBAR_ID_SYS);
//        $plugin_id = $sys_toolbar->getSysMenuTbRootTermPluginId($target, $toolbar_id);
//        $rootterm = SlogTx::getSysSysRootTerm($plugin_id);
//        if (!empty($rootterm)) {
//          return SlogXt::arrangeUrgentUnexpectedErrorForm();
//        }
//todo::current:: TxRootTermGetPlugin - new

        if ($toolbar = SlogTx::getToolbar($toolbar_id)) {
          $plugin_id = $toolbar->getSysMenuTbRootTermPluginId($target);   //$target: 'archive', 'trash'
          $entity = SlogXt::getTbDeafultTargetEntity($toolbar_id);
          $rootterm = SlogTx::getSysSysRootTerm($plugin_id, $entity);
        }
        if (empty($rootterm)) {
          return SlogXt::arrangeUrgentUnexpectedErrorForm();
        }

//todo::current:: SYS_VOCAB_ID_ARCHIVE/SYS_VOCAB_ID_TRASH
//                    if (in_array($target, [SlogXt::SYS_VOCAB_ID_ARCHIVE, SlogXt::SYS_VOCAB_ID_TRASH])) {  //'archive', 'trash'
//                      if ($target === SlogXt::SYS_VOCAB_ID_ARCHIVE) {
//                        $rootterm = SlogXt::getArchiveRootTerm($toolbar_id);
//                      }
//                      else {
//                        $rootterm = SlogXt::getTrashRootTerm($toolbar_id);
//                      }
//                      if (empty($rootterm)) {
//                        return SlogXt::arrangeUrgentUnexpectedErrorForm();
//                      }
//                    }

        $request->attributes->set('maskItemId', '{_%id%_}');
        $request->attributes->set('pathReplaceKey', $next_entity_id);
        $request->attributes->set('root_term', $rootterm);
        return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
      }
    }

    return $this->entityTypeManager()->getListBuilder('slogitem');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->doTbMenuSelect) {
      return t('Select content list');
    }
    else {
      return t('Rearrange %title', ['%title' => $this->menuTerm->label()]);
    }
  }

  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return t('Next');
    }
    else {
      return t('Save');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    if ($this->doTbMenuSelect) {
      return;
    }

    $request = \Drupal::request();
    $op = $request->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if (!$this->doTbMenuSelect) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['runCommand'] = 'wizardFormEdit';

      // no operations
      $entities = &$form['entities'];
      unset($entities['#header']['operations']);
      foreach (Element::children($entities) as $key) {
        unset($entities[$key]['operations']);
      }
    }

    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $slogxtData = & $form_state->get('slogxtData');
    if ($this->doTbMenuSelect) {
      $slogxtData['wizardFinalize'] = FALSE;
      $slogxtData['runCommand'] = 'radios';
      $slogxtData['attachCommand'] = 'attachSysTbmenuList';
    }
    else {
      $slogxtData['tabledrag'] = $form['entities']['#id'];
      if ($this->opSave) {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
      else {
        $slogxtData['wizardFinalize'] = true;
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();

    $vid = $this->menuTerm->getVocabularyId();
    list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vid);
    return [
      'command' => 'sxt_slogitem::finishedXtsiListChange',
      'args' => [
        'tid' => $this->menuTerm->id(),
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
      ],
    ];
  }

}
