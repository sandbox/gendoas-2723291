<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList\SlogitemEditControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ....
 */
abstract class SlogitemEditControllerBase extends XtEditControllerBase {

  protected $isNodeEdit = false;
  protected $createNew = false;
  protected $opPreview = false;
  protected $opSave = false;

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $slogxt = $form_state->get('slogxt');
    if ($slogxt && $slogxt['ajax']) {
      // add callbacks
      // allow submit as the only actions
      // and add callbacks
      $controllerClass = get_class($this);
      $allowed = ['submit'];
      $actions = &$form['actions'];
      if (!empty($actions) && is_array($actions)) {
        foreach (Element::children($actions) as $key) {
          if (!in_array($key, $allowed)) {
            unset($actions[$key]);
          }
          elseif ($key === 'submit') {
            $actions['submit']['#submit'][] = "$controllerClass::save";
          }
        }
      }
    }
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $op = \Drupal::request()->get('op', false);
    $this->opSave = ($op && $op === (string) t('Save'));
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    $readonly = ['target_entity_type', 'target_entity_id'];
    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      if (!empty($field['#group']) || $field['#type'] === 'vertical_tabs' || $field_name === 'comment') {
        $field['#access'] = false;
      }
      elseif (in_array($field_name, $readonly)) {
        $field['#access'] = false;
      }
      elseif ($field['#type'] === 'container' && isset($field['#access']) && $field['#access']) {
        $field['#attributes']['class'][] = 'slogxt-input-field';
      }

      if (isset($field['#type']) && $field['#type'] === 'container' && isset($field['#access']) && $field['#access']) {
        $format = & $field['widget'][0]['format'];
        if ($format && $format['help']) {
          unset($format['help']);
        }
      }
    }


    // enable submit button
    $submit = & $form['actions']['submit'];
    is_array($submit) && $submit['#access'] = true;

    // Set error if required button is missing
    $required = ['submit' => t('Submit')];
    foreach ($required as $key => $label) {
      $action = isset($form['actions'][$key]) ? $form['actions'][$key] : false;
      if (empty($action) || empty($action['#access']) || !$action['#access']) {
        drupal_set_message('Button not available: ' . $label, 'error');
      }
    }

    $slogxtData = & $form_state->get('slogxtData');
    $hasErrors = $form_state->hasAnyErrors();
    if ($hasErrors) {
      // nothing
    }
    elseif ($this->opSave) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    } else {
        $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::renderFormPlus();
   */
  protected function renderFormPlus(&$form, FormStateInterface $form_state) {
    if ($this->submitted && !$this->hasErrors) {
      $this->formResult = 'saved';
      $this->htmlContent = '';
    }
    else {
      $this->formResult = 'edit';
      $this->htmlContent = $this->renderer->renderRoot($form);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    $label = t('Submit');
    if ($this->formResult === 'edit') {
      $label = $this->createNew ? t('Create') : t('Update');
    }
    elseif ($this->formResult === 'preview') {
      $label = t('Close');
    }
    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getLabels();
   */
  protected function getLabels() {
    $labels = parent::getLabels();
    if ($this->formResult === 'edit') {
      $labels['submitNext'] = t('Next');
    }
    return $labels;
  }

  //
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // Static callbacks, 
  // use self::calledObject() as the current ajax form controller
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  /**
   * Submit handler: Save
   * 
   * Make the saved values available for the called object.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * 
   * @see this::hookNodeFormAlter()
   */
  public static function save(array $form, FormStateInterface $form_state) {
    self::calledObject()->savedValues['slogitem'] = $form_state->getValues();
  }

}
