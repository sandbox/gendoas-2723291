<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PromoteSurpriseSelectController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\slogxt\Controller\XtEditControllerBase;

/**
 * Defines a controller ....
 */
class PromoteSurpriseSelectController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\sxt_slogitem\Form\XtsiEdit\xtMain\PromoteSurpriseSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Promoted/Surprise: Select action');
  }

}
