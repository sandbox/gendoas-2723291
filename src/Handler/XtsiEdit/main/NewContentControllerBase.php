<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\NewContentControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtUserData;
use Drupal\sxt_slogitem\Form\xtsiFormUtils;
use Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController;

/**
 * Defines a controller ....
 * 
 * Used for:
 *  - PromoteNewContentController, SurpriseNewContentController
 * 
 */
abstract class NewContentControllerBase extends TbMenuNewController {

  protected $slogitem_data = [];

  abstract protected function roottermToPrepare($do_create = FALSE);

  abstract protected function actionPluginId();

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->baseEntityId = $request->get('base_entity_id');
    $this->default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    if ($this->default_role->id() != $this->baseEntityId) {
      $message = t('Current default role missmatch.');
      throw new \LogicException($message);
    }

    if ($root_term = $this->roottermToPrepare(TRUE)) {
      $this->root_term = $root_term;
      $this->hasChildren = $root_term->hasChildren();
      $this->vocabulary = $root_term->getVocabulary();
      if (!$root_term->hasChildren()) {
        $message = t('Current default role missmatch.');
        throw new \LogicException($message);
      }

      $next_eid = $request->get('next_entity_id');
      $this->doTbMenuSelect = (!empty($next_eid) && $next_eid === '{next_entity_id}');
      if ($this->doTbMenuSelect) {
        $request->attributes->set('user_role', $this->default_role);
        $request->attributes->set('root_term', $this->root_term);
        $request->attributes->set('actionPluginId', $this->actionPluginId());
        return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
      }
      else {
        $this->menuTerm = SlogTx::getMenuTerm($next_eid);
        return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    $role = $this->baseEntityId;
    if ($this->doTbMenuSelect) {
      return t('%role/%name: Select a menu item', ['%role' => $role, '%name' => $name]);
    }
    else {
      return t('%role/%name: Add contents', ['%role' => $role, '%name' => $name]);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->doTbMenuSelect) {
      return t('Next');
    }
    else {
      return t('Add contents');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect) {
      $input = $form_state->getUserInput();
      $value = isset($input['link_sids']) ? $input['link_sids'] : '';
      $form['link_sids'] = [
        '#type' => 'hidden',
        '#value' => $value,
      ];
      //
      $controllerClass = get_class($this);
      $form['#validate'][] = "$controllerClass::formValidate";
    }
  }

  protected function jsAlterFormArgs() {
    $items = $this->getSlogitemsData();
    $has_items = (count($items) > 0);
    $notice = t('Last state: ') . ($has_items //
        ? t('list of already existing contents') //
        : t('there is no existing content yet'));
    return [
      'items' => $items,
      'hasItems' => $has_items,
      'radios' => FALSE,
      'enableSubmit' => TRUE,
      'notice' => SlogXt::htmlMessage($notice),
    ];
  }

  protected function getSlogitemsData() {
    $menuTid = $this->menuTerm->id();
    if (!isset($this->slogitem_data[$menuTid])) {
      $data = SlogXtsi::getSlogitemAttachDataByTid($menuTid);
      $items = [];
      if (!empty($data)) {
        foreach ($data as $key => $item) {
          $items[] = $item['content'];
        }
      }
      $this->slogitem_data[$menuTid] = $items;
    }
    return $this->slogitem_data[$menuTid];
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if ($this->doTbMenuSelect && $this->vocabulary->isSysToolbar()) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['attachCommand'] = 'attachSysTbmenuList';
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitLinkSids';
    if (!$this->doTbMenuSelect) {
      $slogxtData['alterForm'] = [
        'command' => 'sxt_slogitem::alterFormAddList',
        'args' => $this->jsAlterFormArgs(),
      ];
    }
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    if (!$this->doTbMenuSelect) {
      foreach (['name', 'description', 'relations'] as $key) {
        if (isset($form[$key])) {
          $form[$key]['#access'] = FALSE;
        }
      }
      $name = $this->menuTerm->label();
      $msg = t('You are about to add contents for menu item %name', ['%name' => $name]);
      $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
    }
  }

  /**
   * Validate callback.
   * 
   * Use own validator for adding own submitter.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    if (!$calledObject->doTbMenuSelect) {
      $input = $form_state->getUserInput();
      $values = isset($input['link_sids']) ? $input['link_sids'] : '';
      if (empty($values)) {
        $form_state->setErrorByName('link_sids', t('There are no items selected.'));
      }
    }

    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    if ($calledObject->doTbMenuSelect) {
      $root_term = self::calledObject()->root_term;
      $root_term->getTargetTerm()->setStatus(TRUE)->save();
      $root_term->setStatus(TRUE)->save();
      self::calledObject()->opSave = TRUE;
    }
    else {
      if (!empty($calledObject->menuTerm) && $calledObject->menuTerm->isMenuTerm()) {
        $menu_tid = $calledObject->menuTerm->id();
      }
      else {
        $menu_tid = $calledObject->root_term->getFirstChildTid();
      }
      $input = $form_state->getUserInput();
      $sids = isset($input['link_sids']) ? $input['link_sids'] : '';
      $params = [
        'raw_title' => '@itemid_@item',
        'tid' => $menu_tid,
        'link_sids' => explode(',', $sids),
        'slogitem_redirect' => TRUE,
      ];

      $success = xtsiFormUtils::insertNewSlogitem($params);
      $slogxt_data = & $form_state->get('slogxt');
      $slogxt_data['submit_ok'] = $success['done'];
      $slogxt_data['submit_err'] = $success['not_done'];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    $role = $this->baseEntityId;
    if (!$this->doTbMenuSelect) {
      drupal_get_messages();  // clear messages
      $args = ['%name' => $this->root_term->getVocabulary()->getTbtabLabel()];
      drupal_set_message(t('New content for %name; ', $args));
      $data = $this->slogxt_data_final;
      if (!empty($data['submit_ok'])) {
        $list = implode("; ", $data['submit_ok']);
        drupal_set_message(t('Created new content: %list', ['%list' => $list]));
      }
      if (!empty($data['submit_err'])) {
        $list = implode("; ", $data['submit_err']);
        drupal_set_message(t('Could not be created: %list', ['%list' => $list]), 'error');
      }
      return ['command' => FALSE];
    }
  }

}
