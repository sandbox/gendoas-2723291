<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\DeactivateControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtUserData;
use Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController;

/**
 * Defines a controller ....
 */
abstract class DeactivateControllerBase extends TbMenuNewController {

  abstract protected function roottermToDeactivate();

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->baseEntityId = \Drupal::request()->get('base_entity_id');
    $this->default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    if ($this->default_role->id() != $this->baseEntityId) {
      $message = t('Current default role missmatch.');
      throw new \LogicException($message);
    }

    $root_term = $this->roottermToDeactivate();
    if (!$root_term) {
      throw new \LogicException(t('Missing root term.'));
    }
    
    $this->root_term = $root_term;
//                $request = \Drupal::request();
//                $target_term = $root_term->getTargetTerm();
//                $request->attributes->set($target_term->getEntityTypeId(), $target_term);
//                $class = 'Drupal\slogtx_ui\Form\TargetTermEnableForm';
//                $form_arg = $this->classResolver->getInstanceFromDefinition($class);
//                $form_arg->setEntity($target_term);
//                $form_arg->setModuleHandler(\Drupal::moduleHandler());
//                return $form_arg;
    
//    $request = \Drupal::request();
//    $request->attributes->set('confirm_request_data', $this->getConfirmRequestData());
    return 'Drupal\slogxt\Form\XtGenericConfirmForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $args = ['%name' => $this->root_term->getVocabulary()->getTbtabLabel()];
    return t('Deactivate %name', $args);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return t('Deactivate');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $controllerClass = get_class($this);
    $form['#validate'][] = "$controllerClass::formValidate";
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitLinkSids';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    $this->preRenderFormBase($form, $form_state);

    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    $msg = t('You are about to deactivate %name', ['%name' => $name]);
    $form['description']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
    parent::preRenderFormLast($form, $form_state);
  }

  /**
   * Validate callback.
   * 
   * Use own validator for adding own submitter.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
//todo::current:: new::surprise::setStatus
//    $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
    $submit_handlers = [[get_class(), 'formSubmit']];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $root_term = self::calledObject()->root_term;
//    $root_term->getTargetTerm()->setStatus(FALSE)->save();
    $root_term->setStatus(FALSE)->save();
//    self::calledObject()->opSave = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();
    return ['command' => 'sxt_slogitem::reload'];
  }

}
