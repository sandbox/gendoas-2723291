<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\SurprisePrepareController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class SurprisePrepareController extends PrepareControllerBase {

  protected function roottermToPrepare($do_create = FALSE) {
    return SlogXtsi::getSurpriseRootTerm($this->default_role, $do_create);
  }

  protected function targetLabel() {
    return t('Surprise');
  }

}
