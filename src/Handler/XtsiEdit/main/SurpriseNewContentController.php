<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\SurpriseNewContentController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class SurpriseNewContentController extends NewContentControllerBase {

  protected function actionPluginId() {
    return 'xtsi_surprise_newcontent';
  }
  
  protected function roottermToPrepare($do_create = FALSE){
    return SlogXtsi::getSurpriseRootTerm($this->default_role, $do_create);
  }

}
