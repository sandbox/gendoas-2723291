<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PrepareControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtUserData;
use Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController;

/**
 * Defines a controller ....
 * 
 * Used for:
 *  - PromotePrepareController, SurprisePrepareController
 *  - PromoteNewContentController, SurpriseNewContentController
 * 
 */
abstract class PrepareControllerBase extends TbMenuNewController {

  protected $is_dummy_rootterm = FALSE;

  abstract protected function roottermToPrepare($do_create = FALSE);

  abstract protected function targetLabel();

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->baseEntityId = \Drupal::request()->get('base_entity_id');
    $this->default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    if ($this->default_role->id() != $this->baseEntityId) {
      $message = t('Current default role missmatch.');
      throw new \LogicException($message);
    }

    if (!$root_term = $this->roottermToPrepare()) {
      $this->is_dummy_rootterm = TRUE;
      $root_term = SlogTx::getSysSysRootTermDummy();
    }
    if (!$root_term) {
      throw new \LogicException(t('Missing root term.'));
    }

    $this->root_term = $root_term;
    $this->hasChildren = !$this->is_dummy_rootterm ? $root_term->hasChildren() : FALSE;
    if ($this->hasChildren) {
//todo::current:: TxRootTermGetPlugin - new
//                      $request = \Drupal::request();
//                      $target_term = $root_term->getTargetTerm();
//                      $request->attributes->set($target_term->getEntityTypeId(), $target_term);
//                      $class = 'Drupal\slogtx_ui\Form\TargetTermEnableForm';
//                      $form_arg = $this->classResolver->getInstanceFromDefinition($class);
//                      $form_arg->setEntity($target_term);
//                      $form_arg->setModuleHandler(\Drupal::moduleHandler());
//                      return $form_arg;
      return 'Drupal\slogxt\Form\XtGenericConfirmForm';
    }
    else {
      // create the first menu term
      $this->menuTerm = SlogTx::entityStorage('slogtx_mt')->create([
        'vid' => $root_term->getVocabularyId(),
        'parent' => $root_term->id(),
      ]);
      return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->is_dummy_rootterm) {
      $args = [
        '%target' => $this->targetLabel(),
        '%role' => $this->default_role->label(),
      ];
      return t('Prepare %target for %role', $args);
    }
    else {
      $args = ['%name' => $this->root_term->getVocabulary()->getTbtabLabel()];
      if ($this->hasChildren) {
        return t('Activate %name', $args);
      }
      else {
        return t('Create menu item for %name', $args);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    if ($this->is_dummy_rootterm) {
      return t('Prepare');
    }
    elseif ($this->hasChildren) {
      return t('Activate');
    }
    else {
      return t('Create');
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $controllerClass = get_class($this);
    $form['#validate'][] = "$controllerClass::formValidate";
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    $slogxtData['preSubmit'] = 'sxt_slogitem::preSubmitLinkSids';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    if ($this->hasChildren) {
      $this->preRenderFormBase($form, $form_state);

      $name = $this->root_term->getVocabulary()->getTbtabLabel();
      $msg = t('You are about to activate %name', ['%name' => $name]);
      $form['description']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
      parent::preRenderFormLast($form, $form_state);
    }
    else {
      parent::preRenderForm($form, $form_state);
    }
  }

  /**
   * Validate callback.
   * 
   * Use own validator for adding own submitter.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    $submit_handlers[] = [get_class(self::calledObject()), 'formSubmit'];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * {@inheritdoc}
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
    if ($calledObject->is_dummy_rootterm) {
      $calledObject->root_term = $rootterm = $calledObject->roottermToPrepare(TRUE);
      $menu_term = $form_state->getFormObject()->getEntity();
      $menu_term->setParentID($rootterm->id())
          ->setVocabularyId($rootterm->getVocabularyId())
          ->save();
    }
    elseif ($calledObject->hasChildren) {
      $root_term = self::calledObject()->root_term;
//todo::current:: new::surprise::setStatus


      $target_term = $root_term->getTargetTerm();
      $target_term->setStatus(TRUE)->save();
      $root_term->setStatus(TRUE)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    $role = $this->baseEntityId;
    if ($this->hasChildren) {
      $this->setSavedMessage();
      return ['command' => 'sxt_slogitem::reload'];
    }
    else {
      drupal_get_messages();  // clear messages
     $args = ['%name' => $this->root_term->getVocabulary()->getTbtabLabel()];
      drupal_set_message(t('Structure prepared for %name', $args));
      return ['command' => FALSE];
    }
  }

}
