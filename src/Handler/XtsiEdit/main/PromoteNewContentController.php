<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PromoteNewContentController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class PromoteNewContentController extends NewContentControllerBase {

  protected function actionPluginId() {
    return 'xtsi_promote_newcontent';
  }
  
  protected function roottermToPrepare($do_create = FALSE){
    return SlogXtsi::getPromotedRootTerm($this->default_role, $do_create);
  }
  
}
