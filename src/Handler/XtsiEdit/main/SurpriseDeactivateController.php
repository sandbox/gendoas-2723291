<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\SurpriseDeactivateController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class SurpriseDeactivateController extends DeactivateControllerBase {

  protected function roottermToDeactivate(){
    return SlogXtsi::getSurpriseRootTerm($this->default_role);
  }
  
}
