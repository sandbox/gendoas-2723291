<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PromotePrepareController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class PromotePrepareController extends PrepareControllerBase {

  protected function roottermToPrepare($do_create = FALSE){
    return SlogXtsi::getPromotedRootTerm($this->default_role, $do_create);
  }
  
  protected function targetLabel(){
    return t('Promoted');
  }
  
}
