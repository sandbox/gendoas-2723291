<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\SurprisePrepareController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class SurpriseSelectController extends SelectControllerBase {

  protected function roottermToSelect() {
    return SlogXtsi::getSurpriseRootTerm($this->default_role);
  }

  protected function addNoneOption() {
    return (boolean) $this->doTbMenuSelect;
  }

  protected function getArgFormClass() {
    if ($this->doTbMenuSelect) {
      return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
    }
    else {
      return '\Drupal\sxt_slogitem\Form\SelectSurpriseForm';
    }
  }

  protected function getSubmitLabel() {
    return $this->doTbMenuSelect ? t('Preview') : parent::getSubmitLabel();
  }

  protected function getSelectedMenuTid() {
    return (integer) SlogXtsi::getSurpriseTermId();
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if (!$this->doTbMenuSelect) {
      parent::hookFormAlter($form, $form_state, $form_id);
      $form['surprise_tid'] = [
        '#type' => 'hidden',
        '#value' => $this->tbMenuTid,
      ];
      return;
    }
  }

}
