<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PromotePrepareController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class PromoteSelectController extends SelectControllerBase {

  protected function roottermToSelect() {
    return SlogXtsi::getPromotedRootTerm($this->default_role);
  }

  protected function addNoneOption() {
    return (boolean) $this->doTbMenuSelect;
  }

  protected function getArgFormClass() {
    if ($this->doTbMenuSelect) {
      return '\Drupal\slogxt\Form\edit\xtMain\TbMenuSelectForm';
    }
    else {
      return '\Drupal\sxt_slogitem\Form\SelectPromotedForm';
    }
  }

  protected function getSubmitLabel() {
    return $this->doTbMenuSelect ? t('Next') : parent::getSubmitLabel();
  }

  protected function getSelectedMenuTid() {
    $sid = $this->getSelectedSid();
    $slogitem = ($sid > 0) ? SlogItem::load($sid) : FALSE;
    $tid = $slogitem ? $slogitem->getTermId() : 0;
    return (integer) $tid;
  }

  protected function getSelectedSid() {
    return (integer) SlogXtsi::getPromotedSlogitemId();
  }

  protected function jsAlterFormArgs() {
    $args = parent::jsAlterFormArgs();
    $items = $args['items'];
    $sid = $this->getSelectedSid();
    $slogitem = ($sid > 0) ? SlogItem::load($sid) : FALSE;
    if ($slogitem) {
      $sid = (integer) $slogitem->getTargetSid();
    }
    $selIdx = -1;
    foreach ($items as $key => $item) {
      if ($item['sid'] == $sid) {
        $selIdx = $key;
        break;
      }
    }
    
    $args = [
      'radios' => TRUE,
      'selIdx' => $selIdx,
      'fieldSelector' => 'edit-promoted-sid',
    ] + $args;
    return $args;
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if ($this->doTbMenuSelect) {
      // nothing to do
      return;
    }

    parent::hookFormAlter($form, $form_state, $form_id);
    $form['promoted_tid'] = [
      '#type' => 'hidden',
      '#value' => $this->tbMenuTid,
    ];

    $promoted_sid = $this->getSelectedSid();
    $input = $form_state->getUserInput();
    if (is_array($input) && isset($input['promoted_sid'])) {
      $sid = $input['promoted_sid'];
      $result = SlogXtsi::loadByTidAndEntity($this->tbMenuTid, 'slogitem', $sid);
      $slogitem = (count($result) === 1) ? reset($result) : FALSE;
      $promoted_sid = $slogitem ? $slogitem->id() : 0;
    }
    $form['detail_sids']['promoted_sid'] = [
      '#type' => 'hidden',
      '#value' => $promoted_sid,
    ];
  }

}
