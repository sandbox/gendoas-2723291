<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PrepareControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\XtUserData;
use Drupal\slogtb\Handler\edit\TbEditControllerBase;

/**
 * Defines a controller ....
 */
abstract class SelectControllerBase extends TbEditControllerBase {

  protected $slogitem_data = [];
  protected $opSave = false;

  abstract protected function roottermToSelect();
  
  abstract protected function addNoneOption();

  abstract protected function getArgFormClass();

  abstract protected function getSelectedMenuTid();

  protected function jsAlterFormArgs() {
    $items = $this->getSlogitemsData();
    return [
      'items' => $items,
      'hasItems' => (count($items) > 0),
      'radios' => FALSE,
      'onDeactivate' => $this->onDeactivate,
    ];
  }

  protected function getSlogitemsData() {
    return $this->getSlogtbHandler()->getTbMenuContentItems($this->tbMenuTid);
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $this->baseEntityId = $request->get('base_entity_id');
    $next_eid = $request->get('next_entity_id');
    $this->doTbMenuSelect = (!empty($next_eid) && $next_eid === '{next_entity_id}');
    $this->tbMenuTid = !$this->doTbMenuSelect ? (integer) $next_eid : 0;
    $this->default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
    if ($this->default_role->id() != $this->baseEntityId) {
      $message = t('Current default role missmatch.');
      throw new \LogicException($message);
    }

    if ($root_term = $this->roottermToSelect()) {
      $this->root_term = $root_term;
      $this->vocabulary = $root_term->getVocabulary();
      $this->hasChildren = $root_term->hasChildren();
      if ($this->hasChildren) {
        $request->attributes->set('user_role', $this->default_role);
        $request->attributes->set('root_term', $this->root_term);
        $request->attributes->set('selectedMenuTid', $this->getSelectedMenuTid());
        if ($this->tbMenuTid > 0) {
          $this->tbMenuTerm = SlogTx::getMenuTerm($this->tbMenuTid);
          $request->attributes->set('tb_menu_term', $this->tbMenuTerm);
        }
        if ($this->addNoneOption()) {
          $request->attributes->set('addNoneOption', TRUE);
        }
        return $this->getArgFormClass();
      }
      else {
        $message = t('There is no menu item.');
        throw new \LogicException($message);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    return t('Select content for %name', ['%name' => $name]);
  }

  protected function getSubmitLabel() {
    return $this->onDeactivate ? t('Deactivate') : t('Select');
  }

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    if ($this->doTbMenuSelect) {
      // nothing to do
      return;
    }

    $form['actions']['submit']['#value'] = t('Save');

    $this->onDeactivate = FALSE;
    $name = $this->root_term->getVocabulary()->getTbtabLabel();
    if ($this->tbMenuTid > 0) {
      $items = $this->getSlogitemsData();
      if (!empty($items)) {
        $msg = t('You are about to select content for the feature %name', ['%name' => $name]);
      } else {
        $msg = t('There is no content for the feature %name', ['%name' => $name]);
      }
    }
    else {
      $this->onDeactivate = TRUE;
      $msg = t('You are about to deactivate the feature %name', ['%name' => $name]);
    }
    $form['notice']['#markup'] = SlogXt::htmlMessage($msg, 'warning');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);
    // set own validator
    $op = \Drupal::request()->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    if ($this->doTbMenuSelect && $this->vocabulary->isSysToolbar()) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['attachCommand'] = 'attachSysTbmenuList';
    }
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    if ($this->doTbMenuSelect) {
      $slogxtData = &$form_state->get('slogxtData');
      return ['slogxtData' => $form_state->get('slogxtData')];
    }
    else {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['runCommand'] = 'wizardFormEdit';
      $slogxtData['alterForm'] = [
        'command' => 'sxt_slogitem::alterFormAddList',
        'args' => $this->jsAlterFormArgs(),
      ];
      return parent::buildContentResult($form, $form_state);
    }
  }

  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    } elseif (!$this->doTbMenuSelect) {
      $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();
    return ['command' => 'sxt_slogitem::reload'];
  }

}
