<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\main\PromoteDeactivateController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\main;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * Defines a controller ....
 */
class PromoteDeactivateController extends DeactivateControllerBase {

  protected function roottermToDeactivate(){
    return SlogXtsi::getPromotedRootTerm($this->default_role);
  }
  
}
