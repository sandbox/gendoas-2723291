<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent\NodeEditControllerBase.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\node\Controller\NodePreviewController;

/**
 * Defines a controller ....
 */
abstract class NodeEditControllerBase extends XtEditControllerBase {

  protected $isNodeEdit = false;
  protected $createNew = false;
  protected $opPreview = false;
  protected $opSave = false;

  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $slogxt = &$form_state->get('slogxt');
    if ($slogxt && $slogxt['ajax'] && $this->isNodeEdit) {
      // add callbacks
      $controllerClass = get_class($this);
      $form['#after_build'][] = "$controllerClass::formAfterBuild";
      $form['#process'][] = "$controllerClass::formProcess";
      $form['#validate'][] = "$controllerClass::formValidate";

      // preserve status
      $node = $form_state->getFormObject()->getEntity();
      $slogxt['saved_status'] = (integer) $node->status->value;
    
      // allow preview and submit as the only actions
      // and add callbacks
      $allowed = ['submit', 'preview'];
      $actions = &$form['actions'];
      foreach (Element::children($actions) as $key) {
        if (!in_array($key, $allowed)) {
          unset($actions[$key]);
        } elseif ($key === 'preview') {
          $actions['preview']['#submit'][] = "$controllerClass::preview";
        } elseif ($key === 'submit') {
          $actions['submit']['#submit'][] = "$controllerClass::save";
        }
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $request = \Drupal::request();
    $op = $request->get('op', false);
    if ($op) {
      $this->opPreview = ($op === (string) t('Preview'));
      $this->opSave = ($op === (string) t('Save'));
      if ($this->opPreview) {
        $request->attributes->set('nodeEditPreview', TRUE);
      }
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preRenderForm();
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);

    foreach (Element::children($form) as $field_name) {
      $field = & $form[$field_name];
      $field_type = $field['#type'] ?? FALSE;
      if (!empty($field['#group']) || $field_type === 'vertical_tabs' || $field_name === 'comment') {
        $field['#access'] = false;
      } elseif ($field_type === 'container' && isset($field['#access']) && $field['#access']) {
        $field['#attributes']['class'][] = 'slogxt-input-field';
      }

      if ($field_type === 'container' && isset($field['#access']) && $field['#access']) {
        $format = & $field['widget'][0]['format'];
        if ($format && $format['help']) {
          unset($format['help']);
        }
      }
    }

    // enable submit button
    //todo::incorrect::disables access mechanism
    //dev: not needed, live: no access without - not found why !!!
    $submit = & $form['actions']['submit'];
    is_array($submit) && ($submit['#access'] = TRUE);
   
    // Set error/broken if required button is missing
    $required = ['preview' => 'Preview', 'submit' => 'Submit'];
    foreach ($required as $key => $label) {
      $action = isset($form['actions'][$key]) ? $form['actions'][$key] : false;
      if (empty($action) || empty($action['#access']) || !$action['#access']) {
        if ($key === 'preview' && !empty($form['actions']['submit'])) {
          $form['actions']['submit']['#access'] = FALSE;
        }
        $this->formBroken = TRUE;
        drupal_set_message('Button not available/broken: ' . $label, 'error');
        break;
      }
    }

    $slogxtData = & $form_state->get('slogxtData');
    $hasErrors = $form_state->hasAnyErrors();
    if ($hasErrors) {
      if ($this->opPreview) {
        $this->formResult = 'edit';
      }
    } elseif ($this->opSave) {
      $slogxtData['wizardFinished'] = true;
      $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
    } else {
        $slogxtData['wizardFinalize'] = true;
    }
  }
  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::renderFormPlus();
   */
  protected function renderFormPlus(&$form, FormStateInterface $form_state) {
    if ($this->isNodeEdit) {
      // real form, assume not saved
      $slogxt = $form_state->get('slogxt');

      if (empty($slogxt['node_preview'])) {
        if ($this->submitted && !$this->hasErrors) {
          $this->formResult = 'saved';
          $this->htmlContent = '';
        } else {
          $this->formResult = 'edit';
          $this->htmlContent = $this->renderer->renderRoot($form);
        }
      } else {
        $this->formResult = 'preview';
        $this->htmlContent = $this->renderer->renderRoot($form);
        $html_preview = $this->renderer->renderRoot($slogxt['node_preview']);
        $this->htmlPreview = SlogXt::replaceFirstTag($html_preview, 'header');
      }
    } else {
      parent::renderFormPlus($form, $form_state);
    }
  }

  
  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    $label = t('Submit');
    if ($this->formResult === 'edit') {
      $label = t('Preview');
    } elseif ($this->formResult === 'preview') {
      $label = $this->createNew ? t('Create') : t('Update');
    }
    return $label;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getLabels();
   */
  protected function getLabels() {
    $labels = parent::getLabels();
    if ($this->formResult === 'edit') {
      $labels['submitNext'] = t('Next');
    }
    return $labels;
  }

  //
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // Static callbacks, 
  // use self::calledObject() as the current ajax form controller
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  
  /**
   * After form build handler, added by self::hookFormAlter()
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAfterBuild($form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();

    // ensure right triggering element
    if (isset($calledObject->opPreview) && $calledObject->opPreview) {
      $form_state->setTriggeringElement($form['actions']['preview']);
    } elseif (isset($calledObject->opSave) && $calledObject->opSave) {
      $form_state->setTriggeringElement($form['actions']['submit']);
    }

    return $form;
  }

  /**
   * Process handler for the form, added by self::hookFormAlter()
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @param type $complete_form equals $form
   * @return array $form
   */
  public static function formProcess(&$form, FormStateInterface $form_state, $complete_form) {
    return $form;
  }

  /**
   * Validation handler, added by self::hookFormAlter()
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $slogxt = $form_state->get('slogxt');
    $form_state->setValue('status', ['value' => $slogxt['saved_status']]);
  }

  /**
   * Submit handler: Preview
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * 
   * @see self::hookFormAlter()
   */
  public static function preview(&$form, FormStateInterface $form_state) {
    $slogxt = &$form_state->get('slogxt');
    $node = $form_state->getFormObject()->getEntity();
    $title = $node->label();
    $node_preview_controller = NodePreviewController::create(\Drupal::getContainer());
    $slogxt['node_preview'] = $node_preview_controller->view($node);
    $title = '<h2 class="slogxt-preview-title">' . $title . '</h2>';
    $slogxt['node_preview']['#prefix'] = '<div class="preview">' . $title;
    $slogxt['node_preview']['#suffix'] = '</div>';
  }

  /**
   * Submit handler: Save
   * 
   * Make the saved values available for the called object.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * 
   * @see self::hookFormAlter()
   */
  public static function save(array $form, FormStateInterface $form_state) {
    self::calledObject()->savedValues = $form_state->getValues();
  }

}
