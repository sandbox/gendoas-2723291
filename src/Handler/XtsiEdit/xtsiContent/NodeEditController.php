<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent\NodeEditController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent;

use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class NodeEditController extends NodeEditControllerBase {

  protected $node;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $this->isNodeEdit = true;
    $this->createNew = false;
    $this->node = Node::load(\Drupal::request()->get('base_entity_id'));
    $this->label_old = $this->node->label();
    return $this->getEntityFormObject('node.edit', $this->node);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    if ($this->opPreview) {
      $title = t('Preview');
    }
    else {
      $title = $this->t('Edit %title', ['%title' => $this->label_old]);
    }
    return $title;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $node = Node::load($this->node->id());
    $this->setSavedMessage($node->label());
    
    return [
      'command' => 'sxt_slogitem::finishedNodeEdit',
      'args' => [
        'etype' => 'node',
        'eid' => $node->id(),
        'labelOld' => $this->label_old,
        'labelNew' => $node->label(),
      ],
    ];
  }

}
