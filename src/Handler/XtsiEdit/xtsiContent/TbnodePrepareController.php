<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent\TbnodePrepareController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent;

use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtb\Handler\edit\xtMain\TbMenuNewController;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class TbnodePrepareController extends TbMenuNewController {

  protected $node_id;
  protected $node;
  protected $rootterm;
  protected $menuTerm;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $request = \Drupal::request();
    $node_id = $request->get('base_entity_id');
    $this->node = Node::load($node_id);
    if (!$this->node) {
      $args = ['@nid' => $node_id];
      $message = t("Node does not exist: @nid.", $args);
      throw new \LogicException($message);
    }

    if (SlogXtsi::nodeHasTbnode($node_id)) {
      $args = ['@nid' => $node_id];
      $message = t("Tbnode already prepared for node: @nid.", $args);
      throw new \LogicException($message);
    }

    $this->node_id = $node_id;
//    $this->root_term = $rootterm = SlogXtsi::getNodeSubmenuRootTerm($node_id, TRUE);
//    $this->root_term = $rootterm = SlogTx::getSysNodeRootTermSubmenu($node_id, $do_create = FALSE);
    $rootterm = SlogTx::getSysSysRootTermDummy();
//todo::current:: TxRootTermGetPlugin - new
    $this->menuTerm = SlogTx::entityStorage('slogtx_mt')->create([
      'vid' => $rootterm->getVocabularyId(),
      'parent' => $rootterm->id(),
    ]);
    return $this->getEntityFormObject('slogtx_mt.default', $this->menuTerm);
  }

  protected function getSubmitLabel() {
    return $this->t('Create');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $args = ['%node' => $this->node->getTitle()];
    return $this->t('Create toolbar for %node', $args);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $controllerClass = get_class($this);
    $form['#validate'][] = "$controllerClass::formValidate";
  }

  /**
   * Add submit handler.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    $submit_handlers = [[get_class(), 'formSubmit']];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * Move created menu term to the node dependent root term.
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $calledObject = self::calledObject();
//    if ($rootterm = $calledObject->prepareNodeSubmenuRootTerm()) {
//todo::current:: TxRootTermGetPlugin - new
    $rootterm = SlogTx::getSysNodeRootTermSubmenu($calledObject->node_id, $do_create = TRUE);
    if ($rootterm) {
      $formObject = $form_state->getFormObject();
      $formObject->submitForm($form, $form_state);

      $menu_term = $formObject->getEntity();
      $menu_term->setParentID($rootterm->id())
          ->setVocabularyId($rootterm->getVocabularyId())
          ->save();
    }
  }

//  public function prepareNodeSubmenuRootTerm() {
////    $this->root_term = SlogXtsi::prepareNodeSubmenuRootTerm($this->node_id);
/////todo::current:: TxRootTermGetPlugin - new
//    $this->root_term = SlogTx::getSysNodeRootTermSubmenu($this->node_id, $do_create = TRUE);
//    return $this->root_term;
//  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    // clear messages list
    // set finished message
    drupal_get_messages();
    $args = ['%node' => $this->node->getTitle()];
    drupal_set_message($this->t('Toolbar was created for %node.', $args));

    return [
      'command' => 'sxt_slogitem::finishedTbnodePrepared',
      'args' => ['nid' => $this->node_id],
    ];
  }

}
