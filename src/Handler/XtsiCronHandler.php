<?php

/**
 * @file
 * Definition of \Drupal\sxt_slogitem\Handler\XtsiSlogtbHandler.
 */

namespace Drupal\sxt_slogitem\Handler;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\XtsiCronStateData;
use Drupal\sxt_slogitem\XtsiNodeTypeData;

/**
 * //todo::docu
 */
class XtsiCronHandler {

  /**
   * Implements hook_cron().
   */
  public function hookCron() {
    $node_ids = [];
    try {
      $node_ids = XtsiCronStateData::shiftNodeIds(FALSE); // retrieve node ids only
      if (!empty($node_ids)) {
        foreach ($node_ids as $node_id) {
          XtsiNodeTypeData::setXtNodeValues($node_id);
        }
        // shift now again and write to the database
        XtsiCronStateData::shiftNodeIds(TRUE); 
      }
    }
    catch (\RuntimeException $e) {
        $msg = t('Cron run failed for movedNodeIds:  %nids', ['nids' => implode(';', $node_ids)]);
        SlogXt::logger('sxt_slogitem')->error("$msg</br>" . $e->getMessage());
    }
  }

}
