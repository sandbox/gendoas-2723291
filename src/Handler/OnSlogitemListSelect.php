<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\OnSlogitemListSelect.
 */

namespace Drupal\sxt_slogitem\Handler;

use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

/**
 * Defines a controller to load a view via AJAX.
 */
class OnSlogitemListSelect extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $slogitem_id = (integer) $request->get('slogitem_id');
    $term_id = (integer) $request->get('term_id');
    
    if ($slogitem_id > 0) {
      $slogitem = SlogItem::load($slogitem_id);
    } elseif ($slogitem_id < 0) {
      // extra slog item (virtual): 
//      switch ($slogitem_id) {
//        case -1:
//          // Full slogitem list for the term id
//          $slogitem = \Drupal\sxt_slogitem\SlogApp::getSlogitemListFull($term_id);      
//          break;
//
//        default:
//          break;
//      }
    }
    
    if ($slogitem) {
//todo::next:: - $slogitem->ajaxExecute() .......rename
      $result = $slogitem->ajaxExecute();
    } else {
      // Error: slogitem not found
      $result = [
          'header' => "Slogitem not found: $slogitem_id/$term_id",
          'content' => '',
          'settings' => ['nocache' => true],
      ];
    }

    $data = $result + [
        'sid' => $slogitem_id,
    ];
        
    return $data;
  }
  
}
