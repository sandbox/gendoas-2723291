<?php

/**
 * @file
 * Definition of \Drupal\sxt_slogitem\Handler\XtsiSlogtbHandler.
 */

namespace Drupal\sxt_slogitem\Handler;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtb\Handler\SlogtbHandlerBase;

class XtsiSlogtbHandler extends SlogtbHandlerBase {

  protected $slogitem_data = [];

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    return 'sxt_slogitem';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    if (SlogXt::isAdminRoute()) {
      return [];
    }
    return ['sxt_slogitem/sxt_slogitem.core'];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibrariesAutoload() {
    return [
      'sxt_slogitem/sxt_slogitem.core' => ['type' => 'loadLibrary'],
//        'sxt_slogitem/sxt_slogitem.test' => ['type' => 'loadLibrary'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTbMenuContentItems($menu_tid) {
    if (!isset($this->slogitem_data[$menu_tid])) {
      $data = SlogXtsi::getSlogitemAttachDataByTid($menu_tid);
      $items = [];
      foreach ($data as $key => $item) {
        $items[] = $item['content'];
      }
      $this->slogitem_data[$menu_tid] = $items;
    }
    return $this->slogitem_data[$menu_tid];
  }

  /**
   * {@inheritdoc}
   */
  public function getTbMenuContentHashes($menu_tid) {
    if (!isset($this->slogitem_hashes[$menu_tid])) {
      $this->slogitem_hashes[$menu_tid] = SlogXtsi::getSlogitemHashesByTid($menu_tid);
    }
    return $this->slogitem_hashes[$menu_tid];
  }

  /**
   * {@inheritdoc}
   */
  public function getJsDataResolvePathNode($key) {
    return [
      'key' => $key,
      'func' => 'sxt_slogitem::resolvePathNodeId',
      'provider' => 'sxt_slogitem',
    ];
  }

}
