<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\slogtb\OnTbMenuItemSelect.
 */

namespace Drupal\sxt_slogitem\Handler\slogtb;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;
use Drupal\user\RoleInterface;

class OnTbMenuItemSelect extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $tid = $request->get('tbmenu_tid');
    $menu_term = SlogTx::getMenuTerm($tid);
    $entity = $menu_term->setRequiredEntity();
    $listLabel = $menu_term->label();
    $vocabulary = $menu_term->getVocabulary();
    list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
    $pathLabel = $vocabulary->headerPathLabel();

    $entity_type_manager = \Drupal::entityTypeManager();
    $values = [
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
        'tid' => $tid,
    ];
    $build = $entity_type_manager
            ->getHandler('slogitem', 'sidebar_builder')
            ->setValues($values)
            ->build();
    $settings = $build['#xtsiSettings'];
    unset($build['#xtsiSettings']);
    
    if ($vocabulary->isNodeSubsysToolbar()) {
      $settings['basePermTb'] = XtsiNodeTypeData::getPermBaseTbIdByNode($entity);
    }
    
    $this->content = $build;
    $content = $this->getRenderedContent();
    $headerActions = SlogXt::pluginManager('action')->getActionsData('xtsi_list');

    if (SlogXtsi::isDebugMode()) {
      $listLabel = $tid . ':: ' . $listLabel;
    }
    $settings += [
        'listLabel' => $listLabel,
        'pathLabel' => $pathLabel,
        'headerActions' => $headerActions,
    ];

    $data = [
        'tid' => $tid,
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
        'provider' => 'sxt_slogitem',
        'content' => $content,
        'settings' => $settings,
    ];
    if ($toolbar === 'role' && $entity && $entity instanceof RoleInterface) {
      $data['pdRole'] = $entity->id();
    }

    return $data;
  }

}
