<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\slogtb\OnTbnodeMenuLoad.
 */

namespace Drupal\sxt_slogitem\Handler\slogtb;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

class OnTbnodeMenuLoad extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $entity_type = $request->get('entity_type');
    $node_id = $request->get('entity_id');
    $node = SlogTx::entityInstance($entity_type, $node_id);
    $rt_name = $request->get('rootterm');
    if ($entity_type !== 'node' || !$node) {
      $args = [
        '@type' => $entity_type,
        '@nid' => $node_id,
      ];
      throw new \LogicException(t('For valid node only: @type/@nid.', $args));
    }

//              $do_create = TRUE;
//              $options = [
//                'plugin_id' => SlogTx::SYS_VOCAB_ID_SUBMENU,
//                'entity' => $node,
//                'do_create' => $do_create,
//              ];
//              $rtget_plugin = SlogTx::pluginManager('rtget_sys_node')->getInstance($options);
//              $rootterm = $rtget_plugin->getRootTermByName($rt_name, $do_create);
    $rootterm =  SlogTx::getSysNodeRootTermSubmenu($node_id, TRUE);
///todo::current:: TxRootTermGetPlugin - new

    $block = SlogXtsi::getTbSubmenuHiddenBlock($entity_type);
//todo::current:: TxRootTermGetPlugin - $rtget_plugin
///todo::current:: TxRootTermGetPlugin - new
//todo::current:: TxRootTermGetPlugin - getTbSubmenuHiddenBlock

    if ($rootterm && $block && $node) {
      $vocabulary = $rootterm->getVocabulary();
      $toolbar = $vocabulary->getToolbar();
      $build = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block, 'block');
      $xt_render_cache = \Drupal::service('slogxt.render_cache');
      $xt_render_cache->setRequiredCacheContexts($build);
      $rootterm->addRootTermCaches();
      $xt_render_cache->addCacheableDependency($build, $rootterm);

      $data = $xt_render_cache->get($build);
      if (!$data || empty($data['tbnodeHash'])) {
        $cid_read = $xt_render_cache->getCacheID($build);
        $html = (string) \Drupal::service('renderer')->renderRoot($build);

        $toolbar_id = $toolbar->id();
        $vid = $vocabulary->id();
        $toobarData = SlogTx::getJsToolbarData($toolbar_id);
        $data = [
          'toolbar' => $toolbar_id,
          'toolbartab' => SlogTx::getToolbartabFromVid($vid),
          'rootterm' => $rt_name,
          'content' => $html,
          'tbnodeHash' => SlogXtsi::getTbnodeHash($node, FALSE), // FALSE: ensure building
          'settings' => ['toolbarData' => $toobarData],
        ];

        $cid_write = $xt_render_cache->getCacheID($build);
        if ($cid_read !== $cid_write) {
          $args = [
            '%bin' => isset($build['#cache']['bin']) ? $build['#cache']['bin'] : 'render',
            '%cid_read' => $cid_read,
            '%cid_write' => $cid_write,
          ];
          $msg = $this->t('Cache read/write missmatch: %bin / %cid_read / %cid_write.', $args);
          SlogXt::logger('sxt_slogitem')->error($msg);
        }
        $xt_render_cache->set($build, $data);
      }
    }

    if (empty($data)) {
      $data = [
        'toolbar' => '',
        'toolbartab' => '',
        'rootterm' => '',
        'content' => '',
        'settings' => [],
      ];

      $args = [
        '%entity_type' => $entity_type,
        '%entity_id' => $node_id,
      ];
      $msg = $this->t('TbSubmenu content not found: %entity_type.%entity_id.', $args);
      SlogXt::logger('sxt_slogitem')->error($msg);
    }

    return $data;
  }

}
