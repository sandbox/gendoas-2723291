<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Redirect\System4xxController.
 */

namespace Drupal\sxt_slogitem\Handler\Redirect;

use Drupal\slogxt\Controller\AjaxDefaultControllerBase;
use Drupal\slogxt\SlogXt4xx;

/**
 * Defines a controller ...
 */
class System4xxController extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    return SlogXt4xx::responseData(\Drupal::request()->get('errcode'));
  }

}
