<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Redirect\XtsiUserController.
 */

namespace Drupal\sxt_slogitem\Handler\Redirect;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Render\Element;

/**
 * Defines a controller ...
 */
class XtsiUserController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    $param4 = \Drupal::request()->get('param4');
    $this->isResetLogin = (!empty($param4) && $param4 === 'login');
    if ($this->isResetLogin) {
      return 'Drupal\sxt_slogitem\Form\User\XtsiUserResetLoginForm';
    }
    else {
      return 'Drupal\sxt_slogitem\Form\User\XtsiUserResetForm';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getLabels() {
    if ($this->isResetLogin) {
      $labels = [
        'dialogTitle' => $this->form_title,
        'submitLabel' => t('Save'),
      ];
    }
    else {
      $labels = [
        'dialogTitle' => $this->form_title,
        'submitLabel' => t('Log in'),
      ];
    }
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $this->form_title = (string) ($form['#title'] ?: '');
    if ($this->isResetLogin) {
      $form['#title'] = t('Change Password');
      $allowed = ['form_build_id', 'form_id', 'account'];
      foreach (Element::children($form) as $key) {
        if (!in_array($key, $allowed)) {
          $form[$key]['#access'] = FALSE;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxt = & $form_state->get('slogxt');
    if (!empty($slogxt['urgentMsgData'])) {
      $build_content_result = parent::getUrgentMessageData($form_state);
    }
    elseif ($this->isResetLogin) {
      $slogxtData = &$form_state->get('slogxtData');
      $slogxtData['runCommand'] = 'defaultForm';
      if ($form_state->isSubmitted() && !$form_state->hasAnyErrors()) {
        $user = $form_state->getFormObject()->getEntity();
        $timestamp = $slogxt['timestamp'];
        user_login_finalize($user);
        $args = ['%name' => $user->getDisplayName(), '%timestamp' => $timestamp];
        SlogXt::logger('sxt_slogitem')->notice('User %name used one-time login link at time %timestamp.', $args);
        drupal_set_message(t('Reloading is required.'));
        $slogxtData['doReload'] = TRUE;
      }
      $build_content_result = parent::buildContentResult($form, $form_state);
    } else {
      $build_content_result = parent::buildContentResult($form, $form_state);
    }

    return $build_content_result;
  }

}
