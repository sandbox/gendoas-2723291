<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\Printable\PrintableBase.
 */

namespace Drupal\sxt_slogitem\Handler\Printable;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Controller\AjaxDefaultControllerBase;

/**
 * Defines a controller ....
 */
class PrintableBase extends AjaxDefaultControllerBase {

  /**
   * {@inheritdoc}
   */
  protected function responseData() {
    $request = \Drupal::request();
    $entity_type = $request->get('entity_type');
    $entity_id = $request->get('entity_id');

    $path = urldecode($request->getPathInfo());
    $msg = t('Should be redirected: @path', ['@path' => $path]);
    SlogXt::logger('sxt_slogitem')->error($msg);  // see SlogitemEventSubscriber::onKernelRequest()

    return [
      'error' => 'Should be redirected!',
      'path' => $path,
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ];
  }

}
