<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiProps\xtsiContent\ActionsCoPropsController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiProps\xtsiContent;

use Drupal\slogxt\Controller\XtEditControllerBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a controller ....
 */
class ActionsCoPropsController extends XtEditControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return 'Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent\ActionsCoPropsSelectForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return $this->t('Select props action');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    return ['slogxtData' => $form_state->get('slogxtData')];
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::appendAttachments();
   */
  protected function appendAttachments() {
    return TRUE;
  }

}
