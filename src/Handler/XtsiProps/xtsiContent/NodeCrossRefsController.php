<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Handler\XtsiProps\xtsiContent\NodeCrossRefsController.
 */

namespace Drupal\sxt_slogitem\Handler\XtsiProps\xtsiContent;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\AjaxFormControllerBase;

/**
 * Defines a controller ....
 */
class NodeCrossRefsController extends AjaxFormControllerBase {

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return '\Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent\NodeCrossRefsForm';
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    return t('Cross references');
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::hasLabels();
   */
  protected function hasLabels() {
    return TRUE;
  }

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getSubmitLabel();
   */
  protected function getSubmitLabel() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function postBuildForm(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['headerStyle'] = TRUE;
    $slogxtData['icons'] = TRUE;
    $slogxtData['runCommand'] = 'sxt_slogitem::tbMenuCheckboxes';
    $slogxtData['attachCommand'] = 'sxt_slogitem::attachTbmenuCheckboxes';
    $slogxtData['isWizardActionPage'] = TRUE;
    $slogxtData['pageActions'] = SlogXt::pluginManager('action')->getActionsData('xt_dialog_coprops');
  }

}
