<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\Block\XtContentDefaultBlock.
 */

namespace Drupal\sxt_slogitem\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for default content target.
 *
 * @Block(
 *   id = "sxt_slogitem_content_default",
 *   admin_label = @Translation("SlogXt content default"),
 *   category = "SlogXt",
 * )
 */
class XtContentDefaultBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return ['#markup' => 'SlogitemDefaultContentTarget'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['label_display' => FALSE] + parent::defaultConfiguration();
  }

}
