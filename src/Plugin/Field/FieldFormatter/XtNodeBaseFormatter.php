<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\Field\FieldFormatter\XtNodeBaseFormatter.
 */

namespace Drupal\sxt_slogitem\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'sxt_slogitem' formatter.
 *
 * @FieldFormatter(
 *   id = "xtnode_base_formatter",
 *   label = @Translation("XtNode Base"),
 *   description = @Translation(".............XtNode Base Formatter"),
 *   field_types = {
 *     "xtnode_base_fieldtype"
 *   }
 * )
 */
class XtNodeBaseFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $wrapper = [
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    ];
    foreach ($items as $delta => $item) {
      $xx = 0;
      $xtnode_values = [
        'sxtbase_sid' => $item->base_sid,
        'sxtbase_tb' => $item->base_tb,
        'sxtrank_sids' => $item->rank_sids,
      ];

      $elements[$delta] = [
        'xtnodebase' => [
          '#markup' => implode('::', $xtnode_values),
        ] + $wrapper,
      ];
    }

    return $elements;
  }

}
