<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\Field\FieldType\XtNodeBaseFieldType.
 */

namespace Drupal\sxt_slogitem\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemBase;
/**
 * Defines ...
 *
 * @FieldType(
 *   id = "xtnode_base_fieldtype",
 *   label = @Translation("XtNode Base"),
 *   description = @Translation(".................XtNode Base Field Type."),
 *   default_formatter = "xtnode_base_formatter",
 *   default_widget = "xtnode_base_widget"
 * )
 */
class XtNodeBaseFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'base_sid';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['base_sid'] = DataDefinition::create('integer')
        ->setLabel(t('Slogitem ID'))
        ->setSetting('unsigned', TRUE)
        ->setInternal(TRUE)
        ->setRequired(TRUE);

    $properties['base_tb'] = DataDefinition::create('string')
        ->setLabel(t('Toolbar ID'))
        ->setInternal(TRUE)
        ->setRequired(FALSE);

    $properties['rank_sids'] = DataDefinition::create('string')
        ->setLabel(t('Ranked Slogitem IDs'))
        ->setInternal(TRUE)
        ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'base_sid' => [
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'base_tb' => [
          'type' => 'varchar',
          'length' => 64,
        ],
        'rank_sids' => [
          'type' => 'text',
        ],
      ],
      'indexes' => [
        'base_tb' => ['base_tb'],
      ],
    ];
  }

}
