<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\Field\FieldType\XtNodeBaseWidget.
 */

namespace Drupal\sxt_slogitem\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;

/**
 * Plugin implementation of ...
 *
 * @FieldWidget(
 *   id = "xtnode_base_widget",
 *   label = @Translation("XtNode Base"),
 *   description = @Translation(".............XtNode Base Widget"),
 *   field_types = {
 *     "xtnode_base_fieldtype"
 *   },
 *   multiple_values = TRUE
 * )
 */
class XtNodeBaseWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['base_sid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Slogitem ID'),
      '#description' => t('Many slogitems point to the node, but only one of them is the base one.'),
      '#default_value' => 0,
      '#disabled' => TRUE,
      '#size' => 10,
    ];


    $element['base_tb'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base Toolbar ID'),
      '#description' => t('The toolbar, the base slogitem resides in. This is used for permissions by toolbar.'),
      '#default_value' => '',
      '#disabled' => TRUE,
      '#size' => 20,
    ];

    $element['rank_sids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ranked Slogitem IDs'),
      '#description' => t('All slogitems pointing to the node, except the base one.'),
      '#default_value' => '',
      '#disabled' => TRUE,
    ];

    return $element;
  }

}
