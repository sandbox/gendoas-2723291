<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys\TxBmStorageContent.
 */

namespace Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "bmstorage_content",
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxBmStorageContent extends TxSysSysRootTermGet {
  

}
