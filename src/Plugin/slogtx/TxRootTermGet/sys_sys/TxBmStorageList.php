<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys\TxBmStorageList.
 */

namespace Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "bmstorage_list",
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxBmStorageList extends TxSysSysRootTermGet {
  

}
