<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys\TxSurprise.
 */

namespace Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "surprise",
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxSurprise extends TxSysSysRootTermGet {
  

}
