<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys\TxPromoted.
 */

namespace Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_sys;

use Drupal\slogtx\Plugin\slogtx\TxSysSysRootTermGet;

/**
 * @TxSysSysRootTermGet(
 *   id = "promoted",
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxPromoted extends TxSysSysRootTermGet {
  

}
