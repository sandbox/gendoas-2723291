<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_node\TxSubmenu.
 */

namespace Drupal\sxt_slogitem\Plugin\slogtx\TxRootTermGet\sys_node;

use Drupal\slogtx\Plugin\slogtx\TxSysNodeRootTermGet;

/**
 * @TxSysNodeRootTermGet(
 *   id = "submenu",
 *   vocabulary_description = @Translation("Submenu for node entities."),
 *   icon_id = "tbnode",
 * )
 * 
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 */
class TxSubmenu extends TxSysNodeRootTermGet {
  

}
