<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\Derivative\NodeContent.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use \Drupal\node\Entity\NodeType;

/**
 * Provides NodeContent plugin definitions for all node types.
 */
class NodeContent extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $node_types = NodeType::loadMultiple();

    foreach ($node_types as $type_id => $node_type) {
      $label = $node_type->label();
      $this->derivatives[$type_id] = [
          'bundle' => $type_id,
          'admin_label' => "Xtsi Node Type: " . $node_type->label(),
      ];
      $this->derivatives[$type_id] += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
