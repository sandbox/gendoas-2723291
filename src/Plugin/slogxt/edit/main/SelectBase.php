<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\SelectBase.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\slogxt\Plugin\XtPluginEditBase;
use Drupal\slogxt\XtUserData;

/**
 * 
 */
class SelectBase extends XtPluginEditBase {
  
  protected $default_role;
  protected $for_activate = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->default_role = $this->getUserDefaultRole();
    $this->baseEntityId = $this->default_role->id();
  }
  
  /**
   * {@inheritdoc}
   */
  protected function preparedPath() {
    $path = str_replace('{base_entity_id}', $this->baseEntityId, parent::preparedPath());
    return $path;
  }

  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    return (!empty($this->rootterm) //
        && $this->rootterm->hasChildren() //
        && $this->rootterm->isVisible());
  }

}
