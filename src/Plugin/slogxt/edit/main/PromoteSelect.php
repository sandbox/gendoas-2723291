<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\PromoteSelect.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_promote_select",
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted select"),
 *   description = @Translation("Select the promoted content."),
 *   route_name = "sxt_slogitem.edit.main.promoted.select",
 *   skipable = false,
 *   weight = 1000
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class PromoteSelect extends SelectBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    $this->rootterm = SlogXtsi::getPromotedRootTerm($this->default_role);
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-promoted')) {
      return TRUE;
    }
    return FALSE;
  }

}
