<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\SurprisePrepare.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_surprise_prepare",
 *   bundle = "mainprosur",
 *   title = @Translation("Prepare surprise"),
 *   description = @Translation("Prepare surprise feature for the current role."),
 *   route_name = "sxt_slogitem.edit.main.surprise.prepare",
 *   skipable = false,
 *   weight = 1010
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SurprisePrepare extends PrepareBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->rootterm = SlogXtsi::getSurpriseRootTerm($this->default_role);
    $this->for_activate = !empty($this->rootterm) ? $this->rootterm->hasChildren() : FALSE;
    if ($this->for_activate) {
      $this->configuration['title'] = t('Activate surprise');
      $this->configuration['description'] = t('Activate surprise feature for the current role.');
    }
  }

  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-surprise')) {
      return TRUE;
    }
    return FALSE;
  }

}
