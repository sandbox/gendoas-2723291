<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\ActionsPromoteSurprise.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_promote_surprise",
 *   bundle = "main",
 *   title = @Translation("Promoted/Surprise"),
 *   description = @Translation("Select edit action for 'Promoted' or 'Surprise'."),
 *   route_name = "sxt_slogitem.edit.main.promoted_surprise.select",
 *   skipable = false,
 *   weight = 1001
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class ActionsPromoteSurprise extends NewContentBase {

  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
//    $user_role = $this->default_role;
    foreach (['promoted', 'surprise'] as $item) {
      if (SxtGroup::hasPermission("admin xtsi-$item")) {
        return TRUE;
//                $rootterm = ($item === 'promoted') //
//                    ? SlogXtsi::getPromotedRootTerm($this->default_role) //
//                    : SlogXtsi::getSurpriseRootTerm($this->default_role);
//                return ($rootterm && $rootterm->isRootTerm());
      }
    }
    return FALSE;
  }

}
