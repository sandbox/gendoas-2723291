<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\SurpriseSelect.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_surprise_select",
 *   bundle = "mainprosur",
 *   title = @Translation("Surprise select"),
 *   description = @Translation("Select the surprise contents."),
 *   route_name = "sxt_slogitem.edit.main.surprise.select",
 *   skipable = false,
 *   weight = 1010
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SurpriseSelect extends SelectBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    $this->rootterm = SlogXtsi::getSurpriseRootTerm($this->default_role);
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-surprise')) {
      return TRUE;
    }
    return FALSE;
  }

}
