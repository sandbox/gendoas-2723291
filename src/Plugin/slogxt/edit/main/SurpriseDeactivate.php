<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\SurpriseDeactivate.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_surprise_deactivate",
 *   bundle = "mainprosur",
 *   title = @Translation("Surprise deactivate"),
 *   description = @Translation("Deactivate surprise feature for the current role."),
 *   route_name = "sxt_slogitem.edit.main.surprise.deactivate",
 *   skipable = false,
 *   weight = 1012
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SurpriseDeactivate extends SelectBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
//todo::current:: new::promote/surprise - deactivate::disabled
//              $this->rootterm = SlogXtsi::getSurpriseRootTerm($this->default_role);
//              if (parent::access() && SxtGroup::hasPermission('admin xtsi-surprise')) {
//                return TRUE;
//              }
    return FALSE;
  }

}
