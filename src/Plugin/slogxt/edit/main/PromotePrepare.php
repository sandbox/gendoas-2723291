<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\PromotePrepare.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_promote_prepare",
 *   bundle = "mainprosur",
 *   title = @Translation("Prepare promoted"),
 *   description = @Translation("Prepare promoted feature for the current role."),
 *   route_name = "sxt_slogitem.edit.main.promoted.prepare",
 *   skipable = false,
 *   weight = 1000
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class PromotePrepare extends PrepareBase {
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->rootterm = SlogXtsi::getPromotedRootTerm($this->default_role);
    $this->for_activate = !empty($this->rootterm) ? $this->rootterm->hasChildren() : FALSE;
    if ($this->for_activate) {
      $this->configuration['title'] = t('Activate promoted');
      $this->configuration['description'] = t('Activate promoted feature for the current role.');
    }
  }
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-promoted')) {
      return TRUE;
    }
    return FALSE;
  }

}
