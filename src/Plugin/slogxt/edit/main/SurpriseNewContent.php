<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\SurpriseNewContent.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_surprise_newcontent",
 *   bundle = "mainprosur",
 *   title = @Translation("Surprise new content"),
 *   description = @Translation("Select contents and add them to a surprise list."),
 *   description = @Translation("Create links to content and position them beside a selected item."),
 *   route_name = "sxt_slogitem.edit.main.surprise.newcontent",
 *   skipable = false,
 *   weight = 1011
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SurpriseNewContent extends NewContentBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    $this->rootterm = SlogXtsi::getSurpriseRootTerm($this->default_role);
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-surprise')) {
      return TRUE;
    }
    return FALSE;
  }

}
