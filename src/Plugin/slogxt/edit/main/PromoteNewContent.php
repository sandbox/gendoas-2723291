<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\PromoteNewContent.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_promote_newcontent",
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted new content"),
 *   description = @Translation("Select contents and add them to a promoted list."),
 *   route_name = "sxt_slogitem.edit.main.promoted.newcontent",
 *   skipable = false,
 *   weight = 1001
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class PromoteNewContent extends NewContentBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    $this->rootterm = SlogXtsi::getPromotedRootTerm($this->default_role);
    if (parent::access() && SxtGroup::hasPermission('admin xtsi-promoted')) {
      return TRUE;
    }
    return FALSE;
  }

}
