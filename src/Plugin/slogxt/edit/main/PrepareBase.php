<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\PrepareBase.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\slogxt\Plugin\XtPluginEditBase;
use Drupal\slogxt\XtUserData;

/**
 * 
 */
class PrepareBase extends XtPluginEditBase {
  
  protected $default_role;
  protected $for_activate = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->default_role = $this->getUserDefaultRole();
    $this->baseEntityId = $this->default_role->id();
  }
  
  protected function getResolveData() {
    $data = [];
    if (!$this->for_activate) {
//todo::current:: new::promote - js::tmp::disabled
//      $data['{link_entity_id}'] = 'sxt_slogitem::resolvePathSlogitemId';
    }
    
    return $data;
  }
  
  protected function getResolveArgs() {
    return [
      '{link_entity_id}' => [
        'xtTitle' => t('Items to link'),
        'xtInfo' => t('Select the items you want to link.'),
        'multiple' => TRUE,
//todo::next::   allowed.allowAll
        'allowAll' => TRUE,
        'isSource' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function preparedPath() {
    $path = str_replace('{base_entity_id}', $this->baseEntityId, parent::preparedPath());
    if ($this->for_activate) {
      $path = str_replace('{link_entity_id}', 0, $path);
    }
    return $path;
  }

  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    return (empty($this->rootterm) || !$this->for_activate || !$this->rootterm->isVisible());
  }

}
