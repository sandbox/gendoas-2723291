<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\NewContentBase.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\XtUserData;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * 
 */
class NewContentBase extends XtPluginEditBase {

  protected $default_role;
  protected $target_menu_tid;
  protected $has_menu_tid = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->default_role = $this->getUserDefaultRole();
    $this->baseEntityId = $this->default_role->id();
    $this->target_menu_tid = \Drupal::request()->get('next_entity_id', FALSE);
    $this->has_menu_tid = !empty($this->target_menu_tid);
  }

  protected function getResolveData() {
    return $this->has_menu_tid //
        ? ['{link_entity_id}' => 'sxt_slogitem::resolvePathSlogitemId'] : [];
  }

  protected function getResolveArgs() {
    return $this->has_menu_tid ? [
      '{link_entity_id}' => [
        'xtTitle' => t('Content to add'),
        'xtInfo' => t('Select the contents you want to add.'),
        'multiple' => TRUE,
//todo::next::   hasNids: NewContentBase.php/xtsi.dvc.commands.js
//todo::next::   allowed.allowAll
        'allowAll' => TRUE,
        'isSource' => TRUE,
//        'hasNids' => SlogXtsi::getNidsByTid($this->target_menu_tid, TRUE),
      ]] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function preparedPath() {
    $path = str_replace('{base_entity_id}', $this->baseEntityId, parent::preparedPath());
    if ($this->has_menu_tid) {
      $path = str_replace('{next_entity_id}', $this->target_menu_tid, $path);
    }
    return $path;
  }

  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
    return (!empty($this->rootterm) && $this->rootterm->hasChildren() && $this->rootterm->isVisible());
  }

}
