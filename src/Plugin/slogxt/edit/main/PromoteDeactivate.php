<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\main\PromoteDeactivate.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\main;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_group\SxtGroup;

/**
 * @SlogxtEdit(
 *   id = "xtsi_promote_deactivate",
 *   bundle = "mainprosur",
 *   title = @Translation("Promoted deactivate"),
 *   description = @Translation("Deactivate promoted feature for the current role."),
 *   route_name = "sxt_slogitem.edit.main.promoted.deactivate",
 *   skipable = false,
 *   weight = 1002
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class PromoteDeactivate extends SelectBase {
  
  /**
   * Overrides \Drupal\sxt_slogitem\Plugin\sxt_slogitem\edit\XtPluginEditBase::access();
   */
  protected function access() {
//todo::current:: new::promote/surprise - deactivate::disabled
//                $this->rootterm = SlogXtsi::getPromotedRootTerm($this->default_role);
//                if (parent::access() && SxtGroup::hasPermission('admin xtsi-promoted')) {
//                  return TRUE;
//                }
    return FALSE;
  }

}
