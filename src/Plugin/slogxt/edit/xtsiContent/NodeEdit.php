<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiContent\NodeEdit.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiContent;

use Drupal\node\Entity\Node;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "xtsi_edit_node_edit",
 *   bundle = "xtsi_coedit",
 *   title = @Translation("Edit content"),
 *   description = @Translation("Edit the current content item."),
 *   route_name = "sxt_slogitem.edit.content.node.edit",
 *   skipable = true,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class NodeEdit extends XtPluginEditBase {

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $node_id = \Drupal::request()->get('base_entity_id');
      if ($node = Node::load($node_id)) {
        return $node->access('update');
      }
    }

    return FALSE;
  }

}
