<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiContent\TbnodePrepare.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiContent;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\node\Entity\Node;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "xtsi_tbnode_prepare",
 *   bundle = "xtsi_coedit",
 *   title = @Translation("Prepare node submenu"),
 *   description = @Translation("Prepare the submenu for the current content."),
 *   route_name = "sxt_slogitem.edit.content.tbnode.prepare",
 *   skipable = false,
 *   weight = 1
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class TbnodePrepare extends XtPluginEditBase {

  /**
   * Overrides \Drupal\slogxt\Plugin\XtPluginEditBase::access();
   */
  protected function access() {
    if (\Drupal::currentUser()->isAuthenticated()) {
      $node_id = $this->baseEntityId;
      $hasTbnode = SlogXtsi::nodeHasTbnode($node_id);
      if (!$hasTbnode && $node = Node::load($node_id)) {
        return (!XtsiNodeTypeData::nodeTbPreventByDepth($node) && $node->access('update'));
      }
    }

    return FALSE;
  }

}
