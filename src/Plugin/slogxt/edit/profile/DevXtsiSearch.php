<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\profile\DevXtsiSearch.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\profile;

use Drupal\slogxt\Plugin\XtDevToolsPluginBase;

/**
 * @SlogxtEdit(
 *   id = "xtsi_devtools_search",
 *   bundle = "profiledevtools",
 *   title = @Translation("Xtsi Search"),
 *   description = @Translation("Search for node, slogitem, menu term, ..."),
 *   route_name = "sxt_slogitem.profile.devtools_search",
 *   skipable = false,
 *   weight = 100
 * )
*/
class DevXtsiSearch extends XtDevToolsPluginBase {

}
