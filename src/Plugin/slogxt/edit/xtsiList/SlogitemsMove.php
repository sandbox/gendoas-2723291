<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\SlogitemsMove.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

/**
 * @SlogxtEdit(
 *   id = "xtsi_slogitem_move",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("Move contents"),
 *   description = @Translation("Move contents within the toolbar or toolbar's special storage."),
 *   route_name = "sxt_slogitem.edit.list.move",
 *   resolve_next_command = "sxt_slogitem::resolvePathSlogitemId",
 *   resolve_link_command = "sxt_slogitem::resolvePathSlogitemId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "move sxtrole-content"
 *   },
 *   skipable = false,
 *   weight = 12
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SlogitemsMove extends SlogitemEditBase {

  protected function setNextByTid() {
    return FALSE;
  }

  protected function getResolveData() {
    $data = [];
    if (!$this->baseEntityId) {
      $data['{base_entity_id}'] = $this->getResolveBaseCommand();
    }
    if ($resolveNext = $this->getResolveNextCommand()) {
      $data['{next_entity_id}'] = $resolveNext;
    }
    if ($resolveLink = $this->getResolveLinkCommand()) {
      $data['{link_entity_id}'] = $resolveLink;
    }

    return $data;
  }

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{next_entity_id}' => [
        'xtTitle' => t('Where to move'),
        'xtInfo' => t('Select where to move contents.'),
        'isTarget' => TRUE,
      ],
      '{link_entity_id}' => [
        'xtTitle' => t('Select contents to move'),
        'xtInfo' => t('Select the contents to be moved.'),
        'multiple' => TRUE,
        'isSource' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return $this->hasPermission();
  }

}
