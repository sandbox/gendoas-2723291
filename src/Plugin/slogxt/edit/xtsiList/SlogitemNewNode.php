<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\SlogitemNewNode.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

/**
 * @SlogxtEdit(
 *   id = "xtsi_slogitem_newnode",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("New content"),
 *   description = @Translation("Create new content and position it beside a selected item."),
 *   route_name = "sxt_slogitem.edit.list.newnode",
 *   resolve_next_command = "sxt_slogitem::resolvePathSlogitemId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "newnode sxtrole-content"
 *   },
 *   skipable = false,
 *   weight = 10
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SlogitemNewNode extends SlogitemEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{next_entity_id}' => [
        'xtTitle' => t('Where to position'),
        'xtInfo' => t('Select the item where to position the new item.'),
        'isTarget' => TRUE,
      ],
    ];
  }

//  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
//    return $root_term->hasChildren();
//  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return $this->hasPermission();
  }

}
