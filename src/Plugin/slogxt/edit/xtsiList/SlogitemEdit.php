<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\SlogitemEdit.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

/**
 * @SlogxtEdit(
 *   id = "xtsi_slogitem_edit",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("Edit"),
 *   description = @Translation("Select and edit a list item."),
 *   route_name = "sxt_slogitem.edit.list.item.edit",
 *   resolve_next_command = "sxt_slogitem::resolvePathSlogitemId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "edit sxtrole-content"
 *   },
 *   skipable = false,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SlogitemEdit extends SlogitemEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{next_entity_id}' => [
        'xtInfo' => t('Select the list item you want to edit.'),
        'isTarget' => TRUE,
      ],
    ];
  }

//  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
//    return $root_term->hasChildren();
//  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasItems() && $this->hasPermission());
  }

}
