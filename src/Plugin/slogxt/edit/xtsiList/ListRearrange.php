<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\ListRearrange.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

/**
 * @SlogxtEdit(
 *   id = "xtsi_list_rearrange",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("Rearrange"),
 *   description = @Translation("Rearrange this item list."),
 *   route_name = "sxt_slogitem.edit.list.rearrange",
 *   resolve_next_command = "sxt_slogitem::resolvePathSlogitemId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "rearrange sxtrole-content"
 *   },
 *   skipable = false,
 *   weight = 20
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class ListRearrange extends SlogitemEditBase {
  
  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{next_entity_id}' => [
        'isTarget' => TRUE,
      ],
    ];
  }

//  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
//    return $root_term->hasChildren();
//  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasItems(TRUE) && $this->hasPermission());
  }

}
