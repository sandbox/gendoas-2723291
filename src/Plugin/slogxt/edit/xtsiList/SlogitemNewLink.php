<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\SlogitemNewLink.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

/**
 * @SlogxtEdit(
 *   id = "xtsi_slogitem_newlink",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("New links"),
 *   description = @Translation("Create links to content and position them beside a selected item."),
 *   route_name = "sxt_slogitem.edit.list.newlink",
 *   resolve_next_command = "sxt_slogitem::resolvePathSlogitemId",
 *   resolve_link_command = "sxt_slogitem::resolvePathSlogitemId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "newlinks sxtrole-content"
 *   },
 *   skipable = false,
 *   weight = 11
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class SlogitemNewLink extends SlogitemEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{next_entity_id}' => [
        'xtTitle' => t('Where to position'),
        'xtInfo' => t('Select the item where to position the new links.'),
        'isTarget' => TRUE,
      ],
      '{link_entity_id}' => [
        'xtTitle' => t('Items to link'),
        'xtInfo' => t('Select the items you want to link.'),
        'multiple' => TRUE,
        'isSource' => TRUE,
        'allowByTbRank' => TRUE,
      ],
    ];
  }

//  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
//    return $root_term->hasChildren();
//  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return $this->hasPermission();
  }

}
