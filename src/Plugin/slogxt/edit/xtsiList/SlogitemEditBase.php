<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList\SlogitemEditBase.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\edit\xtsiList;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\Plugin\XtPluginEditBase;

abstract class SlogitemEditBase extends XtPluginEditBase {

  protected static $initialized = FALSE;
  protected static $tid = FALSE;
  protected static $menu_term = FALSE;
  protected static $vocabulary = FALSE;
  protected static $toolbar_id = FALSE;
  protected static $has_items = FALSE;
  protected static $has_multiple = FALSE;

//  abstract public function hasActionItemsForSysTab(TxTermInterface $root_term);

  /**
   * Whether action is executable.
   * 
   * This is called by $this->access().
   * 
   * @return boolean
   */
  abstract protected function isActionExecutable();

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $route_provider) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);
    $this->account = \Drupal::currentUser();
    $this->default_role = $this->getUserDefaultRole();
    if (!self::$initialized) {
      self::$initialized = TRUE;
      self::$tid = $this->baseEntityId;
      self::$menu_term = SlogTx::getMenuTerm(self::$tid);
      if (empty(self::$menu_term) || !self::$menu_term->isMenuTerm()) {
        $msg = t('Not a valid menu term id: @termid', ['@termid' => self::$tid]);
        throw new \RuntimeException($msg);
      }
      self::$vocabulary = self::$menu_term->getVocabulary();
      self::$toolbar_id = self::$vocabulary->getToolbarId();
      self::$has_items = SlogXtsi::hasSlogitems(self::$tid, self::$has_multiple);
    }
  }

  protected function setNextByTid() {
    return TRUE;
  }

  protected function preparedPath() {
    $path = parent::preparedPath();
    if (!self::$has_multiple && $this->setNextByTid()) {
      $sids = SlogXtsi::getSidsFromTids([self::$tid]);
      if (empty($sids)) {
        $path = str_replace('{next_entity_id}', '0', $path);
      }
      else {
        $path = str_replace('{next_entity_id}', $sids[0], $path);
      }
    }
    return $path;
  }

  protected function getResolveNextCommand() {
    if (self::$tid && self::$has_multiple || !$this->setNextByTid()) {
      return parent::getResolveNextCommand();
    }
    return FALSE;
  }

  protected function hasItems($multiple = FALSE) {
    return $multiple ? self::$has_multiple : self::$has_items;
  }

  /**
   * 
   * @return boolean
   */
  protected function hasPermission() {
    $toolbar_id = self::$toolbar_id;
    $toolbar = SlogTx::getToolbar($toolbar_id);
    $vocabulary = self::$vocabulary;
    if ($vocabulary->isNodeSubsysToolbar()) {
      $te_plugin = $vocabulary->getTargetEntityPlugin();
      if (!$node = $te_plugin->getCurrentEntity()) {
        $target_term = self::$menu_term->getTargetTerm();
        $node = $te_plugin->getEntityFromTargetTermName($target_term->label());
        $te_plugin->setEntity($node);
      }
      
      if ($node) {
        $toolbar_id = XtsiNodeTypeData::getPermBaseTbIdByNode($node);
        $toolbar = SlogTx::getToolbar($toolbar_id);
      }
    }
    
    if ($toolbar && !$toolbar->isUnderscoreToolbar()) {
      $toolbar_id = $toolbar->id();
      if ($toolbar_id === 'user') {
        return TRUE;
      }
      else if ($toolbar_id === 'role') {
        $permissions = $this->getPermissions();
        if (is_array($permissions) && !empty($permissions['role'])) {
          return SlogXt::hasPermToolbar('role', ['role' => $permissions['role']], 'admin sxtrole-content');
        }
      }
      else {
        if ($this->account->hasPermission('administer slog taxonomy')) {
          return TRUE;
        }
        else {
          return $this->account->hasPermission("administer toolbar $toolbar_id");
        }
      }
    }

    return FALSE;
  }

  protected function access() {
    return ($this->account->isAuthenticated() && $this->isActionExecutable());
  }

}
