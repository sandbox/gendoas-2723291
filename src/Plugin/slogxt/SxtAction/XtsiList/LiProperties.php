<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList\LiProperties.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_list_properties",
 *   title = @Translation("Properties"),
 *   menu = "xtsi_list",
 *   path = "liProperties",
 *   cssClass = "icon-properties",
 *   xtProvider = "sxt_slogitem",
 *   needsAuth = TRUE,
 *   weight = 9990
 * )
 */
class LiProperties extends SxtActionPluginBase {

}
