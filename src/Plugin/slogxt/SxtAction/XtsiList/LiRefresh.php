<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList\LiRefresh.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_list_refresh",
 *   title = @Translation("Refresh"),
 *   menu = "xtsi_list",
 *   path = "refresh",
 *   cssClass = "icon-refresh",
 *   xtProvider = "sxt_slogitem",
 *   weight = 0
 * )
 */
class LiRefresh extends SxtActionPluginBase {


}
