<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList\LiHistoryRemove.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_list_remove_history",
 *   title = @Translation("Remove"),
 *   menu = "xtsi_list",
 *   path = "removeHistory",
 *   cssClass = "icon-history-remove",
 *   xtProvider = "sxt_slogitem",
 *   weight = 99
 * )
 */
class LiHistoryRemove extends SxtActionPluginBase {

  public function access() {
    //todo::disabled::@SlogxtAction
    return FALSE;
  }

}
