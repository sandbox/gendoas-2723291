<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList\LiBookmark.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_list_bookmark",
 *   title = @Translation("Bookmark"),
 *   menu = "xtsi_list",
 *   path = "toggleBookmark",
 *   cssClass = "icon-bookmark",
 *   xtProvider = "sxt_slogitem",
 *   weight = -99
 * )
 */
class LiBookmark extends SxtActionPluginBase {


}
