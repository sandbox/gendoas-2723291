<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList\LiEdit.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiList;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_list_edit",
 *   title = @Translation("Edit"),
 *   menu = "xtsi_list",
 *   path = "edit",
 *   cssClass = "icon-edit",
 *   xtProvider = "sxt_slogitem",
 *   weight = 9980
 * )
 */
class LiEdit extends SxtActionPluginBase {
  
}
