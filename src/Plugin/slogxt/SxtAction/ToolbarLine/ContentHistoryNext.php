<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ContentHistoryNext.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tbcontent_history_next",
 *   title = @Translation("Next"),
 *   menu = "xt_tbline_content",
 *   path = "historyGo",
 *   value = "next",
 *   cssClass = "icon-go-next",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 2
 * )
 */
class ContentHistoryNext extends SxtActionPluginTbLineBase {


}
