<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ListHistoryDialog.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tblist_history_dialog",
 *   title = @Translation("History"),
 *   menu = "xtsi_tbline_list",
 *   path = "historyDialog",
 *   value = "dialog",
 *   cssClass = "icon-history",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 1
 * )
 */
class ListHistoryDialog extends SxtActionPluginTbLineBase {


}
