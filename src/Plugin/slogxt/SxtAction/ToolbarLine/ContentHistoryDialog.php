<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ContentHistoryDialog.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tbcontent_history_dialog",
 *   title = @Translation("History"),
 *   menu = "xt_tbline_content",
 *   path = "historyDialog",
 *   value = "dialog",
 *   cssClass = "icon-history",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 1
 * )
 */
class ContentHistoryDialog extends SxtActionPluginTbLineBase {


}
