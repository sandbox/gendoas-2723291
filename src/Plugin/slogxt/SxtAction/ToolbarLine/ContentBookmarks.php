<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ContentBookmarks.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tbcontent_bookmarks",
 *   title = @Translation("Bookmarks"),
 *   menu = "xt_tbline_content",
 *   path = "bookmarksDialog",
 *   cssClass = "icon-bookmarks",
 *   xtProvider = "sxt_slogitem",
 *   group = "dialog",
 *   weight = -11
 * )
 */
class ContentBookmarks extends SxtActionPluginTbLineBase {


}
