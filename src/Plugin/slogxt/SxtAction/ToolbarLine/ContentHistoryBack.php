<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ContentHistoryBack.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tbcontent_history_prev",
 *   title = @Translation("Previous"),
 *   menu = "xt_tbline_content",
 *   path = "historyGo",
 *   value = "back",
 *   cssClass = "icon-go-back",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 0
 * )
 */
class ContentHistoryBack extends SxtActionPluginTbLineBase {


}
