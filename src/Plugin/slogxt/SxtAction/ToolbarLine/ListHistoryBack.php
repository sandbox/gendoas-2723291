<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ListHistoryBack.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tblist_history_prev",
 *   title = @Translation("Previous"),
 *   menu = "xtsi_tbline_list",
 *   path = "historyGo",
 *   value = "back",
 *   cssClass = "icon-go-back",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 0
 * )
 */
class ListHistoryBack extends SxtActionPluginTbLineBase {


}
