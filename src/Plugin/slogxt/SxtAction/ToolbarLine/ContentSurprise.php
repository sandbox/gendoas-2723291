<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ContentSurprise.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tbcontent_surprise",
 *   title = @Translation("Surprise"),
 *   menu = "xt_tbline_content",
 *   path = "surpriseDialog",
 *   cssClass = "icon-surprise",
 *   xtProvider = "sxt_slogitem",
 *   group = "dialog",
 *   weight = -12
 * )
 */
class ContentSurprise extends SxtActionPluginTbLineBase {


}
