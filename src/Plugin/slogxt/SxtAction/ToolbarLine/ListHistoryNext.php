<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine\ListHistoryNext.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\ToolbarLine;

use Drupal\slogxt\Plugin\SxtActionPluginTbLineBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_tblist_history_next",
 *   title = @Translation("Next"),
 *   menu = "xtsi_tbline_list",
 *   path = "historyGo",
 *   value = "next",
 *   cssClass = "icon-go-next",
 *   xtProvider = "sxt_slogitem",
 *   group = "history",
 *   weight = 2
 * )
 */
class ListHistoryNext extends SxtActionPluginTbLineBase {


}
