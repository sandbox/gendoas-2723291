<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoPdf.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_pdf",
 *   title = @Translation("PDF"),
 *   menu = "xtsi_content",
 *   path = "pdf",
 *   cssClass = "icon-pdf",
 *   xtProvider = "sxt_slogitem",
 *   weight = 101
 * )
 */
class CoPdf extends SxtActionPluginBase {

}
