<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoHistoryRemove.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_history_remove",
 *   title = @Translation("Remove"),
 *   menu = "xtsi_content",
 *   path = "removeHistory",
 *   cssClass = "icon-history-remove",
 *   xtProvider = "sxt_slogitem",
 *   weight = 99
 * )
 */
class CoHistoryRemove extends SxtActionPluginBase {

  public function access() {
    //todo::disabled::@SlogxtAction
    return FALSE;
  }

}
