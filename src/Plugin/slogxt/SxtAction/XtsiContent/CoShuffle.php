<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoShuffle.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_shuffle",
 *   title = @Translation("Shuffle"),
 *   menu = "xtsi_content",
 *   path = "shuffle",
 *   cssClass = "icon-shuffle",
 *   cssItemClass = "action-next",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   weight = 0
 * )
 */
class CoShuffle extends SxtActionPluginBase {


}
