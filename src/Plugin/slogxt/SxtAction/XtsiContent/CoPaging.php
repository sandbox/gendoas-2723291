<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoPaging.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_paging",
 *   title = @Translation("Paging"),
 *   menu = "xtsi_content",
 *   path = "paging",
 *   cssClass = "icon-paging",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   notop = TRUE,
 *   weight = 20
 * )
 */
class CoPaging extends SxtActionPluginBase {


}
