<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoDuplicate.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_duplicate",
 *   title = @Translation("Duplicate"),
 *   menu = "xtsi_content",
 *   path = "duplicate",
 *   cssClass = "icon-duplicate",
 *   cssItemClass = "action-main",
 *   xtProvider = "sjqlout",
 *   theme = "sjqlout",
 *   weight = 1
 * )
 */
class CoDuplicate extends SxtActionPluginBase {


}
