<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoEdit.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_edit",
 *   title = @Translation("Edit"),
 *   menu = "xtsi_content",
 *   path = "edit",
 *   cssClass = "icon-edit",
 *   xtProvider = "sxt_slogitem",
 *   weight = 9980
 * )
 */
class CoEdit extends SxtActionPluginBase {

  //todo::edit::access problem:clientside:cache
//  public function access() {
//    if ($hasAccess = parent::access()) {
//      return \Drupal::currentUser()->isAuthenticated();
//    }
//    return FALSE;
//  }
  
}
