<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoProperties.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_properties",
 *   title = @Translation("Properties"),
 *   menu = "xtsi_content",
 *   path = "coProperties",
 *   cssClass = "icon-properties",
 *   xtProvider = "sxt_slogitem",
 *   needsAuth = TRUE,
 *   weight = 9990
 * )
 */
class CoProperties extends SxtActionPluginBase {

}
