<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoRefresh.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_refresh",
 *   title = @Translation("Refresh"),
 *   menu = "xtsi_content",
 *   path = "refresh",
 *   cssClass = "icon-refresh",
 *   xtProvider = "sxt_slogitem",
 *   weight = 0
 * )
 */
class CoRefresh extends SxtActionPluginBase {


}
