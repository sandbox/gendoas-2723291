<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent\CoBookmark.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtsiContent;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_content_bookmark",
 *   title = @Translation("Bookmark"),
 *   menu = "xtsi_content",
 *   path = "toggleBookmark",
 *   cssClass = "icon-bookmark",
 *   xtProvider = "sxt_slogitem",
 *   weight = -99
 * )
 */
class CoBookmark extends SxtActionPluginBase {


}
