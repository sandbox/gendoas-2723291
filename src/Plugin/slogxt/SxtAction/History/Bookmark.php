<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\History\Bookmark.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\History;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_history_bookmark",
 *   title = @Translation("Bookmark"),
 *   menu = "xt_dialog_history",
 *   path = "bookmark",
 *   cssClass = "icon-bookmark",
 *   xtProvider = "sxt_slogitem",
 *   weight = -10
 * )
 */
class Bookmark extends SxtActionPluginBase {


}
