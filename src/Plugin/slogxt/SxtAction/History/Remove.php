<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\History\Remove.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\History;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_history_remove",
 *   title = @Translation("Remove"),
 *   menu = "xt_dialog_history",
 *   path = "remove",
 *   cssClass = "icon-history-remove",
 *   xtProvider = "sxt_slogitem",
 *   weight = 0
 * )
 */
class Remove extends SxtActionPluginBase {


}
