<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain\Promoted.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\XtUserData;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_xtmain_promoted",
 *   title = @Translation("Promoted"),
 *   menu = "main_dropbutton",
 *   path = "promoted",
 *   cssClass = "icon-promoted",
 *   cssItemId = "slogxtmaindropbutton-li-promoted",
 *   dependsOnRole = TRUE,
 *   xtProvider = "sxt_slogitem",
 *   weight = -999
 * )
 */
class Promoted extends SxtActionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $actions = parent::getActionData();
    $attach_data = [
      'mainActionsData' => ['promoted' => [
        'canApply' => $this->getApply(),
      ]],
      'promotedData' => $this->promoted_data,
      'promotedRootTerm' => [
        'tid' => $this->rootterm->id(),
        'label' => t('Promoted'),
        'info' => t('Execute action for promoted feature and current default role'),
      ],
    ];
    $actions['#attached']['drupalSettings']['sxt_slogitem'] = $attach_data;

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function getApply() {
    return [
      'single' => 'slogtb_edit_tbmenu_new,slogtb_edit_tbmenu_edit,slogtb_edit_tbmenu_rearrange',
//      'source' => 'slogtb_edit_tbmenu_move',
//      'target' => 'slogtb_edit_tbmenu_move',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    if ($hasAccess = parent::access()) {
      $default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
      $this->rootterm = $rootterm = SlogXtsi::getPromotedRootTerm($default_role);
      $hasAccess = (!empty($rootterm) && $rootterm->hasChildren() && $rootterm->isVisible());
    }
    if ($hasAccess) {
      $this->promoted_data = SlogXtsi::getPromotedData();
      if (empty($this->promoted_data)) {
        $hasAccess = FALSE;
        if ($is_role_admin = TRUE /* ... */) {
          $hasAccess = TRUE;
          $this->promoted_data = FALSE;
          $this->configuration['notop'] = TRUE;
          $this->configuration['weight'] = 9998;
        }
      }
    }
    return $hasAccess;
  }

}
