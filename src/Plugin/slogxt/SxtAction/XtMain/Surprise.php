<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain\Surprise.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginBase;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\XtUserData;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_xtmain_surprise",
 *   title = @Translation("Surprise"),
 *   menu = "main_dropbutton",
 *   path = "surprise",
 *   cssClass = "icon-surprise",
 *   cssItemId = "slogxtmaindropbutton-li-surprise",
 *   dependsOnRole = TRUE,
 *   xtProvider = "sxt_slogitem",
 *   weight = -990
 * )
 */
class Surprise extends SxtActionPluginBase {

  public function getActionData() {
    $actions = parent::getActionData();
    $attach_data = [
      'mainActionsData' => ['surprise' => [
        'canApply' => $this->getApply(),
      ]],
      'surpriseData' => $this->surprise_data,
      'surpriseRootTerm' => [
        'tid' => $this->rootterm->id(),
        'label' => t('Surprise'),
        'info' => t('Execute action for surprise feature and current default role'),
      ],
    ];
    $actions['#attached']['drupalSettings']['sxt_slogitem'] = $attach_data;

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function getApply() {
    return [
      'single' => 'slogtb_edit_tbmenu_new,slogtb_edit_tbmenu_edit,slogtb_edit_tbmenu_rearrange',
//      'source' => 'slogtb_edit_tbmenu_move',
//      'target' => 'slogtb_edit_tbmenu_move',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    if ($hasAccess = parent::access()) {
      $default_role = XtUserData::getDefaultRole(\Drupal::currentUser(), TRUE);
      $this->rootterm = $rootterm = SlogXtsi::getSurpriseRootTerm($default_role);
      $hasAccess = (!empty($rootterm) && $rootterm->hasChildren() && $rootterm->isVisible());
    }
    if ($hasAccess) {
      $this->surprise_data = $data = SlogXtsi::getSurpriseData();
      if (!(is_array($data) && count($data) > 1)) {
        $hasAccess = FALSE;
        if ($is_role_admin = TRUE /* ... */) {
          $hasAccess = TRUE;
          $this->surprise_data = FALSE;
          $this->configuration['notop'] = TRUE;
          $this->configuration['weight'] = 9999;
        }
      }
    }

    return $hasAccess;
  }

}
