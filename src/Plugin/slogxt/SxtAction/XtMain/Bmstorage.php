<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain\Bmstorage.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\XtMain;

use Drupal\slogxt\Plugin\SxtActionPluginSysRoottermBase;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\user\Entity\User;
use Drupal\Core\Entity\EntityInterface;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "slogxt_xtmain_bmstorage",
 *   title = @Translation("Bookmark storage"),
 *   description = @Translation("Private stored bookmark collections."),
 *   menu = "main_dropbutton",
 *   path = "bmstorage",
 *   target_select_form = "\Drupal\sxt_slogitem\Form\BmstorageTypeSelectForm",
 *   cssClass = "icon-bmstorage",
 *   xtProvider = "sxt_slogitem",
 *   weight = 2100
 * )
 */
class Bmstorage extends SxtActionPluginSysRoottermBase {

  /**
   * {@inheritdoc}
   */
  public function getSysRootTerm($target_type, EntityInterface $entity = NULL) {
    return SlogXtsi::getBookmarkRootTerm($target_type, $entity);
  }

  public function getXtSysRootTerm($base_entity_id) {
    $result = ['doTbMenuSelect' => TRUE];
    list($target, $targetentity_id, $entity_id) = explode(SlogXt::XTURL_DELIMITER, $base_entity_id . SlogXt::XTURL_DELIMITER3);
    $account = \Drupal::currentUser();
    if ($account->isAuthenticated()) {
      $user = User::load($account->id());
      if ($rootterm = $this->getSysRootTerm($targetentity_id, $user)) {
        $result += [
          'type' => 'rootterm',
          'value' => $rootterm,
          'menuSelectAlter' => '\Drupal\sxt_slogitem\SlogXtsi::bmMenuSelectAlter',
        ];
      }
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getActionData() {
    $actions = parent::getActionData();
    $attach_data = ['mainActionsData' => ['bmstorage' => [
          'canApply' => $this->getApply(),
          'rootterm_ids' => $this->getRootTermIds(),
          'liTitle' => $this->getTitle(),
          'liDescription' => $this->getDescription(),
    ]]];
    // add attach_data to slogxt, handled like archive/trash
    $actions['#attached']['drupalSettings']['slogxt'] = $attach_data;
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function getApply() {
    return ['single' => 'slogtb_edit_tbmenu_new,slogtb_edit_tbmenu_edit,slogtb_edit_tbmenu_rearrange'];
  }

  public function access() {
    if ($hasAccess = parent::access() && \Drupal::currentUser()->isAuthenticated()) {
      $user = User::load(\Drupal::currentUser()->id());
      $this->root_terms = SlogXtsi::getBookmarkRootTerms($user);
      $hasAccess = (!empty($this->root_terms));
    }
    return $hasAccess;
  }

}
