<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark\Open.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_bookmark_open",
 *   title = @Translation("Open"),
 *   menu = "xt_dialog_bookmark",
 *   path = "open",
 *   cssClass = "icon-xt-open",
 *   xtProvider = "sxt_slogitem",
 *   weight = -10
 * )
 */
class Open extends SxtActionPluginBase {


}
