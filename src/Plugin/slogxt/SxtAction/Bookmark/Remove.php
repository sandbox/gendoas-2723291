<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark\Remove.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_bookmark_remove",
 *   title = @Translation("Remove"),
 *   menu = "xt_dialog_bookmark",
 *   path = "remove",
 *   cssClass = "icon-bookmark-remove",
 *   xtProvider = "sxt_slogitem",
 *   weight = 0
 * )
 */
class Remove extends SxtActionPluginBase {


}
