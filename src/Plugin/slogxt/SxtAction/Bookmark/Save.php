<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark\Save.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\Bookmark;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_bookmark_save",
 *   title = @Translation("Save"),
 *   menu = "xt_dialog_bookmark",
 *   path = "save",
 *   cssClass = "icon-xt-save",
 *   xtProvider = "sxt_slogitem",
 *   weight = 10
 * )
 */
class Save extends SxtActionPluginBase {


}
