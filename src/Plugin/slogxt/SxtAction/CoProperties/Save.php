<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\CoProperties\Save.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\CoProperties;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_coprops_save",
 *   title = @Translation("Save"),
 *   menu = "xt_dialog_coprops",
 *   path = "save",
 *   cssClass = "icon-xt-save",
 *   xtProvider = "sxt_slogitem",
 *   weight = 10
 * )
 */
class Save extends SxtActionPluginBase {


}
