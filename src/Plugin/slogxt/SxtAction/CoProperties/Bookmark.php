<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\CoProperties\Bookmark.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\SxtAction\CoProperties;

use Drupal\slogxt\Plugin\SxtActionPluginBase;

/**
 * //todo::text::
 *
 * @SlogxtAction(
 *   id = "sxt_slogitem_dialog_coprops_bookmark",
 *   title = @Translation("Bookmark"),
 *   menu = "xt_dialog_coprops",
 *   path = "bookmark",
 *   cssClass = "icon-bookmark",
 *   xtProvider = "sxt_slogitem",
 *   weight = -10
 * )
 */
class Bookmark extends SxtActionPluginBase {


}
