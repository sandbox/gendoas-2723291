<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\props\xtsiContent\NodeCrossReferences.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\props\xtsiContent;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtProps(
 *   id = "xtsi_props_crossrefs",
 *   bundle = "xtsi_coprops",
 *   title = @Translation("Cross references"),
 *   description = @Translation("Shows cross references to this content."),
 *   route_name = "sxt_slogitem.props.content.crossrefs",
 *   skipable = FALSE,
 *   weight = 0
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class NodeCrossReferences extends XtPluginEditBase {

}
