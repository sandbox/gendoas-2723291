<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches\XtsiMwCommentShowToggler.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "xtsi_localswitches_mwc_showtoggler",
 *   bundle = "local_switches",
 *   title = @Translation("Show MwComments toggler"),
 *   description = @Translation(".....Show MwComments toggler."),
 *   path = "localSwitches",
 *   value = "XtsiMwCommentShowToggler",
 *   xtProvider = "sxt_slogitem",
 *   weight = 60
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class XtsiMwCommentShowToggler extends XtPluginOptionsBase {

  public function access() {
    return \Drupal::currentUser()->isAuthenticated();
  }

}
