<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches\XtsiListEnlarge.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "xtsi_localswitches_listenlarge",
 *   bundle = "local_switches",
 *   title = @Translation("Enlarge listing"),
 *   description = @Translation("..........Enlarge listing."),
 *   path = "localSwitches",
 *   value = "XtsiListEnlarge",
 *   xtProvider = "sxt_slogitem",
 *   weight = 51
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class XtsiListEnlarge extends XtPluginOptionsBase {

}
