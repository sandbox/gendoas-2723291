<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches\XtsiMwtopAutoopen.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "xtsi_localswitches_mwtoc_autoopen",
 *   bundle = "local_switches",
 *   title = @Translation("AutoOpen Mediawiki-Toc"),
 *   description = @Translation("..........AutoOpen Mediawiki-Toc."),
 *   path = "localSwitches",
 *   value = "XtsiMwtopAutoopen",
 *   xtProvider = "sxt_slogitem",
 *   weight = 53
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class XtsiMwtopAutoopen extends XtPluginOptionsBase {

}
