<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches\XtsiContentEnlarge.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "xtsi_localswitches_contentenlarge",
 *   bundle = "local_switches",
 *   title = @Translation("Enlarge content"),
 *   description = @Translation("..........Enlarge content."),
 *   path = "localSwitches",
 *   value = "XtsiContentEnlarge",
 *   xtProvider = "sxt_slogitem",
 *   weight = 50
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class XtsiContentEnlarge extends XtPluginOptionsBase {

}
