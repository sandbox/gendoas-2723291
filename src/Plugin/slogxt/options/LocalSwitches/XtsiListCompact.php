<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches\XtsiListCompact.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\options\LocalSwitches;

use Drupal\slogxt\Plugin\XtPluginOptionsBase;

/**
 * @SlogxtOptions(
 *   id = "xtsi_localswitches_listcompact",
 *   bundle = "local_switches",
 *   title = @Translation("Compact listing"),
 *   description = @Translation("..........Compact listing."),
 *   path = "localSwitches",
 *   value = "XtsiListCompact",
 *   xtProvider = "sxt_slogitem",
 *   weight = 52
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtOptions
 */
class XtsiListCompact extends XtPluginOptionsBase {

}
