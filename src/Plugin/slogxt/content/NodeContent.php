<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\slogxt\content\NodeContent.
 */

namespace Drupal\sxt_slogitem\Plugin\slogxt\content;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_mediawiki\SxtMediawiki;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\sxt_slogitem\XtsiCronStateData;
use Drupal\slogxt\Event\SlogxtGenericEvent;
use Drupal\sxt_slogitem\Event\SlogitemEvents;
use Drupal\comment\CommentInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Render\Element;
use Drupal\sxt_slogitem\Plugin\sxt_slogitem\SlogitemBase;

/**
 * Slogitem plugin for node types.
 * 
 * ID is composed of 'node' and the node's type separated by a dot. 
 * Node's id is passed by $route_parameters['node'].
 *
 * @SlogxtContent(
 *   id = "node",
 *   route = "entity.node.canonical",
 *   deriver = "Drupal\sxt_slogitem\Plugin\slogxt\Derivative\NodeContent"
 * )
 */
class NodeContent extends SlogitemBase {

  /**
   * {@inheritdoc}
   */
  protected function getHeader() {
    $node = $this->getEntity();
    $mainLabel = $node->label();
    if (SlogXtsi::isDebugMode()) {
      $mainLabel = $this->getSlogitemId() . '.' . $node->id() . ':: ' . $mainLabel;
    }
    $header = [
      'mainLabel' => $mainLabel,
    ];
    return $header;
  }

  /**
   * Prepare comment for MediWiki filte, if comments exists.
   * 
   * @param array $elements
   * @return array
   */
  public static function preRenderMwCommented($elements) {
    $mwc_field_name = 'mwnodecomment';
    if (!\Drupal::moduleHandler()->moduleExists('sxt_mediawiki')  //
        || empty($elements['mwcontent']) || empty($elements[$mwc_field_name])) {
      return $elements;
    }

    $node = $elements['#node'];
    $nid = $node->id();
    $node_text = $elements['mwcontent'][0]['#text'];
    $status = (integer) $node->get($mwc_field_name)->status;
    $comments_open = ($status == CommentItemInterface::OPEN);

    $comment_text = '';
    $comments = [];
    $el_comments = $elements[$mwc_field_name][0]['comments'];
    if ($el_comments) {
      foreach (Element::children($el_comments) as $key) {
        $comment = $el_comments[$key]['#comment'] ?? FALSE;
        if (is_integer($key) && $comment && $comment instanceof CommentInterface) {
          $comments[$key] = $comment;   // debug
        }
      }
    }

    if (!empty($comments)) {
      $cur_uid = (integer) \Drupal::currentUser()->id();
      $cid = SxtMediawiki::getCidFromText($node_text);
      SxtMediawiki::cache()->delete($cid);
      $max_depth = SlogXt::getMwCommentMaxDepth();

      $comment_data = [];
      $last_cid = $last_depth = FALSE;
      foreach ($comments as $cid => $comment) {
        $owner_id = (int) $comment->uid->target_id;
        $thread = $comment->get('thread')->value;
        $first = explode('/', $thread)[0];
        $splitted = explode('.', $first);
        $depth = min(count($splitted), $max_depth);
        $actions = ($cur_uid === $owner_id) ? ['delete', 'edit'] : ['none'];
        if ($depth < $max_depth) {
          $actions[] = 'reply';
        }
        $comment_data[$cid] = [
          'depth' => $depth,
          'actions' => implode(':', $actions),
        ];

        if ($last_cid && $depth > $last_depth) {
          // no edit/delete
          $comment_data[$last_cid]['actions'] = 'reply';
        }

        $last_cid = $cid;
        $last_depth = $depth;
      }

      if (!$comments_open) {
        foreach ($comment_data as $cid => &$data) {
          $data['actions'] = 'none';
        }
      }

      $header = [
        1 => '== subject ==',
        2 => '=== subject ===',
        3 => '==== subject ====',
        4 => '===== subject =====',
        5 => '====== subject ======',
      ];

      $itemprefix = "\n\n<nowiki>%%mwcommentitem::§cid§::§depth§::§actions§%%</nowiki>";
      $itemsuffix = "\n\n<nowiki>%%endmwcommentitem%%</nowiki>";
      $bodydiv = "\n\n<nowiki>%%mwcommentbody%%</nowiki>\n\n";
      $bodyenddiv = "\n\n<nowiki>%%endmwcommentbody%%</nowiki>\n\n";
      $last_depth = 0;
      foreach ($comments as $cid => $comment) {
        $depth = $comment_data[$cid]['depth'];
        if ($depth <= $last_depth) {
          $comment_text .= str_repeat($itemsuffix, $last_depth - $depth + 1);
        }

        $actions = $comment_data[$cid]['actions'];
        $subject = (string) $comment->get('subject')->value;
        $comment_body = (string) $comment->get('mwcomment_body')->value;

        $title = str_replace('subject', $subject, $header[$depth]);
        $body = $bodydiv . trim(SxtMediawiki::replaceMwHeader($comment_body)) . $bodyenddiv;
        $prefix = str_replace('§cid§', $cid, $itemprefix);
        $prefix = str_replace('§depth§', $depth, $prefix);
        $prefix = str_replace('§actions§', $actions, $prefix);
        $comment_text .= "{$prefix}\n\n{$title}\n\n{$body}\n\n";

        $last_depth = $depth;
      }
      $comment_text .= str_repeat($itemsuffix, $last_depth);

      preg_match('/^==[^=]\s*.*\S\s*==[^=]/', $node_text, $match);
      if (empty($match)) {
        $node_label = $node->label();
        $node_text = "== $node_label ==\n\n" . $node_text;
      }
    }


    $el_text = &$elements['mwcontent'][0]['#text'];
    $open = $comments_open ? 1 : 0;
    $number = count($comments);
    $prefix = "\n\n<nowiki>%%mwcomments::$nid::$open::$number%%</nowiki>";
    $suffix = "\n\n<nowiki>%%endmwcomments%%</nowiki>";

    $head = $foot = '';
    if ($comments_open || !empty($comment_text)) {
      $head = "\n\n<nowiki>%%mwcommentshead%%</nowiki>";
      $foot = "\n\n<nowiki>%%mwcommentsfoot%%</nowiki>";
      $el_text = $node_text . $prefix . $head . $comment_text . $foot . $suffix;
    }
    else {
      $emptyclosed = "\n\n<nowiki>%%mwcommentsemptyclosed%%</nowiki>";
      $el_text = $node_text . $prefix . $emptyclosed . $suffix;
    }

    unset($elements[$mwc_field_name]);
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function getContent($callable) {
    $arguments = [$this->getEntity()];
    $build = call_user_func_array($callable, $arguments);

    $node = $build['#node'];
    if ($node->hasField('mwcontent') && $node->hasField('mwnodecomment')) {
      $build['#pre_render'][] = [get_class($this), 'preRenderMwCommented'];
    }

    $content = (string) $this->drupalRender($build);
    $content =  SlogXt::replaceFirstTag($content, 'header');
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSpecial() {
    $node = $this->getEntity();
    $event = new SlogxtGenericEvent([$node]);
    $event->entitySpecialData = FALSE;
    \Drupal::service('event_dispatcher')
        ->dispatch(SlogitemEvents::XTSI_SPECIAL_ENTITY_DATA, $event);
    return $event->entitySpecialData ?: FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function preBuild() {
    $cron_key = 'menumove';
    $cron_data = XtsiCronStateData::getCronStateData($cron_key);
    if (!empty($cron_data)) {
      $node = $this->getEntity();
      $nid = $node->id();
      foreach ($cron_data as $key => $single) {
        $single_array = explode(';', $single);
        if (in_array($nid, $single_array)) {
          // calculate and set xt values
          XtsiNodeTypeData::setXtNodeValues($node);

          // remove nid from list and write to database
          $new_nids = array_diff($single_array, [$nid]);
          $cron_data[$key] = implode(';', $new_nids);
          XtsiCronStateData::setCronStateData($cron_key, $cron_data);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    $tid = $this->configuration['term_id'];
    $menu_term = SlogTx::getMenuTerm($tid);
    $vocabulary = $menu_term->getVocabulary();
    $node = $this->getEntity();
    $nid = $node->id();
    $xtnodebase_sid = XtsiNodeTypeData::nodeGetBaseSid($node);

    if (empty($xtnodebase_sid)) {
      $node = XtsiNodeTypeData::setXtNodeValues($node);
      $xtnodebase_sid = XtsiNodeTypeData::nodeGetBaseSid($node);
    }

    $xtnodebase_tb = XtsiNodeTypeData::nodeGetBaseTB($node);
    $xtnoderank_sids = XtsiNodeTypeData::nodeGetRankSids($node);
    $slogitem = SlogItem::load($xtnodebase_sid);

    $hasTbnode = SlogXtsi::nodeHasTbnode($nid);

    $settings = parent::getSettings() + [
      'pathLabel' => $vocabulary->headerPathLabel(),
      'listLabel' => $menu_term->label(),
      'node' => [
        'nodeId' => $nid,
        'nodeTitle' => $node->label(),
        'nodeType' => $node->getType(),
        'hasTbnode' => $hasTbnode,
        'baseSid' => $xtnodebase_sid,
        'baseVid' => $slogitem ? $slogitem->getTermId() : FALSE,
        'baseTb' => $xtnodebase_tb,
        'hasRankSids' => !empty($xtnoderank_sids),
      ],
    ];

    if ($hasTbnode) {
      // set the node's toolbar hash, but from cache only
      $settings['node']['tbnodeHash'] = SlogXtsi::getTbnodeHash($node, TRUE);
    }

    return $settings;
  }

}
