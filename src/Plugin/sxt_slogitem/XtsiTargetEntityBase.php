<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\sxt_slogitem\XtsiTargetEntityBase.
 */

namespace Drupal\sxt_slogitem\Plugin\sxt_slogitem;

use Drupal\Core\Plugin\PluginBase;

/**
 * Defines the base class for target entity plugins.
 */
abstract class XtsiTargetEntityBase extends PluginBase implements XtsiTargetEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->configuration['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getValues() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    if (!isset($this->configuration[$name])) {
      $this->configuration[$name] = [];
    }

    return $this->configuration[$name];
  }

  /**
   * {@inheritdoc}
   */
  public function __set($name, $value) {
    if (isset($value)) {
      $this->configuration[$name] = $value;
    } else {
      unset($this->configuration[$name]);
    }
  }

}
