<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\sxt_slogitem\TargetEntity\Node.
 */

namespace Drupal\sxt_slogitem\Plugin\sxt_slogitem\TargetEntity;

use Drupal\sxt_slogitem\Plugin\sxt_slogitem\XtsiTargetEntityBase;

/**
 * @todo.
 *
 * @XtsiTargetEntity(
 *   id = "node",
 *   title = @Translation("Node"),
 *   description = @Translation("Handler for nodes as entity in slogitems."),
 *   provider = "sxt_slogitem",
 * )
 */
class Node extends XtsiTargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return 'entity.node.canonical';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getRouteParameters($target_entity_id) {
    return ['node' => $target_entity_id];
  }

}
