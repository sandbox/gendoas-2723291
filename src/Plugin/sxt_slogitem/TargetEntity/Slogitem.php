<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\sxt_slogitem\TargetEntity\Node.
 */

namespace Drupal\sxt_slogitem\Plugin\sxt_slogitem\TargetEntity;

use Drupal\sxt_slogitem\Plugin\sxt_slogitem\XtsiTargetEntityBase;

/**
 * @todo.
 *
 * @XtsiTargetEntity(
 *   id = "slogitem",
 *   title = @Translation("Slogitem"),
 *   description = @Translation("Handler for slogitem as entity in slogitems. This redirects to a slogitem with node entity."),
 *   provider = "sxt_slogitem",
 * )
 */
class Slogitem extends XtsiTargetEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getRouteName() {
    return 'entity.slogitem.redirect';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getRouteParameters($target_entity_id) {
    return ['slogitem' => $target_entity_id];
  }

}
