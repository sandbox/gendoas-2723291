<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\Plugin\sxt_slogitem\SlogitemBase.
 */

namespace Drupal\sxt_slogitem\Plugin\sxt_slogitem;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Executable\ExecutableInterface;

/**
 * Defines a base class for all slog node plugins.
 */
abstract class SlogitemBase extends PluginBase implements ExecutableInterface {

  protected $route_system_path;     // cache retrieved path
  protected $route_request_pushed;  // remember if request already pushed

  protected function getSlogitemId() {
    return $this->configuration['slogitem_id'];
  }

  protected function getExtendedId() {
    return $this->configuration['extended_id'];
  }

  protected function getTermId() {
    return $this->configuration['term_id'];
  }

  protected function getEntity() {
    return $this->configuration['entity'];
  }

  /**
   * Do actions before retrieving content.
   * 
   * @return NULL
   */
  protected function preBuild() {
    return;
  }

  public function execute() {
    // find controller and build result array
    $route = $this->configuration['route'];
    $controller_key = $this->getContollerKey();
    if ($controller_definition = $route->getDefault($controller_key)) {
      $controllerResolver = \Drupal::service('controller_resolver');
      $callable = $controllerResolver->getControllerFromDefinition($controller_definition);
      $settings = $this->getSettings();
      if (empty($settings['headerActions'])) {
        $settings['headerActions'] = FALSE;
      }
      else {
        $settings['headerActions'] = SlogXt::sortElements($settings['headerActions'], true);
      }
      
      // do actions before retrieving content
      $this->preBuild();
      
      // retrieve and return data
      return [
        'header' => $this->getHeader(),
        'content' => $this->getContent($callable),
        'settings' => $this->extendSettings($settings),
        'special' => $this->getSpecial(),
      ];
    }

    // no result, causes fallback
    return false;
  }

  /**
   * Return the key of route's contoller defaults.
   * 
   * Defaults to '_content'.
   * Overwrite for desired key.
   * 
   * @return string
   */
  protected function getContollerKey() {
    return '_controller';
  }

  /**
   * Return array with header label.
   * 
   * Needs override. 
   * Returns 'not implemented' message if not overwriten.
   * 
   * @return array with header label
   *  - mainLabel
   *  - (pathLabel not required, is computed clientside)
   */
  protected function getHeader() {
    return ['mainLabel' => $this->getEntity()->label()];
  }

//  protected function sxtTemplateHeader() {
//    return SlogXt::sxtTemplateHeader();
//  }

  /**
   * Return html text for slogitem's content in content region.
   * 
   * Needs override. 
   * Returns 'not implemented' message if not overwriten.
   * 
   * @param array/string $callable
   *  - From route retrieved callable controller
   * @return string
   *  - Rendered elements, i.e. html text 
   */
  protected function getContent($callable) {
    return get_class($this) . '->getContent() not implemented!';
  }

  /**
   * Return settings array for java script behavior.
   * 
   * Available settings:
   *  - nocache (defaults to false) 
   * 
   * @return array
   */
  protected function getSettings() {
    return $this->defaultSettings();
  }

  /**
   * 
   * @return mixed array if has data, FALSE otherwise
   */
  protected function getSpecial() {
    return FALSE;
  }

  protected function defaultSettings() {
    $headerActions = SlogXt::pluginManager('action')->getActionsData('xtsi_content');
    return [
      'extendedId' => $this->getExtendedId(),
      'nocache' => false,
      'headerActions' => $headerActions,
    ];
  }

  protected function extendSettings($settings) {
    return $settings;
  }

  /**
   * Return path from route as needed for request->_system_path.
   * 
   * With tokens in route's path replaced by values of route_parameters.
   * 
   * @return string
   */
  protected function getRouteSystemPath() {
    if (!$this->route_system_path) {
      $path = $this->configuration['route']->getPath();
      $parameters = $this->configuration['route_parameters'];
      foreach ($parameters as $key => $value) {
        if (is_string($value)) {
          $path = str_replace('{' . $key . '}', $value, $path);
        }
      }
      $this->route_system_path = $path;
    }
    return $this->route_system_path;
  }

  /**
   * Wraps drupal_render.
   *
   * @param array $elements
   *   The structured array describing the data to be rendered.
   *
   * @return string
   *   The rendered HTML.
   *
   * @todo Remove once drupal_render is converted to autoloadable code.
   * @see https://drupal.org/node/2171071
   */
  protected function drupalRender(array $elements, $is_root_call = FALSE) {
//    \Drupal::service('twig')->enableDebug();
//    return drupal_render($elements);
    return \Drupal::service('renderer')->render($elements, $is_root_call);
  }

}
