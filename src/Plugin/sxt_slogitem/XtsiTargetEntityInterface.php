<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Plugin\sxt_slogitem\XtsiTargetEntityInterface.
 */

namespace Drupal\sxt_slogitem\Plugin\sxt_slogitem;

/**
 * Defines the interface for xtsi target entity plugins.
 */
interface XtsiTargetEntityInterface {
  
  /**
   * Return the route name for this plugin.
   */
  public function getRouteName();
  
  /**
   * Return the required route parameters for the route name. 
   * 
   * @param integer $target_entity_id
   */
  public function getRouteParameters($target_entity_id);

}
