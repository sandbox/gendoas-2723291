<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\XtsiCronStateData.
 */

namespace Drupal\sxt_slogitem;

use Drupal\sxt_slogitem\SlogXtsi;

/**
 * //todo::docu
 */
class XtsiCronStateData {

  public static function getCronStateDataService() {
    return \Drupal::service('slogxt.cron_state_data');
  }

  public static function getCronStateData($key) {
    return self::getCronStateDataService()->getCronStateData('sxt_slogitem', $key);
  }

  public static function setCronStateData($key, $data) {
    return self::getCronStateDataService()->setCronStateData('sxt_slogitem', $key, $data);
  }

  public static function deleteCronStateData($key) {
    return self::getCronStateDataService()->deleteCronStateData('sxt_slogitem', $key);
  }

  /**
   * 
   * @param array $node_ids
   * @return NULL
   */
  public static function pushNodeIds(array $node_ids) {    
    if (empty($node_ids)) {
      return;   // nothing to do
    }
    
    rsort($node_ids);
    $cron_key = 'menumove';
    $cron_data = self::getCronStateData($cron_key);
    if (!empty($cron_data)) {
      foreach ($cron_data as $single) {
        $single_array = explode(';', $single);
        $node_ids = array_diff($node_ids, $single_array);
        if (empty($node_ids)) {
          break;
        }
      }
    }
    
    if (!empty($node_ids)) {
      $per_step = SlogXtsi::isDebugMode() ? 10 : 50;
      if (!empty($single_array) && count($single_array) < $per_step) {
        $cron_data = array_slice($cron_data, 0 , -1);
        $node_ids = array_merge($single_array, $node_ids);
      }
      
      $ids_length = count($node_ids);
      $step_off = 0;
      $timestamp = \Drupal::time()->getRequestTime();
      while ($step_off < $ids_length) {
        $step_nids = array_slice($node_ids, $step_off, $per_step);
        $step_key = 'step.' . $timestamp++;
        $cron_data[$step_key] = implode(';', $step_nids);
        $step_off += $per_step;
      }

      self::setCronStateData($cron_key, $cron_data);
    }
  }

  /**
   */
  public static function shiftNodeIds($permanent) {    
    $cron_key = 'menumove';
    $cron_data = self::getCronStateData($cron_key);
    if (!empty($cron_data)) {
      $shifted = array_shift($cron_data);
      if ($permanent) {
        self::setCronStateData($cron_key, $cron_data);
      }
      if (!empty($shifted)) {
        $node_ids = explode(';', $shifted);
        return $node_ids;
      }
    }
    
    return FALSE;
  }

}
