<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Routing\XtsiRoutes.
 */

namespace Drupal\sxt_slogitem\Routing;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogxt\SlogxtMin;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class XtsiRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $basePath = trim(SlogXt::getBasePath('sxt_slogitem'), '/');
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('sxt_slogitem'), '/');
    $xtsiBaseViewHandle = '\Drupal\sxt_slogitem\Controller\XtsiBaseViewController::handle';
    $routes = [];

    // this option will be evaluated by \Drupal\sxt_slogitem\Theme\XtsiNegotiator,
    // i.e. each path with this option uses sjqlout theme.
    $jqlout_true_option = [SlogxtMin::SJQLOUT_THEME_ROUTE => TRUE];
    // prevent big pipe for some routes
    $no_big_pipe_option = ['_no_big_pipe' => TRUE];

    $raw_data = [
      'base.view' => [
        'path' => "/$basePath/{xtsi_userrole_hash}",
        'defaults' => [
          '_controller' => $xtsiBaseViewHandle,
          'xtsi_userrole_hash' => '',
        ],
        'options' => $jqlout_true_option + $no_big_pipe_option,
      ],
      'sid.view' => [
        'path' => "/$basePath/sid/{slogitem_id}",
        'defaults' => [
          '_controller' => $xtsiBaseViewHandle,
        ],
        'options' => $jqlout_true_option + $no_big_pipe_option,
      ],
      'tid.view' => [
        'path' => "/$basePath/tid/{tbmenu_tid}",
        'defaults' => [
          '_controller' => $xtsiBaseViewHandle,
        ],
        'options' => $jqlout_true_option + $no_big_pipe_option,
      ],
      'content.view' => [
        'path' => "/$baseAjaxPath/sid/{slogitem_id}/{deeprefresh}",
        'defaults' => [
          '_controller' => '\Drupal\sxt_slogitem\Handler\OnSlogitemListSelect::ajaxResponse',
          'deeprefresh' => '',
        ],
        'options' => $jqlout_true_option,
      ],
      'retrieve.attachdata' => [
        'path' => "/$baseAjaxPath/retrieve/attachdata",
        'defaults' => [
          '_controller' => '\Drupal\sxt_slogitem\Handler\OnRetrieveAttachdata::ajaxResponse',
        ],
        'options' => $jqlout_true_option,
      ],
      'user.redirect' => [
        'path' => "/$baseAjaxPath/user/{command}/{param1}/{param2}/{param3}/{param4}",
        'defaults' => [
          '_controller' => '\Drupal\sxt_slogitem\Handler\Redirect\XtsiUserController::getContentResult',
          'param1' => '',
          'param2' => '',
          'param3' => '',
          'param4' => '',
        ],
        'options' => $jqlout_true_option,
      ],
      'system.4xx.redirect' => [
        'path' => "/$baseAjaxPath/4xx/system/{errcode}",
        'defaults' => [
          '_controller' => '\Drupal\sxt_slogitem\Handler\Redirect\System4xxController::ajaxResponse',
        ],
        'options' => $jqlout_true_option,
      ],
    ];

    $raw_data += $this->_rawAjaxSlogtb($baseAjaxPath);
    $raw_data += $this->_rawAjaxEditMain($baseAjaxPath);
    $raw_data += $this->_rawAjaxEditList($baseAjaxPath);
    $raw_data += $this->_rawAjaxEditContent($baseAjaxPath);
    $raw_data += $this->_rawAjaxPropsContent($baseAjaxPath);
    $raw_data += $this->_rawAjaxBookmarkStorage($baseAjaxPath);
    $raw_data += $this->_rawAjaxUser($baseAjaxPath);
    $raw_data += $this->_rawAjaxOther($baseAjaxPath);

    $defaults = [
//        'requirements' => ['_access' => 'TRUE'],
      'requirements' => ['_permission' => 'access content'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["sxt_slogitem.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  private function _rawAjaxSlogtb($baseAjaxPath) {
    $controllerPath = '\Drupal\sxt_slogitem\Handler';
    $jqlout_true_option = [SlogxtMin::SJQLOUT_THEME_ROUTE => TRUE];
    $raw_data = [
      'on.tbnode_menu_load' => [
        'path' => "/$baseAjaxPath/tbsubmenu/{entity_type}/{entity_id}/{rootterm}",
        'defaults' => [
          '_controller' => "{$controllerPath}\slogtb\OnTbnodeMenuLoad::ajaxResponse",
          'rootterm' => SlogTx::ROOTTERM_DEFAULT,
        ],
        //todo::top::top 'options' => ['_theme' => 'ajax_base_page']
        'options' => self::$optAjaxBase + $jqlout_true_option,
      ],
      //do not change this id, determined for slogtb handler 
      'on.menu_item_select' => [
        'path' => "/$baseAjaxPath/menutid/{tbmenu_tid}",
        'defaults' => [
          '_controller' => "{$controllerPath}\slogtb\OnTbMenuItemSelect::ajaxResponse",
        ],
        'options' => self::$optAjaxBase,
        'requirements' => [
          '_access' => 'TRUE',
        ],
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxEditMain($baseAjaxPath) {
    //todo::actions::main
    $base_entity_id = '{base_entity_id}';
    $next_entity_id = '{next_entity_id}';
    $link_entity_id = '{link_entity_id}';
    $baseEditPath = $baseAjaxPath . '/edit/xtmain';
    $controllerPath = '\Drupal\sxt_slogitem\Handler\XtsiEdit\main';
    $raw_data = [
      'edit.main.promoted_surprise.select' => [
        'path' => "/$baseEditPath/role/$base_entity_id/psselect",
        'defaults' => [
          '_controller' => "{$controllerPath}\PromoteSurpriseSelectController::getContentResult",
        ],
      ],
      'edit.main.promoted.prepare' => [
        'path' => "/$baseEditPath/role/$base_entity_id/promoted/prepare/$link_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\PromotePrepareController::getContentResult",
        ],
      ],
      'edit.main.surprise.prepare' => [
        'path' => "/$baseEditPath/role/$base_entity_id/surprise/prepare/$link_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SurprisePrepareController::getContentResult",
        ],
      ],
      'edit.main.promoted.select' => [
        'path' => "/$baseEditPath/role/$base_entity_id/promoted/select/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\PromoteSelectController::getContentResult",
        ],
      ],
      'edit.main.surprise.select' => [
        'path' => "/$baseEditPath/role/$base_entity_id/surprise/select/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SurpriseSelectController::getContentResult",
        ],
      ],
      'edit.main.promoted.newcontent' => [
        'path' => "/$baseEditPath/role/$base_entity_id/promoted/newcontent/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\PromoteNewContentController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.main.surprise.newcontent' => [
        'path' => "/$baseEditPath/role/$base_entity_id/surprise/newcontent/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SurpriseNewContentController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.main.promoted.deactivate' => [
        'path' => "/$baseEditPath/role/$base_entity_id/promoted/deactivate/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\PromoteDeactivateController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.main.surprise.deactivate' => [
        'path' => "/$baseEditPath/role/$base_entity_id/surprise/deactivate/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SurpriseDeactivateController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxEditList($baseAjaxPath) {
    //todo::actions::list
    $base_entity_id = '{base_entity_id}';
    $next_entity_id = '{next_entity_id}';
    $link_entity_id = '{link_entity_id}';
    $node_type = '{node_type}';
    $baseEditPath = $baseAjaxPath . '/edit_xtsilist';
    $controllerPath = '\Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiList';
    $raw_data = [
      'edit.list.actions' => [
        'path' => "/$baseEditPath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsLiEditController::getContentResult",
        ],
      ],
      'edit.list.item.edit' => [
        'path' => "/$baseEditPath/$base_entity_id/edit/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SlogitemEditController::getContentResult",
        ],
      ],
      'edit.list.newnode' => [
        'path' => "/$baseEditPath/$base_entity_id/newnode/$next_entity_id/$node_type",
        'defaults' => [
          '_controller' => "{$controllerPath}\SlogitemNewNodeController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.list.newlink' => [
        'path' => "/$baseEditPath/$base_entity_id/newlink/$next_entity_id/$link_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SlogitemNewLinkController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.list.move' => [
        'path' => "/$baseEditPath/$base_entity_id/move/$link_entity_id/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\SlogitemsMoveController::getContentResult",
          'next_entity_id' => 0,
        ],
      ],
      'edit.list.rearrange' => [
        'path' => "/$baseEditPath/$base_entity_id/rearrange/$next_entity_id",
        'defaults' => [
          '_controller' => "{$controllerPath}\ListRearrangeController::getContentResult",
          'next_entity_id' => 0,
        ],
        'options' => self::$optAjaxBase + [
      'parameters' => [
        'base_entity_id' => ['type' => 'entity:slogtx_mt'],
      ],
        ],
      ],
    ];

    return $raw_data;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxEditContent($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $baseEditPath = $baseAjaxPath . '/edit_xtsicontent';
    $controllerPath = '\Drupal\sxt_slogitem\Handler\XtsiEdit\xtsiContent';
    $raw_data = [
      'edit.content.actions' => [
        'path' => "/$baseEditPath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsCoEditController::getContentResult",
        ],
      ],
      'edit.content.node.edit' => [
        'path' => "/$baseEditPath/$base_entity_id/edit",
        'defaults' => [
          '_controller' => "{$controllerPath}\NodeEditController::getContentResult",
        ],
      ],
      'edit.content.tbnode.prepare' => [
        'path' => "/$baseEditPath/$base_entity_id/tbnode/prepare",
        'defaults' => [
          '_controller' => "{$controllerPath}\TbnodePrepareController::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

  private function _rawAjaxPropsContent($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $basePropsPath = $baseAjaxPath . '/props_xtsicontent';
    $controllerPath = '\Drupal\sxt_slogitem\Handler\XtsiProps\xtsiContent';
    $raw_data = [
      'props.content.actions' => [
        'path' => "/$basePropsPath/$base_entity_id/actions",
        'defaults' => [
          '_controller' => "{$controllerPath}\ActionsCoPropsController::getContentResult",
        ],
      ],
      'props.content.crossrefs' => [
        'path' => "/$basePropsPath/$base_entity_id/crossrefs",
        'defaults' => [
          '_controller' => "{$controllerPath}\NodeCrossRefsController::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxBookmarkStorage($baseAjaxPath) {
    $bm_type = '{bookmark_type}';
    $menu_tid = '{menu_tid}';
    $controllerPath = '\Drupal\sxt_slogitem\Handler\Bmstorage';
    $raw_data = [
      'bmstorage.save' => [
        'path' => "/$baseAjaxPath/bmstorage/save/$bm_type/$menu_tid",
        'defaults' => [
          '_controller' => "{$controllerPath}\BmstorageSave::getContentResult",
        ],
      ],
      'bmstorage.open' => [
        'path' => "/$baseAjaxPath/bmstorage/open/$bm_type/$menu_tid",
        'defaults' => [
          '_controller' => "{$controllerPath}\BmstorageOpen::getContentResult",
        ],
      ],
      'bmstorage.view_content' => [
        'path' => "/$baseAjaxPath/bmstorage/view/$bm_type/$menu_tid",
        'defaults' => [
          '_controller' => "{$controllerPath}\BmstorageView::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxUser($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $controllerPath = '\Drupal\sxt_slogitem\Controller\User';
    $raw_data = [
      'profile.devtools_search' => [
        'path' => "/$baseAjaxPath/edit/profile/$base_entity_id/devxtsisearch",
        'defaults' => [
          '_controller' => "{$controllerPath}\profile\DevXtsiSearchController::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

  /**
   * //todo::text
   * 
   * @param string $baseAjaxPath
   * @return array
   */
  private function _rawAjaxOther($baseAjaxPath) {
    $controllerPath = '\Drupal\sxt_slogitem\Handler\Other';
    $raw_data = [
      'mwcomment.editactions' => [
        'path' => "/$baseAjaxPath/mwdiscussion/{node_id}/{mwc_action}/{comment_id}",
        'defaults' => [
          '_controller' => "{$controllerPath}\MwCommentEditActions::getContentResult",
        ],
      ],
    ];

    return $raw_data;
  }

}
