<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\SlogitemInstall.
 */

namespace Drupal\sxt_slogitem;

use Drupal\sxt_slogitem\XtsiNodeTypeData;

/**
 * Static install functions for sxt_slogitem module
 */
class SlogitemInstall {

  /**
   * Implements hook_requirements().
   */
  public static function requirements() {
    $requirements = [];
    $req_id = 'sxt_slogitem_requirements';
    $req_title = 'SlogSys Xtsi';

    $available = [];
    $node_types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    foreach ($node_types as $node_type_id => $node_type) {
      $is_xttype = (boolean) $node_type->get('xtsi_is_xtnodetype');
      if ($is_xttype) {
        $available[] = $node_type_id;
        if (!XtsiNodeTypeData::canXtNodeType($node_type)) {
          $args = ['%type_id' => $node_type_id, '%missing' => 'xtnodebase'];
          $requirements[$req_id] = [
            'title' => $req_title,
            'value' => t('This is not a valid xt-node-type: %type_id. Missing field: %missing.', $args),
            'severity' => REQUIREMENT_ERROR,
          ];
        }
      }
    }

    if (empty($available)) {
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => t('There is no xt-node-type prepared.'),
        'severity' => REQUIREMENT_WARNING,
      ];
    }

    if (empty($requirements)) {
      if (!SlogXtsi::hasBasepagesRedirect()) {
        $requirements["{$req_id}_redirect"] = [
          'title' => "$req_title Redirect",
          'value' => 'Note: redirect base pages is NOT active',
          'description' => 'To change config only for uid=1 (search for basepages_redirect)',
          'severity' => REQUIREMENT_WARNING,
        ];
      }
      if (SlogXtsi::isDebugMode()) {
        $requirements["{$req_id}_debug"] = [
          'title' => "$req_title Debug",
          'value' => 'Note: debug mode is active',
          'description' => 'To change config only for uid=1 (search for debug_mode)',
          'severity' => REQUIREMENT_WARNING,
        ];
      }
    }

    if (empty($requirements)) {
      $str_available = implode(', ', $available);
      $requirements[$req_id] = [
        'title' => $req_title,
        'value' => "Available xt-node-types: $str_available",
//        'description' => $description,
        'severity' => REQUIREMENT_OK,
      ];
    }

    return $requirements;
  }

}
