<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Event\SlogitemEventSubscriber.
 */

namespace Drupal\sxt_slogitem\Event;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\XtsiCronStateData;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtb\SlogTb;
use Drupal\slogxt\XtUserData;
use Drupal\slogtx\Event\SlogtxEvents;
use Drupal\slogtb\Event\SlogtbEvents;
use Drupal\slogxt\Event\SlogxtEvents;
use Drupal\sxt_group\SxtGroup;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Component\Utility\Crypt;

//use Drupal\Core\Url;

/**
 * Slogitem event subscriber.
 */
class SlogitemEventSubscriber implements EventSubscriberInterface {

  const XTSI_ROUTE_BASE = 'sxt_slogitem.base.view';

  /**
   * A router implementation which does not check access.
   *
   * @var \Symfony\Component\Routing\Matcher\UrlMatcherInterface
   */
  protected $accessUnawareRouter;

  protected function getAccessUnawareRouter() {
    if (empty($this->accessUnawareRouter)) {
      $this->accessUnawareRouter = \Drupal::service('router.no_access_checks');
    }
    return $this->accessUnawareRouter;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    //
    //
    $events[KernelEvents::REQUEST][] = ['onKernelRequest', 30];

    //
    $events[KernelEvents::RESPONSE][] = ['onKernelResponse', 9999];

    //
    $events[SlogtbEvents::REGISTER_SLOGTB_HANDLER][] = ['onRegisterSlogtbHandler', 9999];

    //
    //
    $events[SlogtbEvents::SLOGTB_MENU_FORM_ALTER][] = ['onSlogtbMenuFormAlter', 0];

    //
    //
    $events[SlogtbEvents::SLOGTB_HAS_TBMENU_EDIT_PERM][] = ['getTbmenuEditOtherPerm', 9999];

    //
    //
    $events[SlogxtEvents::SLOGXT_HAS_MAIN_EDIT_PERM][] = ['hasMainEditOtherPerm', 9999];

    //
    //
    $events[SlogtxEvents::TX_MENU_TERM_MOVED][] = ['onTxMenuTermMoved', 0];

    //
    //
    $events[SlogtxEvents::TX_MENU_TERM_DELETED][] = ['onTxMenuTermDeleted', 0];

    //
    $events[SlogxtEvents::ACTION_GET_APPLY_ALTER][] = ['onActionGetApplyAlter', 0];

    return $events;
  }

  private function _handleUserRedirect($request) {
    $timestamp = (integer) $request->get('param2');
    $hash = $request->get('param3');
    $param4 = $request->get('param4');
    $isResetLogin = (!empty($param4) && $param4 === 'login');
    if ($isResetLogin && $timestamp && $hash) {
      $session = $request->getSession();
      $token = Crypt::randomBytesBase64(55);
      $_SESSION['pass_reset_' . $request->get('param1')] = $token;
      $request->query->set('pass-reset-token', $token);
    }
  }

  public function onKernelRequest(Event $event) {
    $request = $event->getRequest();
    $route = $request->get('_route', '');
    if ($request->getRealMethod() === 'POST') {
      if ($route == 'sxt_slogitem.user.redirect') {
        $this->_handleUserRedirect($request);
      }

      return;
    }

    $exception = $request->get('exception', FALSE);
    $do_redirect = $is_system_4xx_route = (boolean) preg_match('/^system\.4\d*$/', $route);

    if (!$do_redirect && $exception && $exception instanceof HttpExceptionInterface) {
      $status = $exception->getStatusCode();
      if (!empty($status) && (integer) ($status / 100) === 4) {
        $route = "system.{$status}";
        $do_redirect = $is_system_4xx_route = TRUE;
      }
    }
    if ($is_system_4xx_route) {
      list(, $err_code) = explode('.', $route);
    }

    $core_redirect_user = $request->get('core_redirect_user', FALSE);
    $routes_to_redirect = [];

    if (!$do_redirect && $core_redirect_user) {
      $destination = (string) $request->get('destination');
      if (!empty($destination) && preg_match('/\/admin\//', $destination)) {
        return;
      }

//todo::current::   XtsiRoutes: one route for all user redirections:: 'entity.user.canonical' ???
//todo::current::   XtsiRoutes: one route for all node redirections:: 'entity.node.canonical' ???
//      $user_routes = [
//      ];
      $do_redirect = TRUE;
    }

    $basepages_redirect = SlogXtsi::hasBasepagesRedirect();
    if ($do_redirect && !$basepages_redirect) {
      return;
    }

    if (!$do_redirect) {
      $routes_to_redirect = [
        'slogtb.on.menu_tab_select', // ajax call, allways redirected
        'sxt_slogitem.content.view', // ajax call, node content
        'sxt_slogitem.on.menu_item_select', // ajax call, list content
        'sxt_slogitem.tid.view', // e.g. /xtsi/tid/123
        'sxt_slogitem.sid.view', // e.g. /xtsi/sid/234
        self::XTSI_ROUTE_BASE, // e.g. /xtsi
          //todo::next::redirect node -> like user
          //todo::next::redirect node -> like user
          //todo::next::redirect node -> like user
//        'entity.node.canonical', // e.g. /node/345
      ];

      if ($basepages_redirect) {
        $routes_to_redirect[] = 'entity.node.canonical';
      }

      $do_redirect = in_array($route, $routes_to_redirect);
    }

    if (!$do_redirect) {
      return;
    }

    //
    $redirect_response = FALSE;
    $tempstore = \Drupal::service('user.private_tempstore')->get('sxt_slogitem');
    $url_generator = \Drupal::service('url_generator');

    if ($is_system_4xx_route) {
      $basePath = trim(SlogXt::getBasePath('sxt_slogitem'), '/');
      $path = $url_generator->getPathFromRoute($route);

      if ($route === 'system.403') {
        // prevent endless loop
        $this->_preventEndlessLoop($request, $basePath);
      }

      // prepare redirect and autorun
      $tempstore->set('auto_run_system4xx', "$err_code::4xx/$path");
      $route = self::XTSI_ROUTE_BASE;
    }
    elseif ($core_redirect_user) {
      $basePath = trim(SlogXt::getBaseAjaxPath('sxt_slogitem'), '/');
      $path_info = $request->getPathInfo();
      if ($route === 'user.reset') {
        $session = $request->getSession();
        $session->set('pass_reset_hash', $request->get('hash'));
        $session->set('pass_reset_timeout', $request->get('timestamp'));
        $route = 'user.reset.form';
        $path_info = $url_generator->getPathFromRoute($route, ['uid' => $request->get('uid')]);
      }
      $tempstore->set('auto_run_user', trim($path_info, '/'));
      $route = self::XTSI_ROUTE_BASE;
    }
    elseif ($route === 'slogtb.on.menu_tab_select') {
      $rootterm_id = $request->get('rootterm_id');
      $rootterm = SlogTx::getRootTerm($rootterm_id);
      if ($rootterm && $rootterm->isRootTerm() && $rootterm->hasChildren()) {
        $tbmenu_tid = $rootterm->getStorage()->getFirstChildTid($rootterm_id);
        if (!empty($tbmenu_tid)) {
          $tempstore->set('auto_run_tid', $tbmenu_tid);
          $route = self::XTSI_ROUTE_BASE;
        }
      }
    }
    elseif (in_array($route, ['sxt_slogitem.on.menu_item_select', 'sxt_slogitem.tid.view'])) {
      $tbmenu_tid = $request->get('tbmenu_tid');
      if (!empty($tbmenu_tid) && $route === 'sxt_slogitem.tid.view') {
        if (!$menu_term = SlogTx::getMenuTerm($tbmenu_tid)) {
          $tempstore->set('auto_run_system4xx', '404::4xx/system/404');
        }
      }

      if (!empty($tbmenu_tid)) {
        $tempstore->set('auto_run_tid', $tbmenu_tid);
        $route = self::XTSI_ROUTE_BASE;
      }
    }
    elseif ($route === 'sxt_slogitem.content.view') {
      $slogitem_id = $request->get('slogitem_id');
    }
    elseif ($route === 'entity.node.canonical') {
      if ($node = $request->get('node')) {
        $slogitem_id = $node->get('xtnodebase')->base_sid;
        if (empty($slogitem_id)) {
          $sids = SlogXtsi::getSidsByEntity('node', $node->id(), 1);
          if (!empty($sids)) {
            $slogitem_id = $sids[0];
          }
        }
      }
    }

    if (!$redirect_response && (!empty($slogitem_id) || $route === 'sxt_slogitem.sid.view')) {
      if (empty($slogitem_id)) {
        $slogitem_id = $request->get('slogitem_id', '');
      }

      if (!empty($slogitem_id) && $route === 'sxt_slogitem.sid.view') {
        if (!$slogitem = SlogXtsi::loadSlogitem($slogitem_id)) {
          $tempstore->set('auto_run_system4xx', '404::4xx/system/404');
        }
      }

      if (!empty($slogitem_id)) {
        $tempstore->set('auto_run_sid', $slogitem_id);
        $route = self::XTSI_ROUTE_BASE;
      }
    }

    if (!$redirect_response && $route === self::XTSI_ROUTE_BASE) {
      list($request_hash, $current_hash) = $this->_getHashes($request);
      if ($request_hash !== $current_hash) {
        $url = $url_generator->generateFromRoute($route, ['xtsi_userrole_hash' => $current_hash]);
        $redirect_response = new RedirectResponse($url);
      }
    }

    if ($redirect_response) {
      $event->setResponse($redirect_response);
      $event->stopPropagation();
    }
    else {
      $auto_run_system4xx = $tempstore->get('auto_run_system4xx');
      $auto_run_user = empty($auto_run_system4xx) ? $tempstore->get('auto_run_user') : FALSE;
      $tbmenu_tid = $tempstore->get('auto_run_tid');
      $slogitem_id = $tempstore->get('auto_run_sid');
      $tempstore->delete('auto_run_system4xx');
      $tempstore->delete('auto_run_user');
      $tempstore->delete('auto_run_tid');
      $tempstore->delete('auto_run_sid');

      if (empty($auto_run_system4xx) && empty($auto_run_user)) {
        if (!empty($tbmenu_tid) && empty($slogitem_id)) {
          $sids = SlogXtsi::getSidsFromTidsSorted([$tbmenu_tid], 1);
          if (!empty($sids)) {
            $slogitem_id = $sids[0];
          }
          elseif ($menu_term = SlogTx::getMenuTerm($tbmenu_tid)) {
            $vocabulary = $menu_term->getVocabulary();
            list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
            $slogtb_handler = SlogTb::getSlogtbHandler();
            $list_path = $slogtb_handler->getPathPrefix() . $tbmenu_tid;
            $menu_data = [
              'content' => FALSE,
              'list' => [
                'path' => $list_path,
                'tid' => $tbmenu_tid,
                'toolbar' => $toolbar,
                'toolbartab' => $toolbartab,
              ],
            ];
          }
        }
      }

      if (!empty($auto_run_system4xx)) {  //handle error: 403, 404, ...
        list($err_code, $path) = explode('::', $auto_run_system4xx);
        SlogXtsi::setJsAutorun([
          'fromPath' => [
            'type' => 'msgDialog',
            'data' => [
              'type' => 'path',
              'status' => 'error',
              'title' => SlogXt::htmlErrText($err_code),
              'path' => $path,
              'provider' => 'sxt_slogitem',
              'cachable' => TRUE,
            ],
          ]
        ]);
      }
      elseif (!empty($auto_run_user)) {
        SlogXtsi::setJsAutorun([
          'fromPath' => [
            'type' => 'userDialog',
            'data' => $auto_run_user,
          ]
        ]);
      }
      elseif (!empty($slogitem_id) && $slogitem = SlogXtsi::loadSlogitem($slogitem_id)) {
        SlogXtsi::setJsAutorun([
          'fromPath' => [
            'type' => 'xtsiContent',
            'data' => $slogitem->getAttachData(),
          ],
        ]);
      }
      elseif (!empty($tbmenu_tid) && !empty($menu_data)) {
        SlogXtsi::setJsAutorun([
          'fromPath' => [
            'type' => 'xtsiList',
            'data' => $menu_data,
          ],
        ]);
      }
      else {
        SlogXtsi::setJsAutorun(['fromPath' => FALSE]);
      }
    }
  }

  public function onKernelResponse(Event $event) {
    $response = $event->getResponse();
    if ($response instanceof HtmlResponse && !SlogXt::isAdminRoute()) {
      $attachments = $this->_getHtmlAttachments() + ['autorun' => SlogXtsi::getJsAutorun()];
      $to_add['drupalSettings']['sxt_slogitem'] = $attachments;
      $response->addAttachments($to_add);
    }
  }

  function _getHtmlAttachments() {
    $attachments = [];
    // attach if visible slog toorbar block exist
    if (SlogTb::hasSlogtbBlock(SlogTb::STB_ID_TOOLBAR, TRUE)) {
      $action_manager = SlogXt::pluginManager('action');
      $tblineList = $action_manager->getActionsData('xtsi_tbline_list', TRUE);
      $tblineContent = $action_manager->getActionsData('xt_tbline_content', TRUE);
      $actions = [
        'dialogHistory' => $action_manager->getActionsData('xt_dialog_history', FALSE, TRUE),
        'dialogBookmark' => $action_manager->getActionsData('xt_dialog_bookmark', FALSE, TRUE),
        'dialogSurprise' => $action_manager->getActionsData('xt_dialog_surprise', FALSE, TRUE),
      ];

      $base_path = SlogXt::getBasePath('sxt_slogitem');
      $attachments = [
        'basePath' => $base_path,
        'baseAjaxPath' => SlogXt::getBaseAjaxPath('sxt_slogitem'),
        'editableList' => SlogXtsi::getJsTbEditable('xtsi_liedit'),
        'editableContent' => SlogXtsi::getJsTbEditable('xtsi_coedit'),
        'tblineList' => $tblineList,
        'tblineContent' => $tblineContent,
        'maxItems' => [
          'history' => SlogXtsi::getHistoryMaxItems(),
          'bookmark' => SlogXtsi::getBookmarkMaxItems(),
        ],
        'bmstorage_tids' => [
          'list' => $this->_getBmStorageTids('list'),
          'content' => $this->_getBmStorageTids('content'),
        ],
        'actions' => $actions,
        'tbsubmenu' => [],
      ];
    }

    return $attachments;
  }

  function _getBmStorageTids($bmtype, $as_string = FALSE) {
    $bm_tids = [];
    if (\Drupal::currentUser()->isAuthenticated()) {
      $base_entity_id = 'bmstorage' . SlogXt::XTURL_DELIMITER . $bmtype;
      $result = SlogTb::getXtSysRootTermFormArg($base_entity_id);
      $rootterm = $result['value'];

      if (!$rootterm || !$rootterm->isRootTerm()) {
        $message = t("Root term not found for bookmark type %bmtype", ['%bmtype' => $bmtype]);
        throw new \LogicException($message);
      }

      $vocabulary = $rootterm->getVocabulary();
      $bm_tids = SlogTx::entityStorage('slogtx_mt')->getTreeTermIds($vocabulary->id(), $rootterm->id());
    }

    if ($as_string) {
      $bm_tids = implode(',', $bm_tids);
    }

    return $bm_tids;
  }

  private function _getHashes($request) {
    $user = \Drupal::currentUser();
    $default_role = XtUserData::getDefaultRole($user);
    $current_uid = $user->id();
    $current_hash = Crypt::hashBase64("uid:$current_uid;role:$default_role");
    $request_hash = $request->get('xtsi_userrole_hash', '');
    return [$request_hash, $current_hash];
  }

  private function _preventEndlessLoop($request, $basePath) {
    $accessUnawareRouter = $this->getAccessUnawareRouter();
    $request_context = clone ($accessUnawareRouter->getContext());
    $request_context->setMethod('GET')->setPathInfo($basePath);
    $accessUnawareRouter->setContext($request_context);
    $front_match = $accessUnawareRouter->match('');

    $request_stack = \Drupal::requestStack();
    $rstack = [];
    $tmp_request = $request;
    while ($parent_request = $request_stack->getParentRequest()) {
      $tmp_request->attributes->replace($front_match);
      $tmp_request->query->replace([]);
      $tmp_request->request->replace([]);
      $tmp_request = $parent_request;
      array_push($rstack, $request_stack->pop());
    }
    while ($tmp_request = array_pop($rstack)) {
      $request_stack->push($tmp_request);
    }
  }

  /**
   * Set the slog toolbar handler in $event->slogtbHandlerClass.
   * 
   * - Set the class as string
   * - The class has to implement Drupal\slogtb\Handler\SlogtbHandlerInterface
   */
  public function onRegisterSlogtbHandler(Event $event) {
    $event->slogtbHandlerClass = 'Drupal\sxt_slogitem\Handler\XtsiSlogtbHandler';
  }

  /**
   * 
   * @param Event $event
   */
  public function onSlogtbMenuFormAlter(Event $event) {
    $data = $event->getData();
    $form_id = $data[2] ?: '';
    if (in_array($form_id, [
          'slogtx_mt__sys__bmstorage_content_form',
          'slogtx_mt__sys__bmstorage_list_form',
        ])) {
      $form = & $data[0];

      if (is_array($form['name'])) {
        $element = &$form['name']['widget'][0];
        $element['#title'] = $element['value']['#title'] = t('Title');
        $form['name']['#description_display'] = 'invisible';
      }

      if (is_array($form['description'])) {
        $element = &$form['description']['widget'][0];
        $element['#title'] = $element['value']['#title'] = t('Raw data');
        $element['#attributes']['disabled'] = 'disabled';
        $form['description']['#attributes']['class'][] = 'visually-hidden';
      }
    }
  }

  /**
   * 
   * @param Event $event
   */
  public function getTbmenuEditOtherPerm(Event $event) {
    $data = $event->getData();
    if (empty($data['result']['has_perm'])) {
      $affected = [
        'slogtb_edit_tbmenu_edit',
        'slogtb_edit_tbmenu_new',
        'slogtb_edit_tbmenu_rearrange',
      ];
      $plugin_ids = empty($data['plugin_id']) ? $affected : [$data['plugin_id']];
      foreach ($plugin_ids as $plugin_id) {
        if (in_array($plugin_id, $affected)) {
          foreach (['promoted', 'surprise'] as $item) {
            if (SxtGroup::hasPermission("admin xtsi-$item")) {
              $data['result']['has_perm'] = TRUE;
              // all done
              return;
            }
          }
        }
      }
    }
  }

  /**
   * 
   * @param Event $event
   */
  public function hasMainEditOtherPerm(Event $event) {
    $data = $event->getData();
    if (empty($data['result']['has_perm'])) {
      foreach (['promoted', 'surprise'] as $item) {
        if (SxtGroup::hasPermission("admin xtsi-$item")) {
          $data['result']['has_perm'] = TRUE;
          // all done
          return;
        }
      }
    }
  }

  /**
   * 
   * @param Event $event
   */
  public function onActionGetApplyAlter(Event $event) {
    $affected = [
      'slogxt_xtmain_trash',
      'slogxt_xtmain_archive',
    ];
    $data = $event->getData();
    if (in_array($data['plugin_id'], $affected)) {
      $can_apply = & $data['can_apply'];

      $source = !empty($can_apply['source']) ? $can_apply['source'] . ',' : '';
      $target = !empty($can_apply['target']) ? $can_apply['target'] . ',' : '';

      $can_apply['source'] = $source . 'xtsi_slogitem_move';
      $can_apply['target'] = $target . 'xtsi_slogitem_move,xtsi_list_rearrange';
    }
  }

  /**
   * Add affected Node ids to the cron stack.
   * 
   * - Not if the ranking is not affected.
   * - Ranking may be by toolbars or by vocabularies.
   * 
   * @param Event $event
   */
  public function onTxMenuTermMoved(Event $event) {
    $data = $event->getData();
    if (SlogXtsi::rankByVocabulary()) {
      if ($data['source_vid'] === $data['target_vid']) {
        return;
      }
    }
    else {
      if ($data['source_tbid'] === $data['target_tbid']) {
        return;
      }
    }

    $moved_term = SlogTx::getMenuTerm($data['moved_menu_tid']);
    $tree_terms = $moved_term->getSubtree();

    $moved_tids = [$data['moved_menu_tid']];
    if (!empty($tree_terms)) {
      foreach ($tree_terms as $item) {
        $moved_tids[] = $item->tid;
      }
    }

    $sids = SlogXtsi::getSidsFromTids($moved_tids);
    $node_ids = SlogXtsi::getNodeIdsBySids($sids);
    XtsiCronStateData::pushNodeIds($node_ids);
  }

  /**
   * Delete slog items for deleted menu term.
   * 
   * @param Event $event
   */
  public function onTxMenuTermDeleted(Event $event) {
    $storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    $menu_terms = $event->getEntities();
    foreach ($menu_terms as $tid => $menu_term) {
      $storage->deleteByTid($tid);
    }
  }

}
