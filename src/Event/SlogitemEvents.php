<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Event\SlogitemEvents.
 */

namespace Drupal\sxt_slogitem\Event;

/**
 * Defines events thrown by sxt_slogitem.
 */
final class SlogitemEvents {

  /**
   * Retrieve special data for an entity
   * 
   * - Pass entity in $event->getData()[0]
   * - Return data in $event->entitySpecialData
   * - For now only for node entity realized
   * 
   */
  const XTSI_SPECIAL_ENTITY_DATA = 'sxt_slogitem.special_entity_data';
  
}
