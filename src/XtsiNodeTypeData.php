<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\XtsiNodeTypeData.
 */

namespace Drupal\sxt_slogitem;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_group\SxtGroup;
use Drupal\sxt_slogitem\XtsiCronStateData;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\Role;
use Drupal\node\NodeTypeInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Static functions and callbacks for extended node type data.
 */
class XtsiNodeTypeData {

  public static function nodeGetBaseSid(NodeInterface $node) {
    $xtnodebase = $node->get('xtnodebase');
    return $xtnodebase ? $xtnodebase->base_sid : 0;
  }

  public static function nodeGetBaseTB(NodeInterface $node) {
    $xtnodebase = $node->get('xtnodebase');
    return $xtnodebase ? $xtnodebase->base_tb : '';
  }

  public static function nodeGetRankSids(NodeInterface $node) {
    $xtnodebase = $node->get('xtnodebase');
    return $xtnodebase ? $xtnodebase->rank_sids : '';
  }

  public static function nodeGetBaseSlogitem(NodeInterface $node) {
    $sid = self::nodeGetBaseSid($node);
    if (!$sid) {
      self::setXtNodeValues($node);
      $sid = self::nodeGetBaseSid($node);
    }
    $slogitem = SlogXtsi::loadSlogitem($sid);
    if ($slogitem->getTargetEntityId() !== $node->id()) {
      self::setXtNodeValues($node);
      $sid = self::nodeGetBaseSid($node);
      $slogitem = SlogXtsi::loadSlogitem($sid);
    }
    
    return $slogitem;
  }

  /**
   * Implements hook_node_access().
   */
  public static function access(NodeInterface $node, $op, $account) {
    $account = $account ?: \Drupal::currentUser();
    $has_perm = FALSE;
    if ($op === 'view') {
      return AccessResult::allowed();
    }
    elseif ($op === 'delete') {
      return AccessResult::forbidden();
    }
    elseif (in_array($op, ['create', 'update'])) {
      $toolbar_id = self::getPermBaseTbIdByNode($node);
      if (empty($toolbar_id) || $toolbar_id === '__error__') {
        $has_perm = $node->isNew() ? TRUE : $account->hasPermission('administer nodes');
      }
      elseif ($toolbar_id === 'user') {
        $xtnodebase_sid = self::nodeGetBaseSid($node);
        if ($uid = self::getTargetEntityIdFromBaseSid($xtnodebase_sid)) {
          $has_perm = ($account->id() == $uid);
        }
      }
      elseif ($toolbar_id === 'role') {
        $xtnodebase_sid = self::nodeGetBaseSid($node);
        $role_id = self::getTargetEntityIdFromBaseSid($xtnodebase_sid);
        if ($role_id && $role = Role::load($role_id)) {
          $has_perm = SxtGroup::hasPermission('admin sxtrole-content', $account, $role);
          if (!$has_perm) {
            $has_perm = SxtGroup::hasPermission('edit sxtrole-content', $account, $role);
          }
        }
      }
      else {
        $vg_tbs = SlogTx::getToolbarsVisibleGlobal();
        if (in_array($toolbar_id, array_keys($vg_tbs))) {
          $has_perm = $account->hasPermission("administer toolbar $toolbar_id");
        }
      }
    }
    else {
      // No opinion.
      return AccessResult::neutral();
    }

    return ($has_perm ? AccessResult::allowed() : AccessResult::forbidden());
  }

  public static function nodeTbPreventByDepth(NodeInterface $node) {
    $xtnodebase_tb = self::nodeGetBaseTB($node);
    $parts = explode(';', $xtnodebase_tb);
    if (count($parts) > 2) {
      $sxtbase_tb_depth = (integer) $parts[2];
      if ($parts[0] === SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
        return ($sxtbase_tb_depth >= SlogXtsi::getTbnodeMaxDepth());
      }
    }

    return FALSE;
  }

  public static function getPermBaseTbIdByNode(NodeInterface $node) {
    $xtnodebase_tb = self::nodeGetBaseTB($node);
    return self::getPermBaseTbId($xtnodebase_tb);
  }

  public static function getPermBaseTbId($xtnodebase_tb) {
    $xtnodebase_tb = $xtnodebase_tb ?: '__error__';
    $parts = explode(';', $xtnodebase_tb);
    if (count($parts) > 1) {
      $xtnodebase_tb = $parts[1];
    }
    return $xtnodebase_tb;
  }

  public static function getTargetEntityIdFromBaseSid($xtnodebase_sid) {
    if ($slogitem = SlogXtsi::getSlogitem($xtnodebase_sid)) {
      try {
        $menu_term = $slogitem->getTerm();
        $vocabulary = $menu_term->getVocabulary();
        $target_term = $menu_term->getTargetTerm();
        $te_plugin = $vocabulary->getTargetEntityPlugin();
        $eid = $te_plugin->getTargetEntityId($target_term->label());
        return $eid;
      }
      catch (Exception $ex) {
        // return NULL
      }
    }

    return NULL;
  }

  /**
   * Add fields to config_export for entity node_type.
   * 
   * @see data for @ConfigEntityType annotation in NodeType
   * 
   * @param array $entity_types
   *  Array of Drupal\Core\Entity\EntityTypeInterface
   */
  public static function entityTypeBuild(array &$entity_types) {
    if (isset($entity_types['node_type'])) {
      $node_type = $entity_types['node_type'];
      $config_export = $node_type->get('config_export');
      $config_export[] = 'xtsi_is_xtnodetype';
      $node_type->set('config_export', $config_export);
    }
  }

  /**
   * Add form fields for sxt_slogitem module.
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function formAlter(&$form, FormStateInterface $form_state) {
    $node_type = $form_state->getFormObject()->getEntity();
    $canSetXtNodeType = self::canXtNodeType($node_type);
    $xttype_value = (boolean) $node_type->get('xtsi_is_xtnodetype');

    $form['sxt_slogitem'] = [
      '#type' => 'details',
      '#title' => t('Xtsi settings'),
      '#group' => 'additional_settings',
    ];

    $form['sxt_slogitem']['xtsi_is_xtnodetype'] = [
      '#type' => 'checkbox',
      '#title' => t('Active as Xt-Node-Type'),
      '#description' => t('Select for activating this node type as xt-node-type.'),
      '#default_value' => ($canSetXtNodeType && $xttype_value),
      '#disabled' => !$canSetXtNodeType,
    ];
    $form['additional_settings']['#default_tab'] = 'edit-sxt-slogitem';

    if (!$canSetXtNodeType) {
      $hint = '';
      if ($xttype_value) {
        $hint = '<br /><br />' . t('Add the required field or save for fixing missmatch.'); 
      }
      $msgtype = $xttype_value ? 'error' : 'warning';
      $message = t('You have to add the field %name before activating this setting.', ['%name' => 'xtnodebase']);
      $form['sxt_slogitem']['xtsi_is_xtnodetype_warning'] = [
        '#markup' => $message . $hint,
        '#prefix' => '<div class="messages messages--' . $msgtype . '">',
        '#suffix' => '</div>',
      ];
    }

    $class = get_class();
    $form['#validate'][] = "$class::formValidate";
  }

  /**
   * Reteun an array of extra fields of the node type.
   * 
   * @param NodeTypeInterface $node_type
   * @return array
   */
  public static function getNodeExtraFields(NodeTypeInterface $node_type) {
    return \Drupal::entityManager()->getStorage('field_config')
            ->loadByProperties(['entity_type' => 'node', 'bundle' => $node_type->id()]);
  }

  /**
   * Whether the node type can handle as xt-node-type.
   * 
   * @param NodeTypeInterface $node_type
   * @return boolean
   */
  public static function canXtNodeType(NodeTypeInterface $node_type) {
    $fields = self::getNodeExtraFields($node_type);
    $bundle = $node_type->id();
    $field_id = "node.$bundle.xtnodebase";

    return !empty($fields[$field_id]);
  }

  /**
   *  Retrive all node types that are xt-node-type.
   * 
   * @return array of NodeTypeInterface objects
   */
  public static function getXtNodeTypes($sort_by_label = FALSE) {
    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $xtnode_types = $labels = [];
    foreach ($types as $type_id => $node_type) {
      if ($is_xttype = (boolean) $node_type->get('xtsi_is_xtnodetype')) {
        if (!self::canXtNodeType($node_type)) {
          $args = ['@type' => $type_id, '@missing' => 'xtnodebase'];
          $msg = t('Missing required fields in xt-activated type @type: @missing', $args);
          \Drupal::logger('sxt_slogitem')->error($msg);
        }

        $xtnode_types[$type_id] = $node_type;
        $labels[$type_id] = $node_type->label();
      }
    }

    if ($sort_by_label) {
      $sorted = [];
      natcasesort($labels);
      foreach ($labels as $type_id => $label) {
        $sorted[$type_id] = $xtnode_types[$type_id];
      }
      $xtnode_types = $sorted;
    }

    return $xtnode_types;
  }

  /**
   *  Retrive the ids of all node types that are xt-node-type.
   * 
   * @return array of node type ids
   */
  public static function getXtNodeTypeIds($sort_by_label = FALSE) {
    $node_types = self::getXtNodeTypes($sort_by_label);
    return array_keys($node_types);
  }

  /**
   * Return sorted array for ranking toolbars.
   * 
   * @return array
   */
  public static function getXtToolbarsRank() {
    $toolbars = SlogXt::getToolbarsRankSorted(TRUE);
    $idx = 0;
    $rank_tb = [];
    foreach ($toolbars as $toolbar_id => $toolbar) {
      $rank_tb[$toolbar_id] = sprintf("x%'.03d", ++$idx);
    }

    return $rank_tb;
  }

  /**
   * Return the base xt-node values from node, if xt-node.
   * 
   * @param NodeInterface $node
   * @return array|FALSE
   *  Array with base fields base_sid, base_tb and rank_sids (if exist)
   */
  public static function getXtNodeBaseValues(NodeInterface $node) {
    $xtnode_values = FALSE;
    $xtnodebase = $node->get('xtnodebase');
    if (!empty($xtnodebase)) {
      $xtnode_values = [
        'base_sid' => $xtnodebase->base_sid,
        'base_tb' => $xtnodebase->base_tb,
        'rank_sids' => $xtnodebase->rank_sids,
      ];
    }
    return $xtnode_values;
  }

  /**
   * Return node's ranking values or FALSE.
   * 
   * - Search all slogitem with entity id is node id
   * - Get base sid and base toolbar id
   * - Get more sids ranked by toolbars or vocabularies
   * 
   * Ranking by vocabularies ist set by Xtsi-settings
   * 
   * @param NodeInterface $node
   * @return array|FALSE 
   *  array with values if any, otherwise FALSE
   */
  public static function findXtNodeBaseValues(NodeInterface $node) {
    $xtnode_values = FALSE;
    $rank_tb = self::getXtToolbarsRank();
    $storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    $rank_by_vocabulary = SlogXtsi::rankByVocabulary();

    $sids = $storage->getSidsByEntity('node', $node->id());
    if (!empty($sids)) {
      $weight = 0;
      $prepared = [];
      $toolbar_ids = [];
      foreach ($sids as $sid) {
        $slogitem = $storage->load($sid);
        $menu_term = $slogitem->getTerm();
        $vocabulary = $menu_term->getVocabulary();
        $toolbar_id = $vocabulary->getToolbarId();
        if (!empty($rank_tb[$toolbar_id])) {
          $weight++;
          $rank_key = $rank_by_vocabulary ? $vocabulary->id() : $toolbar_id;
          $key = $rank_tb[$toolbar_id] . $rank_key . ($weight / 1000000);
          $prepared[$key] = $sid;
          $toolbar_ids[$sid] = $toolbar_id;
        }
      }

      if (!empty($prepared)) {
        ksort($prepared);
        $sorted = array_values($prepared);
        $xtnodebase_sid = array_shift($sorted);
        $xtnoderank_sids = implode(';', $sorted);
        //maybe $xtnodebase_sid is in toolbar _node itself: .....
        $xtnodebase_tb = $toolbar_ids[$xtnodebase_sid];
        if ($xtnodebase_tb === SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
          $parts = [$xtnodebase_tb];
          $parent_base_tb = self::findTbnodeBaseTb($node->id(), $xtnodebase_sid, $depth);
          $parts[] = self::getPermBaseTbId($parent_base_tb);
          $parts[] = $depth;
          $xtnodebase_tb = implode(';', $parts);
        }

        $xtnode_values = [
          'base_sid' => $xtnodebase_sid,
          'base_tb' => $xtnodebase_tb,
          'rank_sids' => $xtnoderank_sids,
        ];
      }
    }

    return $xtnode_values;
  }

  public static function findTbnodeBaseTb($node_id, $xtnodebase_sid, &$depth) {
    $storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    $max_depth = SlogXtsi::getTbnodeMaxDepth();
    $tested_nids = [$node_id];
    $err_result = '__error__';
    $xtnodebase_tb = $err_result;
    $depth = 0;

    $sid = $xtnodebase_sid;
    for ($idx = 0; $idx < $max_depth; $idx++) {
      $slogitem = $storage->load($sid);
      $menu_term = $slogitem->getTerm();
      $vocabulary = $menu_term->getVocabulary();
      $target_term = $menu_term->getTargetTerm();
      $te_plugin = $vocabulary->getTargetEntityPlugin();
      $parent_node = $te_plugin->getEntityFromTargetTermName($target_term->label());
      if (!$parent_node) {
        $args = [
          '@node_id' => $node_id,
          '@vid' => $vocabulary->id(),
        ];
        $msg = t('TbnodeBaseTb not found: @vid/@node_id', $args);
        \Drupal::logger('sxt_slogitem')->error($msg);
        $xtnodebase_tb = FALSE;
        break;
      }
      
      $parent_nid = $parent_node->id();
      $xtnodebase_tb = self::nodeGetBaseTB($parent_node);
      $sxtbase_tb_depth = 0;
      if (empty($xtnodebase_tb) || self::needsRebuildBaseTb($parent_nid)) {
        if (in_array($parent_nid, $tested_nids)) {
          $msg = t('Circular call for node: %node_id', ['%node_id' => $node_id]);
          \Drupal::logger('sxt_slogitem')->error($msg);
          return $err_result;
        }

        $tested_nids[] = $parent_nid;
        $parent_node = self::setXtNodeValues($parent_node);
        $xtnodebase_tb = self::nodeGetBaseTB($parent_node);
        $parts = explode(';', $xtnodebase_tb);
        if (count($parts) > 2) {
          $xtnodebase_tb = $parts[1];
          $sxtbase_tb_depth = (integer) $parts[2];
        }
      }

      if ($xtnodebase_tb !== SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
        $depth = $sxtbase_tb_depth + 1;
        break;
      }

      // next run
      $xtnodebase_tb = $err_result;
      $sid = self::nodeGetBaseSid($node);
    }

    if (empty($xtnodebase_tb)) {
      $xtnodebase_tb = $err_result;
    }
    elseif ($xtnodebase_tb === $err_result) {
      $depth = -1;
      $msg = t('Maximum depth exceeded for node: %node_id', ['%node_id' => $node_id]);
      \Drupal::logger('sxt_slogitem')->error($msg);
    }

    return $xtnodebase_tb;
  }

  /**
   * Retrieve ranking values and write them to the node.
   * 
   * - Write only if any value changed
   * 
   * @param integer|NodeInterface $node
   *  Either node id or node instance
   */
  public static function setXtNodeValues($node) {
    if (!$node instanceof NodeInterface) {
      $node = Node::load($node);
    }

    if ($node) {
      $curvalues = self::getXtNodeBaseValues($node);
      if ($xtnode_values = self::findXtNodeBaseValues($node)) {
        $changed = ($curvalues['base_sid'] != $xtnode_values['base_sid']  //
            || $curvalues['base_tb'] != $xtnode_values['base_tb']  //
            || $curvalues['rank_sids'] != $xtnode_values['rank_sids']   //
            );
        if ($changed) {
          $node->xtnodebase->setValue($xtnode_values);
          $node->save();
        }
      }
      elseif (!empty($curvalues)) {
        $xtnode_values = [
          'base_sid' => 0,
          'base_tb' => '',
          'rank_sids' => '',
        ];
        $node->xtnodebase->setValue($xtnode_values);
        $node->save();
      }
    }

    return $node;
  }

  public static function isXtNode(NodeInterface $node) {
    $node_type = $node->getEntityType();
    return (!empty($node->xtnodebase) && (boolean) $node_type->get('xtsi_is_xtnodetype'));
  }

  public static function needsRebuildBaseTb($node_id) {
    $cron_key = 'menumove';
    $cron_data = XtsiCronStateData::getCronStateData($cron_key);
    if (!empty($cron_data)) {
      foreach ($cron_data as $key => $single) {
        $single_array = explode(';', $single);
        if (in_array($node_id, $single_array)) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  public static function onNodeChanged(NodeInterface $node) {
//todo::current::permissions::_node - owner/access
    static $on_work = [];
    $node_id = $node->id();
    if (!empty($on_work[$node_id])) {
      return;
    }

    $on_work[$node_id] = TRUE;
    if (self::isXtNode($node)) {
      self::setXtNodeValues($node);
    }
    unset($on_work[$node_id]);
  }

}
