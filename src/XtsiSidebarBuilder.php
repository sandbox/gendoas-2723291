<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\XtsiSidebarBuilder.
 */

namespace Drupal\sxt_slogitem;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Url;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;

/**
 * Defines a class to build a listing of user entities.
 *
 * @see \Drupal\user\Entity\User
 */
class XtsiSidebarBuilder extends EntityListBuilder {

  /**
   * Toolbar id.
   *
   * @var string 
   */
  protected $toolbar;

  /**
   * Vocabulary part of the vocabulary id.
   *
   * @var string 
   */
  protected $toolbartab;

  /**
   * Term id of the selected menu item.
   *
   * @var integer
   */
  protected $tid;

  /**
   * Sets an array of values as object properties.
   *
   * @param array $values
   *   Array with values indexed by property name.
   * @param bool $override
   *   (optional) Whether to override already set fields, defaults to TRUE.
   *
   * @return $this
   */
  public function setValues(array $values, $override = TRUE) {
    foreach ($values as $key => $value) {
      if (property_exists($this, $key) && ($override || !isset($this->$key))) {
        $this->$key = $value;
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $slogitems = SlogXtsi::loadSlogitems($this->tid);
    $valid_items = [];
    if (!empty($slogitems)) {
      foreach ($slogitems as $item) {
        if ($is_valid = ($item->isValidTerm() && $item->isValidTarget(TRUE))) {
          if ($item->isRedirectItem()) {
            $item = $item->getRedirectTargetItem();
            $is_valid = $item->isValidTerm();
          }
          $term = $item->getTerm();
          if ($term = $item->getTerm() && TRUE) {
            $valid_items[] = $item;
          }
        }
      }
    }
    
    return $valid_items;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $slogitem) {
    if ($slogitem->isRedirectItem()) {
      $slogitem = $slogitem->getRedirectTargetItem();
    }
    
    //. '  #' . $slogitem->get('weight')->value
    if ($slogitem->access('view')) {
//      $entity = $slogitem->getTargetEntity();
      $sid = $slogitem->id();
      $xtsi_route = $slogitem->getRouteKey(TRUE);
      $path = SlogXt::getBaseAjaxPath('sxt_slogitem') . "sid/$sid";
      $url = Url::fromUri("base:$path");
      $row = [
        '#type' => 'link',
//        '#title' => $slogitem->label() . ':: ' . $entity->label(),
        '#title' => $slogitem->label(),
        '#url' => $url,
        '#attributes' => [
          'id' => Html::getId("{$sid}-{$xtsi_route}"),
          'class' => ["xtsi-ajax"],
          'data-toolbar' => $this->toolbar,
        ],
      ];
      return $row;
    }
  }

  /**
   * {@inheritdoc}
   * 
   * Build an unordered list, not a table.
   * 
   */
  public function build() {
    $items = [];
    $settings = ['sids' => [], 'hashes' => [], 'entities' => []];
    foreach ($this->load() as $slogitem) {
      if ($row = $this->buildRow($slogitem)) {
        $sid = $slogitem->id();
        $items[$sid] = $row;
        if ($prepared = $slogitem->hashPrepare(FALSE)) { // FALSE: ensure building (do not change)
          $settings['sids'][$prepared['sid']] = $prepared['cid'];
          $settings['hashes'][$prepared['cid']] = $prepared['hash'];
          $settings['entities'][$sid] = [
            'type' => $slogitem->getTargetEntityType(),
            'eid' => $slogitem->getTargetEntityId(),
          ];
          $xtsi_route = $slogitem->getRouteKey(TRUE);
          $settings['byEntity'][$xtsi_route] = $sid;
        }
      }
    }
    $item_list = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    $tid_hashes = array_values($settings['hashes']);
    $settings['tidHash'] = Crypt::hashBase64(serialize($tid_hashes));
    
    $build = [
      'item_list' => $item_list,
      '#prefix' => '<div class="xtsi-list">',
      '#suffix' => '</div>',
      '#xtsiSettings' => $settings,
    ];

    return $build;
  }

}
