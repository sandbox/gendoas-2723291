<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Annotation\XtsiTargetEntity.
 */

namespace Drupal\sxt_slogitem\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an xtsi target entity annotation object.
 *
 * @see \Drupal\sxt_slogitem\XtsiTargetEntityPluginManager
 *
 * @Annotation
 */
class XtsiTargetEntity extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the provider that owns the plugin.
   *
   * @var string
   */
  public $provider;

  /**
   * The human-readable name of the plugin.
   *
   * This is used as an administrative summary of what the plugin does.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * Additional administrative information about the plugin's behavior.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

  /**
   * Whether this plugin is enabled or disabled by default.
   *
   * @var bool (optional)
   */
  public $status = TRUE;

}
