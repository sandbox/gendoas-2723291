<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\SlogitemAccessController.
 */

namespace Drupal\sxt_slogitem;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Language\LanguageInterface;

/**
 * Defines the access controller for the slogitem entity type.
 */
class SlogitemAccessController extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
//    $account = $this->prepareUser($account);
    if ($return_as_object) {
      return AccessResult::allowed();
    }
    return TRUE;
  }


}
