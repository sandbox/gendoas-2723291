<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\SlogitemListBuilder.
 */

namespace Drupal\sxt_slogitem;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\slogtx\Entity\MenuTerm;



// maybe to build the list by views ?????????????????????????
// see hook_views_query_substitutions in user.views_execution.inc
//                  function user_views_query_substitutions(ViewExecutable $view) {
//                    return ['***CURRENT_USER***' => \Drupal::currentUser()->id()];
//                  }


/**
 * Defines a class to build a listing of slogitem entities.
 *
 * @see \Drupal\sxt_slogitem\Entity\Slogitem
 */
class SlogitemListBuilder extends DraggableListBuilder {

  /**
   * Menu term instance of the selected menu item.
   * 
   * @var integer
   */
  protected $slogtx_mt;

  /**
   * Term id of the selected menu item.
   *
   * Selected menu item's ID is passed by route path.
   *
   * @var integer
   */
  protected $tid;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_list_builder';
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    //todo::config::default list items number (defaults to 40)
    return SlogXtsi::loadSlogitems($this->tid);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->labelExt();
    $row += parent::buildRow($entity);
    // fix weight
    $row['#weight'] = $entity->getWeight();
    $row['weight']['#default_value'] = $row['#weight'];

    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    if (!$this->slogtx_mt = $request->get('slogtx_mt')) {
      $this->slogtx_mt = $request->get('base_entity_id');
    }
    if (!$this->slogtx_mt || !$this->slogtx_mt instanceof MenuTerm) {
      throw new \LogicException(t('Menu term not found.'));
    }


    $this->tid = $this->slogtx_mt->id();
    $form = parent::buildForm($form, $form_state);

    $table = & $form['entities'];
    $table['#id'] = 'xtsiitemlist';
    $table['#attributes'] = ['id' => $table['#id']];
    $form['entities']['#attached']['library'][] = 'slogtx_ui/tabledrag';
    
    return $form;
  }

  /**
   * {@inheritdoc}
   * 
   * Override \Drupal\Core\Config\Entity\DraggableListBuilder::submitForm()
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue($this->entitiesKey) as $id => $value) {
      if (isset($this->entities[$id]) && $this->entities[$id]->get($this->weightKey) != $value['weight']) {
        // Save entity only when its weight was changed.
        $this->entities[$id]->setWeight(-1 * $value['weight']);
        $this->entities[$id]->save();
      }
    }

    drupal_set_message(t('Slogitem list has been saved.'));

    //todo::cache - clear cache for sidebar list
  }

}
