<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\SlogitemDeleteForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Deletion confirmation form for slogitem entity.
 */
class SlogitemDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_slogitem_confirm_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('entity.slogtx_mt.overview', ['slogtx_mt' => $this->entity->getTerm()->id()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

}
