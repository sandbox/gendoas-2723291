<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiRolePrepareConfirm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;

/**
 * Deletion confirmation form for slog toolbar.
 */
class XtsiRolePrepareConfirm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_userrole_confirm_prepare';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $args = [
        '%target' => ucfirst($this->target),
        '%role_id' => $this->role_id,
    ];
    return $this->t('Prepare %target for role %role_id?', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $target = $this->target;
    $rtname = 'UserRole.' . $this->role_id;
    $default = SlogTx::ROOTTERM_DEFAULT;
    $path = "_sys/$target/$rtname/$default";
    $args = ['%path' => "_sys/$target/$rtname/$default"];
    return $this->t('This creates all Objects until the root term %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $target = $this->target;
    $args = ['user_role' => $this->role_id];
    return new Url("sxt_slogitem.userrole.select.$target", $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Prepare');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->user_role = $request->get('user_role');
    $this->role_id = $this->user_role->id();
    $this->target = $request->get('target');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    // execute prepare
//todo::current:: TxRootTermGetPlugin - new
//    $result = SlogXtsi::prepareSysSurpriseRootTerm($this->target, $this->user_role);
    $rootterm = SlogTx::getSysSysRootTerm($this->target, $this->user_role, TRUE);
    
    // message/log
    $args = [
        '%target' => ucfirst($this->target),
        '%role_id' => $this->role_id,
    ];
    if (!empty($rootterm)) {
      $msg = $this->t('%target has been prepared for role %role_id.', $args);
      drupal_set_message($msg);
      SlogXt::logger('sxt_slogitem')->notice($msg);
    } else {
      $msg = $this->t("%target couldn't be prepared for role %role_id.", $args);
      drupal_set_message($msg, 'error');
      SlogXt::logger('sxt_slogitem')->error($msg);
    }
  }

}
