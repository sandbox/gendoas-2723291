<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiSettingsForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SlogTx settings form.
 */
class XtsiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sxt_slogitem.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sxt_slogitem.settings');
    $request_context = \Drupal::service('router.request_context');

    $form['module_base_path'] = [
      '#type' => 'textfield',
      '#title' => t('Base Path'),
      '#description' => t('Set the base path. Many routes are created by this path. <br />'
          . 'Notice: You can set this path as front page after the routes are rebuild.'),
      '#default_value' => $config->get('module_base_path'),
      '#size' => 10,
      '#field_prefix' => $request_context->getCompleteBaseUrl() . '/',
    ];

    $max_history = $config->get('max_items_history') ?: 30;
    $form['max_items_history'] = [
      '#type' => 'textfield',
      '#title' => t('Max Items - History'),
      '#description' => t('How many items to track in history. Min=20, Max=50, default=30'),
      '#default_value' => $max_history,
      '#size' => 10,
    ];

    $max_bookmark = $config->get('max_items_bookmark') ?: 40;
    $form['max_items_bookmark'] = [
      '#type' => 'textfield',
      '#title' => t('Max Items - Bookmark'),
      '#description' => t('How many items to track in bookmark. Min=30, Max=80, default=40'),
      '#default_value' => $max_bookmark,
      '#size' => 10,
    ];

    $max_depth_tbnode = $config->get('max_depth_tbnode') ?: 2;
    $form['max_depth_tbnode'] = [
      '#type' => 'textfield',
      '#title' => t('TbNode Max Depth'),
      '#description' => t("Max depth for creating content's submenues . Min=1, Max=5, default=2"),
      '#default_value' => $max_depth_tbnode,
      '#size' => 10,
    ];

    $form['rank_by_vocabulary'] = [
      '#type' => 'checkbox',
      '#title' => t('Rank by vocabulary'),
      '#description' => t('Rank contents by vocabulary. By default it is ranked by toolbar. See also the ranking order of the toolbars.'),
      '#default_value' => $config->get('rank_by_vocabulary'),
    ];

    
    

    // ++++++++++++++++++++
    $debug_mode = $config->get('debug_mode');
    $basepages_redirect = $config->get('basepages_redirect');
    if ((integer) \Drupal::currentUser()->id() === 1) {
      $form['basepages_redirect'] = [
        '#type' => 'checkbox',
        '#title' => t('Redirect base pages'),
        '#description' => t('Go to Xtsi page and show content in a dialog box (user.*, system.4*)'),
        '#default_value' => $basepages_redirect,
      ];      
      $form['debug_mode'] = [
        '#type' => 'checkbox',
        '#title' => t('Debug mode'),
        '#description' => t('Set debug mode for some more infos (list.listLabel, content.mainLabel)'),
        '#default_value' => $debug_mode,
      ];
    }
    else {
      $form['basepages_redirect'] = [
        '#type' => 'hidden',
        '#value' => $basepages_redirect,
      ];
      $form['debug_mode'] = [
        '#type' => 'hidden',
        '#value' => $debug_mode,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $max_history = (integer) $form_state->getValue('max_items_history');
    $msg_not_valid = t('Not a valid value.');
    if ($max_history < 20 || $max_history > 50) {
      $form_state->setErrorByName('max_items_history', $msg_not_valid);
    }
    $max_bookmark = (integer) $form_state->getValue('max_items_bookmark');
    if ($max_bookmark < 30 || $max_bookmark > 80) {
      $form_state->setErrorByName('max_items_bookmark', $msg_not_valid);
    }
    $max_depth_tbnode = (integer) $form_state->getValue('max_depth_tbnode');
    if ($max_depth_tbnode < 1 || $max_depth_tbnode > 5) {
      $form_state->setErrorByName('max_depth_tbnode', $msg_not_valid);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('sxt_slogitem.settings');
    $values = $form_state->getValues();
    $config->set('module_base_path', trim(trim($values['module_base_path']), '/'))
        ->set('max_items_history', (integer) trim($values['max_items_history']))
        ->set('max_items_bookmark', (integer) trim($values['max_items_bookmark']))
        ->set('max_depth_tbnode', (integer) trim($values['max_depth_tbnode']))
        ->set('rank_by_vocabulary', (boolean) $values['rank_by_vocabulary'])
        ->set('basepages_redirect', (boolean) $values['basepages_redirect'])
        ->set('debug_mode', (boolean) $values['debug_mode'])
        ->save();
    drupal_set_message($this->t('Xtsi settings have been saved.'));
  }

}
