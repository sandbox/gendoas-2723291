<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiNodePrepareConfirm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;

/**
 * Deletion confirmation form for slog toolbar.
 */
class XtsiNodePrepareConfirm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_node_confirm_prepare';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $args = [
        '%target' => ucfirst($this->target),
        '%node_id' => $this->node_id,
    ];
    return $this->t('Prepare %target for node %node_id?', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $tb = SlogTx::TOOLBAR_ID_NODE_SUBSYS;
    $target = $this->target;
    $rtname = 'Node.' . $this->node_id;
    $default = SlogTx::ROOTTERM_DEFAULT;
    $args = ['%path' => "$tb/$target/$rtname/$default"];
    return $this->t('This creates all Objects until the root term %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Prepare');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $this->node = $request->get('node');
    $this->node_id = $this->node->id();
    $this->target = $request->get('target');
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    // execute prepare
    $node_id = $this->node_id;
    switch ($this->target) {
      case 'submenu':
//        $result = SlogXtsi::prepareNodeSubmenuRootTerm($this->node_id);
///todo::current:: TxRootTermGetPlugin - new
        $rootterm = SlogTx::getSysNodeRootTermSubmenu($this->node_id);
        break;
    }

    // message/log
    $args = [
        '%target' => ucfirst($this->target),
        '%node_id' => $this->node_id,
    ];
    if (!empty($rootterm)) {
      $msg = $this->t('%target has been prepared for node %node_id.', $args);
      drupal_set_message($msg);
      SlogXt::logger('sxt_slogitem')->notice($msg);
    } else {
      $msg = $this->t("%target couldn't be prepared for node %node_id.", $args);
      drupal_set_message($msg, 'error');
      SlogXt::logger('sxt_slogitem')->error($msg);
    }
  }

}
