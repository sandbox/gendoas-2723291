<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\SlogitemForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\Core\Entity\ContentEntityForm;
//use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Form controller for the node edit forms.
 */
class SlogitemForm extends ContentEntityForm {

  /**
   * Init the menu term this slogitem belongs to.
   * 
   * @param FormStateInterface $form_state
   */
  protected function init(FormStateInterface $form_state) {
    if ($this->entity->isNew()) {
      $this->menu_term = \Drupal::request()->get('slogtx_mt');
    }
    else {
      $this->menu_term = $this->entity->getTerm();
    }
    parent::init($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $slogitem = $this->entity;
    $menu_term = $this->menu_term;
    $is_new = $slogitem->isNew();

    // set page title
    if ($is_new) {
      $form['#title'] = t('Add a new slogitem for %menuterm', ['%menuterm' => $menu_term->label()]);
    }
    elseif ($this->operation == 'edit') {
      $args = [
        '%slogitem' => $slogitem->label(),
        '%menuterm' => $menu_term->label(),
      ];
      $form['#title'] = t('Edit slogitem %slogitem in menu %menuterm', $args);
    }

    $form['sid'] = [
      '#type' => 'hidden',
      '#value' => $slogitem->id(),
    ];
    $form['tid'] = [
      '#type' => 'hidden',
      '#value' => $menu_term->id(),
    ];

    // allow all target entity plugins
    $options = [];
    $plugins = SlogXtsi::getTargetEntityPlugins();
    foreach ($plugins as $id => $plugin) {
      $options[$id] = $plugin->getTitle();
    }

    $keys = array_keys($options);
    $default = $is_new ? $keys[0] : $slogitem->getTargetEntityType();
    $form['target_entity_type'] = [
      '#type' => 'select',
      '#title' => t('Target entity type'),
      '#description' => t('The entity type for which to generate the route name.'),
      '#options' => $options,
      '#default_value' => $default,
      '#disabled' => (!$is_new || count($options) === 1),
    ];

    $form['target_entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Target entity id'),
      '#description' => t('The ID of an existing entity instance, e.g. an existing content.'),
      '#default_value' => $slogitem->getTargetEntityId(),
      '#disabled' => TRUE,
      '#size' => 10,
    ];

    $form['status'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Status'),
//        '#description' => t('The ID of an existing entity instance, e.g. an existing content.'),
      '#default_value' => $slogitem->getStatus(),
      '#disabled' => TRUE,
      '#size' => 10,
    ];

    return $form;
  }

  /**
   * Return the cancel url.
   * 
   * @return \Drupal\Core\Url
   */
  private function getCancelUrl() {
    return Url::fromRoute('entity.slogtx_mt.overview', ['slogtx_mt' => $this->menu_term->id()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->getCancelUrl(),
      '#attributes' => [
        'class' => ['button'],
      ],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   *
   * Validate that the entity instance by id exists.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    $tid = $values['tid'];
    $te_type = $values['target_entity_type'];
    $te_id = $values['target_entity_id'];
    $target_entity = \Drupal::entityTypeManager()->getStorage($te_type)->load($te_id);
    $is_new = $this->entity->isNew();

    if (!$target_entity) {
      $err_msg = t('Content not found for the target entity id.');
      $form_state->setErrorByName('target_entity_id', $err_msg);
    }
    elseif ($is_new && SlogXtsi::hasSidWithTidAndEntity($tid, $te_type, $te_id)) {
      $err_msg = t('Content already in this list.');
      $form_state->setErrorByName('target_entity_id', $err_msg);
    }

    if (!$form_state->hasAnyErrors()) {
      $this->replacePlaceholder($form_state);
    }
  }

  public function replacePlaceholder(FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $te_type = $values['target_entity_type'];
    $te_id = $values['target_entity_id'];
    $target_entity = \Drupal::entityTypeManager()->getStorage($te_type)->load($te_id);

    $title = & $form_state->getValue('title')[0]['value'];
    $args = [
      '@nodeid' => $target_entity->id(),
      '@node' => $target_entity->label(),
    ];
    $title = (string) (new FormattableMarkup($title, $args));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $slogitem = $this->entity;
    $insert = $slogitem->isNew();

    $slogitem->setChangedTime(REQUEST_TIME);
    $slogitem->setRouteData($values['target_entity_type'], $values['target_entity_id']);

    $slogitem->save();

    if ($slogitem->id()) {
      $args = [
        '@action' => $insert ? 'added' : 'updated',
        '@title' => $slogitem->label(),
      ];
      $msg = $this->t('Slogitem has been @action: @title.', $args);
      drupal_set_message($msg);
      SlogXt::logger('sxt_slogitem')->notice($msg);
      $form_state->setRedirectUrl($this->getCancelUrl());
    }
    else {
      drupal_set_message(t('The post could not be saved.'), 'error');
      $form_state->setRebuild();
    }
  }

}
