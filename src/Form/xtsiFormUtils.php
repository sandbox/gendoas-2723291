<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\xtsiFormUtils.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\XtsiCronStateData;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\node\Entity\Node;

/**
 * Static helper functions and ...
 */
class xtsiFormUtils {

  public static function insertNewSlogitem(array $params) {
    $params = $params + [
      'raw_title' => '',
      'status' => 0,
      'pos_sid' => 0,
      'insert_after' => TRUE,
      'link_sids' => [],
      'is_new_content' => FALSE,
      'slogitem_redirect' => FALSE,
    ];

    extract($params);
    if (empty($tid) || empty(SlogTx::getMenuTerm($tid))) {
      $message = t("Unvalid menu term id: @tid.", ['@tid' => $tid]);
      throw new \LogicException($message);
    }

    $done_sids = $done = $not_done = [];
    $next_weight = 100;
    $pos_sid = (integer) $pos_sid;
    $slogitems = SlogXtsi::loadSlogitems($tid);

    // 1. set new weight for items before
    if (!empty($slogitems)) {
      foreach ($slogitems as $key => $slogitem) {
        if ($key === $pos_sid) {
          if ($insert_after) {
            $slogitem->setWeight($next_weight--);
            $slogitem->save();
            unset($slogitems[$key]);
          }
          break;
        }

        $slogitem->setWeight($next_weight--);
        $slogitem->save();
        unset($slogitems[$key]);
      }
    }

    // 2. craete new items and set them as next
    if (isset($is_new_content) && $is_new_content) {
      if (empty($new_type) || ($new_type !== 'node') || empty($node = Node::load($new_eid))) {
        $args = ['@type' => $new_type, '@id' => $new_eid];
        $message = t("Unvalid content, given: @type / @id.", $args);
        throw new \LogicException($message);
      }

      $params = [
        'title' => $raw_title,
        'node' => $node,
      ];
      $title = self::prepareSlogitemTitle($params);
      $nid = $node->id();
      $slogitem = SlogItem::create([
            'tid' => $tid,
            'title' => $title,
            'status' => $status,
            'weight' => $next_weight--,
            'route_name' => 'entity.node.canonical',
            'route_parameters' => ['node' => $nid],
            'entity' => 'node', // restricted to node
            'eid' => $nid,
            'created' => REQUEST_TIME,
      ]);
      $slogitem->save();
      
      $sid = $slogitem->id();
      $done_sids["$sid"] = $slogitem->getAttachData();
      $done[] = $slogitem->label();
    }
    elseif (isset($link_sids) && is_array($link_sids)) {
      // insert new items
      $has_nids = SlogXtsi::getNidsByTid($tid);
      $number = 0;

      foreach ($link_sids as $link_sid) {
        $link_item = SlogXtsi::loadSlogitem($link_sid);
        if (!$link_item->isNodeEntity()) { // for nodes only !!!
          $args = ['@found' => $link_item->getTargetEntityType()];
          $message = t("For nodes only, found: @found.", $args);
          throw new \LogicException($message);
        }

        $content_type = $link_item->getTargetEntityType();
        $nid = $link_item->getTargetEntityId();
        if ($content_type !== 'node' || empty($node = Node::load($nid))) {
          $args = ['@type' => $content_type, '@id' => $nid];
          $message = t("Unvalid content, given: @type / @id.", $args);
          throw new \LogicException($message);
        }

        if (in_array($nid, $has_nids)) {
          $not_done[] = $link_item->label();
        }
        else {
          $params = [
            'title' => $raw_title,
            'node' => $node,
            'item' => $link_item,
            '$number' => ++$number,
          ];
          //        $title = SlogXtsi::prepareSlogitemTitle($raw_title, $link_item, $node, ++$number);
          $title = self::prepareSlogitemTitle($params);
          if ($slogitem_redirect) {
            $route_name = 'entity.slogitem.redirect';
            $route_parameters = ['slogitem' => $link_sid];
            $entity = 'slogitem';
            $eid = $link_sid;
          } else {
            $route_name = 'entity.node.canonical';
            $route_parameters = ['node' => $nid];
            $entity = 'node';
            $eid = $nid;
          }
          $slogitem = SlogItem::create([
                'tid' => $tid,
                'title' => $title,
                'status' => $status,
                'weight' => $next_weight--,
                'route_name' => $route_name,
                'route_parameters' => $route_parameters,
                'entity' => $entity, // restricted to node or slog_item
                'eid' => $eid,
                'created' => REQUEST_TIME,
          ]);
          $slogitem->save();
          
          $has_nids[] = $nid;      
          $sid = $slogitem->id();
          $done_sids["$sid"] = $slogitem->getAttachData();
          $done[] = $link_item->label();
        }
      }
    }

    // 3. set new weight for items after
    if (!empty($slogitems)) {
      foreach ($slogitems as $key => $slogitem) {
        $slogitem->setWeight($next_weight--);
        $slogitem->save();
      }
    }

    // add created to cron
    $node_ids = SlogXtsi::getNodeIdsBySids(array_keys($done_sids));
    XtsiCronStateData::pushNodeIds($node_ids);
    
    $success = [
      'done_sids' => $done_sids,
      'done' => $done,
      'not_done' => $not_done,
    ];

    return $success;
  }

  public static function prepareSlogitemTitle($params) {
    extract($params);
    $replace = [
      '@nodeid' => $node->id(),
      '@node' => $node->label(),
    ];
    isset($item) && ($replace['@itemid'] = $item->id());
    isset($item) && ($replace['@item'] = $item->label());
    isset($number) && ($replace['@number'] = $number);
    return str_replace(array_keys($replace), array_values($replace), $title);
  }

  public static function moveSlogitem(array $params) {
    $params = $params + [
      'pos_sid' => 0,
      'insert_after' => TRUE,
      'link_sids' => [],
    ];

    extract($params);
    $done_sids = $done = $not_warn = $not_err = [];
    $next_weight = 100;
    $pos_sid = (integer) $pos_sid;
    $target_tid = $tid;
    if ($pos_sid > 0) {
      $target_tid = SlogXtsi::loadSlogitem($pos_sid)->getTermId();
      $target_menu_term = SlogTx::getMenuTerm($target_tid);
      if (empty($target_menu_term) || !$target_menu_term instanceof MenuTerm) {
        $message = t("Unvalid target menu term id: @tid.", ['@tid' => $target_tid]);
        throw new \LogicException($message);
      }
    }
    $slogitems = SlogXtsi::loadSlogitems($target_tid);

    // 1. set new weight for items before
    if (!empty($slogitems)) {
      foreach ($slogitems as $key => $slogitem) {
        if ($key === $pos_sid) {
          if ($insert_after) {
            $slogitem->setWeight($next_weight--);
            $slogitem->save();
            unset($slogitems[$key]);
          }
          break;
        }

        $slogitem->setWeight($next_weight--);
        $slogitem->save();
        unset($slogitems[$key]);
      }
    }

    // 2. set items to move behind them (insert/append)
    $has_nids = SlogXtsi::getNidsByTid($target_tid);
    $number = 0;
    foreach ($link_sids as $link_sid) {
      $slogitem = SlogXtsi::loadSlogitem($link_sid);
      if (!$slogitem) {
        $message = t("Slog item not found for moving: %link_sid.", ['%link_sid' => $link_sid]);
        throw new \LogicException($message);
      }
      if (!$slogitem->isNodeEntity()) { // for nodes only !!!
        $args = ['@found' => $slogitem->getTargetEntityType()];
        $message = t("For nodes only, found: @found.", $args);
        throw new \LogicException($message);
      }

      $content_type = $slogitem->getTargetEntityType();
      $nid = $slogitem->getTargetEntityId();
      if ($content_type !== 'node' || empty($node = Node::load($nid))) {
        $args = ['@type' => $content_type, '@id' => $nid];
        $message = t("Unvalid content, given: @type / @id.", $args);
        throw new \LogicException($message);
      }

      if (in_array($nid, $has_nids)) {
        $not_warn[] = $slogitem->label();
      }
      else {
        //setTermId
        $slogitem->setTermId($target_tid);
        $slogitem->setWeight($next_weight--);
        $slogitem->save();
        $sid = $slogitem->id();
        $done_sids["$sid"] = $slogitem->getAttachData();
        $done[] = $slogitem->label();
      }
    }

    // 3. set new weight for items after
    if (!empty($slogitems)) {
      foreach ($slogitems as $key => $slogitem) {
        $slogitem->setWeight($next_weight--);
        $slogitem->save();
      }
    }
    
    // add moved to cron
    $node_ids = SlogXtsi::getNodeIdsBySids(array_keys($done_sids));
    XtsiCronStateData::pushNodeIds($node_ids);
    
    $success = [
      'done_sids' => $done_sids,
      'done' => $done,
      'not_warn' => $not_warn,
      'not_err' => $not_err,
    ];

    return $success;
  }

}
