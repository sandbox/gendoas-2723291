<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\User\XtsiUserResetForm.
 */

namespace Drupal\sxt_slogitem\Form\User;

use Drupal\slogxt\SlogXt;
use Drupal\user\Entity\User;
use Drupal\slogxt\SlogXt4xx;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserPasswordResetForm;

/**
 * Defines a controller ....
 */
class XtsiUserResetForm extends UserPasswordResetForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_user_reset_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();

    $session = $request->getSession();
    $timestamp = $session->get('pass_reset_timeout');
    $hash = $session->get('pass_reset_hash');
    // As soon as the session variables are used they are removed to prevent the
    // hash and timestamp from being leaked unexpectedly. This could occur if
    // the user does not click on the log in button on the form.
    $session->remove('pass_reset_timeout');
    $session->remove('pass_reset_hash');
    if (!$hash || !$timestamp) {
      return SlogXt4xx::setFormStateErr4xx($form_state, 403);
    }

    $uid = $request->get('param1');
    $user = User::load($uid);
    if ($user === NULL || !$user->isActive()) {
      return SlogXt4xx::setFormStateErr4xx($form_state, 403);
    }

    // Time out, in seconds, until login URL expires.
    $timeout = \Drupal::config('user.settings')->get('password_reset_timeout');

    $date_formatter = \Drupal::service('date.formatter');
    $expiration_date = $user->getLastLoginTime() ? $date_formatter->format($timestamp + $timeout) : NULL;
    $form = parent::buildForm($form, $form_state, $user, $expiration_date, $timestamp, $hash);

    // alter action path
    $path_info = trim($request->getPathInfo(), '/');
    $basePath = SlogXt::getBasePath('sxt_slogitem');
    $search = "%^{$basePath}%";
    $path_info = preg_replace($search, '', $path_info);
    $form['#action'] = "$path_info/$timestamp/$hash/login";

    return $form;
  }

}
