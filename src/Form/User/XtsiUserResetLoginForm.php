<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\User\XtsiUserResetLoginForm.
 */

namespace Drupal\sxt_slogitem\Form\User;

use Drupal\user\Entity\User;
use Drupal\slogxt\SlogXt4xx;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\ProfileForm;
use Drupal\Core\Render\Element;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Defines a controller ....
 */
class XtsiUserResetLoginForm extends ProfileForm {

  public function __construct(EntityManagerInterface $entity_manager, LanguageManagerInterface $language_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_manager, $language_manager, $entity_type_bundle_info, $time);
    $this->setModuleHandler(\Drupal::moduleHandler());

    $request = \Drupal::request();
    $uid = $request->get('param1');
    $user = User::load($uid);
    $this->setEntity($user);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_user_reset_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::request();
    $timestamp = $request->get('param2');
    $hash = $request->get('param3');

    if (!$hash || !$timestamp) {
      return SlogXt4xx::setFormStateErr4xx($form_state, 403);
    }

    $user = $this->getEntity();
    if ($user === NULL || !$user->isActive()) {
      return SlogXt4xx::setFormStateErr4xx($form_state, 403);
    }

    // The current user is not logged in, so check the parameters.
    $current = REQUEST_TIME;
    // Time out, in seconds, until login URL expires.
    $timeout = \Drupal::config('user.settings')->get('password_reset_timeout');

    $slogxt = & $form_state->get('slogxt');
    $err_message = FALSE;
    if ($user->getLastLoginTime() && $current - $timestamp > $timeout) {
      $err_message = t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.');
    }
    elseif ($user->isAuthenticated() && ($timestamp >= $user->getLastLoginTime()) //
        && ($timestamp <= $current) && Crypt::hashEquals($hash, user_pass_rehash($user, $timestamp))) {
      $slogxt['timestamp'] = $timestamp;
    }
    else {
      $err_message = t('You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.');
    }

    if ($err_message) {
      $slogxt['urgentMsgData'] = [
        'message' => $err_message,
        'type' => 'error',
      ];

      $form['dummy'] = [
        '#type' => 'hidden',
        '#value' => 'dummy',
      ];
    }
    else {
      $form = parent::buildForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $allowed = ['form_build_id', 'form_id'];
    foreach (Element::children($form) as $key) {
      if ($key === 'account') {
        foreach (Element::children($form[$key]) as $acc_key) {
          if ($acc_key === 'pass') {
            $form[$key][$acc_key]['#required'] = TRUE;
          } else {
            unset($form[$key][$acc_key]);
          }
        }
      }
      elseif (!in_array($key, $allowed)) {
        unset($form[$key]);
      }
    }

    return $form;
  }

}
