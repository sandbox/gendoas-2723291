<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\DummyForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * This is a dummy form, for development and extra actions only.
 */
class DummyForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slogtx_ui_dummy';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uid = (integer) \Drupal::currentUser()->id();
    drupal_set_message('DummyForm', 'warning');

    $description = t('Fix nodes: sid/toolbar');
    $form['dummy'] = [
      '#type' => 'details',
      '#title' => t('My Extra Action'),
      '#description' => $description . '<br />&nbsp;',
      '#open' => TRUE,
    ];

    $form['dummy']['submit_action'] = [
      '#type' => 'submit',
      '#value' => t('Fix nodes'),
      '#submit' => ['::submitExtraAction'],
      '#disabled' => ($uid !== 1),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (TRUE) {
//      $form_state->setErrorByName('submit_action', 'Disabled: function submitExtraAction()');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // do nothing
  }

  /**
   * ....
   */
  public function submitExtraAction(array &$form, FormStateInterface $form_state) {
    $xt_types = array_keys(XtsiNodeTypeData::getXtNodeTypes());
    $node_ids = \Drupal::database()->select('node')
        ->fields('node', ['nid'])
        ->condition('type', $xt_types, 'IN')
        ->execute()
        ->fetchCol();

    $per_step = 50;
    try {
      $ids_length = count($node_ids);
      $sync_steps = [];
      $step_off = 0;
      while ($step_off < $ids_length) {
        $step_nids = array_slice($node_ids, $step_off, $per_step);
        $sync_steps[] = implode(';', $step_nids);
        $step_off += $per_step;
      }

      $batch = [
        'operations' => [],
        'finished' => [self::class, 'finishBatch'],
        'title' => $this->t('Fixing nodes'),
        'init_message' => $this->t('Starting ...'),
        'progress_message' => $this->t('Completed @current steps of @total.'),
        'error_message' => $this->t('Fixing nodes has encountered an error.'),
      ];
      foreach ($sync_steps as $step_nids) {
        $batch['operations'][] = [[self::class, 'processBatch'], [$step_nids]];
      }

      batch_set($batch);
    }
    catch (\RuntimeException $e) {
      drupal_set_message($this->t('Fixing nodes failed'), 'error');
    }
  }

  /**
   * Process batch step.
   */
  public static function processBatch($step_nids, &$context) {
    try {
      $errors = [];
      $node_ids = explode(';', $step_nids);
      
      if (FALSE) {
        // this no longer works, field_mw_content and findXtNodeValues no longer exists
//              $xtsi_storage = \Drupal::entityTypeManager()->getStorage('slogitem');
//              foreach ($node_ids as $node_id) {
//      //          XtsiNodeTypeData::setXtNodeValues($node_id);
//                $node = Node::load($node_id);
//
//                $values = XtsiNodeTypeData::findXtNodeValues($node);
//                $xtnode_values = [
//                  'base_sid' => $values['sxtbase_sid'],
//                  'base_tb' => $values['sxtbase_tb'],
//                  'rank_sids' => $values['sxtrank_sids'],
//                ];
//
//                $node->mwcontent->value = $node->field_mw_content->value;
//                $node->xtnodebase->setValue($xtnode_values);
//                $node->save();
//              }
      }
    }
    catch (\RuntimeException $e) {
      $errors[] = $e->getMessage();
    }

    if (!empty($errors)) {
      if (!isset($context['results']['errors'])) {
        $context['results']['errors'] = [];
      }
      $context['results']['errors'] += $errors;
    }
  }

  /**
   * Finish batch.
   */
  public static function finishBatch($success, $results, $operations) {
    if ($success) {
      if (!empty($results['errors'])) {
        foreach ($results['errors'] as $error) {
          drupal_set_message($error, 'error');
          \Drupal::logger('dummy')->error($error);
        }
        drupal_set_message(\Drupal::translation()->translate('Nodes were fixed with errors.'), 'warning');
      }
      else {
        drupal_set_message(\Drupal::translation()->translate('Nodes were fixed successfully.'));
      }
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = \Drupal::translation()->translate('An error occurred while processing %error_operation with arguments: @arguments', //
          ['%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)]);
      drupal_set_message($message, 'error');
    }
  }

}
