<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiFormBase.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\slogxt\XtExtrasTrait;
use Drupal\slogxt\XtDateTimeTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

/**
 * Description of ...
 */
abstract class XtsiFormBase extends FormBase {

  use XtExtrasTrait;
  use XtDateTimeTrait;

  abstract public function getFormId();

  abstract public function submitForm(array &$form, FormStateInterface $form_state);

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the form array using the possibly updated entity in form state.
    $form = $this->form($form, $form_state);

    // Retrieve and add the form actions array.
    $actions = $this->actionsElement($form, $form_state);
    if (!empty($actions)) {
      $form['actions'] = $actions;
    }

    return $form;
  }

  /**
   * Gets the actual form array to be built.
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Add #process and #after_build callbacks.
    $form['#process'][] = '::processForm';
    $form['#after_build'][] = '::afterBuild';

    return $form;
  }

  /**
   * Process callback: .....
   */
  public function processForm($element, FormStateInterface $form_state, $form) {
    return $element;
  }

  /**
   * Form element #after_build callback: .....
   */
  public function afterBuild(array $element, FormStateInterface $form_state) {
    return $element;
  }

  /**
   * Returns the action form element for the current form.
   */
  protected function actionsElement(array $form, FormStateInterface $form_state) {
    $element = $this->actions($form, $form_state);

    if (isset($element['delete'])) {
      // Move the delete action as last one, unless weights are explicitly
      // provided.
      $delete = $element['delete'];
      unset($element['delete']);
      $element['delete'] = $delete;
      $element['delete']['#button_type'] = 'danger';
    }

    if (isset($element['submit'])) {
      // Give the primary submit button a #button_type of primary.
      $element['submit']['#button_type'] = 'primary';
    }

    $count = 0;
    foreach (Element::children($element) as $action) {
      $element[$action] += [
        '#weight' => ++$count * 5,
      ];
    }

    if (!empty($element)) {
      $element['#type'] = 'actions';
    }

    return $element;
  }

  /**
   * Returns an array of supported actions for the current form.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => ['::submitForm'],
    ];

    return $actions;
  }

}
