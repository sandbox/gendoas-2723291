<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\BmstorageTypeSelectForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class BmstorageTypeSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_dummy_checkboxes';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $request = \Drupal::request();
    $path_info = urldecode($request->getPathInfo());
    $path_target = $request->get('path_target');
    $path_search = "/{$path_target}/";


    $options = [];
    $path_replace = "/{$path_target}" . SlogXt::XTURL_DELIMITER . 'list/';
    $options[] = [
      'entityid' => 'list',
      'liTitle' => t('List'),
      'liDescription' => t('Storage for list bookmarks.'),
      'path' => str_replace($path_search, $path_replace, $path_info),
    ];

    $path_replace = "/{$path_target}" . SlogXt::XTURL_DELIMITER . 'content/';
    $options[] = [
      'entityid' => 'content',
      'liTitle' => t('Content'),
      'liDescription' => t('Storage for content bookmarks.'),
      'path' => str_replace($path_search, $path_replace, $path_info),
    ];

    return $options;
  }

}
