<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent\NodeCrossRefsForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\slogxt\Form\XtCheckboxesFormBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Url;

/**
 * Provides a ... form.
 */
class NodeCrossRefsForm extends XtCheckboxesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_node_crossrefs';
  }

  protected function getPath($id) {
    $route_name = SlogXt::getSlogxtHandler()->getRouteName('.on.menu_item_select', TRUE);
    $parameters = ['tbmenu_tid' => $id];
    $url = Url::fromRoute($route_name, $parameters);
    return $url->getInternalPath();
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $options = [];
    $node = Node::load(\Drupal::request()->get('base_entity_id'));
    if ($xtnodebase_values = XtsiNodeTypeData::getXtNodeBaseValues($node)) {
      extract($xtnodebase_values);
      $sids = array_merge([$base_sid], explode(';', $rank_sids));
      foreach ($sids as $sid) {
        if ($slogitem = SlogItem::load($sid)) {
          $menu_term = $slogitem->getTerm();
          if (!$menu_term) {
            $args = [
              '%nid' => $node->id(),
              '%sid' => $sid,
              '%tid' => $slogitem->getTermId(),
            ];
            $msg = $this->t("Menu term not found (nid/sid/tid): %nid/%sid/%tid", $args);
            SlogXt::logger('sxt_slogitem')->warning($msg);
          }
          elseif (SlogXt::hasViewPermToolbar($menu_term)) {
            $vocabulary = $menu_term->getVocabulary();
            $pathLabel = $vocabulary->headerPathLabel();
            $menu_tid = $menu_term->id();
            list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());

            $option = [
              'liTitle' => $menu_term->label(),
              'liDescription' => $pathLabel,
              'path' => $this->getPath($menu_tid),
              'entityid' => $menu_tid,
              'toolbar' => $toolbar,
              'toolbartab' => $toolbartab,
            ];

            if ($toolbar === 'role') {
              $target_term = $menu_term->getTargetTerm();
              $te_plugin = $vocabulary->getTargetEntityPlugin();
              $te_plugin->setCurrentTargetTermName($target_term->label());
              $role_id = $te_plugin->getCurrentEntity()->id();
              $option['liDescription'] = $vocabulary->headerPathLabel();
              $option['roleId'] = $role_id;
              //reset entity
              $te_plugin->setDefaultEntity();
            }
            elseif ($toolbar === SlogTx::TOOLBAR_ID_NODE_SUBSYS) {
              $target_term = $menu_term->getTargetTerm();
              $te_plugin = $vocabulary->getTargetEntityPlugin();
              $parent_node = $te_plugin->getEntityFromTargetTermName($target_term->label());
              $te_plugin->setEntity($parent_node);
              $option['liDescription'] = $vocabulary->headerPathLabel();
            }

            $options[] = $option;
          }
        }
      }
    }

    return $options;
  }

}
