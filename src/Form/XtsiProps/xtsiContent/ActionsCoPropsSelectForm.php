<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent\ActionsCoPropsSelectForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiProps\xtsiContent;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class ActionsCoPropsSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_actions_coprops_select_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('props')->getActionsData('xtsi_coprops', $baseEntityId);
  }

}
