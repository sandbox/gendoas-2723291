<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\SelectPromotedForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\user\RoleInterface;
use Drupal\sxt_slogitem\Entity\SlogItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Defines a ...
 */
class SelectPromotedForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_select_promoted';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sxt_slogitem.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\user\RoleInterface $user_role
   *   The role instance to use for this form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, RoleInterface $user_role = NULL) {
    if (empty($user_role)) {
      $user_role = \Drupal::request()->get('user_role');
    }
    $role_id = $user_role->id();
    $form['role_id'] = [
        '#type' => 'hidden',
        '#value' => $role_id,
    ];
    
    $rootterm = SlogXtsi::getPromotedRootTerm($user_role);
    if ($rootterm) {
      $vocabulary = $rootterm->getVocabulary();
      $tree = $vocabulary->getVocabularyMenuTree($rootterm->id(), 3, TRUE);
    } else {
      $form_state->set('not_prepared', TRUE);
    }

    if (!empty($tree)) {
      $parents = [];
      foreach ($tree as $item) {
        if (isset($item->parents[0])) {
          $parents[] = $item->parents[0];
        }
      }

      $options = [];
      $prefix = ['', '--.', '--.--.'];
      foreach ($tree as $item) {
        $options[$item->id()] = $prefix[$item->depth] . $item->label();
      }

      if (!empty($options)) {
        $promoted_sids = $this->config('sxt_slogitem.settings')->get('promoted_sid');
        $promoted_sid = isset($promoted_sids[$role_id]) ? $promoted_sids[$role_id] : 0;
        $slogitem = SlogItem::load($promoted_sid);
        $saved_term_id = $slogitem ? $slogitem->getTermId() : 0;
        $values = $form_state->getValues();
        $term_id = isset($values['promoted_tid']) ? $values['promoted_tid'] : $saved_term_id;
        $options = [t('None (disable promote for this role)')] + $options;
        $ajax_settings = [
            'callback' => '::ajaxOnSelectedTid',
            'wrapper' => 'detail-sids-wrapper',
            'effect' => 'fade',
        ];
        $form['promoted_tid'] = [
            '#type' => 'select',
            '#options' => $options,
            '#title' => t('Select a menu term'),
            '#description' => t("Select a menu term where to select the promoted slogitem. </br>Select 'None' for no promoted slogitem."),
            '#default_value' => $term_id,
            '#ajax' => $ajax_settings,
        ];
        $form['detail_sids'] = [
            '#type' => 'details',
            '#id' => 'detail-sids-wrapper',
            '#title' => $this->t('Slogitems'),
            '#open' => TRUE,
            '#tree' => FALSE,
        ];

        $slogitems = SlogXtsi::loadSlogitems($term_id);
        $sid_options = [];
        if ($slogitems) {
          foreach ($slogitems as $key => $slogitem) {
            $sid_options[$key] = $slogitem->labelExt();
          }
        }
        if (!empty($sid_options)) {
          if (isset($values['promoted_sid'])) {
            $promoted_sid = $values['promoted_sid'];
          }
          if (!array_key_exists($promoted_sid, $sid_options)) {
            $promoted_sid = NULL;
          }

          $form['detail_sids']['promoted_sid'] = [
              '#type' => 'radios',
              '#id' => 'edit-promoted-sid--wrapper',
              '#options' => $sid_options,
              '#title' => t('Select a slogitem'),
              '#description' => t('Select a slogitem as promoted for the role %role.', //
                  ['%role' => $role_id]),
              '#default_value' => $promoted_sid,
          ];
        } elseif (!empty($values['promoted_tid'])) {
          $msg = t('No Slogitems found.');
          $form['detail_sids']['promoted_sid'] = SlogXt::xtRenderMessage($msg, 'warning');
        }
        $form = parent::buildForm($form, $form_state);
      } else {
        $msg = t('Menu options not built.');
        $form['xt_error'] = SlogXt::xtRenderMessage($msg, 'error');
      }
    } elseif ($rootterm) {
      $msg = t('Menu terms not created for role  %role.', ['%role' => $role_id]);
      $form['xt_waring'] = SlogXt::xtRenderMessage($msg, 'warning');
      if (\Drupal::moduleHandler()->moduleExists('slogtx_ui')) {
        $form_state->set('no_menu_terms', TRUE);
        $this->rootterm = $rootterm;
        $form = parent::buildForm($form, $form_state);
        $submit = &$form['actions']['submit'];
        $submit['#value'] = t('Edit menu terms');
      }
    } else {
      $msg = t('Not prepared for role %role.', ['%role' => $role_id]);
      $form['xt_waring'] = SlogXt::xtRenderMessage($msg, 'warning');
      $form = parent::buildForm($form, $form_state);
      $submit = &$form['actions']['submit'];
      $submit['#value'] = t('Prepare promoted');
    }
    return $form;
  }

  /**
   * Ajax callback for field promoted_tid.
   * 
   * @param type $form
   * @param FormStateInterface $form_state
   * @return array
   *  Renderable array for ajax response.
   */
  public static function ajaxOnSelectedTid($form, FormStateInterface $form_state) {
    return $form['detail_sids'];
  }

  /**
   * {@inheritdoc}
   */
//        public function validateForm(array &$form, FormStateInterface $form_state) {
//          $not_prepared = $form_state->get('not_prepared');
//          $no_menu_terms = $form_state->get('no_menu_terms');
//          if (empty($not_prepared) && empty($no_menu_terms)) {
//            $values = $form_state->getValues();
//            if (empty($values['promoted_tid']) || empty($values['promoted_sid'])) {
//              $form_state->setError($form, t('Slogitem not selected.'));
//            }
//          }
//        }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $role_id = $values['role_id'];
    $not_prepared = $form_state->get('not_prepared');
    $no_menu_terms = $form_state->get('no_menu_terms');
    if (!empty($not_prepared)) {
      $form_state->setRedirect('sxt_slogitem.user_role.prepare_confirm', [
          'user_role' => $role_id,
          'target' => SlogXtsi::SYS_VOCAB_ID_PROMOTED,
      ]);
    } elseif (!empty($no_menu_terms)) {
      $form_state->setRedirect('entity.slogtx_rt.overview_form', [
          'slogtx_rt' => $this->rootterm->id(),
      ]);
    } else {
      $config = $this->config('sxt_slogitem.settings');
      $promoted_sid = $config->get('promoted_sid');
      if (!is_array($promoted_sid)) {
        $promoted_sid = [];
      }
      if (!empty($values['promoted_sid'])) {
        $promoted_sid[$role_id] = $values['promoted_sid'];
      } else {
        unset($promoted_sid[$role_id]);
      }
      $config->set('promoted_sid', $promoted_sid)->save();
      drupal_set_message($this->t('Settings have been saved.'));
      
      // invalidate tags
      SlogXt::invalidateTagsMainDropButton();
    }
  }

  /**
   * The _title_callback for the sxt_slogitem.userrole.select.promoted route.
   *
   * @param \Drupal\user\RoleInterface $user_role
   *   The current role.
   *
   * @return string
   *   The select promoted page title.
   */
  public function selectPromotedTitle(RoleInterface $user_role) {
    return $this->t('Select promoted slogitem for role %name', ['%name' => $user_role->label()]);
  }

}
