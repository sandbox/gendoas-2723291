<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\SelectSurpriseForm.
 */

namespace Drupal\sxt_slogitem\Form;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\slogxt\SlogXt;
use Drupal\user\RoleInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Defines a ...
 *
 */
class SelectSurpriseForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_select_surprise';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sxt_slogitem.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\user\RoleInterface $user_role
   *   The role instance to use for this form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, RoleInterface $user_role = NULL) {
    if (empty($user_role)) {
      $user_role = \Drupal::request()->get('user_role');
    }
    $role_id = $user_role->id();
    $form['role_id'] = [
        '#type' => 'hidden',
        '#value' => $role_id,
    ];
    
    $rootterm = SlogXtsi::getSurpriseRootTerm($user_role);
    if ($rootterm) {
      $vocabulary = $rootterm->getVocabulary();
      $tree = $vocabulary->getVocabularyMenuTree($rootterm->id(), 3, TRUE);
    } else {
      $form_state->set('not_prepared', TRUE);
    }

    if (!empty($tree)) {
      $parents = [];
      foreach ($tree as $item) {
        if (isset($item->parents[0])) {
          $parents[] = $item->parents[0];
        }
      }

      $options = [];
      $prefix = ['', '--.', '--.--.'];
      foreach ($tree as $item) {
        $options[$item->id()] = $prefix[$item->depth] . $item->label();
      }

      if (!empty($options)) {
        $surprise_tid = $this->config('sxt_slogitem.settings')->get('surprise_tid');
        $options = [t('None (disable surprise for this role)')] + $options;
        $form['surprise_tid'] = [
            '#type' => 'radios',
            '#options' => $options,
            '#title' => t('Select a menu term'),
            '#description' => t('Select a menu term for serving surprise contents.'),
            '#default_value' => $surprise_tid[$role_id] ?: 0,
        ];
        $form = parent::buildForm($form, $form_state);
      } else {
        $msg = t('Menu options not built.');
        $form['xt_error'] = SlogXt::xtRenderMessage($msg, 'error');
      }
    } elseif ($rootterm) {
      $msg = t('Menu terms not created for role  %role.', ['%role' => $role_id]);
      $form['xt_waring'] = SlogXt::xtRenderMessage($msg, 'warning');
      if (\Drupal::moduleHandler()->moduleExists('slogtx_ui')) {
        $form_state->set('no_menu_terms', TRUE);
        $this->rootterm = $rootterm;
        $form = parent::buildForm($form, $form_state);
        $submit = &$form['actions']['submit'];
        $submit['#value'] = t('Edit menu terms');
      }
    } else {
      $msg = t('Not prepared for %role.', ['%role' => $role_id]);
      $form['xt_waring'] = SlogXt::xtRenderMessage($msg, 'warning');
      $form = parent::buildForm($form, $form_state);
      $submit = &$form['actions']['submit'];
      $submit['#value'] = t('Prepare surprise');
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
//        public function validateForm(array &$form, FormStateInterface $form_state) {
//          $not_prepared = $form_state->get('not_prepared');
//          $no_menu_terms = $form_state->get('no_menu_terms');
//          if (empty($not_prepared) && empty($no_menu_terms)) {
//            $values = $form_state->getValues();
//            if (empty($values['surprise_tid'])) {
//              $form_state->setError($form, t('Menu item not selected.'));
//            }
//          }
//        }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $role_id = $values['role_id'];
    $not_prepared = $form_state->get('not_prepared');
    $no_menu_terms = $form_state->get('no_menu_terms');
    if (!empty($not_prepared)) {
      $form_state->setRedirect('sxt_slogitem.user_role.prepare_confirm', [
          'user_role' => $role_id,
          'target' => SlogXtsi::SYS_VOCAB_ID_SURPRISE,
      ]);
    } elseif (!empty($no_menu_terms)) {
      $form_state->setRedirect('entity.slogtx_rt.overview_form', [
          'slogtx_rt' => $this->rootterm->id(),
      ]);
    } else {
      $config = $this->config('sxt_slogitem.settings');
      $surprise_tid = $config->get('surprise_tid');
      if (!is_array($surprise_tid)) {
        $surprise_tid = [];
      }
      if (!empty($values['surprise_tid'])) {
        $surprise_tid[$role_id] = $values['surprise_tid'];
      } else {
        unset($surprise_tid[$role_id]);
      }
      $config->set('surprise_tid', $surprise_tid)->save();
      drupal_set_message($this->t('Settings have been saved.'));
      
      // invalidate tags
      SlogXt::invalidateTagsMainDropButton();
    }
  }

  /**
   * The _title_callback for the sxt_slogitem.userrole.select.surprise route.
   *
   * @param \Drupal\user\RoleInterface $user_role
   *   The current role.
   *
   * @return string
   *   The select surprise page title.
   */
  public function selectSurpriseTitle(RoleInterface $user_role) {
    return $this->t('Select surprise menu term for role %name', ['%name' => $user_role->label()]);
  }

}
