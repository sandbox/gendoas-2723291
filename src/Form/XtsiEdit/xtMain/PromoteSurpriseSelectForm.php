<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtMain\PromoteSurpriseSelectForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtMain;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class PromoteSurpriseSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_edit_select_promoted_surprise_action';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    //todo::actions::main
    return SlogXt::pluginManager('edit')->getActionsData('mainprosur', $baseEntityId);
  }

}
