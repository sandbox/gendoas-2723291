<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\ActionsLiEditSelectForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class ActionsLiEditSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_actions_liedit_select_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('edit')->getActionsData('xtsi_liedit', $baseEntityId);
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyWarning() {
    return t("You don't have the permission to edit this list.");
  }

}
