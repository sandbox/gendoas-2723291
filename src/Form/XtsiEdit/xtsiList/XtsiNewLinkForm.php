<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiNewLinkForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\Form\xtsiFormUtils;
use Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiListFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Description of ...
 */
class XtsiNewLinkForm extends XtsiListFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitem_newlink_form';
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $input = $form_state->getUserInput();
    $wrapper = $this->getInputFieldWrapper();

    $value = isset($input['title']) ? $input['title'] : '__.@node';
    $description = t('Title of the new item(s). <br />Allowed palceholder: %item, %node, %nodeid, %number', [
      '%item' => '@item',
      '%node' => '@node',
      '%nodeid' => '@nodeid',
      '%number' => '@number',
    ]);
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => t('Title'),
      '#description' => $description,
      '#value' => $value,
      '#required' => TRUE,
      '#weight' => -100,
        ] + $wrapper;
    
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $pos_target = isset($values['target_position']) ? (integer) $values['target_position'] : 0;

    $params = [
      'raw_title' => $values['title'],
      'status' => $values['status'],
      'tid' => $values['tid'],
      'pos_sid' => $values['pos_sid'],
      'insert_after' => ($pos_target === SlogXt::XTOPTS_TARGETS_AFTER),
      'link_sids' => explode(',', $values['link_sids']),
      'is_new_content' => FALSE,
    ];
    $success = xtsiFormUtils::insertNewSlogitem($params);

    // return success
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['done_sids'] = $success['done_sids'];
    $slogxt_data['submit_ok'] = $success['done'];
    $slogxt_data['submit_err'] = $success['not_done'];
  }

}
