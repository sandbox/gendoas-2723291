<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiListFormBase.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList;

use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\Form\XtsiFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Description of ...
 */
abstract class XtsiListFormBase extends XtsiFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $request = \Drupal::request();
    $tid = $request->get('base_entity_id');
    $pos_sid = (integer) $request->get('next_entity_id');
    $input = $form_state->getUserInput();
    $wrapper = $this->getInputFieldWrapper();

    $form['tid'] = [
      '#type' => 'hidden',
      '#value' => $tid,
    ];
    $form['pos_sid'] = [
      '#type' => 'hidden',
      '#value' => $pos_sid,
    ];
    $value = isset($input['link_sids']) ? $input['link_sids'] : '';
    $form['link_sids'] = [
      '#type' => 'hidden',
      '#value' => $value,
    ];

    $value = isset($input['status']) ? $input['status'] : 0;
    $form['status'] = [
      '#type' => 'hidden',
      '#value' => $value,
    ];
//    $form['status'] = [
//      '#type' => 'textfield',
//      '#title' => t('Status'),
//      '#default_value' => '0',
//      '#size' => 10,
//        ] + $wrapper;

    if (!empty($pos_sid)) {
      $slogitem = SlogXtsi::loadSlogitem($pos_sid);
      $menu_term = $slogitem->getTerm();
      
      $args = [
        '%menu' => $slogitem ? $slogitem->label() : '???',
        '%menu_decr' => $menu_term ? $menu_term->label() : '???',
      ];
      $description = t('Selected: %menu <br />Menu item: %menu_decr', $args);
      $value = isset($input['target_position']) ? $input['target_position'] : SlogXt::XTOPTS_TARGETS_AFTER;
      $disabled = FALSE;
      if ($force_before = $request->get('forceInsertBefore')) {
        $value = SlogXt::XTOPTS_TARGETS_BEFORE;
        $disabled = TRUE;
        $description .= '<br />' . t('You may rearrange later.');
      }

      $build = [
        '#type' => 'select',
        '#options' => SlogXt::getAddTargetOptions(FALSE),
        '#title' => t('Where to position the contents'),
        '#description' => $description,
        '#value' => $value,
        '#disabled' => $disabled,
        '#parents' => ['target_position'],
          ] + $wrapper;
      $form['target_position'] = \Drupal::formBuilder()
          ->doBuildForm($this->getFormId(), $build, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['link_sids']) || $values['link_sids'] === '-1') {
      $err_msg = t('Empty list');
      $form_state->setErrorByName('link_sids', $err_msg);
    }
  }

}
