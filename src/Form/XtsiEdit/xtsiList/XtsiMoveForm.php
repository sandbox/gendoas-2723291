<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiMoveForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_slogitem\Form\xtsiFormUtils;
use Drupal\sxt_slogitem\Form\XtsiEdit\xtsiList\XtsiListFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Description of ...
 */
class XtsiMoveForm extends XtsiListFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_slogitems_move_form';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $pos_target = isset($values['target_position']) ? (integer) $values['target_position'] : 0;

    $params = [
      'tid' => $values['tid'],
      'pos_sid' => $values['pos_sid'],
      'insert_after' => ($pos_target === SlogXt::XTOPTS_TARGETS_AFTER),
      'link_sids' => explode(',', $values['link_sids']),
    ];
    
    $success = xtsiFormUtils::moveSlogitem($params);
    // return success
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['done_sids'] = $success['done_sids'];
    $slogxt_data['submit_ok'] = $success['done'];
    $slogxt_data['submit_warn'] = $success['not_warn'];
    $slogxt_data['submit_err'] = $success['not_err'];
  }

}
