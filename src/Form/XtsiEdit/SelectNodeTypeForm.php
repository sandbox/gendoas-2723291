<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\SelectNodeTypeForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit;

use Drupal\slogxt\Form\XtRadiosFormBase;
use Drupal\sxt_slogitem\XtsiNodeTypeData;

/**
 * Provides a ... form.
 */
class SelectNodeTypeForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_edit_select_node_type';
  }

  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $items = [];
    $path_info = urldecode(\Drupal::request()->getPathInfo());
    $types = XtsiNodeTypeData::getXtNodeTypes();
    foreach ($types as $type_id => $type) {
      $items[] = [
        'liTitle' => $type->label(),
        'liDescription' => $type->getDescription(),
        'skipable' => true,
        'path' => str_replace('{node_type}', $type_id, $path_info),
      ];
    }

    return $items;
  }

}
