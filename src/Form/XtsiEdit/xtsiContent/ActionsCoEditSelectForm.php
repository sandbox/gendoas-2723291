<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Form\XtsiEdit\xtsiContent\ActionsCoEditSelectForm.
 */

namespace Drupal\sxt_slogitem\Form\XtsiEdit\xtsiContent;

use Drupal\slogxt\SlogXt;
use Drupal\slogxt\Form\XtRadiosFormBase;

/**
 * Provides a ... form.
 */
class ActionsCoEditSelectForm extends XtRadiosFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'xtsi_actions_coedit_select_form';
  }
  
  /**
   * {@inheritdoc}
   */
  public function getXtOptions() {
    $baseEntityId = \Drupal::request()->get('base_entity_id');
    return SlogXt::pluginManager('edit')->getActionsData('xtsi_coedit', $baseEntityId);
  }

  /**
   * {@inheritdoc}
   */
  public function getEmptyWarning() {
    return t("You don't have permission to edit this content.<br /><br />NOTE: Content may be part of a superordinate section.");
  }

}
