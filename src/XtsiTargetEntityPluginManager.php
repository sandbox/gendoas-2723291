<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\XtsiTargetEntityPluginManager.
 */

namespace Drupal\sxt_slogitem;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages xtsi target entity plugins.
 * 
 * Target entity plugins are used by slogitems.
 *
 */
class XtsiTargetEntityPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Caches the settings of target entity plugins.
   *
   * @var array of target entity plugin settings 
   */
  protected $settings;

  /**
   * Constructs a XtsiTargetEntityPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/sxt_slogitem/TargetEntity', $namespaces, $module_handler, // 
            'Drupal\sxt_slogitem\Plugin\sxt_slogitem\XtsiTargetEntityInterface', //
            'Drupal\sxt_slogitem\Annotation\XtsiTargetEntity');
    $this->setCacheBackend($cache_backend, 'sxt_slogitem.target_entity_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'node';
  }
  
  /**
   * Stores the settings of target entity plugins.
   * 
   * Settings are stored as state in the key_value table,
   *   with name=sxt_slogitem.settings.target_entity_plugins.
   * Settings for all target entity plugins are stored under this key name.
   * 
   * @see self::getSettingsKey()
   * 
   * @param array $plugin_settings
   *  The settings for an individual plugin
   * @param string $plugin_id
   */
  public function setSettings(array $plugin_settings, $plugin_id) {
    $settings_key = $this->getSettingsKey();
    $settings = \Drupal::state()->get($settings_key);
    $settings[$plugin_id] = $settings[$plugin_id] ?: [];
    $settings[$plugin_id] = array_merge($settings[$plugin_id], $plugin_settings);
    
    // store the state in db
    \Drupal::state()->set($settings_key, $settings);
    
    // refresh cached settings
    $this->settings = $settings;
  }
  
  /**
   * Retrieves the settings of target entity plugins.
   * 
   * @see self::setSettings()
   * 
   * @return array
   */
  public function getSettings() {
    if (!isset($this->settings)) {
      $settings_key = $this->getSettingsKey();
      $this->settings = \Drupal::state()->get($settings_key);
      $this->settings = isset($this->settings) ? $this->settings : [];
    }
    
    return $this->settings;
  }
  
  /**
   * Deletes the settings of all target entity plugins.
   * 
   * This is deleting one db record, since all settings are saved in one record.
   * 
   * @see self::setSettings()
   */
  public function deleteAllSettings() {
      \Drupal::state()->delete($this->getSettingsKey());
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $return = NULL;
    // get the configuration from state (key_value table)
    $configuration += $this->getDefinition($plugin_id, FALSE);
    if ($configuration) {
      $settings = $this->getSettings();
      if (isset($settings[$plugin_id])) {
        $configuration['settings'] = $settings[$plugin_id];
        $configuration = array_merge($configuration, $settings[$plugin_id]);
      }

      $return = parent::createInstance($plugin_id, $configuration);
    }

    return $return;
  }

  /**
   * Returns the key (field 'name') where the settings are stored.
   * 
   * The settings are stored as state in the key_value table.
   * 
   * @return string
   */
  protected function getSettingsKey() {
    return 'sxt_slogitem.settings.target_entity_plugins';
  }

  /**
   * Returns the ids of all target entity plugins.
   * 
   * @return array of ids
   */
  public function getAllTargetEntityIds() {
    $definitions = $this->getDefinitions();
    return array_keys($definitions);
  }

  /**
   * Return target entity plugin objects.
   * 
   * Default/Fallback first, the other sorted by key.
   * 
   * @param boolean $all
   *   If TRUE, return plugin objects, independent of status.
   * @param boolean $status
   *   if $all == FALSE, return plugin objects, with the specified status.
   * @return array of target entitys plugin objects.
   *   \Drupal\sxt_slogitem\Plugin\sxt_slogitem\TargetEntityInterface
   */
  public function getTargetEntityPlugins($all = FALSE, $status = TRUE) {
    $fallback_id = $this->getFallbackPluginId(NULL);
    $plugins = [];
    $all_plugins = [];    
    $plugins_tmp = [];
    $ids = $this->getAllTargetEntityIds();
    foreach ($ids as $plugin_id) {
      if ($instance = $this->createInstance($plugin_id)) {
        $all_plugins[$plugin_id] = $instance;
        $title = (string) $instance->title;
        
        $enabled = (boolean) $instance->status;
        if (!$all && $status == FALSE && $status == $enabled) {
          $plugins_tmp[$title] = $plugin_id;
        } elseif ($plugin_id == $fallback_id) {
          $plugins[$plugin_id] = $instance;
        } elseif ($all || $status == $enabled) {
          $plugins_tmp[$title] = $plugin_id;
        }
      }
    }
    
    // sort by title
    ksort($plugins_tmp);
    foreach ($plugins_tmp as $id) {
      $plugins[$id] = $all_plugins[$id];
    }

    // return sorted array
    return $plugins;
  }

}
