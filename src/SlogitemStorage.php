<?php

/**
 * @file
 * Definition of Drupal\sxt_slogitem\SlogitemStorage.
 */

namespace Drupal\sxt_slogitem;

//use Drupal\Core\Entity\ContentEntityDatabaseStorage;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

//use Drupal\Core\Entity\EntityInterface;
//use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Defines a ... @todo.
 */
class SlogitemStorage extends SqlContentEntityStorage {

  public function getTidsByEntity($entity, $eid) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['tid'])
            ->condition("si.entity", $entity)
            ->condition("si.eid", $eid)
            ->distinct()
            ->execute()
            ->fetchCol();
  }

  public function getSidsFromTids(array $tids) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['sid'])
            ->condition("si.tid", $tids, 'IN')
            ->execute()
            ->fetchCol();
  }

  public function getNodeIdsBySids(array $sids) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['eid'])
            ->condition("si.entity", 'node')
            ->condition("si.sid", $sids, 'IN')
            ->execute()
            ->fetchCol();
  }

  public function getSidsByEntity($entity, $eid, $limit = 100) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['sid'])
            ->condition("si.entity", $entity)
            ->condition("si.eid", $eid)
            ->range(0, $limit)
            ->orderBy('weight', 'DESC')
            ->execute()
            ->fetchCol();
  }

  public function loadByTid($tid) {
    $sids = $this->getSidsFromTids([$tid]);
    return $this->loadMultiple($sids);
  }

  public function getSidsFromTidsSorted(array $tids, $limit = 100) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['sid'])
            ->condition("si.tid", $tids, 'IN')
            ->range(0, $limit)
            ->orderBy('weight', 'DESC')
            ->execute()
            ->fetchCol();
  }

  public function loadByTidSorted($tid, $limit = 100) {
    $sids = $this->getSidsFromTidsSorted([$tid], $limit);
    return $this->loadMultiple($sids);
  }

  public function getSidFromTidAndEntity($tid, $entity, $eid) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['sid'])
            ->condition("si.tid", $tid)
            ->condition("si.entity", $entity)
            ->condition("si.eid", $eid)
            ->execute()
            ->fetchCol();
  }

  public function getSidsFromTidAndTitle($tid, $name, $limit = 100) {
    return $this->database->select($this->entityType->getBaseTable(), 'si')
            ->fields('si', ['sid'])
            ->condition("si.tid", $tid)
            ->condition("si.title", $name)
            ->range(0, $limit)
            ->orderBy('weight', 'DESC')
            ->execute()
            ->fetchCol();
  }

  public function loadSlogitemFromTidAndTitle($tid, $name) {
    $sids = $this->getSidsFromTidAndTitle($tid, $name, 1);
    return !empty($sids[0]) ? $this->load($sids[0]) : NULL;
  }

  public function hasSidWithTidAndEntity($tid, $entity, $eid) {
    $sids = $this->getSidFromTidAndEntity($tid, $entity, $eid);
    return !empty($sids);
  }

  public function loadByTidAndEntity($tid, $entity, $eid) {
    $sids = $this->getSidFromTidAndEntity($tid, $entity, $eid);
    return $this->loadMultiple($sids);
  }

  public function deleteByTid($tid) {
    $this->delete($this->loadByTid($tid));
  }

}
