<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Entity\SlogItemInterface.
 */

namespace Drupal\sxt_slogitem\Entity;

/**
 *
 */
interface SlogItemInterface {

  public function isValidTerm();

  public function isValidTarget($invalidate, $redirected = []);

  public function invalidate($msg = FALSE);

  public function getTerm();

  public function getExtendedId();

  public function getTargetEntityType();

  public function getTargetEntityId();

  public function getTargetSid();

  public function getRouteName();

  public function getRouteParameters();

  public function getRouteKey($values_only = FALSE);

  public function setRouteData($target_entity_type, $target_entity_id);

  public function getTermId();

  public function getWeight();

  public function getStatus();

  public function hashPrepare();

  public function invalidateRootTermsCache();

  public function getTargetEntity();

  public function getContentTitle();

  public function isNodeEntity();

  public function isRedirectItem();

  public function getRedirectTargetItem($redirected = []);

  public function getAttachData($redirected = []);
}
