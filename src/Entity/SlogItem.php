<?php

/**
 * @file
 * Contains \Drupal\sxt_slogitem\Entity\SlogItem.
 */

namespace Drupal\sxt_slogitem\Entity;

use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogtb\SlogTb;
use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Component\Utility\Crypt;
use Drupal\Component\Serialization\Json;
use Drupal\node\Entity\Node;
use Drupal\Core\Cache\Cache;
use Drupal\user\RoleInterface;

/**
 * Defines the slog entity class.
 *
 * @ContentEntityType(
 *   id = "slogitem",
 *   label = @Translation("Slogitem"),
 *   handlers = {
 *     "storage" = "Drupal\sxt_slogitem\SlogitemStorage",
 *     "sidebar_builder" = "Drupal\sxt_slogitem\XtsiSidebarBuilder",
 *     "list_builder" = "Drupal\sxt_slogitem\SlogitemListBuilder",
 *     "access" = "Drupal\sxt_slogitem\SlogitemAccessController",
 *     "form" = {
 *       "default" = "Drupal\sxt_slogitem\Form\SlogitemForm",
 *       "delete" = "Drupal\sxt_slogitem\Form\SlogitemDeleteForm",
 *       "edit" = "Drupal\sxt_slogitem\Form\SlogitemForm"
 *     },
 *   },
 *   base_table = "slogitem",
 *   entity_keys = {
 *     "id" = "sid",
 *     "label" = "title",
 *     "weight" = "weight",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/slogsys/sxt_slogitem/manage/{slogitem}/edit",
 *     "delete-form" = "/admin/slogsys/sxt_slogitem/manage/{slogitem}/delete"
 *   }
 * )
 */
class SlogItem extends ContentEntityBase implements SlogItemInterface {

  use EntityChangedTrait;



//  public function label() {
//    //todo::debug::disable
//    return parent::label()  . '  #' . $this->get('weight')->value;
//  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['sid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Slog ID'))
        ->setDescription(t('The slog item  ID.'))
        ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
        ->setLabel(t('UUID'))
        ->setDescription(t('The slog item UUID.'))
        ->setReadOnly(TRUE);

    $fields['tid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Taxonomy term'))
        ->setDescription(t('The taxonomy term to which the slog item is assigned.'))
        ->setRequired(TRUE);


    $description = t('Title of the slogitem. Allowed palceholder: %node, %nodeid', [
      '%node' => '@node',
      '%nodeid' => '@nodeid'
    ]);
    $fields['title'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Title'))
        ->setDescription($description)
        ->setRequired(TRUE)
        ->setTranslatable(TRUE)
        ->setDefaultValue('')
        ->setSetting('max_length', 255)
        ->setDisplayOptions('form', [
      'type' => 'string',
      'weight' => -10,
      'settings' => [
        'size' => 40,
      ],
    ]);

    $fields['route_name'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Route name'))
        ->setDescription(t('The machine name of a defined Route this slog item represents.'))
        ->setDefaultValue('slogitem.placeholder');

    $fields['route_parameters'] = BaseFieldDefinition::create('map')
        ->setLabel(t('Route parameters'))
        ->setDescription(t('A serialized array of route parameters of this slog item.'));

    $fields['entity'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Target Entity'))
        ->setDescription(t('Each route points to a target entity, e.g. node.'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 32);

    $fields['eid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Target Entity ID'))
        ->setDescription(t('The ID of the entity the route points to.'))
        ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Status'))
        ->setDescription(t('The status of this slog item (status in progress).'))
        ->setDefaultValue(0);

    $fields['weight'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Weight'))
        ->setDescription(t('The weight of this slog item in relation to other items.'))
        ->setDefaultValue(0);

    $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Created'))
        ->setDescription(t('The time that the slog item was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the slog item was last edited.'));

    return $fields;
  }

  /**
   * Whether the term is valid.
   * 
   * @return boolean
   */
  public function isValidTerm() {
    if ($this->getStatus() >= 0 && !SlogTx::termExists($this->getTermId())) {
      $tid = $this->getTermId();
      $msg = "Term not found ($tid)";
      $this->invalidate($msg);
    }
    return ($this->getStatus() >= 0);
  }

  public function isValidTarget($invalidate, $redirected = []) {
    $valid = FALSE;
    if ($target_entity = $this->getTargetEntity()) {
      if ($this->isRedirectItem()) {
        $sid = (int) $this->id();
        if (count($redirected) > 2) {
          $redirected[] = $sid;
          $sids = implode(';', $redirected);
          $msg = "Too many redirections ($sids)";
        }
        elseif (in_array($sid, $redirected)) {
          $redirected[] = $sid;
          $sids = implode(';', $redirected);
          $msg = "Circular redirection ($sids)";
        }
        elseif ($target_entity instanceof \Drupal\sxt_slogitem\Entity\SlogItem) {
          $redirected[] = $sid;
          $valid = $target_entity->isValidTarget(FALSE, $redirected);
        }
      }
      else {
        $valid = TRUE;
      }
    }
    else {
      $type = $this->getTargetEntityType();
      $eid = $this->getTargetEntityId();
      $msg = "Entity not found ($type/$eid)";
    }

    if (!$valid && $invalidate && $this->getStatus() >= 0) {
      $this->invalidate($msg);
    }
    return $valid;
  }

  /**
   * Invalidate this entity, ie set status to -1.
   */
  public function invalidate($msg = FALSE) {
    $this->setStatus(-1);
    $this->save();
    if ($msg) {
      $sid = $this->id();
      \Drupal::logger('sxt_slogitem')->error("Invalid item disabled($sid): $msg");
    }
  }

  /**
   * Return affiliated target term if slogitem entity is valid.
   * 
   * @return Drupal\slogtx\Interfaces\TxTermInterface or NULL
   */
  public function getTerm() {
    return ($this->isValidTerm() ? SlogTx::getMenuTerm($this->getTermId()) : FALSE);
  }

  public function getExtendedId() {
    $sid = $this->id();
    return ($sid < 0) ? $sid . '/' . $this->getTermId() : $sid;
  }

  public function labelExt() {
    $label = $this->label();
    if ($this->isRedirectItem()) {
      $slogitem = $this->getRedirectTargetItem();
      $ext = $slogitem ? $slogitem->label() : '??';
      if ($label === $ext) {
        $label = '->' . $label;
      }
      else {
        $label .= ' -> ' . $ext;
      }
    }
    return $label;
  }

  public function getTargetEntityType() {
    return $this->entity->value;
  }

  public function getTargetEntityId() {
    return $this->eid->value;
  }

  public function getTargetSid() {
    $type = $this->getTargetEntityType();
    return ($type === 'slogitem') ? $this->getTargetEntityId() : $this->id();
  }

  public function getRouteName() {
    return $this->route_name->value;
  }

  public function getRouteParameters() {
    return $this->route_parameters->getValue()[0];
  }

  public function getRouteKey($values_only = FALSE) {
    $prefix = $values_only ? '' : 'xtsiRoute::';
    $etype = $this->getTargetEntityType();
    $eid = $this->getTargetEntityId();
    return "{$prefix}{$etype}.{$eid}";
  }

  public function setRouteData($target_entity_type, $target_entity_id) {
    $target_entity = \Drupal::entityTypeManager()
        ->getStorage($target_entity_type)
        ->load($target_entity_id);
    $plugin = SlogXtsi::getTargetEntityPlugin($target_entity_type);
    if ($target_entity && $plugin) {
      $this->entity->value = $target_entity_type;
      $this->eid->value = $target_entity_id;
      $this->route_name->value = $plugin->getRouteName();
      $this->route_parameters->setValue($plugin->getRouteParameters($target_entity_id));
    }
    else {
      $args = [
        '@sid' => $this->id(),
        '@title' => $this->label(),
      ];
      $msg = t('Route data not set in slogitem @sid / @title', $args);
      throw new \RuntimeException($msg);
    }
    
    return $this;
  }

  public function setLabel($label) {
    $this->title->value = $label;
    return $this;
  }

  public function getTermId() {
    return $this->get('tid')->value;
  }

  public function setTermId($tid) {
    $this->tid->value = $tid;
    return $this;
  }

  public function getWeight() {
    return $this->weight->value;
  }

  public function setWeight($weight) {
    $this->weight->value = $weight;    
    return $this;
  }

  public function getStatus() {
    return $this->status->value;
  }

  public function setStatus($status) {
    $this->status->value = $status;
    return $this;
  }

  public function hashPrepare($cached_only = FALSE) {
    $sid = $this->id();
    $ext = ($sid < 0) ? '/' . $this->getTermId() : '';
    if ($sid < 0) {
      // not supported for now
      $msg = "SlogItem::hashPrepare(): invalid sid: $sid";
      \Drupal::logger('sxt_slogitem')->error($msg);
      return FALSE;
    }
    if (!$this->isValidTarget(TRUE)) {
      return FALSE;
    }

    // now prepare and return hash data
    $cid = $this->getRouteKey();
    if (!$cache = SlogXt::cache()->get($cid)) {
      if ($cached_only) {
        $hash = '';
      }
      else {
        $content = $this->getTargetEntityHashValues();
        $hash = Crypt::hashBase64(serialize($content));
        $this->invalidateRootTermsCache();
        $tags = $this->getCacheTagsToInvalidate();
        SlogXt::cache()->set($cid, $hash, Cache::PERMANENT, $tags);
      }
    }
    else {
      $hash = $cache->data;
    }
    return ['sid' => $sid . $ext, 'cid' => $cid, 'hash' => $hash];
  }

  public function invalidateRootTermsCache() {
    if (!$this->isRedirectItem()  //
        && ($entity = $this->getTargetEntityType())  //
        && ($eid = $this->getTargetEntityId())) {
      $invalid_tids = NULL;
      $tids = SlogXtsi::getTidsByEntity($entity, $eid);
      $tids = SlogTx::dbGetValidTids($tids, $invalid_tids);
//            if (!empty($invalid_tids)) {
//              // do nothing
//              // do not remove garbage caused by unlock/delete of slogtx entities
//            }
      foreach ($tids as $tid) {
        if ($menu_term = SlogTx::getMenuTerm($tid)) {
          $menu_term->getRootTerm()->invalidateCache();
        }
      }
    }
  }

  /**
   * Overrides \Drupal\Core\Entity\Entity::getCacheTagsToInvalidate();
   */
  public function getCacheTagsToInvalidate() {
    if ($this->isValidTarget(FALSE) && $entity = $this->getTargetEntity()) {
      $tags = $entity->getCacheTagsToInvalidate();
      if ($this->isNodeEntity() && SlogXtsi::nodeHasTbnode($entity->id())) {
//        $rootterm = SlogXtsi::getNodeSubmenuRootTerm($entity->id());
        $rootterm = SlogTx::getSysNodeRootTermSubmenu($entity->id());
//todo::current:: TxRootTermGetPlugin - new
        $tags[] = 'slogtx_rt:' . $rootterm->id();
      }
      return $tags;
    }
    return [];
  }

  public function getTargetEntityHashValues() {
    $target_content = 'none:' . $this->id();
    if ($this->isValidTarget(FALSE) && $entity = $this->getTargetEntity()) {
      if ($this->isRedirectItem()) {
        $target_content = $entity->getTargetEntityHashValues();
      }
      else {
        $target_content = $entity->toArray();
        if ($this->isNodeEntity() && SlogXtsi::nodeHasTbnode($entity->id())) {
          $target_content['tbnodeHash'] = SlogXtsi::getTbnodeHash($entity, FALSE); // FALSE: ensure building
        }
      }
      
      unset($target_content['uid']);
      unset($target_content['vid']);
    }

    return $target_content;
  }

  public function getTargetEntityHash($cached_only = FALSE) {
    $hash = FALSE;
    if ($this->isRedirectItem()) {
      if ($this->isValidTarget(FALSE) && $entity = $this->getTargetEntity()) {
        $hash = $entity->getTargetEntityHash($cached_only);
      }
    }
    else {
      $hash = $this->hashPrepare($cached_only)['hash'];
    }
    return $hash;
  }

  public function getTargetEntity() {
    if (!isset($this->targetEntity)) {
      $this->targetEntity = \Drupal::entityTypeManager()
          ->getStorage($this->getTargetEntityType())
          ->load($this->getTargetEntityId());
    }
    return $this->targetEntity;
  }

  public function getContentTitle() {
    $routeName = $this->getRouteName();
    $routeParams = $this->getRouteParameters();
    switch ($routeName) {
      case 'entity.node.canonical':
        if ($node = Node::load($routeParams['node'])) {
          return $node->getTitle();
        }
        break;
    }
    return '???: ' . $routeName . '/' . Json::encode($parameters);
  }

  public function ajaxExecute() {
    if ($this->isRedirectItem()) {
      return $this->getRedirectTargetItem()->ajaxExecute();
    }

    $route_name = $this->getRouteName();
    $route_parameters = $this->getRouteParameters();
    if ($route_matcher = SlogXt::getRouteMatch($route_name, $route_parameters)) {
      $entity_type = SlogXt::getEntityTypeByRouteName($route_name);
      $entity = $route_matcher->getParameter($entity_type);
      $hasAccess = $entity ? $entity->access('view') : FALSE;
      }

    if (isset($entity) && $hasAccess) {
      $bundle = !empty($entity->entityKeys['bundle']) ? $entity->bundle() : $id;
      $plugin_id = "$entity_type:$bundle";
      $configuration = [
        'slogitem_id' => $this->id(),
        'term_id' => $this->getTermId(),
        'extended_id' => $this->getExtendedId(),
        'entity' => $entity,
        'route_name' => $route_name,
        'route_parameters' => $route_parameters,
        'route' => $route_matcher->getRouteObject(),
      ];

      $manager = SlogXt::pluginManager('content');
      if ($manager->hasDefinition($plugin_id)) {
        $plugin = $manager->createInstance($plugin_id, $configuration);
        $result = $plugin->execute();
        if (!empty($result) && !empty($result['settings'])) {
          $attach_data = $this->getAttachData();
          $pdRole = false;
          $menu_term = $this->getTerm();
          $entity = $menu_term->setRequiredEntity();
          if ($entity && $entity instanceof RoleInterface //
              && $menu_term->getVocabulary()->getToolbarId() === 'role') {
            $pdRole = $entity->id();
          }

          $result['settings']['pdRole'] = $pdRole;
          $result['liData'] = $attach_data['content'];
        }
      }
      else {
        $msg = 'SlogItem::ajaxExecute(): Plugin not found: ' . $plugin_id;
        \Drupal::logger('sxt_slogitem')->error($msg);
      }
    }
    elseif (!isset($entity)) {
      $msg = 'SlogItem::ajaxExecute(): Entity not found: ' . "$name($id)";
      \Drupal::logger('sxt_slogitem')->error($msg);
    }

    if (empty($result)) {
      // fallback if no result found
      $cid = $this->getRouteKey();
      $title = $this->title->value;
      $content = 'PHP-Fault in ...\SlogItem::ajaxExecute()'
          . '<br />Title:: ' . $title
          . '<br />extended_id:: ' . $this->getExtendedId()
          . '<br />route_name:: ' . $route_name
          . '<br />cid:: ' . $cid
          . '<br />access:: ' . $hasAccess;
      $plabel = '<div class="header-silist-label"><span></span></div>';
      $result = [
        'header' => $plabel . t('Content not available.'),
        'special' => false,
        'settings' => [
          'nocache' => false,
          'headerActions' => [
            [
              'label' => t('Refresh'),
              'do' => 'refresh',
              'href' => SlogXt::getHRefClientside('refresh'),
              'class' => 'icon-refresh',
            ],
          ],
        ],
      ];
    }

    return $result;
  }

  public function isNodeEntity() {
    return ($this->entity->value === 'node');
  }

  public function isRedirectItem() {
    return ($this->getRouteName() === 'entity.slogitem.redirect' && $this->getTargetEntityType() === 'slogitem');
  }

  public function getRedirectTargetItem($redirected = []) {
    if ($this->isRedirectItem()) {
      $sid = (int) $this->id();
      if (!in_array($sid, $redirected)) {
        if ($slogitem = $this->getTargetEntity()) {
          // return data from slogitem redirected to
          $redirected[] = $sid;
          return $slogitem->getRedirectTargetItem($redirected);
        }
        // redirection failed, return FALSE after logging
        $redirect_sid = $this->getTargetEntityId();
        $msg = "SlogItem::getRedirectTargetItem(): redirection failed: $sid/$redirect_sid";
        \Drupal::logger('sxt_slogitem')->error($msg);
        return FALSE;
      }
      else {
        // circular redirection, return FALSE after logging
        $msg = "SlogItem::getRedirectTargetItem(): circular redirection (sid=$sid)";
        \Drupal::logger('sxt_slogitem')->error($msg);
        return FALSE;
      }
    }

    return $this;
  }

  public function getAttachData($redirected = []) {
    $sid = (int) $this->id();
    if ($sid <= 0 || !$this->isValidTarget(TRUE)) {
      return FALSE;
    }

    if ($this->isRedirectItem()) {
      return $this->getRedirectTargetItem()->getAttachData();
    }

    // collect and return attach data
    $data = FALSE;
    $term = $this->getTerm();
    $vocabulary = $term ? $term->getVocabulary() : false;
    if ($vocabulary) {
      $entity = $term->setRequiredEntity();
      $tid = $term->id();
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      $slogtb_handler = SlogTb::getSlogtbHandler();
      $list_path = $slogtb_handler->getPathPrefix() . $tid;
      $content_path = SlogXt::getBaseAjaxPath('sxt_slogitem') . "sid/$sid";

      $list = [
        'path' => $list_path,
        'tid' => $tid,
        'toolbar' => $toolbar,
        'toolbartab' => $toolbartab,
      ];

      $content = [
        'label' => $this->label(),
        'mainLabel' => $this->label(),
        'path' => $content_path,
        'sid' => $sid,
        'tid' => $tid,
        'extendedId' => $sid,
        'toolbar' => $toolbar,
        'routeKey' => $this->getRouteKey(),
        'testHash' => $this->getTargetEntityHash(FALSE), // FALSE: ensure building
        'listLabel' => $term->label(),
        'pathLabel' => $vocabulary->headerPathLabel(),
        'tbPathData' => $list,
        'byEntity' => $this->getRouteKey(TRUE),
      ];

      if ($toolbar === 'role' && $entity && $entity instanceof RoleInterface) {
        $content['pdRole'] = $entity->id();
      }
      if ($this->getTargetEntityType() === 'node' && ($node = Node::load($this->getTargetEntityId()))) {
        $content['mainLabel'] = $node->label();
      }

      $data = [
        'list' => $list,
        'content' => $content,
      ];
    }

    return $data;
  }

}
