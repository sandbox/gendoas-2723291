/**
 * @file sxt_slogitem.js
 *
 * Defines the behavior of the sxt_slogitem toolbars.
 */
(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  Drupal.sxt_slogitem = {
    cpdph: '{@@}'
  };
  var _xtsi = Drupal.sxt_slogitem
      , _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt;

  /**
   *
   */
  Drupal.behaviors.sxt_slogitem = {
    attach: function (context) {
//      slogLog('Drupal.behaviors.sxt_slogitem.....attach()');
      var xtsiModels = _xtsi.models
          , xtsiColls = _xtsi.colls
          , xtsiViews = _xtsi.views;
      if (xtsiModels.mainModel) {
        return;
      }

      // main (first of all)
      xtsiModels.mainModel = new _xtsi.XtsiMainModel();
      xtsiViews.mainView = new _xtsi.XtsiMainView({
        el: $('body').first(),
        model: xtsiModels.mainModel
      });

      // model for histore, bookmark, surprise
      xtsiModels.historyModel = new _xtsi.XtsiHistoryModel();
      xtsiModels.bookmarkModel = new _xtsi.XtsiBookmarkModel();
      xtsiModels.bmstorageModel = new _xtsi.XtsiBmstorageModel();

      xtsiColls.xtSiList = new Backbone.Collection();
      xtsiColls.xtSiContent = new Backbone.Collection();

      // Lists
      xtsiModels.itemList = {};
      xtsiViews.itemList = {};
      $.each(_stb.views, function (toolbar, views) {
        _xtsi.createListView(toolbar, views.slogtbView);
      });

      // Contents
      xtsiModels.itemContent = {};
      xtsiViews.itemContent = {};
      $.each(_xtsi.targetsContent, function (idx, targetid) {
        var $content = _xtsi.getTargetElement(targetid, true);
        var model = xtsiModels.itemContent[targetid] = new _xtsi.XtsiContentModel({
          id: targetid,
          target: targetid
        });
        xtsiColls.xtSiContent.add(model);
        xtsiViews.itemContent[targetid] = new _xtsi.XtsiContentView({
          el: $content.first(),
          model: model,
          collection: xtsiColls.xtSiContent,
          target: targetid
        });
      });

      // Toolbar lines for xtsiList and xtsiContent
      xtsiModels.itemTbLine = {};
      xtsiViews.itemTbLine = {};
      var xtRegionData = xtsiModels.mainModel.get('xtRegionData') || {};
      $.each(xtRegionData, function (key, rData) {
        var model = xtsiModels.itemTbLine[key] = new _sxt.TbLineModel({id: key});
        xtsiViews.itemTbLine[key] = new _sxt.TbLineView({
          el: _sxt.getElementTbLine(rData.$region),
          model: model,
          xtRegionData: rData,
          id: key
        });
        rData.tblineView = xtsiViews.itemTbLine[key];
        xtsiViews.itemTbLine[key].xtsiInitialize();
      });
      xtsiModels.historyModel.xtRegionData = xtRegionData;
    } // attach: function
  };  // Drupal.behaviors.sxt_slogitem




  /**
   * Slog namespace.
   */
  var initXtsi = {
    // A hash of instances: views, models and colls.
    views: {},
    models: {},
    colls: {},
    commands: {},
    targetsContent: ['tContentMain'],
    hasTargetNext: false,
    setHistoryEnabled: false,
    bmMaxError: Drupal.t('Too many bookmarks'),
    bmLoaded: Drupal.t('Bookmarks have been loaded'),
    lastEdited: Drupal.t('Last Edit ...'),
    basePath: function (trim) {
      return _sxt.basePath('sxt_slogitem', trim);
    },
    baseAjaxPath: function (trim) {
      return _sxt.baseAjaxPath('sxt_slogitem', trim);
    },
    enableSetHistory: function (enable) {
      var last = this.setHistoryEnabled;
      this.setHistoryEnabled = !!enable;
      return last;
    },
    canSetHistory: function () {
      return (this.setHistoryEnabled || !_sxt.isOpenModalDialog());
    },
    createListView: function (toolbar, slogtbView) {
      var xtsiColls = this.colls
          , xtsiModels = this.models
          , xtsiViews = this.views
          , model = xtsiModels.itemList[toolbar] = new this.XtsiListModel({
        id: toolbar
      });

      xtsiColls.xtSiList.add(model);

      xtsiViews.itemList[toolbar] = new _xtsi.XtsiListView({
        el: slogtbView.model.statics.$contentTarget.first(),
        model: model,
        collection: xtsiColls.xtSiList,
        toolbar: toolbar,
        slogtbView: slogtbView
      });
    },
    getToolbarListItem: function (toolbar) {
      var model = _stb.getSlogtbModel(toolbar);
      return model && model.statics ? model.statics.$contentTarget || $() : $();
    },
    getModel: function (collection, id) {
      return this.colls[collection].get({'id': id});
    },
    mainModel: function () {
      return this.models.mainModel;
    },
    mainView: function () {
      return this.views.mainView;
    },
    historyModel: function () {
      return this.models.historyModel;
    },
    bookmarkModel: function () {
      return this.models.bookmarkModel;
    },
    bookmarkView: function () {
      return this.views.bookmarkView;
    },
    bmstorageModel: function () {
      return this.models.bmstorageModel;
    },
    bmstorageView: function () {
      return this.models.bmstorageModel;
    },
    surpriseModel: function () {
      return this.models.surpriseModel;
    },
    surpriseView: function () {
      return this.views.surpriseView;
    },
    getActiveContentView: function () {
      var targetid = this.mainModel().get('activeContentTarget');
      return this.getSiContentView(targetid);
    },
//    isActiveOver: function () {
//      return false;
//    },
    getAlternateCTarget: function () {
      // overridden in sjqlout (slogitemx.js)
      return this.mainModel().get('activeContentTarget');
    },
    getAlternateCView: function () {
      var targetid = this.getAlternateCTarget();
      return this.getSiContentView(targetid);
    },
    getSiListView: function (toolbar) {
      return this.views.itemList[toolbar];
    },
    getSysListView: function () {
      return this.getSiListView(_sxt.getTbSysId());
    },
    getToolbarByData: function (data) {
      data = data || {};
      var toolbar = data.data ? data.data.toolbar : undefined
          , pdRole = data.pdRole;
      if (data.type === 'list' && !!pdRole && pdRole !== _sxt.getUserDefaultRole()) {
        toolbar = _sxt.getTbSysId();
      }
      return toolbar;
    },
    getSiContentModel: function (targetid) {
      return this.models.itemContent[targetid];
    },
    getSiContentView: function (targetid) {
      return this.views.itemContent[targetid];
    },
    getContentTargetByTb: function (toolbar) {
      var mm = this.mainModel()
          , activeTarget = mm.get('activeContentTarget')
          , isTbNode = (toolbar === _sxt.getTbSysNode())
          , hasNext = _xtsi.hasTargetNext
          , nextTarget = mm.get('visibleOverNext') ? 'tContentNextOver' : 'tContentNext'
          , targetid = (isTbNode && hasNext) ? nextTarget : activeTarget;
      return targetid;
    },
    getContentViewBySid: function (sid) {
      var self = this
          , cView = false;
      $.each(_xtsi.targetsContent, function (idx, targetid) {
        var view = self.getSiContentView(targetid)
            , data = view.getListItemData();
        if (data.sid == sid) {
          cView = view;
          return false;
        }
      });
      return cView;
    },
    contentPathLabel: function (pathLabel, listLabel, elementLabel) {
      return (pathLabel + '/' + listLabel + ' - ' + elementLabel);
    },
    getTargetElement: function () {
      // overridden in sjqlout (slogitemx.js)
      return $('#block-slogxtcontentdefault');
    },
    getTargetFromElement: function () {
      // overridden in sjqlout (slogitemx.js)
      return 'tContentMain';  // the only targetid
    },
    setHeaderClass: function ($el) {
      var $w = $el.find('.dropbutton-wrapper')
          , hasDBtn = !!$w.length
          , isSingle = hasDBtn ? $w.hasClass('dropbutton-single') : false;
      $el.toggleClass('has-dropbutton', hasDBtn);
      $el.toggleClass('dropbutton-is-single', isSingle);
    },
    getTblineModels: function () {
      return this.models.itemTbLine;
    },
    getTblineViews: function () {
      return this.views.itemTbLine;
    },
    getTblineViewByKey: function (key) {
      return this.views.itemTbLine[key];
    },
    getCurrentContentViewByKey: function (key) {
      var tbline = !!key ? this.getTblineViewByKey(key) : false
          , view = tbline ? tbline.getCurrentContentView() : false;
      return view;
    },
    getTblineView: function (toolbar) {
      var tblineViews = this.getTblineViews()
          , tblIdx, tblView;
      for (tblIdx in tblineViews) {
        tblView = tblineViews[tblIdx];
        if (tblView.hasToolbarTab(toolbar)) {
          return tblView;
        }
      }
    },
    getTblineActions: function (forContent) {
      var key = forContent ? 'tblineContent' : 'tblineList';
      return drupalSettings.sxt_slogitem[key] || {};
    },
    getActions: function (key) {
      var actions = drupalSettings.sxt_slogitem['actions'] || {};
      return actions[key] || {};
    },
    getHistoryActions: function () {
      return this.getActions('dialogHistory');
    },
    getBookmarkActions: function () {
      return this.getActions('dialogBookmark');
    },
    getSurpriseActions: function () {
      return this.getActions('dialogSurprise');
    },
    getMaxItems: function (scope) {
      var maxItems = drupalSettings.sxt_slogitem['maxItems'] || {};
      return (maxItems[scope] || false);
    },
    getFromPathData: function (types) {
      var fromPath = (drupalSettings.sxt_slogitem.autorun || {}).fromPath || {};
      if (fromPath.type && _.contains(types, fromPath.type) && fromPath.data) {
        return fromPath;
      }
      return false;
    },
    deleteAutorunFromPath: function () {
      var autorun = drupalSettings.sxt_slogitem.autorun;
      if (autorun && autorun.fromPath) {
        delete autorun.fromPath;
      }
    },
    getBmstorageActionPath: function (bmtype, action) {
      return 'bmstorage/' + action + '/' + bmtype + '/{next_entity_id}';
    },
    bmstorageViewPath: function (bmtype, tid) {
      var path = 'bmstorage/view/{bookmark_type}/{next_entity_id}';
      !!bmtype && (path = path.replace('{bookmark_type}', bmtype));
      !!tid && (path = path.replace('{next_entity_id}', tid));
      return path;
    },
    bmstorageViewStorageKey: function (path) {
      var baseAjaxPath = _xtsi.baseAjaxPath()
          , pdata = _sxt.helper.splitPath(path, baseAjaxPath, ['none', 'nonex', 'bmtype', 'bmtid'])
          , bmpath = pdata.bmtype ? this.bmstorageViewPath(pdata.bmtype, pdata.bmtid) : path;
      return _sxt.storageKey('BMS', bmpath);
    },
    hasPermRoleContent: function (perm) {
      if (_.isBoolean(perm)) {
        return perm;
      }

      if (Drupal.sxt_group.hasPermission('admin sxtrole-content')) {
        return true;
      }

      return Drupal.sxt_group.hasPermission(perm);
    },
    isEditable: function (bundle, toolbar) {
      var editable = drupalSettings.sxt_slogitem[bundle] || {};
      return !!editable[toolbar]
    }
  };
  $.extend(true, _xtsi, initXtsi);


  var sxtCmds = {
    applyLocalSwitches: function (data) {
//      slogLog('_xtsi.commands.applyLocalSwitches...............');
      _.each(data, function (val, key) {
        switch (key) {
          case 'XtsiContentEnlarge':
            $('#lout-c-center-content').toggleClass('xtsi-enlarge-content', val);
            break;
          case 'XtsiListEnlarge':
            $('.stb-content.block-slogtb').toggleClass('xtsi-enlarge-list', val);
            break;
          case 'XtsiListCompact':
            $('.stb-content.block-slogtb').toggleClass('xtsi-compact-list', val);
            break;
          case 'XtsiMwtopAutoopen':
            _sxt.localSwitches.mwtocAutoopen = val;
            break;
          case 'XtsiMwCommentShowToggler':
            _sxt.localSwitches.mwcShowToggler = val;
            _xtsi.mainView().refreshMwcTogglerAll();
            break;
          default:
            _sxt.notImplemented('_xtsi.commands.applyLocalSwitches::' + key);
            break;
        }
      });
    }
  }

  // extend _xtsi.commands
  $.extend(true, _xtsi.commands, sxtCmds);
  sxtCmds = null;


  //
  // on document ready
  //
  $(document).ready(function () {
    _xtsi.mainView().onDocumentReady();
  });


})(jQuery, Drupal, drupalSettings, Backbone, _);
