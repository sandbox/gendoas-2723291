/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var mainActionsExt = {
    setXtSysDonData: function (actionKey, rolePerm) {
      var donView = _sxt.getDoNavigateView()
          , baseActionId = donView.dialogView.baseActionId || false
          , actionData = _sxt.getMainActionData(actionKey, 'sxt_slogitem')
          , baseData = _sxt.getDonBaseData() || {}
      , baseArgs = baseData.args || {}
      donView.resetSelected(baseData, _sxt.invalidSel);
      if (baseData.noDonSelect) {
        donView.highlight && donView.highlight();
      } else if (_sxt.canApply(baseActionId, actionData.canApply, baseArgs)) {
//        slogLog('xtsi.action.js...... setXtSysDonData: ' + actionKey + ' / ' + baseActionId);
        var targetKey = baseData.targetKey
            , isTbMenu = (targetKey === 'SlogtbMenuItem')
//            , isTbTab = (targetKey === 'SlogtbTabItem')
            , rtKey = actionKey + 'RootTerm'
            , rootterm = drupalSettings.sxt_slogitem[rtKey] || {}
        , tid = rootterm.tid || 0
            , donData = {
              toolbar: _sxt.getTbSysId(),
              toolbartab: actionKey,
              targetKey: targetKey,
              rolePerm: rolePerm,
              noServerResolve: true,
              entityId: isTbMenu ? -tid : tid,
              label: rootterm.label || '[rootterm.label]',
              info: rootterm.info || '[rootterm.info]'
            };
        _sxt.setDoNavigateRawData(donData);
      }
    },
    actionPromoted: function () {
      var baseData = _sxt.getDonBaseData() || {};
      if (_sxt.isDoNavigateActive() && !baseData.noDonSelect) {
        this.getActionsRoot('sxt_slogitem').setXtSysDonData('promoted', 'admin xtsi-promoted');
      } else {
        _xtsi.mainView().populatePromoted();
      }
    },
    actionSurprise: function () {
      var baseData = _sxt.getDonBaseData() || {};
      if (_sxt.isDoNavigateActive() && !baseData.noDonSelect) {
        this.getActionsRoot('sxt_slogitem').setXtSysDonData('surprise', 'admin xtsi-surprise');
      } else {
        _xtsi.mainView().populateSurprise();
      }
    },
    actionBmstorage: function () {
      if (_sxt.isDoNavigateActive()) {
        // call in slogxt, handled like archive/trash
        this.getActionsRoot('slogxt').setXtSysDonData('bmstorage');
      } else {
        var args = {
          view: 'XtsiBmstorageView',
          provider: 'sxt_slogitem',
          options: {
//            infoText: 'actionBmstorage'
          }
        };
        _sxt.dialogViewOpen(args);
      }
    }
  };
  _sxt.MainActionsView.prototype.actions.sxt_slogitem = mainActionsExt;

})(jQuery, Drupal, drupalSettings);

