/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt;

  var xtsiMainViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiMainView',
    initializedReady: $.noop,
    showContent: $.noop,
    activeContentMoveToMainView: $.noop,
    onChangeActiveContentTarget: $.noop,
    initialize: function (options) {
      this.sxtIsPermanent = true;
      this.$btnItemAction = $('<div id="xtsi-item-action"></div>');
      this.resetBtnItemAction();
      var stateModel = _sxt.stateModel()
          , tbmainModel = _stb.mainModel()
          , xtsiMainModel = _xtsi.mainModel()
          , donView = _sxt.getDoNavigateView(true);
      this.listenTo(stateModel, 'change:stateLoadReady', this.onChangeStateLoadReady);
      this.listenTo(tbmainModel, 'change:editedTbMenu', this.onChangeEditedTbMenu);
      this.listenTo(tbmainModel, 'change:editedTbTab', this.onChangeEditedTbTab);
      this.listenTo(tbmainModel, 'change:movedTbMenu', this.onChangeMovedTbMenu);
      this.listenTo(this.model, 'change:editedSlogitem', this.onChangeEditedSlogitem);
      this.listenTo(tbmainModel, 'change:rebuildRoleState', this.onChangeRebuildRoleState);
      this.listenTo(this.model, 'change:activeContentTarget', this.onChangeActiveContentTarget);
      this.listenTo(this.model, 'change:activeSiListItem', this.onChangeActiveSiListItem);
      this.listenTo(donView.model, 'change:isActive', this.onChangeIsActive);
      this.listenTo(donView.model, 'change:rawData', this.onChangeRawData);
      this.listenTo(xtsiMainModel, 'change:movedSlogitems', this.onChangeMovedSlogitems);

      this.showPromotedSurprise(false);
      this.initializedReady();
      return this;
    },
    resetBtnItemAction: function () {
      this.$el.prepend(this.$btnItemAction);
    },
    onChangeIsActive: function (dm, isActive) {
      this.showPromotedSurprise(isActive);
    },
    onChangeRawData: function (dm, rawData) {
      if (rawData.targetKey === 'SlogtbMenuItem' && rawData.toolbar === _sxt.getTbSysNode()) {
        rawData.liData = _xtsi.getSiContentView('tContentMain').getListItemData();
      }
    },
    detechItemAction: function () {
      this.$btnItemAction.detach();
    },
    onDocumentReady: function () {
      this.activateContent('tContentMain');

      var tblineViews = _xtsi.getTblineViews();
      _.each(tblineViews, function (tblineView) { // xtsi list
        !tblineView.isContent && tblineView.onDocumentReady();
      });
    },
    showPromotedSurprise: function (force) {
      var hasPromoted = force ? true : this.model.get('promotedData') || false
          , hasSurprise = force ? true : this.model.get('surpriseData') || false;
      $('body')
          .toggleClass('xtsi-no-promoted', !hasPromoted)
          .toggleClass('xtsi-no-surprise', !hasSurprise);
    },
    onChangeStateLoadReady: function (stateModel, ready) {
      var tblineViews = _xtsi.getTblineViews()
          , xtsiFromPath = _xtsi.getFromPathData(['msgDialog', 'userDialog']);
      if (ready && tblineViews) {
        _.each(tblineViews, function (tblineView) { // xtsi list
          !tblineView.isContent && tblineView.ensureContent();
        });
        _.each(tblineViews, function (tblineView) { // xtsi content
          tblineView.isContent && tblineView.ensureContent();
        });
      }
      if (xtsiFromPath) {
        var fpData = xtsiFromPath.data;
        _xtsi.deleteAutorunFromPath();
        if (xtsiFromPath.type === 'msgDialog') {
          var args = {
            view: 'XtMessageView',
            provider: 'slogxt',
            options: fpData
          };
          setTimeout(function () {
            _sxt.dialogViewOpen(args);
          }, 10);
        } else if (xtsiFromPath.type === 'userDialog') {
          setTimeout($.proxy(this.handleUserPath, this, fpData), 10);
        }
      }
    },
    handleUserPath: function (path) {
      var pathPrefix = 'user/'
//        'path' => "/$basePath/user/{command}/{param1}/{param2}/{param3}/{param4}",
          , keys = ['command', 'param1', 'param2', 'param3', 'param4']
          , pdata = _sxt.helper.splitPath(path, pathPrefix, keys)
          , cmd = pdata.command || ''
          , handled = false;
      if (_.contains(['login', 'register', 'password'], cmd)) {
        handled = true;
        _sxt.views.mainActionsView.openLoginView(cmd);
      } else if (cmd === 'reset') {
        handled = true;
        var args = {
          view: 'UserResetView',
          provider: 'sxt_slogitem',
          options: {
            path: path,
            splitted: pdata
          }
        };
        _sxt.dialogViewOpen(args);
      }

      if (!handled) {
        slogErr('User path not handled: ' + path);
      }
    },
    updateDataContent: function (sids, fromPreLoadReady) {
      if (!sids)
        return;

      var sids_done = [];
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cmView = _xtsi.getSiContentView(target)
            , cmData = cmView ? cmView.getListItemData() : {}
        , cmSid = cmData ? cmData.sid || false : false;
        if (cmSid && !!sids[cmSid] && !sids_done[cmSid]) {
          sids_done[cmSid] = true;
          var newData = cmData ? sids[cmSid].content || false : false;
          if (newData) {
            var lView = _xtsi.getSiListView(newData.toolbar)
                , slogitemList = !!lView ? lView.getSlogitemListData() || {} : {}
            , sils = slogitemList.settings || false;
            if (sils && slogitemList.tid == newData.tid) {
              sils.hashes[newData.routeKey] = newData.testHash;
            }
          }
          cmView.fromPreLoadReady = !!fromPreLoadReady;
          cmView.setListItemData(newData);
          delete cmView.fromPreLoadReady;
        }
        newData && (cmData.testHash = newData.testHash);
      });
    },
    onChangeMovedSlogitems: function (xtsimm, done_sids) {
      if (_.isEmpty(done_sids)) {
        return;
      }

      // contents
      this.updateDataContent(done_sids);

      // promoted
      if (this.model.hasPromotedData()) {
        var pData = (this.model.getPromotedData()) || false
            , cData = pData ? pData.content : false;
        if (cData && !!done_sids[cData.sid]) {
          var newData = done_sids[cData.sid];
          pData.content = newData.content;
          pData.list = newData.list;
        }
      }

      // surprise
      if (this.model.hasSurpriseData()) {
        $.each(this.model.getSurpriseData(), function (idx, sData) {
          var cData = sData['content'] || false;
          if (cData && !!done_sids[cData.sid]) {
            var newData = done_sids[cData.sid];
            sData.content = newData.content;
            sData.list = newData.list;
          }
        });
      }
    },
    onChangeEditedTbMenu: function (tbmm, data) {
      // contents
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cmView = _xtsi.getSiContentView(target)
            , cmData = cmView ? cmView.getListItemData() : {};
        if (!!cmData.tid && cmData.tid === data.tid) {
          var $pEl = cmView.hview.$el.find('.path-label')
              , cmSiContent = cmView.model.get('slogitemContent') || {}
          , cmSettings = cmSiContent.settings || false
              , hModel = cmView.hmodel
              , hData = hModel.get('headerData')
              , listLabel = cmData.listLabel
              , pl = cmData.pathLabel + '/'
              , re = new RegExp(pl + listLabel);
          hData.pathLabel = hData.pathLabel.replace(re, pl + data.name);
          cmData.listLabel = data.name;
          cmView.itemLData && (cmView.itemLData.pathLabel = hData.pathLabel);
          $pEl.text(hData.pathLabel);
          cmSettings && (cmSettings.listLabel = data.name);
        }
      });

      // promoted
      if (this.model.hasPromotedData()) {
        var cData = (this.model.getPromotedData())['content'] || false;
        if (cData && !!cData.tid && cData.tid === data.tid) {
          cData.listLabel = data.name;
        }
      }

      // surprise
      if (this.model.hasSurpriseData()) {
        $.each(this.model.getSurpriseData(), function (idx, sData) {
          var cData = sData['content'] || false;
          if (cData && !!cData.tid && cData.tid === data.tid) {
            cData.listLabel = data.name;
          }
        });
      }
    },
    onChangeEditedTbTab: function (tbmm, data) {
      // list view
      var defaultRole = _sxt.getUserDefaultRole()
          , toolbar = data.toolbar
          , toolbartab = data.toolbartab
          , pdRole = data.pdRole || false
          , lView = _xtsi.getSiListView(toolbar);
      pdRole && _sxt.setRoleHeaderPath(pdRole, toolbartab, data.pathLabel);
      if (lView && lView.isCurToolbartab(toolbartab)) {
        lView.show();
        lView.refresh();
      }

      // contents
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cmView = _xtsi.getSiContentView(target)
            , cmData = cmView ? cmView.getListItemData() : {}
        , pd = cmData.tbPathData || {}
        , okTb = !!pd.toolbartab && pd.toolbartab === toolbartab && pd.toolbar === toolbar
            , okRole = cmData.pdRole ? cmData.pdRole === defaultRole : true;
        if (okTb && okRole) {
          var $pEl = cmView.hview.$el.find('.path-label')
              , cmSiContent = cmView.model.get('slogitemContent') || {}
          , cmSettings = cmSiContent.settings || false
              , hModel = cmView.hmodel
              , hData = hModel.get('headerData')
              , ll = '/' + cmData.listLabel
              , parts = hData.pathLabel.split(ll);
          hData.pathLabel = data.pathLabel + ll + (parts[1] || '');
          cmData.pathLabel = data.pathLabel;
          cmView.itemLData && (cmView.itemLData.pathLabel = hData.pathLabel);
          $pEl.text(hData.pathLabel);
          cmSettings && (cmData.pathLabel = cmSettings.pathLabel = data.pathLabel);
        }
      });

      // promoted
      if (this.model.hasPromotedData()) {
        var cData = (this.model.getPromotedData())['content'] || {}
        , pd = cData.tbPathData || {};
        if (!!pd.toolbartab && pd.toolbartab === toolbartab && pd.toolbar === toolbar) {
          cData.pathLabel = data.pathLabel;
        }
      }

      // surprise
      if (this.model.hasSurpriseData()) {
        $.each(this.model.getSurpriseData(), function (idx, sData) {
          var cData = sData['content'] || {}
          , pd = cData.tbPathData || {};
          if (!!pd.toolbartab && pd.toolbartab === toolbartab && pd.toolbar === toolbar) {
            cData.pathLabel = data.pathLabel;
          }
        });
      }
    },
    onChangeMovedTbMenu: function (tbmm, data) {
//      slogLog('XtsiMainView: ................onChangeMovedTbMenu');
      var tbPathData = {
        path: data.moved_path,
        tid: data.moved_tid,
        toolbar: data.tToolbar,
        toolbartab: data.tToolbartab
      };

      if (data.reload) {
        localStorage.removeItem(_sxt.storageKey('StbMenu', tbPathData.path, 'hash'));
        drupalSettings.sxt_slogitem.xtsiAutorunFromPath = {
          type: 'xtsiList',
          data: {
            content: false,
            list: tbPathData
          }
        };
        return; // do nothing
      }

      var moved_tids = data.moved_tids
          , oldPathLabel = data.sPathLabel
          , newPathLabel = data.tPathLabel
          , newToolbar = data.tToolbar
          , newToolbartab = data.tToolbartab
          , tbChanged = (data.sToolbar !== newToolbar)
          , isTbSys = _sxt.isTbSysId(data.sToolbar)
          , lView = _xtsi.getSiListView(data.sToolbar)
          , pd = lView.curPathData
          , tbMenuView = _stb.getSlogtbMenuView(tbPathData.toolbar, tbPathData.toolbartab);
      tbMenuView && tbMenuView.loadContent(tbPathData, true);

      // list view
      if (!isTbSys && pd.tid && _.contains(moved_tids, pd.tid)) {
        lView.show();
        tbChanged ? lView.remove() : lView.refresh();
      }

      // contents
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cmView = _xtsi.getSiContentView(target)
            , pdata = cmView.getCurPathData() || {}
        , ldata = cmView.getListItemData() || {}
        , pdTid = (ldata.tbPathData || {})['tid'] || false;
        if (pdata.pathLabel && pdTid && _.contains(moved_tids, pdTid)) {
          var newLabel = (pdata.pathLabel).replace(oldPathLabel, newPathLabel);
          cmView.hview.setPathLabel(newLabel)
        }
        if (data.withNodeSubsys && data.node_id && cmView.isMainContent()) {
//        if (data.withNodeSubsys && data.node_id && !cmView.isOver) {
          var rSource = _sxt.getDonResultSource(true) || {}
          , liData = rSource.liData;
          if (liData && liData.path) {
            setTimeout(function () {
              cmView.activateContent();
              if (cmView.curPath === liData.path) {
                cmView.loadSlogitemContent(liData.path, liData, true);
              } else {
                liData.reset = true;
                cmView.setListItemData(liData);
              }
            }, 300);
          }
        }
      });

      // promoted
      if (this.model.hasPromotedData()) {
        var pData = (this.model.getPromotedData()) || {}
        , cData = pData.content || {}
        , pd = cData.tbPathData || false;
        if (pd && _.contains(moved_tids, pd.tid)) {
          cData.pathLabel = newPathLabel;
          cData.toolbar = newToolbar;
          pd.toolbar = newToolbar;
          pd.toolbartab = newToolbartab;
          pData.list = pd;
        }
      }

      // surprise
      if (this.model.hasSurpriseData()) {
        $.each(this.model.getSurpriseData(), function (idx, sData) {
          var cData = sData.content || {}
          , pd = cData.tbPathData || false;
          if (pd && _.contains(moved_tids, pd.tid)) {
            cData.pathLabel = newPathLabel;
            cData.toolbar = newToolbar;
            pd.toolbar = newToolbar;
            pd.toolbartab = newToolbartab;
            sData.list = pd;
          }
        });
      }
    },
    onChangeEditedSlogitem: function (tbmm, data) {
      if (data.labelOld !== data.labelNew) {
        $.each(_xtsi.targetsContent, function (idx, target) {
          var cmView = _xtsi.getSiContentView(target)
              , pdata = cmView.getCurPathData();
          if (pdata && pdata.sid && pdata.sid === data.sid && !!pdata.pathLabel) {
            var liData = cmView.getListItemData()
                , curML = cmView.hview.curMainLabel
                , oldLabel = (data.labelOld).replace(curML, _xtsi.cpdph)
                , newLabel = (pdata.pathLabel).replace(oldLabel, (data.labelNew).replace(curML, _xtsi.cpdph));
            cmView.hview.setPathLabel(newLabel)
            liData && liData.sid == data.sid && (liData.label = data.labelNew);
          }
        });
      }
    },
    onChangeRebuildRoleState: function (tbmm, data) {
//      slogLog('XtsiMainView: ..........onChangeRebuildRoleState: ' + data.action);
      var tbLView = _xtsi.getSiListView(data.toolbar);
      if (data.action === 'ondestroy') {
        tbLView && tbLView.tbDetach();
      } else if (data.action === 'recreated') {
        !tbLView && _xtsi.createListView(data.toolbar, data.tbView);
        tbLView.model.set('slogtbView', data.tbView);
        tbLView.tbAttach();
      }
    },
    onChangeActiveSiListItem: function (model, data) {
      data.prev && $(data.prev).removeClass('active');
      data.active && $(data.active).addClass('active');
    },
    activateContent: function (targetid) {
      this.model.set('activeContentTarget', targetid);
    },
    syncActiveSiListItem: function (liData, liView) {
      var curLiToken = false;
      if (liView) {
        curLiToken = liView.getTermId() + '::' + liView.getPath();
        if (this.lastLiToken && this.lastLiToken === curLiToken //
            && !liView.getItemsEl().filter('.xtsi-by-entity').length) {
          return;
        }
      }

      this.lastLiToken = curLiToken;
      liData = liData || {};
      !liData.href && liData.path && (liData.href = Drupal.url(liData.path));
      if (liData.toolbar && _stb.hasSlogtbToolbar(liData.toolbar)) {
        var sysLiView = _xtsi.getSysListView();
        liView = liView || _xtsi.getSiListView(liData.toolbar);
        if (!liView.isVisible() && sysLiView && sysLiView.isVisible()) {
          liView = sysLiView;
        }

        liView.getItemsEl().removeClass('xtsi-by-entity');
        var prevSiLItem = this.model.get('activeSiListItem') || false
            , search_id = liData.sid + '-' + liData.byEntity.replace('.', '')
            , selector = '.xtsi-list a#' + search_id
            , hasActive = false;

        if (liView.toolbar === liData.toolbar) {
          var $activeNew = liView.$el.find(selector).first()
              , activeSiListItem = {
                href: liData.href,
                tblineView: liView.tblineView,
                listPath: liView.getPath(),
                active: $activeNew,
                prev: prevSiLItem ? prevSiLItem.active || false : false
              };
          this.model.set('activeSiListItem', activeSiListItem);
          hasActive = $activeNew.length;
        }

        if (!hasActive && !!liData.byEntity) {
          liView.getItemsEl().removeClass('active');
          var sid = liView.getSidByEntity(liData.byEntity);
          if (!!sid) {
            search_id = sid + '-' + liData.byEntity.replace('.', '');
            selector = '.xtsi-list a#' + search_id;
            liView.$el.find(selector).addClass('xtsi-by-entity');
          }
        }
      } else {
        var siLiItem = this.model.get('activeSiListItem');
        siLiItem && siLiItem.active && siLiItem.active.removeClass('active');
      }
    },
    populatePromoted: function (targetView) {
      var data = (this.model.getPromotedData())['content'] || false
          , view = targetView || _xtsi.getActiveContentView();
      view && data && view.setListItemData(data);
    },
    populateSurprise: function () {
      var view = _xtsi.getActiveContentView()
          , liData = this.model.getRandomSurpriseLiData();
      view && liData && view.setListItemData(liData);
    },
    refreshListContents: function (toolbar, tid) {
      var lView = _xtsi.getSiListView(toolbar)
          , testTid = lView.itemLData ? lView.itemLData.tid || false : false;
      !!tid && tid === testTid && lView.refresh();
    },
    refreshNodeContents: function (nid, goto) {
      if (nid && nid > 0) {
//        slogLog('_xtsi.XtsiMainView.............refreshNodeContents');
        var activeView = _xtsi.getActiveContentView()
            , isNodeContent = activeView.isNodeContent(nid)
            , data = activeView.getListItemData();
        if (isNodeContent && data && data.path) {
          !!goto && activeView.provideGoTo(goto);
          activeView.loadSlogitemContent(data.path, data, true);
        } else {
          $.each(_xtsi.targetsContent, function (idx, target) {
            var view = _xtsi.getSiContentView(target);
            if (view.isNodeContent(nid)) {
              var data = view.getListItemData();
              if (data && data.path) {
                view.loadSlogitemContent(data.path, data, true);
                return false; // break loop
              }
            }
          });
        }
      }
    },
    refreshMwcTogglerAll: function () {
      $.each(_xtsi.targetsContent, function (idx, target) {
        var cmView = _xtsi.getSiContentView(target);
        cmView && cmView.cview.refreshMwcToggler();
      });
    },
    executePrintable: function (entity_type, entity_id, print_type) {
      if (!this.$linkPrintable) {
        var _tse = Drupal.theme.slogxtElement
            , link = _tse('a', {id: 'xtsi-link-printable', 'href': '#', target: '_blank'});
        $('body').prepend(_tse('div', {id: 'xtsi-printable', class: 'visually-hidden'}, link))
        this.$linkPrintable = $('#xtsi-printable #xtsi-link-printable');
        if (!this.$linkPrintable.length) {
          slogErr('Printable link not created.');
        }
        this.$linkPrintable.on('click', function () {
          window.open(this.href);
          return false;
        });
      }

      var path = entity_type + '/' + entity_id + '/printable/' + print_type;
      this.$linkPrintable.attr('href', Drupal.url(path));
      this.$linkPrintable.click();
    }


  };

  /**
   * Backbone View for a slogitem list.
   */
  _xtsi.XtsiMainView = Backbone.View.extend(xtsiMainViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
