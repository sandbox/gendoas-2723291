/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem;

  var xtsiContentViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiContentViewBase',
    actions: {},
    show: $.noop,
    showMainAlone: $.noop,
    initializedReady: $.noop,
    syncHistory: function () {
      if (this.doSyncHistory) {
        this.doSyncHistory = false;
        this.historyModel.doSyncHistory(this.getItemListData(), this.getHistoryTarget());
        this.activateHistoryButtons();
      }
    },
    onChangeStateLoadReady: function (sModel, data) {
      this.doSyncHistory = true;
      this.syncHistory();
    },
    onChangeDialogClosed: function (sModel, data) {
      this.syncHistory();
    },
    onChangeBookmarkLastAction: function (bmModel, data) {
      var view = this.isContent ? this.hview : this;
      if (view.dropButton) {
        var action = data.action
            , path = this.isContent ? this.getListItemData().path : view.getPath()
            , cls = 'has-bookmark';
        if (data.path === path && (action === 'add' || action === 'remove')) {
          view.dropButton.$dropbutton.toggleClass(cls, (data.action === 'add'));
        } else if (action === 'removeMultiple') {
          var hasBookmark = bmModel.hasBookmark(path, data.type)
          view.dropButton.$dropbutton.toggleClass(cls, hasBookmark);
          !this.isContent && bmModel.setCurrentIdx(path, 'list');
        }
      }
    },
    setBookmark: function (add) {
      var data = this.getItemListData()
          , type = this.isContent ? 'content' : 'list';
      add ? this.bmModel.addBookmark(data) : this.bmModel.removeBookmark(data.path, type);
    },
    //todo::better::Drupal.debounce = function (func, wait, immediate)
    historySetCurrentItemTO: function (force, refresh) {
      setTimeout($.proxy(function () {
        this.historySetCurrentItem(force);
        this.activateHistoryButtons();
        if (!!refresh) {
          setTimeout($.proxy(function () {
            this.refresh();
          }, this), 10);
        }
      }, this), 10);
    },
    historySetCurrentItem: function (force) {
      if (this.stateModel.get('stateLoadReady') && (force || _xtsi.canSetHistory())) {
        this.historyModel.setCurrentItem(this.getItemListData(), this.getHistoryTarget());
        this.tblineModel.slogxtReTrigger('change:currentTabIndex');
      } else if (!_xtsi.canSetHistory()) {
        this.doSyncHistory = true;
      }
    },
    activateHistoryButtons: function () {
      var target = this.getHistoryTarget()
          , hData = this.historyModel.getData(target)
          , view = this.isContent ? this.hview : this
          , $dropbutton = view.dropButton ? view.dropButton.$dropbutton : $()
          , $rBtn = $dropbutton.find('a.icon-history-remove')
          , disabled = (hData.items.length < 1);
      this.tblineView.activateHistoryButtons(target, hData);
      $rBtn.toggleClass('disabled', disabled);
    },
    getHistoryTarget: function () {
      var targetid = this.targetid
          , prefix = this.isContent ? 'content-' : 'list-'
          , cls = prefix + (targetid).toLowerCase();
      return {
        tbline: this.tblineView.id,
        index: this.tblineIndex,
        isContent: this.isContent,
        id: targetid,
        class: cls,
        tblineView: this.tblineView,
        view: this
      };
    },
    getItemListData: function () {
      this.itemLData = this.itemLData || {};
      var result = {}
      , itemLData = this.itemLData
          , type = this.isContent ? 'content' : 'list'
          , data = this.isContent ? this.getListItemData() : this.getCurPathData();
      if (!!itemLData.mainLabel && !!itemLData.pathLabel) {
        result = {
          type: type,
          tbline: this.tblineView.id,
          tblineIndex: this.tblineIndex,
          data: _.clone(data),
          path: itemLData.path || false,
          pathLabel: itemLData.pathLabel,
          mainLabel: itemLData.mainLabel,
          pdRole: itemLData.pdRole || false
        };
      }

      return result;
    }
  };

  var actionsExt = {
    actionToggleBookmark: function () {
      var view = this.isContent ? this.hview : this
          , $dBtn = view.dropButton.$dropbutton
          , cls = 'has-bookmark'
          , add = !$dBtn.hasClass(cls);
      this.setBookmark(add);
      $dBtn.toggleClass(cls, add);
    },
    actionRemoveHistory: function () {
      this.historyModel.removeCurrentItem(this, this.getHistoryTarget());
    }
  };
  xtsiContentViewExt.actions.sxt_slogitem = actionsExt;


  /**
   * Backbone View for content base
   */
  _xtsi.XtsiContentViewBase = Backbone.View.extend(xtsiContentViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
