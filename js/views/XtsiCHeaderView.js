/**
 * @file
 * A Backbone View for a ???.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiCHeaderViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiCHeaderView',
    initialize: function (options) {
      this.pmodel = options.pmodel;
      this.pview = options.pview;

      this.listenTo(this.model, 'change:headerData', this.onChangeHeaderData);
      this.listenTo(this.model, 'change:dropbuttonIsOpen', this.onChangeDropbuttonIsOpen);

      var stbMModel = Drupal.slogtb.mainModel();
      this.listenTo(stbMModel, 'change:tbTabClicked', this.onChangeTbTabClicked);

      return this;
    },
    onChangeHeaderData: function (model, data) {
//      slogLog('XtsiCHeaderView: onChangeHeaderData');
      data = data || {};
      if (data.hasHeader) {
        this.detach();
        this.setMainLabel(data.mainLabel);
        this.setPathLabel(data.pathLabel);
        this.headerActions = data.actions || false;
        this.attach();
        this.bookmarkRefreshTO();
        _xtsi.setHeaderClass(this.$el);
        this.$el.show();
      } else {
        this.$el.hide()
      }
    },
    onChangeDropbuttonIsOpen: function (model, isOpen) {
//      slogLog('XtsiCHeaderView: onChangeDropbuttonIsOpen.IsOpen: ' + isOpen);
      if (isOpen && !!Drupal.sjqlout) {
//        slogLog('XtsiCHeaderView: onChangeDropbuttonIsOpen.IsOpen: ' + isOpen);
        var tbView = _xtsi.getSiToolbarView('tbCenter')
            , s = tbView.calcSplitState()
            , testHeight = this.pview.isNext ? s.nHeight : s.cHeight
            , btnHeight = this.dropButton.$dropbutton.height() + 15;
        if (!!testHeight && btnHeight > testHeight) {
          var ratio = tbView.calcSplitRatio(s.full, btnHeight);
          ratio = this.pview.isNext ? ratio : 1 - ratio;
          //todo:splitted
          tbView.model.set('splitRatio', ratio);
        }
      }
    },
    onChangeTbTabClicked: function (stbmmodel, view) {
      if (this.pview.isMainContent() && view.toolbar === _sxt.getTbSysNode()) {
//      if (!this.pview.isOver && view.toolbar === _sxt.getTbSysNode()) {
        this.$el.click();
      }
    },
    getNodeDonData: function (e) {
      var hData = this.model.get('headerData')
          , tbnData = this.pmodel.get('tbnodeData') || {}
      , tbData = !!tbnData.settings ? tbnData.settings.toolbarData || {} : {}
      , submenu = !!tbData.menutabs ? tbData.menutabs.submenu || {} : {};
      var rSource = _sxt.getDonResultSource(true)
          , rootTermId = submenu.rootTermId || 0
          , donData = {
            targetKey: 'XtsiNodeItem',
            nodeId: hData.nodeId,
            rootTermId: rootTermId,
            liData: this.pview.getListItemData(),
            label: hData.mainLabel,
            info: hData.pathLabel
          };
      !!rSource && (donData.updateSource = ['rootTermId', 'liData']);
      return donData;
    },
    onHeaderClick: function (e) {
      if (_sxt.isDoNavigateActive() && this.pview.hasTbnodeVisible()) {
        _sxt.setDoNavigateRawData(this.getNodeDonData());
      }
    },
    attach: function () {
      if (!!this.headerActions) {
        this.buildHeaderDropbutton(this.headerActions);
        Drupal.behaviors.dropButton.attach(this.el, {});
        this.dropButton = _sxt.getDropbutton(this.$el.find('.dropbutton-wrapper'));
        this.$el.find('.slogxt-dropbutton ul.dropbutton li.dropbutton-action a')
            .on('contextmenu', function () {
              return false;
            });

        var $paging = this.$el.find('.slogxt-dropbutton .sxt-paging');
        if ($paging.length) {
          this.pmodel.slogxtReTrigger('change:pagingOpen');
          this.$el.find('.slogxt-dropbutton .sxt-paging .sxt-pg-button')
              .once('slogxt')
              .on('click', $.proxy(function (e) {
                e.stopPropagation();
                if (!this.isActive()) {
                  this.activateContent();
                  this.findAssociatedListItem()
                } else {
                  var $target = $(e.target);
                  if ($target.hasClass('sxt-pgstop')) {
                    this.togglePaging(false);
                  } else {
                    var liData = this.getListItemData()
                        , tbPathData = liData.tbPathData
                        , lView = _xtsi.getSiListView(tbPathData.toolbar) || false;
                    if (lView) {
                      var visible = lView.isVisible()
                          , pdata = lView.getActivePathData() || {};
                      if (!(visible && pdata.sid == this.curSid)) {
                        this.findAssociatedListItem()
                      } else {
                        lView.moveActiveNext($target.hasClass('sxt-pgnext'));
                      }
                    }
                  }
                }
              }, this.pview));
        }
      }

      this.$el.once('don').on('click', $.proxy(function (e) {
        this.onHeaderClick(e);
      }, this));
      this.$el.find('a.header-action')
          .once('xtsi-action')
          .on('click', $.proxy(function (e) {
            this.pview.activateContent();
            _sxt.dropbuttonActionDispatch(e, this.pview);
          }, this));
      this.$el.find('.dropbutton-toggle')
          .once('dropbutton-toggle')
          .on('click', $.proxy(function (e) {
            this.pview.activateContent();
            _sxt.dropbuttonClickHandler(e);
          }, this));
    },
    detach: function () {
      this.$el.removeOnce('don').off();
      this.$el.removeOnce('xtsi-action').off();
      if (!!this.headerActions) {
        _sxt.clearDropbuttons(this.$el);
        this.$el.removeOnce('dropbutton-toggle').off();
        this.$el.find('.dropbutton-wrapper').remove();
      }
    },
    bookmarkRefreshTO: function () {
      this.bmRefreshTO && clearTimeout(this.bmRefreshTO);
      this.bmRefreshTO = setTimeout($.proxy(this.bookmarkRefresh, this), 10);
    },
    bookmarkRefresh: function () {
      if (!!this.headerActions) {
        var $dBtn = this.dropButton.$dropbutton
            , hasBookmark = this.pview.bmModel.hasBookmark(this.pview.curPath, 'content');
        $dBtn && $dBtn.toggleClass('has-bookmark', hasBookmark);
      }
    },
    buildHeaderDropbutton: function (actions) {
      if (actions && !$.isEmptyObject(actions)) {
        actions = this.prepareAuthActions(actions);
        actions = this.prepareEditAction(actions);
        actions = this.prepareFilterAction(actions);
        actions = _sxt.dropbuttonPrepareActions(actions, this.pview.curAction);
        var html = Drupal.theme.slogxtDropbutton(actions, {aClass: 'header-action'});
        html && this.$el.prepend($(html));
      }
    },
    prepareAuthActions: function (actions) {
      if (_sxt.isUserAuthenticated()) {
        return actions;
      }

      actions = actions.filter(function (action) {
        return (!!action.needsAuth ? false : true);
      });
      return _.compact(actions);
    },
    prepareEditAction: function (actions) {
      if (!_sxt.isUserAuthenticated()) {
        actions = _sxt.dropbuttonPreventActions(actions, ['edit']);
      } else {
        var pView = this.pview;
        actions = actions.filter(function (action) {
          return (action.path !== 'edit' || pView.isEditable());
        });
      }
      return _.compact(actions);
    },
    prepareFilterAction: function (actions) {
      var nodeData = this.pview.nodeData;
      actions = actions.filter(function (action) {
        if (!!action.useFilter) {
          var provider = action.provider
              , func = action.useFilter
              , parts = func.split('::');
          if (parts.length > 1) {
            provider = parts[0];
            func = parts[1];
          }
          return Drupal[provider]['filters'][func](nodeData);
        }

        return true;
      });

      return _.compact(actions);
    },
    setMainLabel: function (label) {
      label = label || '???';
      this.$el.find('.main-label').text(label)
      var parts = label.split(':: ');
      (parts.length === 2) && (label = parts[1]);
      this.curMainLabel = label.trim(label);
    },
    setPathLabel: function (label) {
      label = label || '???';
      label = label.replace(this.curMainLabel, _xtsi.cpdph);
      this.$el.find('.path-label').text(label)
    }
  };


  /**
   * Backbone View for Content Header
   */
  _xtsi.XtsiCHeaderView = Backbone.View.extend(xtsiCHeaderViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
