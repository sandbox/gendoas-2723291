/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _stb = Drupal.slogtb
      , _sxt = Drupal.slogxt
      , _sxth = _sxt.helper
      , _tse = Drupal.theme.slogxtElement;

  var xtsiContentViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiContentView',
    isOver: false,
    curPath: false,
    curSid: false,
    loading: false,
    doSyncOnChange: false,
    actions: {sxt_slogitem: {}},
    onScrollChange: $.noop,
    constructor: function () {
      Backbone.View.prototype.constructor.apply(this, arguments);
    },
    events: {
      'click .content, .sxt-header-wrapper': 'onClickContent'
    },
    initialize: function (options) {
      this.sxtIsPermanent = true;
      this.isContent = true;
      this.targetid = options.target;
      this.stateModel = _sxt.stateModel();
      this.historyModel = _xtsi.historyModel();
      this.bmModel = _xtsi.bookmarkModel()
      this.mainModel = _xtsi.mainModel();
      this.mainView = _xtsi.mainView();
      this.stbMainView = _stb.mainView();

      this.initElement();
      this.initRegionData();

      var hmodel = this.hmodel = new _xtsi.XtsiCHeaderModel({})
          , cmodel = this.cmodel = new _xtsi.XtsiCContentModel({})
          , tbmainModel = this.stbMainView.model;
      this.hview = new _xtsi.XtsiCHeaderView({
        el: this.getHeaderEl().first(),
        model: hmodel,
        pmodel: this.model,
        pview: this
      });
      this.cview = new _xtsi.XtsiCContentView({
        el: this.getContentEl().first(),
        model: cmodel,
        pmodel: this.model,
        pview: this
      });

      this.listenTo(this.model, 'change:listItemData', this.onChangeListItemData);
      this.listenTo(this.model, 'change:slogitemContent', this.onChangeSlogitemContent);
      this.listenTo(this.model, 'change:tbnodeData', this.onChangeTbnodeData);
      this.listenTo(this.model, 'change:pagingOpen', this.onChangePagingOpen);
      this.listenTo(this.bmModel, 'change:lastAction', this.onChangeBookmarkLastAction);
      this.listenTo(tbmainModel, 'change:editedTbMenu', this.refreshOnMenuEdited);
      this.listenTo(tbmainModel, 'change:rearrangedTbMenu', this.refreshOnMenuEdited);
      this.listenTo(this.stateModel, 'change:stateLoadReady', this.onChangeStateLoadReady);
      this.listenTo(this.stateModel, 'change:dialogClosed', this.onChangeDialogClosed);

      // ajax
      if (this.isMainContent() && this.$tbnodeWrapper) {
        this.ajaxTbnode = _sxt.ajaxCreate('slogitemNodeToolbar', this.$tbnodeWrapper, true);
        this.ajaxTbnode.progress.type = 'throbber';
        this.ajaxTbnode.progress.message = '';
      }
      this.ajax = _sxt.ajaxCreate('slogitemListSelect', this.$el);

      this.isMainContent() && (_sxt.xtContentMainView = this);
      this.initializedReady();
      return this;
    },
    getAjaxOther: function () {
      if (!this.ajaxOther) {
        this.ajaxOther = _sxt.ajaxCreate('xtsiAjaxOther', this.$el);
      }
      return this.ajaxOther;
    },
    initElement: function () {
      this.$el.addClass('xtsi-content');
      this.$el.prepend($(Drupal.theme.slogxtHeader()));

      this.$tbnodeWrapper = false;
      var tbSysNode = _sxt.getTbSysNode()
          , tbnData = (drupalSettings.slogtb.tbsubmenu || {})[tbSysNode];
      if (this.isMainContent() && tbnData && !!(tbnData.wrapperId)) {
        var selector = '#' + tbnData.wrapperId
            , $tbnWrapper = this.$el.find(selector);
        if (!$tbnWrapper.length) {
          $tbnWrapper = $('body').find(selector);
          !$tbnWrapper.length && slogErr('XtsiContentView: tbnode wrapper not found.');
        }
        if ($tbnWrapper.length) {
          this.$el.prepend($tbnWrapper);
          this.$tbnodeWrapper = $tbnWrapper
        }
      }
    },
    initRegionData: function () {
      if (this.isMainContent()) {
        var key = 'tContentMain'
            , $region = this.$el.closest('.region')
            , regionId = $region.attr('id')
            , xtRegionData = this.mainModel.get('xtRegionData') || {};
        if (!regionId) {
          regionId = 'xtsi-region-' + key;
          $region.attr('id', regionId);
        }

        xtRegionData[regionId] = xtRegionData[regionId] || {
          regionId: regionId,
          $region: $region,
          isContent: true,
          xtsiViews: {}
        };
        xtRegionData[regionId].xtsiViews[key] = this;
        this.mainModel.set('xtRegionData', xtRegionData);
      }
    },
    initTblineView: function () {
      this.listenTo(this.tblineModel, 'change:currentContent', this.onChangeCurrentContent);
    },
    onChangeCurrentContent: function (tblmm, data) {
      if (!this.stateModel.get('stateLoadReady')) {
        var liData = data[this.targetid] || false;
        liData && liData.path && this.setListItemData(liData);
      }
    },
    isActive: function () {
      return (this.mainModel.get('activeContentTarget') === this.targetid);
    },
    onClickContent: function (e) {
      _sxt.closeOpenPopup();
    },
    onChangeListItemData: function (model, data) {
//      slogLog('XtsiContentView: onChangeListItemData');
      var tblCC = this.tblineModel.get('currentContent')
          , reset = data && !!data.reset;
      if (data && data.path) {
        this.fromPreLoadReady && (data.preLoadReady = true);
        delete this.fromPreLoadReady;
        if (reset) {
          delete data.reset;
          this.setListItemData(data, {silent: true});
        }
        this.loadSlogitemContent(data.path, data, reset)
        tblCC[this.targetid] = data;
      } else {
        this.model.set('slogitemContent', false);
        tblCC[this.targetid] = false;
      }
    },
    loadSlogitemContent: function (path, data, reset, deeprefresh) {
      function reloadCurrentSlogitem() {
        data.preLoadReady = preLoadReady;
        thisView.loadSlogitemContent(data.path, data, true);
      }

      if (!path || path === '') {
        slogErr('XtsiContentView.loadSlogitemContent: unexpected fault.');
        this.curPath = false;
        this.curSid = false;
        return;
      }

      if (this.curPath === path && this.curSid === data.sid && !reset) {
        this.mainView.syncActiveSiListItem(this.getListItemData());
        return; // no changes, no reset
      }

      this.setListItemData({noContent: true}, {silent: true});
      this.curPath = false;
      this.curSid = false;
      this.loading = true;

      if (this.isMainContent()) {
        this.model.set('tbnodeData', false, {silent: true});
        this.storageKey = false;
      }
      // 
      var thisView = this
          , isExtraSid = (data.sid < 0)
          , routeKey = data.routeKey
          , strgKeyID = isExtraSid ? routeKey + '::' + data.extendedId : routeKey
          , keyPrefix = ''
          , storageKey = _sxt.storageKey(keyPrefix, strgKeyID + '::data')
          , skeyHash = _sxt.storageKey(keyPrefix, routeKey + '::hash')
          , testHash = data.testHash
          , cachedHash = localStorage.getItem(skeyHash)
          , preLoadReady = data.preLoadReady || !this.stateModel.get('stateLoadReady')
          , allDone = false;
      delete data.preLoadReady;

      if (isExtraSid) {
        var query = path.split('?')[1] || false;
        if (query) {
          storageKey = _sxt.storageKey(keyPrefix, strgKeyID + '?' + query + '::data');
        }
        if (reset) {
          sessionStorage.removeItem(storageKey);
        } else {
          var result = JSON.parse(sessionStorage.getItem(storageKey));
          if (result) {
            result.targetid = this.targetid;
            this.model.set('slogitemContent', result);
            allDone = true;
          } else {
            reset = true;
          }
        }
      }

      if (!allDone) {
        this.storageKey = storageKey;
        if (!reset && cachedHash && cachedHash === testHash) {
          $.sxtIdbStorage.getItem({store: 'slogxtContent'}, storageKey)
              .fail(function (error, event) {
                reloadCurrentSlogitem();
              })
              .done(function (result, event) {
                if (result) {
                  var targetid = data.targetid || 'tContentMain'
                      , result = JSON.parse(result);
                  result.targetid = targetid;
                  thisView.model.set('slogitemContent', result);
                } else {
                  reloadCurrentSlogitem();
                }
              });
        }
        // get content via ajax call
        else {
          if (!isExtraSid) {
            // Remove cached data.
            localStorage.setItem(skeyHash, testHash);
            $.sxtIdbStorage.removeItem({store: 'slogxtContent'}, storageKey);
          }

          // 
          var ajax = this.ajax;
          deeprefresh && (path += '/deeprefresh');
          ajax.url = ajax.options.url = Drupal.url(path);
          // hide header and content, do not delete
          this.getHeaderEl().hide(0);
          this.getContentEl().hide(0);
          !preLoadReady && this.show();
          if (this.isOver) {
            // show throbber
            !this.isNext && _xtsi.getSiContentMainView().showTbnode(false);
            this.$el.css('top', '30px');
          }
          this.model.set('slogitemContent', {}, {silent: true});
          this.hmodel.set('headerData', {});
          this.cmodel.set('contentData', {});
          this.showSpecial(false);

          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = this.ajaxResponse;
          ajax.slogxt.xtData = ajax.slogxt.xtData || [];
          ajax.slogxt.xtData[data.sid] = {storageKey: storageKey};

          // ajax call by triggering slogitemListSelect event
          // receiving response in this.ajaxResponse
          // see Drupal.AjaxCommands.prototype.slogxtInvoke in slogxt.core.ajax.js
          ajax.$xtelement.trigger('slogitemListSelect');
        }
      }
    },
    ajaxResponse: function (ajax, response, status) {
      this.ajaxResponseUrl = ajax.url;
      var sid = response.data.sid || 0
          , settings = response.data.settings || {}
      , nocache = settings.nocache || false
          , xtData = ajax.slogxt.xtData[sid] || {}
      , storageKey = xtData.storageKey || false;
      delete ajax.slogxt.xtData[sid];

      if (sid > 0 && storageKey && !nocache) {
        // cache data in indexedDB storage
        $.sxtIdbStorage.setItem({store: 'slogxtContent'}, storageKey, JSON.stringify(response.data));
      }

      // set content
      if (!this.curSid) {
        response.data.isAjaxResponse = true;
        this.model.set('slogitemContent', response.data);
//      } else if (!!ajax.slogxt.xtData[curSid]) {
//        this.model.slogxtReTrigger('change:listItemData');
      }
    },
    loadOtherContent: function (path) {
      this.setListItemData({noContent: true}, {silent: true});
      this.curPath = false;
      this.curSid = false;
      this.loading = true;

      var ajax = this.getAjaxOther();
      ajax.url = ajax.options.url = Drupal.url(path);
      // hide header and content, do not delete
      this.getHeaderEl().hide(0);
      this.getContentEl().hide(0);
      this.show();
      this.activateContent();
      if (this.isOver) {
        // show throbber
        !this.isNext && _xtsi.getSiContentMainView().showTbnode(false);
        this.$el.css('top', '30px');
      }
      this.model.set('slogitemContent', {}, {silent: true});
      this.hmodel.set('headerData', {});
      this.cmodel.set('contentData', {});
      this.showSpecial(false);

      ajax.slogxt.targetThis = this;
      ajax.slogxt.targetCallback = this.ajaxOtherResponse;
      ajax.slogxt.xtData = ajax.slogxt.xtData || [];
//      ajax.slogxt.xtData.path = path;

      // ajax call by triggering slogitemnoprogressListSelect event
      // receiving response in this.ajaxOtherResponse
      ajax.$xtelement.trigger('xtsiAjaxOther');
    },
    ajaxOtherResponse: function (ajax, response, status) {
      if (!this.curSid) {
        response.data.settings.nocache = true;
        this.ajaxResponse(ajax, response, status);
      }
    },
    setSyncOnchange: function () {
      this.doSyncOnChange = true;
    },
    doSyncOnchange: function (preserveTblIndex) {
      this.doSyncOnChange = false;
      this.syncOnChangeTO && clearTimeout(this.syncOnChangeTO);

      var thisView = this
          , liData = this.getListItemData() || {}
      , ok = liData.toolbar && liData.tbPathData
          , lView = ok ? _xtsi.getSiListView(liData.toolbar) || false : false
          , curTblIdx = lView ? lView.tblineView.getCurrentTabIndex() : 0;
      if (lView) {
        this.syncOnChangeTO = setTimeout(function () {
          var liData = thisView.getListItemData();
          if (liData && liData.toolbar && liData.tbPathData) {
            var lView = _xtsi.getSiListView(liData.toolbar) || false;
            if (lView) {
              lView.show();
              lView.setPathData(liData.tbPathData);
              preserveTblIndex && lView.tblineView.setCurrentTabIndex(curTblIdx);
            }
          }
        });
      }
    },
    onChangeSlogitemContent: function (model, data) {
//      slogLog('XtsiContentView.onChangeSlogitemContent...: ' + this.targetid);
      this._sxtDeleteHintEl();
      this.showSpecial(false);
      this.itemLData = {};
      this.loading = false;

      var liData = data.liData || {};
      if (!data) {
        this.isMainContent() && this.showTbnode(false);
        this.ensureContent();
        return;
      } else if (data.noContent) {
        var msg = Drupal.theme.slogxtInfoMessage(_sxt.txtNoContentSelected, 'warning', true);
        this.hmodel.set('headerData', {});
        this.cmodel.set('contentData', {html: msg});
        return;
      } else if (!liData.tbPathData) {
        var msg = Drupal.theme.slogxtInfoMessage(_sxt.txtSorryError, 'warning', true);
        this.hmodel.set('headerData', {});
        this.cmodel.set('contentData', {html: msg});
        return;
      }

      // data exist
      this.setListItemData(liData, {silent: true});
      this.curPath = liData.path;
      this.curSid = liData.sid;

      var hasSpecial = data.special && data.special.nodeStateShow
          , hasHeader = !!data.header
          , s = data.settings
          , isExtraSid = (data.sid < 0)
          , node = s.node || false
          , hActions = hasHeader ? s.headerActions : false
          , toolbartab = liData.tbPathData.toolbartab
          , pl = s.pathLabel = liData.pathLabel = _sxt.getRoleHeaderPath(s.pdRole, toolbartab) || s.pathLabel
          , label = isExtraSid ? liData.label + ': ' + s.pathLabel : _xtsi.contentPathLabel(s.pathLabel, s.listLabel, liData.label)
          , mainLabel = isExtraSid ? s.listLabel : node.nodeTitle
          , isAjaxResponse = data.isAjaxResponse
          , isMainContent = this.isMainContent()
          , headerData = {
            hasHeader: hasHeader,
            pathLabel: label,
            mainLabel: data.header.mainLabel,
            nodeId: node ? node.nodeId : 0,
            actions: hActions
          }
      , contentData = {
        html: data.content || ''
      };

      // syncing list view ?
      (liData.toolbar === '_node') && (this.doSyncOnChange = true);
      if (this.doSyncOnChange) {
        var cView = _xtsi.getActiveContentView()
            , preserveTblIndex = (this.targetid !== cView.targetid);
        this.doSyncOnchange(preserveTblIndex);
      }

      if (isAjaxResponse) {
        delete data.isAjaxResponse;
        this.model.set('slogitemContent', data, {silent: true});
      }

      if (this.isOver) {
        !this.isNext && _xtsi.getSiContentMainView().showTbnode(false);
        this.$el.css('top', '0');   // reset top
      }

      if (this.depositPathLabel) {
        headerData.pathLabel = this.depositPathLabel.cPathLabel;
        this.hview.bookmarkRefreshTO();
        delete this.depositPathLabel;
      }

      if (this.providedGoTo) {
        var providedGoTo = this.providedGoTo;
        this.providedGoTo = false;
        setTimeout($.proxy(function () {
          var selector = '#' + providedGoTo
              , iCls = 'sxtmwc-action xtsi-temp-new'
              , hintNew = _tse('div', {class: iCls}, _xtsi.lastEdited)
              , $actions = this.$el.find('span' + selector).siblings('.sxtmwc-action-wrapper');
          this.$el.find('#toc a[href="' + selector + '"]').click();
          $actions.prepend($(hintNew));
          this._sxtSetHintDelay($actions.find('.sxtmwc-action.xtsi-temp-new'));
        }, this));
      }

      this.nodeData = node || false;
      this.nodeBaseTb = node ? node.baseTb : '__error__';
      this.permBaseTb = node ? _stb.getPermBaseTbId(node.baseTb) : '__error__';
      hasSpecial && this.showSpecial(true);
      this.model.set('specialData', (hasSpecial ? data.special : false));
      this.hmodel.set('headerData', headerData);
      this.cmodel.set('contentData', contentData);
      this.mainModel.slogxtSetAndTrigger('change:contentChanged', null, this);

      if (this.isMainContent()) {
        if (!!this.autorunPath && liData.path === this.autorunPath) {
          delete this.autorunPath;
          setTimeout($.proxy(function () {
            this.showMainAlone();
            this.mainView.syncActiveSiListItem(liData);
          }, this), 10);
        }
        this.showTbnodeBlock(false);
        this.hasTbnode = node && node.hasTbnode;
        this.hasTbnode ? this.loadNodeToolbar(node) : this.showTbnode(false);
      }

      if (this.isActive()) {
        this.mainView.syncActiveSiListItem(liData);
        var $active = (this.mainModel.get('activeSiListItem') || {}).active || false
            , active_id = $active ? $active.attr('id') : false
            , test_id = liData.sid + '-' + liData.byEntity.replace('.', '')
            , liView = _xtsi.getSiListView(liData.toolbar);
        if (active_id && active_id === test_id && liView) {
          var slogitemList = liView.getSlogitemListData() || {}
          , sils = slogitemList.settings || false;
          headerData.pathLabel = _xtsi.contentPathLabel(sils.pathLabel, sils.listLabel, liData.label);
          this.hview.setPathLabel(headerData.pathLabel);
        }
      }

      isExtraSid && s.pageLabel && (mainLabel += ' - ' + s.pageLabel);
      this.itemLData = {
        path: liData.path,
        pathLabel: headerData.pathLabel,
        mainLabel: mainLabel,
        pdRole: s.pdRole || false
      };
      this.historySetCurrentItem();
      this.activateHistoryButtons();

      if (isAjaxResponse && node && node.nodeId) {
        // synchronize other views with this node content
        var thisModelCid = this.model.cid
            , thisNodeId = node.nodeId;
        this.collection.each(function (model) {
          if (model.cid !== thisModelCid && model.getTbnodeId() === thisNodeId) {
            model.set('slogitemContent', data);
          }
        });
      }
    },
    provideGoTo: function (goto) {
      !!goto && (this.providedGoTo = goto);
    },
    _sxtDeleteHintEl: function () {
      if (this.$actionHint) {
        this.$actionHint.remove();
        delete this.$actionHint;
      }
    },
    _sxtSetHintDelay: function ($hint) {
      this._sxtDeleteHintEl();

      if ($hint.length) {
        this.$actionHint = $hint;
        this.$actionHintTO && clearTimeout(this.$actionHintTO);
        this.$actionHintTO = setTimeout($.proxy(function () {
          this.$actionHint.hide('slow', this._sxtDeleteHintEl);
        }, this), 10000);
      }
    },
    loadNodeToolbar: function (node, reset) {
      function reloadCurrentTbnode() {
//        slogLog('XtsiContentView: reloadCurrentTbnode');
        thisView.loadNodeToolbar(node, true);
      }

//      reset = true;
//      slogLog('XtsiContentView: loadNodeToolbar');
      var thisView = this
          , nodeId = node.nodeId
          , nodeTitle = node.nodeTitle
          , tbnodeHash = node.tbnodeHash || false
          , nodeKey = 'node.' + nodeId
          , storageKey = _sxt.storageKey('xtsiTbSubmenu', nodeKey + '::data')
          , hashKey = _sxt.storageKey('xtsiTbSubmenu', nodeKey + '::hash');

      reset = reset || !tbnodeHash || (localStorage.getItem(hashKey) !== tbnodeHash);
      //todo::current::tbnode::cache::reset=true
//      reset = true;
      if (!reset) {
        $.sxtIdbStorage.getItem({store: 'slogxtContent'}, storageKey)
            .fail(function (error, event) {
              reloadCurrentTbnode();
            })
            .done(function (result, e) {
              if (result) {
                var result = JSON.parse(result);
                thisView.model.set('tbnodeData', result);
              } else {
                reloadCurrentTbnode();
              }
            });
      } else if (this.ajaxTbnode) {
//"/$baseAjaxPath/tbsubmenu/{entity}/{entity_id}/{rootterm}",
        localStorage.removeItem(hashKey);
        var ajax = this.ajaxTbnode
            , baseAjaxPath = _xtsi.baseAjaxPath(true)
//                , path = 'slogtb/xtajx/rootterm/' + this.tbnodeToolbar + '/' + nodeKey;
            , path = _sxth.buildPath([baseAjaxPath, 'tbsubmenu', 'node', nodeId])
            , $tbnodeBlock = this.$tbnodeWrapper.find('.block-slogtb');
        ajax.url = ajax.options.url = Drupal.url(path);
        ajax.slogxt.targetThis = this;
        ajax.slogxt.targetCallback = this.ajaxTbnodeResponse;
        ajax.slogxt.storageKey = storageKey;
        ajax.slogxt.hashKey = hashKey;
        ajax.element = $tbnodeBlock.get(0);
        $tbnodeBlock.hide();
        this.showTbnode(true);

        // ajax call by triggering slogitemNodeToolbar event
        // receiving response in this.ajaxResponse
        // see Drupal.AjaxCommands.prototype.slogxtInvoke in slogxt.core.ajax.js
        ajax.$xtelement.trigger('slogitemNodeToolbar');
      }
    },
    ajaxTbnodeResponse: function (ajax, response, status) {
//      slogLog('XtsiContentView::ajaxTbnodeResponse.nodeId: ' + ajax.slogxt.nodeId);
      !response.data.settings && (response.data.settings = {});
      var settings = response.data.settings
          , sicData = this.model.get('slogitemContent') || {}
      , sicS = sicData.settings || {}
      , node = sicS.node || {}
      , nocache = settings.nocache || false;
      settings.nodeId = node.nodeId || '';
      settings.nodeTitle = node.nodeTitle || '';
      settings.baseSid = node.baseSid;
      settings.baseVid = node.baseVid;
      settings.baseTb = node.baseTb;
      settings.hasRankSids = node.hasRankSids;

      if (!nocache && !!node.nodeId) {
        var sdata = JSON.stringify(response.data)
            , storageKey = ajax.slogxt.storageKey
            , tbnodeHash = response.data.tbnodeHash;
        // cache data in indexedDB storage
        $.sxtIdbStorage.setItem({store: 'slogxtContent'}, storageKey, sdata);
        // refresh tbnodeHash: model/storage
        node.tbnodeHash = tbnodeHash;
        this.model.set('slogitemContent', sicData, {silent: true});
        $.sxtIdbStorage.setItem({store: 'slogxtContent'}, this.storageKey, JSON.stringify(sicData));
        localStorage.setItem(ajax.slogxt.hashKey, tbnodeHash);
      }
      this.model.set('tbnodeData', response.data);
    },
    onChangeTbnodeData: function (model, data) {
//      slogLog('XtsiContentView: onChangeTbnodeData');
      if (data && data.content) {
        var toolbar = data.toolbar
            , tbNodeView = _stb.getSlogtbView(toolbar)
            , tbnodeLView = _xtsi.getSiListView(toolbar)
            , tbMainModel = this.stbMainView.model
            , s = data.settings
            , tbIconUrl = tbNodeView.tbIconUrl;

        // detach slogtb tbnode view
        if (tbNodeView) {
          tbNodeView.slogxtDestroy();
          tbnodeLView && tbnodeLView.tbDetach();
        }

        // replace content first
        this.$tbnodeWrapper.html(data.content);
        this.showTbnodeBlock(true);
        // create views
        $('#slogtb-' + toolbar).once('slogtb').each(function () {
          var $el = $(this)
              , tbView = _stb.createTbView(toolbar, $el, s.toolbarData);
          tbView.model.statics = tbMainModel.hookDataAlter('staticSettings', tbView.model.statics, true);
          $el.css('backgroundImage', tbIconUrl);
          drupalSettings.slogtb.toolbars[toolbar] = true;
        });

        // 
        tbNodeView = _stb.getSlogtbView(toolbar);
        !tbnodeLView && _xtsi.createListView(toolbar, tbNodeView);
        tbnodeLView = _xtsi.getSiListView(toolbar)

        tbNodeView.nodeInfo = {
          nodeId: s.nodeId,
          nodeTitle: s.nodeTitle,
          baseSid: s.baseSid,
          baseVid: s.baseVid,
          baseTb: s.baseTb,
          hasRankSids: s.hasRankSids
        };
        tbnodeLView.model.set('slogtbView', tbNodeView);
        tbnodeLView.tbAttach();

        this.curTbnodeNid = s.nodeId;
        this.showTbnode(true);
      } else {
        this.showTbnode(false);
      }
    },
    isEditable: function () {
      if (_sxt.isAdmin()) {
        return true;
      } else if (this.permBaseTb === 'role') {
        return _xtsi.hasPermRoleContent('edit sxtrole-content');
      } else {
        return _xtsi.isEditable('editableContent', this.permBaseTb);
      }
    },
    refreshOnMenuEdited: function (tbmm, data) {
      if (this.isMainContent() && data.toolbar === _sxt.getTbSysNode()) {
        this.refresh();
      }
    },
    refresh: function () {
      var data = this.getListItemData();
      if (data && data.path) {
        this.loadSlogitemContent(data.path, data, true, true);


//todo::current:: - XtsiContentView: broken content >>>> msg with refresh button.



      }
    },
    getListItemData: function () {
      return this.model.get('listItemData');
    },
    setListItemData: function (itemData, opts) {
      this.model.set('listItemData', itemData, opts);
    },
    getHeaderEl: function () {
      return this.$el.find('>.sxt-header-wrapper');
    },
    getContentEl: function () {
      return this.$el.find('>.content');
    },
    isVisible: function () {
      var listItemData = this.getListItemData();
      return listItemData ? listItemData.path || false : false;
    },
    activateContent: function () {
      this.mainView.activateContent(this.targetid);
    },
    setClassContentActive: function (isActive) {
      this.$el.toggleClass('content-active', isActive);
    },
    setDepositPathLabel: function (dpl) {
      this.depositPathLabel = dpl;
    },
    togglePaging: function (open) {
      var $dropbutton = this.hview.dropButton ? this.hview.dropButton.$dropbutton : $();
      if ($dropbutton.length) {
        this.model.slogxtSetAndTrigger('change:pagingOpen', null, open);
        open && this.findAssociatedListItem();
      }
    },
    onChangePagingOpen: function (m, open) {
      var $dropbutton = this.hview.dropButton ? this.hview.dropButton.$dropbutton : $();
      $dropbutton.find('.dropbutton-widget').toggleClass('sxt-paging-open', open);
    },
    findAssociatedListItem: function () {
      var liData = this.getListItemData()
          , tbPathData = liData.tbPathData
          , lView = _xtsi.getSiListView(tbPathData.toolbar) || false;
      if (lView) {
        setTimeout(function () {
          lView.show();
          lView.setPathData(tbPathData);
        });
      }
    },
    ensureContent: function () {
      if (this.loading || !this.stateModel.get('stateLoadReady')) {
        return;
      }

//      slogLog('XtsiContentView: ...........ensureContent......');
      var liData = this.getListItemData()
          , path = liData ? liData.path || false : false
          , xtsiFromPath = _xtsi.getFromPathData(['xtsiContent'])
          , done = false;

      if (!path || xtsiFromPath) {
        path && (done = true);
        if (xtsiFromPath && this.isMainContent()) { // from path
          _xtsi.deleteAutorunFromPath();
          var fpData = xtsiFromPath.data
              , cData = fpData.content || {}
          , lData = cData.tbPathData || fpData.list || false;
          if (cData.path) {
            this.autorunPath = cData.path;
            this.setListItemData(cData);
            done = true;
          }
          if (lData && !!lData.toolbar) {
            var lView = _xtsi.getSiListView(lData.toolbar);
            if (lView) {
              lView.show();
              setTimeout(function () {
                lView.show();
                lView.setPathData(lData);
              }, 200);
            }
          }
        }

        if (!done) {  // from history
          done = this.historyModel.goToCurrent(this, this.getHistoryTarget());
        }

        if (!done && this.mainModel.hasPromotedData()) {  // from promoted
          this.mainView.populatePromoted(this);
          done = true;
        }

        if (!done && this.mainModel.hasSurpriseData()) {  // from surprise
          this.setListItemData(this.mainModel.getRandomSurpriseLiData());
          done = true;
        }

        if (!done && !this.isMainContent()) {  // from main content
          var cmView = _xtsi.getSiContentView('tContentMain')
              , cmData = cmView ? cmView.getListItemData() : false;
          cmData && this.setListItemData(cmData);
        }

        if (!done) {
          this.setListItemData({noContent: true});
        }

        this.activateHistoryButtons();
      }
    },
    restoreTbnode: function () {
      if (this.isMainContent()) {
        this.showTbnode(!!this.model.get('tbnodeData'));
      }
    },
    showTbnodeBlock: function (show) {
      if (this.isMainContent() && this.$tbnodeWrapper) {
        this.doTbnbShow = !!show;
        this.showTbnbTO && clearTimeout(this.showTbnbTO);
        this.showTbnbTO = setTimeout($.proxy(function () {
          var $tbnode = this.$tbnodeWrapper.find('.block-slogtb');
          this.doTbnbShow ? $tbnode.show() : $tbnode.hide();
          delete this.doTbnbShow;
        }, this), 100);
      }
    },
    showTbnode: function (show, testNid) {
      if (this.isMainContent()) {
        this.$tbnodeWrapper && this.$el.toggleClass('has-tbnode', show);
        if (!show || testNid) {
          testNid && (show = (this.curTbnodeNid === testNid));
          var toolbar = _sxt.getTbSysNode()
              , tblView = _xtsi.getTblineView(toolbar);
          if (!show && tblView) {
            tblView.enableSlogtbTabByToolbar(false, toolbar);
            _xtsi.getSiListView(toolbar).model.unset('slogitemList', {silent: true});
          }
        }
      }
    },
    showSpecial: function (show) {
      if (!!this.specialView) {
        var thisView = this;
        if (show) {
          thisView.$el.toggleClass('has-special', true);
        } else {
          thisView.$el.toggleClass('has-special', false);
          this.specialView.clearState();
          this.model.set('specialData', false, {silent: true});
        }
      }
    },
    hasTbnodeVisible: function () {
      return this.$el.hasClass('has-tbnode');
    },
    getTbnodeId: function () {
      return this.model.getTbnodeId();
    },
    isNodeContent: function (test_nid) {
      var nid = this.getTbnodeId();
      return !!test_nid ? (nid == test_nid) : !!nid;
    },
    getCurPathData: function () {
      if (this.curPath) {
        var baseAjaxPath = _xtsi.baseAjaxPath()
            , pdata = _sxth.splitPath(this.curPath, baseAjaxPath, ['none', 'sid']);
        pdata.pathLabel = this.hview.$el.find('.path-label').text()
        return pdata;
      }
    },
    onOverClosed: function () {
      if (this.isMainContent()) {
        this.showTbnodeBlock(true);
        this.showTbnode(this.hasTbnode);
      }
    },
    isMainContent: function () {
      return (this.targetid === 'tContentMain');
    }
  };


  /**
   * Backbone View for a slogitem content.
   */
  _xtsi.XtsiContentView = _xtsi.XtsiContentViewBase.extend(xtsiContentViewExt);
  var xtsiView = _xtsi.XtsiContentView
      , xtsiViewProto = xtsiView.prototype;

  // actions
  var actionsExt = {
    actionEdit: function () {
//todo::extend for more then only node content
      //todo::actions::main
      var nid = this.getTbnodeId();
      if (nid) {
        _sxt.resetGroupPerms();
        var args = {
          view: 'XtEditFormView',
          provider: 'slogxt',
          options: {
            editTarget: 'edit_xtsicontent',
            entityId: nid,
            contentProvider: 'sxt_slogitem',
            mainView: _xtsi.mainView()
          }
        };
        _sxt.dialogViewOpen(args);
      }
    },
    actionPdf: function () {
      this.mainView.executePrintable('node', this.getTbnodeId(), 'xtsipdf')
    },
    actionRefresh: function () {
      this.refresh();
    },
    actionCoProperties: function () {
      var nid = this.getTbnodeId();
      if (nid) {
        _sxt.resetGroupPerms();
        var args = {
          view: 'XtsiCoPropsView',
          provider: 'sxt_slogitem',
          options: {
            editTarget: 'props_xtsicontent',
            entityId: nid,
            noWizardPath: true,
            contentProvider: 'sxt_slogitem',
            mainView: _xtsi.mainView()
          }
        };
        _sxt.dialogViewOpen(args);
      }
    }
  };


  $.extend(true, actionsExt, xtsiView.__super__.actions.sxt_slogitem);
  $.extend(true, xtsiViewProto.actions.sxt_slogitem, actionsExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
