/**
 * @file
 * A Backbone View for a ???.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt
      , _tse = Drupal.theme.slogxtElement;

  var xtsiCContentViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiCContentView',
    $mwComments: false,
    mwcToggleAll: false,
    initialize: function (options) {
      this.pmodel = options.pmodel;
      this.pview = options.pview;

      this.listenTo(this.model, 'change:contentData', this.onChangeContentData);

      return this;
    },
    onChangeContentData: function (model, data) {
      this.$mwComments = false;
//      slogLog('XtsiCContentView: onChangeContentData...target: ' + this.pview.target);
      if (data.html) {
        this.detach();
        this.$el.html(data.html);
        this.prepareMwToc();
        this.prepareMwComment();
        this.$el.show();
        this.attach();
      } else {
        this.$el.hide();
      }
    },
    prepareMwToc: function () {
//todo::current:: --- current - prepareMwToc
// mwtocAutoopen  ???
// ??? keep last state instead
//
      var $toc = this.$mwToc = this.$el.find('#toc');
      if ($toc.length) {
        $toc.prepend($(_tse('div', {class: 'mw-toc-toggler', title: Drupal.t('Toggle contents')})));
        $toc.find('.mw-toc-toggler')
            .on('click', $.proxy(this.onClickMwToc, this));
        $toc.find('li>a')
            .on('click', $.proxy(this.onClickMwTocItem, this));
        _sxt.localSwitches.mwtocAutoopen && $toc.addClass('open');
      }
    },
    onClickMwToc: function (event) {
      event.preventDefault();
      var isOpen = this.$mwToc.hasClass('open');
      this.$mwToc.toggleClass('open', !isOpen);
    },
    onClickMwTocItem: function (event) {
      event.preventDefault();
      var href = $(event.currentTarget).attr('href');
      if (this.$mwComments.length && this.$mwComments.find(href).length) {
        this.mwcEnsureOpen(href);
      }
      this.$mwToc.scrollParent().scrollTo(href);
    },
    mwcEnsureOpen: function (href) {
      var $mwcItem = this.$mwComments.find(href)
          , $parents = $mwcItem.parentsUntil(this.$mwComments, '.sxt-mwcomment-item');
      this.mwcToggleAll = {isClosed: true, toParents: true};
      $parents.find('.sxtmwc-toggler').click();
      this.mwcToggleAll = false;
    },
    prepareMwComment: function () {
      var $mwc = this.$mwComments = this.$el.find('.sxt-mwcomments')
          , mwcData = $mwc.data('mwcommentsData') || ''
          , mwcSplitted = mwcData.split(':')
          , mwcMany = mwcSplitted[1] || false
          , $mwcItems = $mwc.find('.sxt-mwcomment-item')
          , actionsAvailable = ['delete', 'edit', 'reply'];
      this.mwcNid = mwcSplitted[0] || 0;
      this.$el.removeClass('many-mwcomments');
      if ($mwc.length) {
        var hasToggler = !!_sxt.localSwitches.mwcShowToggler;
        $mwc.toggleClass('has-toggler', hasToggler);
        mwcMany && this.$el.addClass('many-mwcomments');
        $mwcItems.each(function () {
          var $this = $(this)
              , cid = $this.data('mwcommentCid')
              , aString = ($this.data('mwcommentActions') || '')
              , aArray = aString.split(':')
              , actions = _.intersection(actionsAvailable, aArray)
              , $toggler = hasToggler ? $(_tse('div', {class: 'sxtmwc-action sxtmwc-toggler'}, '&nbsp;')) : '';
          if (actions.length) {
            var htmlWrapper = ''
                , htmlItems = ''
                , iCls = 'sxtmwc-action'
                , lidx, iLabel;
            _.each(actions, function (action) {
              lidx = 'comment' + action.capitalize();
              iLabel = _sxt[lidx];
              htmlItems += _tse('div', {class: iCls, title: iLabel, 'data-action': action, 'data-cid': cid}, iLabel);
            });
            htmlWrapper = _tse('div', {class: iCls + '-wrapper'}, htmlItems);
            $this.find('> :header > span.mw-headline').before($toggler).after($(htmlWrapper));
          }
          else {
            $this.find('> :header > span.mw-headline').before($toggler);
          }
        });

        // attach
        $mwc.find('.sxtmwc-action').on('click', $.proxy(this.onClickMwcAction, this));
        $mwc.on('click', $.proxy(this.onClickMwc, this));
      }
    },
    onClickMwc: function (e) {
      e.stopPropagation();
      this.pview.activateContent();      
      var $mwcItems = this.$mwComments.find('.sxt-mwcomment-item');
      $mwcItems.removeClass('mwc-item-marked');
      $(e.target).closest('.sxt-mwcomment-item').addClass('mwc-item-marked');
    },
    onClickMwcAction: function (e) {
      e.stopPropagation();
      this.pview.activateContent();      
      var $target = $(e.target)
          , action = $target.data('action') || 'addnew'
          , cid = $target.data('cid') || 0
          , nid = this.mwcNid;
      if ($target.hasClass('sxtmwc-toggler-all')) {
        var isClosed = this.$mwComments.hasClass('closed');
        this.mwcToggleAll = {isClosed: !!isClosed};
        this.$mwComments.toggleClass('closed', !isClosed);
        this.$mwComments.find('.sxtmwc-toggler').click();
        this.mwcToggleAll = false;
      } else if ($target.hasClass('sxtmwc-toggler')) {
        var $mwcitem = $target.closest('.sxt-mwcomment-item')
            , doClose = !(this.mwcToggleAll ? this.mwcToggleAll.isClosed : $mwcitem.hasClass('closed'))
            , $subitem = $mwcitem.find('> .sxt-mwcomment-item');
        $mwcitem.toggleClass('closed', doClose);
        $mwcitem.find('> .sxt-mwcomment-body').toggleClass('visually-hidden', doClose);
        $mwcitem.find('> :header > .sxtmwc-action-wrapper').toggleClass('visually-hidden', doClose);
        $subitem.toggleClass('visually-hidden', doClose);
        this.mwcToggleAll && !this.mwcToggleAll.toParents && $subitem.toggleClass('closed', doClose);
        !doClose && this.$mwComments.removeClass('closed');
      } else if (nid) {
        var args = {
          view: 'XtEditFormView',
          provider: 'slogxt',
          options: {
            startTitle: $target.text(),
            startPath: 'mwdiscussion/' + nid + '/' + action + '/' + cid,
            editTarget: 'discussion',
            entityId: nid,
            contentProvider: 'sxt_slogitem',
            mainView: _xtsi.mainView()
          }
        };
        _sxt.dialogViewOpen(args);
      }
    },
    refreshMwcToggler: function () {
      if (this.$mwComments && this.$mwComments.length) {
        var hasToggler = !!_sxt.localSwitches.mwcShowToggler;
        this.$mwComments.toggleClass('has-toggler', hasToggler);
      }
    },
    attach: function () {
    },
    detach: function () {
//      Drupal.detachBehaviors(this.el);
      this.$mwToc && this.$mwToc.find('.mw-toc-toggler').off();
    },
    onClickContent: function (event) {
      event.preventDefault();
    },
    onClickViewsPager: function (event) {
//      slogLog('XtsiCContentView: onClickViewsPager');
      event.preventDefault();
      var listItemData = _.clone(this.pview.getListItemData());
      this.pview.hmodel.set('headerData', {}, {silent: true});
      listItemData.path = $(event.target).data('drupalLinkSystemPath');
      this.pview.setListItemData(listItemData);
    }

  };

  /**
   * Backbone View for a ....
   */
  _xtsi.XtsiCContentView = Backbone.View.extend(xtsiCContentViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
