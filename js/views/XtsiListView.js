/**
 * @file
 * A Backbone View for a Slogitem List.
 */

(function ($, Drupal, drupalSettings, Backbone, _, sessionStorage, undefined) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt
      , _sxth = _sxt.helper;

  var xtsiListViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiListView',
    actions: {sxt_slogitem: {}},
    constructor: function () {
      Backbone.View.prototype.constructor.apply(this, arguments);
    },
    initialize: function (options) {
      this.sxtIsPermanent = true;
      this.isContent = false;
      this.toolbar = this.targetid = this.model.id
      this.itemLData = this.curPathData = {};
      this.stateModel = _sxt.stateModel();
      this.historyModel = _xtsi.historyModel();
      this.bmModel = _xtsi.bookmarkModel()
      this.mainModel = _xtsi.mainModel();
      this.mainView = _xtsi.mainView();
//      this.bmModel = _xtsi.bookmarkModel();

      this.listenTo(this.model, 'change:slogtbView', this.onChangeSlogtbView);
      this.model.set('slogtbView', options.slogtbView);
//      this.targetLabel = this.slogtbModel.statics.toolbarBarLabel;

      this.initElement();
      this.initRegionData();

      this.listenTo(this.model, 'change:slogitemList', this.onChangeSlogitemList);
      this.listenTo(this.bmModel, 'change:lastAction', this.onChangeBookmarkLastAction);

      this.listenTo(this.stateModel, 'change:stateLoadReady', this.onChangeStateLoadReady);
      this.listenTo(this.stateModel, 'change:dialogClosed', this.onChangeDialogClosed);

      // more listeners for this.slogtbModel
      this.tbAttach();
      return this;
    },
    initElement: function () {
      this.$el.prepend($(Drupal.theme.slogxtHeader()));
      this.$header = this.$el.find('>.sxt-header-wrapper');
      this.$content = this.$el.find('>.content');
    },
    initRegionData: function () {
      var $region = this.$el.closest('.region')
          , $blocks = $region.find('.block.stb-content')
          , regionId = $region.attr('id')
          , xtRegionData = this.mainModel.get('xtRegionData') || {}
      , stbToolbars = $blocks.length ? drupalSettings.slogtb.toolbars || {} : {};
      if (!regionId) {
        regionId = 'xtsi-list-' + ($region.closest('.lout-top-pane').attr('id') || this.toolbar);
        $region.attr('id', regionId);
      }

      // remove toolbar content blocks where there is no toolbar
      $.each($blocks, function (idx, block) {
        var $block = $(block)
            , toolbar = $block.attr('toolbar') || false;
        if (!!toolbar && !stbToolbars[toolbar]) {
          $block.remove();
        }
      });

      xtRegionData[regionId] = xtRegionData[regionId] || {
        regionId: regionId,
        $region: $region,
        $blocks: $region.find('.block.stb-content, .block.block-search'),
        isContent: false,
        xtsiViews: {}
      };
      xtRegionData[regionId].xtsiViews[this.toolbar] = this;
      this.mainModel.set('xtRegionData', xtRegionData);
    },
    initTblineView: function () {
      this.listenTo(this.tblineModel, 'change:currentTabIndex', this.onChangeCurrentTabIndex);
    },
    onChangeCurrentTabIndex: function (tblm, index) {
      if (index === this.tblineIndex) {
        setTimeout($.proxy(function () {
          var curIdx = this.tblineModel.get('currentTabIndex');
          if (curIdx === this.tblineIndex && !!this.getItemsEl().length) {
            var activeView = _xtsi.getActiveContentView()
                , liData = activeView.getListItemData();
            this.mainView.syncActiveSiListItem(liData, this);
          }
        }, this), 100);
      }
    },
    tbDetach: function () {
//      slogLog('XtsiListView: ..................tbDetach');
      this.stopListening(this.slogtbModel);
    },
    tbAttach: function () {
//      slogLog('XtsiListView: .....................tbAttach: ' + this.toolbar);
      this.listenTo(this.slogtbModel, 'change:contentData', this.onChangeTbContentData);
    },
    onChangeSlogtbView: function (model, slogtbView) {
//      slogLog('XtsiListView: onChangeSlogtbView');
      this.slogtbView = slogtbView;
      this.slogtbModel = slogtbView.model;
      this.targetLabel = this.slogtbModel.statics.toolbarBarLabel;
      this.isTbSys = slogtbView.isTbSys;
    },
    onChangeTbContentData: function (model, data) {
      if (data.provider === 'sxt_slogitem') {
        this.curPath = data.path;
        data.isContent = false;
        this.setSlogitemListData(data);
      }
    },
    onChangeSlogitemList: function (model, data) {
//      slogLog('XtsiListView: ..............onChangeSlogitemList: ' + data.tid);
      this.detach();
      this.itemLData = this.curPathData = {};
      delete this.$items;
      this.mainModel.set('activeSiListItem', {}, {silent: true});
      if (data.noContent) {
        var msg = Drupal.theme.slogxtInfoMessage(_sxt.txtNoMenuSelected, 'warning', true);
        this.$content.html(msg);
        this.$header.hide();
        this.$el.show(400);
      } else if (data.content && data.settings) {
        this.$content.html(data.content);
        var s = data.settings;
        s.basePermTb && (this.basePermTb = s.basePermTb);
        this.$header.find('.main-label').text(s.listLabel);
        this.$header.find('.path-label').text(s.pathLabel);
        this.headerActions = s.headerActions || false;
        this.attach();
        this.bookmarkRefresh();
        _xtsi.setHeaderClass(this.$header);
        this.$header.show();
        this.$el.show(400);

        this.itemLData = {
          tid: data.tid,
          path: data.path,
          pathLabel: s.pathLabel,
          mainLabel: s.listLabel,
          pdRole: data.pdRole || false,
        };

        var pd = this.getActiveMenuPathData();
        pd.path && pd.path === data.path && (this.curPathData = pd);

        this.historySetCurrentItem(this.slogtbView.isTbSys);
        this.activateHistoryButtons();
        // sync selected
        var activeView = _xtsi.getActiveContentView()
            , liData = activeView.getListItemData();
        this.mainView.syncActiveSiListItem(liData, this);
        this.tblineView.enableSlogtbTab();
        if (this.isTbSys) {
          var tbsysdata = {
            tid: data.tid,
            liView: this
          };
          this.mainModel.slogxtSetAndTrigger('change:curSysListView', null, tbsysdata);
        }
      } else {
        this.ensureContent();
      }
    },
    getTbMenueView: function (toolbartab) {
      return this.slogtbView.childViews[toolbartab + 'View'];
    },
    getPath: function () {
      return this.curPathData.path || 0;
    },
    getTermId: function () {
      return this.curPathData.tid || 0;
    },
    getCurPathData: function () {
      return this.curPathData;
    },
    getActiveMenuPathData: function () {
      return this.slogtbModel.get('activeMenuItemPath') || {};
    },
    setPathData: function (pathData, opts) {
      this.slogtbModel.set('activeMenuItemPath', pathData, opts);
    },
    setPathDataAndShow: function (pathData, opts) {
      this.setPathData(pathData, opts);
      this.show();
    },
    moveActiveNext: function (next) {
      next = next || false;
      var $li = this.getActiveItem().parent()
          , $next = next ? $li.next() : $li.prev();
      $next.length && $next.find('>a').click();
    },
    getActiveItem: function () {
      return this.$el.find('.xtsi-list li>a.xtsi-ajax.active');
    },
    getActivePathData: function () {
      var $item = this.getActiveItem();
      if ($item.length) {
        var href = $item.attr('href')
            , baseAjaxPath = _xtsi.baseAjaxPath()
            , pdata = _sxth.splitPath(href, baseAjaxPath, ['none', 'sid']);
        pdata.itemLabel = $item.text();
        return pdata;
      }
    },
    getCurItemData: function () {
      var pdata = this.getActivePathData()
          , curSid = pdata ? pdata.sid : 0
          , contentView = curSid ? _xtsi.getContentViewBySid(curSid) : false
          , itemData = !!contentView ? contentView.getListItemData() : false;
      return itemData;
    },
    getSlogitemDonData: function (forEmptyList) {
      var slogitemList = this.getSlogitemListData()
          , itemData = this.getCurItemData() || false
          , tbPathData = itemData ? itemData.tbPathData : {
            path: slogitemList.path,
            tid: slogitemList.tid,
            toolbar: slogitemList.toolbar,
            toolbartab: slogitemList.toolbartab
          }
      , sils = slogitemList.settings
          , isEmpty = _.isEmpty(sils.entities) || !itemData
          , donData = false;
      if (!!forEmptyList && isEmpty) {
        // don data from empty list
        donData = {
          isEmptyList: true,
          toolbar: slogitemList.toolbar,
          toolbartab: slogitemList.toolbartab,
          targetKey: 'XtsiSlogitem',
          entityId: -slogitemList.tid,
          targetEntity: false,
          menuTid: slogitemList.tid,
          siLiData: slogitemList,
          itemData: slogitemList,
          tbPathData: tbPathData,
          label: sils.listLabel,
          info: sils.pathLabel,
          $curLiItem: $(),
          liView: this
        };
      } else if (!isEmpty) {
        // don data from current selected list item
        var pdata = this.getActivePathData();
        donData = !pdata ? false : {
          toolbar: slogitemList.toolbar,
          toolbartab: slogitemList.toolbartab,
          targetKey: 'XtsiSlogitem',
          entityId: pdata.sid,
          targetEntity: sils.entities[pdata.sid] || false,
          menuTid: slogitemList.tid,
          siLiData: slogitemList,
          itemData: itemData,
          tbPathData: tbPathData,
          label: pdata.itemLabel,
          info: sils.listLabel,
          $curLiItem: this.$el.find('.xtsi-list li>a.active'),
          liView: this
        };
      }

      !!this.basePermTb && (donData.basePermTb = this.basePermTb);
      return donData;
    },
    getSlogitemListData: function () {
      return this.model.get('slogitemList');
    },
    getSysViewStartTarget: function () {
      var toolbar = this.toolbar;
      if (_sxt.isTbSysId(toolbar)) {
        var slogitemList = this.getSlogitemListData() || {}
        , toolbartab = slogitemList.toolbartab || '';
        toolbar = toolbartab.slice(toolbartab.indexOf('_') + 1);
        if (toolbar.indexOf('sys_') === 0) {
          toolbar = toolbar.slice(3);
        }
      }
      return toolbar;
    },
    setSlogitemListData: function (data, opts) {
      var testPdRole = !!data && !!data.pdRole && !this.slogtbView.isTbSys;
      if (testPdRole && data.pdRole !== _sxt.getUserDefaultRole()) {
        this.model.set('slogitemList', null, {silent: true});
        setTimeout($.proxy(function () {
          this.ensureContent();
        }, this), 50);
      } else {
        this.model.set('slogitemList', data, opts);
      }
    },
    onItemClick: function (e) {
      e.stopPropagation();
      e.preventDefault();
      _sxt.closeOpenPopup();

      var $element = $(e.target)
          , $li = $element.closest('li')
          , href = $element.attr('href')
          , baseAjaxPath = _xtsi.baseAjaxPath()
          , slogitemList = this.getSlogitemListData()
          , pdata = _sxth.splitPath(href, baseAjaxPath, ['none', 'sid'])
          , extendedId = (pdata.sid < 0) ? pdata.sid + '/' + slogitemList.tid : pdata.sid
          , toolbar = slogitemList.toolbar
          , targetid = _xtsi.getContentTargetByTb(toolbar)
          , sils = slogitemList.settings
          , routeKey = (sils && sils.sids) ? sils.sids[extendedId] || false : false
          , testHash = routeKey ? sils.hashes[routeKey] || false : false
          , contentView = _xtsi.getSiContentView(targetid)
          , entity = sils.entities[pdata.sid] || false
          , elLabel = $element.text()
          , listItemData = {
            label: elLabel,
            href: href,
            path: pdata.path,
            sid: pdata.sid,
            tid: slogitemList.tid,
            extendedId: extendedId,
            toolbar: toolbar,
            routeKey: routeKey,
            testHash: testHash,
            listLabel: sils.listLabel,
            pathLabel: sils.pathLabel,
            pdRole: slogitemList.pdRole || false,
            byEntity: entity ? entity.type + '.' + entity.eid : false,
            tbPathData: {
              path: this.itemLData.path,
              tid: slogitemList.tid,
              toolbar: toolbar,
              toolbartab: slogitemList.toolbartab
            }
          }
      , cPathLabel = _xtsi.contentPathLabel(sils.pathLabel, sils.listLabel, elLabel)
          , dPathLabel = {
            sid: pdata.sid,
            cPathLabel: cPathLabel
          }
      , activeCView = _xtsi.getActiveContentView();

//      slogLog('XtsiListView: onItemClick....path: ' + pdata.path);
      this.mainView.activateContent(targetid);
      activeCView.setDepositPathLabel(dPathLabel);
      activeCView.hview.setPathLabel(cPathLabel);

      this.mainView.syncActiveSiListItem(listItemData, this);
      contentView.setListItemData(listItemData);
      this.mainModel.slogxtSetAndTrigger('change:listItemClicked', null, targetid);

      if (_sxt.isDoNavigateActive() && _sxt.isDonTargetKey('XtsiSlogitem')) {
        this.donClickTO && clearTimeout(this.donClickTO);
        var itemData = this.getCurItemData() || false;
        if (!!itemData) {
          var $itemAction = this.mainView.$btnItemAction;
          this.setItemAction(false);
          $li.prepend($itemAction);
          _sxt.setDoNavigateRawData(this.getSlogitemDonData(), true);
          _sxt.getDoNavigateView().attachItemAction($itemAction);
        } else {
//todo::current:: isDoNavigateActive::donClickTO
//todo::current:: TxRootTermGetPlugin - new::todo:: ????
//setListItemData added (prevent endless loop)
//but there are other problems !!!
          var thisView = this
          , xx = 0;


          this.donClickTO = setTimeout(function () {
            contentView.setListItemData(listItemData, {silent: true});
            $element.click();
          });
        }
      }
    },
    setItemAction: function (valid, multiple) {
      var aClass = multiple ? 'documentadd' : 'select';
      !valid && (aClass = 'blocked');
      this.mainView.$btnItemAction
          .removeClass('documentadd blocked')
          .addClass(aClass);
      this.$el.find('.xtsi-list').addClass('has-action');
    },
    getItemsEl: function () {
      this.$items = this.$items || this.$content.find('.xtsi-list .item-list a.xtsi-ajax');
      return this.$items;
    },
    onHeaderClick: function (e) {
      var $items = this.getItemsEl();
      if ($items.length) {
        e.stopPropagation();
//        slogLog('XtsiListView: ...........onHeaderClick.......');
      }
    },
    onListClick: function (e) {
      e.stopPropagation();
      if (_sxt.isDoNavigateActiveWizard()) {
//        slogLog('XtsiListView: ..............onListClick');
        var $items = this.getItemsEl();
        if (!$items.length) {
          var donData = this.getSlogitemDonData(true);
          _sxt.setDoNavigateRawData(donData, true);
        } else {
          $items.last().click();
        }
      }
    },
    attach: function () {
      if (!!this.headerActions) {
        this.buildHeaderDropbutton(this.headerActions);
        Drupal.behaviors.dropButton.attach(this.$header.first(), {});
        this.$header.find('.slogxt-dropbutton ul.dropbutton li.dropbutton-action a')
            .on('contextmenu', function () {
              return false;
            });
      }
      this.$header.once('xtsi-ajax')
          .on('click', $.proxy(this.onHeaderClick, this));
      this.getItemsEl().once('xtsi-ajax')
          .on('click', $.proxy(this.onItemClick, this));
      this.$content.find('.xtsi-list').once('xtsi-ajax')
          .on('click', $.proxy(this.onListClick, this));
      this.$el.find('a.header-action').once('xtsi-action')
          .on('click', $.proxy(function (e) {
            _sxt.dropbuttonActionDispatch(e, this);
          }, this));
      this.$el.find('.dropbutton-toggle').once('dropbutton-toggle')
          .on('click', _sxt.dropbuttonClickHandler);
    },
    detach: function () {
      this.$el.removeOnce('xtsi-ajax').off();
      this.$el.removeOnce('xtsi-action').off();
      if (!!this.headerActions) {
        _sxt.clearDropbuttons(this.$header);
        this.$header.removeOnce('dropbutton-toggle').off();
        this.$header.find('.dropbutton-wrapper').remove();
      }
    },
    bookmarkRefresh: function () {
      if (!!this.headerActions) {
        this.dropButton = _sxt.getDropbutton(this.$header.find('.dropbutton-wrapper'));
        var $dBtn = this.dropButton.$dropbutton
            , hasBookmark = this.bmModel.hasBookmark(this.curPath, 'list');
        $dBtn && $dBtn.toggleClass('has-bookmark', hasBookmark);
      }
    },
    buildHeaderDropbutton: function (actions) {
      if (actions && !$.isEmptyObject(actions)) {
        actions = this.prepareAuthActions(actions);
        actions = this.prepareEditAction(actions);
        actions = this.prepareFilterAction(actions);
        actions = _sxt.dropbuttonPrepareActions(actions, this.curAction);
        var html = Drupal.theme.slogxtDropbutton(actions, {aClass: 'header-action'});
        html && this.$header.prepend($(html));
      }
    },
    prepareAuthActions: function (actions) {
      if (_sxt.isUserAuthenticated()) {
        return actions;
      }

      actions = actions.filter(function (action) {
        return (!!action.needsAuth ? false : true);
      });
      return _.compact(actions);
    },
    prepareEditAction: function (actions) {
      if (!_sxt.isUserAuthenticated() || this.slogtbView.isTbSys) {
        actions = _sxt.dropbuttonPreventActions(actions, ['edit']);
      } else {
        var thisView = this;
        actions = actions.filter(function (action) {
          return (action.path !== 'edit' || thisView.isEditable());
        });
      }
      return _.compact(actions);
    },
    prepareFilterAction: function (actions) {
      var xx_listData = this.xx_listData;
      actions = actions.filter(function (action) {
        if (!!action.useFilter) {
          var provider = action.provider
              , func = action.useFilter
              , parts = func.split('::');
          if (parts.length > 1) {
            provider = parts[0];
            func = parts[1];
          }
          return Drupal[provider]['filters'][func](xx_listData);
        }

        return true;
      });

      return _.compact(actions);
    },
    isEditable: function () {
      if (_sxt.isAdmin()) {
        return true;
      } else {
        var toolbar = this.toolbar;
        if (toolbar === _sxt.getTbSysNode() && this.basePermTb) {
          toolbar = this.basePermTb;
        }
        return _xtsi.isEditable('editableList', toolbar);
      }
    },
    ensureContent: function () {
      if (!this.stateModel.get('stateLoadReady')) {
        return;
      }
//      slogLog('XtsiListView: ...........ensureContent: ' + this.toolbar);
      var xtsiFromPath = _xtsi.getFromPathData(['xtsiList'])
          , pathData = this.getPath()
          , path = pathData ? pathData.path || false : false
          , pathData, done = false;
      if (xtsiFromPath) { // first from path
        _xtsi.deleteAutorunFromPath();
        var fpData = xtsiFromPath.data
            , cData = fpData.content || {}
        , lData = cData.tbPathData || fpData.list || false;
        lData && lData.toolbar === this.toolbar && (done = true);
      }

      if (!done && !path) {
        // next from history
        done = this.historyModel.goToCurrent(this, this.getHistoryTarget());
        if (!done && this.mainModel.hasPromotedData()) {
          // next from promoted
          pathData = (this.mainModel.getPromotedData())['list'] || false;
          if (pathData && pathData.toolbar) {
            if (pathData.toolbar === this.toolbar) {
              this.setPathData(pathData);
              done = true;
            } else {
              var tabIdx = this.tblineView.contentIndizes[pathData.toolbar]
                  , lView = this.tblineView.tabsContentViews[tabIdx];
              if (lView) {
                lView.show();
                lView.ensureContent();
                return; // prevent activateHistoryButtons()
              }
            }
          }
        }

        if (!done) {
          this.setSlogitemListData({noContent: true});
        }

        // buttons
        this.activateHistoryButtons();
      }
    },
    getMyTblineTabIndex: function () {
      if (this.myTblineTabIndex === undefined) {
        var cIndizes = this.tblineView.contentIndizes;
        this.myTblineTabIndex = cIndizes[this.toolbar];
      }
      return this.myTblineTabIndex;
    },
    getSidByEntity: function (byEntity) {
      var slogitemList = this.getSlogitemListData();
      if (slogitemList) {
        var belist = (slogitemList.settings || {}).byEntity || {};
        return belist[byEntity];
      }
    },
    isVisible: function () {
      return this.$el.is(':visible') && !this.$el.hasClass('visually-hidden');
    },
    show: function () {
      this.tblineView.setCurrentTabIndex(this.getMyTblineTabIndex());
    },
    refresh: function () {
      this.actions.sxt_slogitem.actionRefresh.call(this);
    },
    remove: function () {
      this.actions.sxt_slogitem.actionRemoveHistory.call(this);
    },
    isCurToolbartab: function (toolbartab) {
      return (this.curPathData.toolbartab && this.curPathData.toolbartab === toolbartab);
    }

  };



  /**
   * Backbone View for a slogitem list.
   */
  _xtsi.XtsiListView = _xtsi.XtsiContentViewBase.extend(xtsiListViewExt);
  var xtsiView = _xtsi.XtsiListView
      , xtsiViewProto = xtsiView.prototype;

  var actionsExt = {
    actionEdit: function () {
      function onChangedActiveTab(model, data) {
        var actionBase = this._sxtCacheGet('dialog', 'actionBase') || {}
        , liViewBase = actionBase.view;
        if (data.targetid == 0) {
          if (liViewBase && actionBase.data.tbPathData) {
            liViewBase.setPathDataAndShow(actionBase.data.tbPathData);
          }
        } else if (this.donView) { // this for XtEditFormView
          var tabData = this.tabsData[data.targetid] || {}
          , baseData = tabData.baseData || {}
          //todo::isTarget::xtsi::inconsistent
          , isTarget = !!baseData.isTarget
              , isSource = !!baseData.isSource;
          if (isSource && this.$donSourceItem) {
            if (this.$donSourceItem.length) {
              this.$donSourceItem.click();
            } else {
              var sCache = this._sxtCacheGet('action', 'source') || {}
              , liView = sCache.liView;
              if (liView && sCache.tbPathData) {
                liView.setPathDataAndShow(sCache.tbPathData);
              } else {
                if (liViewBase && actionBase.data.tbPathData) {
                  liViewBase.setPathDataAndShow(actionBase.data.tbPathData);
                }
              }
            }
          } else if (isTarget && this.$donTargetItem && this.$donTargetItem.length) {
            this.$donTargetItem.click();
          }
        }
      }
      function onDialogClosed() {
        _xtsi.enableSetHistory(false);
      }
      function onViewInitialized() {
        function onChangeCurSysListView(m, data) {
          var pageData = this._sxtWizardPageData()
              , baseData = (pageData.tabData || {})['baseData'];
          if (this.isWizard && baseData && baseData.onChangeCurSysListView) {
            baseData.onChangeCurSysListView(data);
          }
        }
        this.listenTo(this.mainView.model, 'change:curSysListView', onChangeCurSysListView);
      }


      // open dialog
      var termId = this.getTermId();
      if (!!termId) {
        _sxt.resetGroupPerms();
        var args = {
          view: 'XtEditFormView',
          provider: 'slogxt',
          options: {
            editTarget: 'edit_xtsilist',
            entityId: termId,
            contentProvider: 'sxt_slogitem',
            mainView: _xtsi.mainView(),
            actionBase: {
              view: this,
              menuTb: this.toolbar,
              nodePermTb: this.basePermTb ? {
                basePermTb: this.basePermTb,
                inTermId: termId
              } : false,
              data: {
                tbPathData: this.getCurPathData(),
                itemListData: this.getItemListData(),
                itemData: this.getCurItemData()
              }
            },
            onViewInitialized: onViewInitialized,
            onChangedActiveTab: onChangedActiveTab,
            onDialogClosed: onDialogClosed
          }
        };
        _xtsi.enableSetHistory(true);
        _sxt.dialogViewOpen(args);
      }
    },
    actionRefresh: function () {
      var pathData = this.curPathData
          , toolbartab = this.slogtbView.isTbSys ? 'dummy' : pathData.toolbartab
          , tbMenuView = this.getTbMenueView(toolbartab);
      if (tbMenuView) {
        this.slogtbModel.set('contentData', null, {silent: true});
        this.setSlogitemListData(null, {silent: true});
        tbMenuView.loadContent(pathData, true);
      }
    },
    actionLiProperties: function () {
      var args = {
        view: 'AtWorkView',
        provider: 'slogxt',
        options: {
          infoText: 'actionLiProperties'
        }
      };
      _sxt.dialogViewOpen(args);
    }
  };


  $.extend(true, actionsExt, xtsiView.__super__.actions.sxt_slogitem);
  $.extend(true, xtsiViewProto.actions.sxt_slogitem, actionsExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, sessionStorage);
