/**
 * @file
 * A Backbone View for History Dialog.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiPropsViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiLiPropsView',
    actions: {},
    nix: {}
  };

  var actionsExt = {
    __actionBookmark: function () {
      alert('XtsiLiPropsView::__actionBookmark');
    },
    __actionSave: function () {
      alert('XtsiLiPropsView::__actionSave');
    }
  };
  xtsiPropsViewExt.actions.sxt_slogitem = actionsExt;

  /**
   * Backbone View for Content Properties Dialog
   */
  _xtsi.XtsiLiPropsView = _sxt.XtEditFormView.extend(xtsiPropsViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
