/**
 * @file
 * A Backbone View for History Dialog.
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse, undefined) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiHistoryViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiHistoryView',
    actions: {},
    onPreInitialize: function (opts) {
      this.mainView = _xtsi.mainView();
      this.mainModel = this.mainView.model;
      this.mainView.resetBtnItemAction();
      this.restoreEnabledHistory = _xtsi.enableSetHistory(false);
      this.historyModel = _xtsi.historyModel();
      this.startView = opts.startView;
      this.showNavigateBtn = true;
    },
    dialogOptions: function () {
      var actions = _xtsi.getHistoryActions();
      var options = {
        title: 'HistoryDialog',
        dialogClass: 'xtsi-history',
        slogxt: {
          dialogId: 'HistoryDialog',
          restoreState: true,
          btnSubmitShow: false,
          hasPushpin: true,
          tabsData: this.buildTabsData(),
          actions: Drupal.theme.slogxtDropbutton(actions)
        }
      };
      return options;
    },
    hasValidItems: function (items, toolbar) {
      var toolbartabs = drupalSettings.slogtb.toolbartabs
          , idx, idata, tbtab;
      for (idx in items) {
        idata = items[idx].data || {};
        tbtab = idata.toolbartab || false;
        if (tbtab && !!toolbartabs[tbtab] && idata.toolbar === toolbar) {
          return true;
        }
      }
      return false;
    },
    buildTabsData: function () {
      var allData = this.historyModel.getAllData()
          , targets = []
          , tabsData = {}
      , startTarget = this.startView.model.id
          , tbSysNode = _sxt.getTbSysNode()
          , tblIdx, tblAdd, tblineData
          , idx, hData, tiPre, isContent, isTbnode, isTbSys;
      for (tblIdx in allData) {
        tblineData = allData[tblIdx] || {};
        for (idx in tblineData) {
          hData = tblineData[idx];
          isContent = hData.isContent;
          isTbnode = !isContent && (hData.id === tbSysNode);
          isTbSys = _sxt.isTbSysId(hData.id);
          tiPre = (isContent ? Drupal.t('Content') : Drupal.t('List')) + ' ';

          tblAdd = true;
          if (!isContent) {
            tblAdd = this.hasValidItems(hData.items, hData.id);
          }

          if (tblAdd) {
            targets.push(hData.id);
            tabsData[hData.id] = {
              type: 'checkboxes',
              isContent: isContent,
              useActions: true,
              labels: {
                dialogTitle: tiPre + Drupal.t('history') + ': ' + (hData.id).capitalize(),
                tabLabel: hData.id
              },
              // class 'xtsi-icon-..' is required for theme sjqlout
              cls: (isContent || isTbSys) ? 'xtsi-icon-' + (hData.id).toLowerCase() : '',
              tbline: hData.tbline,
              tblineIndex: idx,
              contentHtmlCallback: this.buildTabContentHtml
            };
          }
        }
      }

      tabsData.iconized = true;
      tabsData.targets = targets;
      if (_.indexOf(targets, startTarget) >= 0) {
        tabsData.startTarget = startTarget;
      }
      return tabsData;
    },
    buildTabContentHtml: function (targetid) {
      targetid = targetid || this.model.get('activeTab').targetid;
      var defaultRole = _sxt.getUserDefaultRole()
          , hData = this.historyModel.getDataById(targetid)
          , data = [], idx, item, idata;
      for (idx in hData.items) {
        item = hData.items[idx];
        idata = item.data || {};
        if (hData.isContent || idata.toolbar === hData.id) {
          if (item.pdRole && item.pdRole === defaultRole) {
            var idata = item.data
                , ll = idata.listLabel ? '/' + idata.listLabel : false
                , toolbartab = idata.toolbartab || idata.tbPathData.toolbartab
                , pl = _sxt.getRoleHeaderPath(item.pdRole, toolbartab);
            if (ll) {
              var parts = item.pathLabel.split(ll);
              item.pathLabel = pl + ll + (parts[1] || '');
              idata.pathLabel = pl;
            } else {
              item.pathLabel = idata.pathLabel = pl;
            }
          }
          data.push({
            index: idx,
            liTitle: item.mainLabel,
            liDescription: item.pathLabel,
            liCls: (idx == hData.currentIdx) ? 'is-current' : null
          });
        }
      }
      return Drupal.theme.slogxtListCheckboxes(data, null, true);
    },
    onTabsCreated: function () {
      var tabsData = this.tabsData
          , $tabs = this.$el.find('#slogxt-dialog-tabs');
      for (var idx in tabsData.targets) {
        var toolbar = tabsData.targets[idx]
            , tabData = tabsData[toolbar]
            , isToolbar = !tabData.isContent
            , tbView = Drupal.slogtb.getSlogtbView(toolbar);
        if (isToolbar && tbView && !!tbView.tbIconDarkUrl) {
          $tabs.find('.tabs-item[targetid=' + toolbar + ']')
              .css('backgroundImage', tbView.tbIconDarkUrl);
        }
      }
    },
    onChangedActiveTab: function (model, data) {
      if (!this.actionWizardBaseData) {
        var tData = this.historyModel.getTargetDataById(data.targetid)
            , targetView = tData.isContent ? _xtsi.getActiveContentView() : tData.view;
        this.activeTargetData = tData;
        targetView.show();
        this.$activeContent.find('ul>li.is-current').click();
      }
    },
    onListItemClicked: function (e) {
      if (this.$Tabs.hasClass('visually-hidden')  //todo::problem::hard coded
          && _.contains(['open', 'save'], this.curAction)) {
        return;
      }
      var $li = $(e.currentTarget)
          , newIdx = parseInt($li.attr('index'), 10)
          , tData = this.activeTargetData
          , targetView = tData.isContent ? _xtsi.getActiveContentView() : tData.view;
      tData.isContent && this._sxtPushpinChecked() && targetView.setSyncOnchange();
      this.historyModel.goTo(targetView, newIdx, tData);
      !this._sxtPushpinChecked() && targetView.historySetCurrentItemTO();
      targetView.tblineView.activateHistoryButtons(targetView.getHistoryTarget());
      tData.isContent && (this.contentItemClicked = true);

      if (tData.isContent) {
        if (_sxt.isDoNavigateActive() && _sxt.isDonTargetKey('XtsiSlogitem')) {
          var baseData = _sxt.getDonBaseData();
          !!baseData.onDlgListItemClicked //
              && baseData.onDlgListItemClicked($li, lView, lData);
        }
      }
    },
    onDialogClosed: function (e) {
      _xtsi.enableSetHistory(this.restoreEnabledHistory);
    },
    executeBookmark: function () {
      var awbData = this.actionWizardBaseData
          , data = this.historyModel.getDataById(awbData.targetid) || {}
      , items = data.items || []
          , indizes = awbData.selected.checked
          , bmModel = _xtsi.bookmarkModel()
          , idx, iidx, idata, msg;

      for (idx in indizes.reverse()) {
        iidx = indizes[idx];
        idata = items[iidx];
        idata && idata.data && bmModel.addBookmark(idata);
      }

      msg = Drupal.t('Bookmarks have been set.');
      this._sxtActionsWizardReset(msg);
    },
    executeRemove: function () {
//      slogLog('XtsiHistoryView..........executeRemove');
      var awbData = this.actionWizardBaseData
          , indizes = awbData.selected.checked;
      this.historyModel.removeMultiple(awbData.targetid, indizes);
      this._sxtActionsBaseContentRebuild(this.buildTabContentHtml(awbData.targetid));

      var msg = Drupal.t('Items have been removed.');
      this._sxtActionsWizardReset(msg);
    },
    getContentType: function () {
      return (this.activeTargetData.isContent ? 'content' : 'list');
    }
  };

  var actionsExt = {
    actionBookmark: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.from > 0) {
        var $ul = this.$activeContent.find('ul.slogxt-list');
        idxData.message = Drupal.t('Confirm bookmarking items.');
        var tabData = {
          labels: {
            dialogTitle: Drupal.t('Bookmark items'),
            submitLabel: Drupal.t('Bookmark')
          },
          selected: idxData,
          contentElements: this._sxtDomUlCloneSelected($ul, idxData),
          submitCallback: $.proxy(this.executeBookmark, this)
        };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    },
    actionSave: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.count > 0) {
        var type = this.getContentType()
            , tabData = {
              labels: {
                dialogTitle: Drupal.t('Save'),
                submitLabel: Drupal.t('Next')
              },
              selected: idxData,
              path: _xtsi.getBmstorageActionPath(type, 'save'),
              provider: 'sxt_slogitem'
            };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    },
    actionRemove: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.from > 0) {
        var $ul = this.$activeContent.find('ul.slogxt-list');
        idxData.message = Drupal.t('Confirm removing history items.');
        var tabData = {
          labels: {
            dialogTitle: Drupal.t('Remove items'),
            submitLabel: Drupal.t('Remove')
          },
          selected: idxData,
          contentElements: this._sxtDomUlCloneSelected($ul, idxData),
          submitCallback: $.proxy(this.executeRemove, this)
        };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    }
  };
  xtsiHistoryViewExt.actions.sxt_slogitem = actionsExt;

  /**
   * Backbone View for History Dialog
   */
  _xtsi.XtsiHistoryView = _sxt.DialogViewBase.extend(xtsiHistoryViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
