/**
 * @file
 * A Backbone View for History Dialog.
 */

(function ($, Drupal, drupalSettings, Backbone, _, undefined) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiPropsViewExt = {
    sxtThisClass: 'sxt_slogitem.XtsiCoPropsView',
    actions: {},
    getSelectedIds: function () {
      var $content = this.actionWizardBaseData.$content || $()
          , $selected = $content.find('ul.slogxt-list li.checkbox.checked')
          , result = [];
      $selected.each(function () {
        var $li = $(this)
            , tid = $li.attr('entityid') || false;
        tid && result.push(tid);
      });
      return result.join(',');
    },
    executeBookmark: function () {
      var awbData = this.actionWizardBaseData
          , indizes = awbData.selected.checked
          , $ul = this.$activeContent.find('ul.slogxt-list')
          , mainModel = _xtsi.mainModel()
          , bmModel = _xtsi.bookmarkModel()
          , idx, iidx, $li, labels, liSel, path, toolbar, tbData, idata, msg;

      for (idx in indizes.reverse()) {
        iidx = indizes[idx];
        liSel = 'li[index=\'' + iidx + '\']'
        $li = $ul.find(liSel);
        if ($li.length === 1) {
          labels = _sxt.getLiLabels($li);
          path = $li.data('path');
          toolbar = $li.data('toolbar');
          tbData = mainModel.getTblineRegionTbData(toolbar);
          idata = {
            data: {
              path: path,
              tid: $li.attr('entityid'),
              toolbar: toolbar,
              toolbartab: $li.data('toolbartab')
            },
            path: path,
            mainLabel: labels.mainLabel,
            pathLabel: labels.pathLabel,
            pdRole: $li.data('role') || false,
            tbline: tbData.tbline,
            tblineIndex: tbData.tblineIndex,
            type: 'list'
          };
          bmModel.addBookmark(idata);
        }
      }

      msg = Drupal.t('Bookmarks have been set.');
      this._sxtActionsWizardReset(msg);
    }
  };

  var actionsExt = {
    actionBookmark: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.from > 0) {
        var $ul = this.$activeContent.find('ul.slogxt-list');
        idxData.message = Drupal.t('Confirm bookmarking items.');
        var tabData = {
          type: 'wizardstart',
          cls: 'actions-wizard',
          labels: {
            dialogTitle: Drupal.t('Bookmark items'),
            submitLabel: Drupal.t('Bookmark')
          },
          selected: idxData,
          contentElements: this._sxtDomUlCloneSelected($ul, idxData),
          submitCallback: $.proxy(this.executeBookmark, this)
        };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    },
    actionSave: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.count > 0) {
        var type = 'list'
            , tabData = {
              labels: {
                dialogTitle: Drupal.t('Save'),
                submitLabel: Drupal.t('Next')
              },
              selected: idxData,
              path: _xtsi.getBmstorageActionPath(type, 'save'),
              provider: 'sxt_slogitem'
            };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    }
  };
  xtsiPropsViewExt.actions.sxt_slogitem = actionsExt;

  /**
   * Backbone View for Content Properties Dialog
   */
  _xtsi.XtsiCoPropsView = _sxt.XtEditFormView.extend(xtsiPropsViewExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
