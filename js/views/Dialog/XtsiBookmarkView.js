/**
 * @file
 * A Backbone View for Bookmark Dialog
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiBookmarkExt = {
    sxtThisClass: 'sxt_slogitem.XtsiBookmarkView',
    txtBmType: {list: Drupal.t('List'), content: Drupal.t('Content')},
    actions: {},
    bmRefresh: {},
    bmTypes: ['list', 'content'],
    onPreInitialize: function (opts) {
      this.bmModel = _xtsi.bookmarkModel();
      this.mainView = _xtsi.mainView();
      this.mainModel = this.mainView.model;
      this.mainView.resetBtnItemAction();
      this.restoreEnabledHistory = _xtsi.enableSetHistory(false);
      var listItemData = _xtsi.getActiveContentView().getListItemData()
          , siliItem = this.mainModel.get('activeSiListItem') || false
          , siliContentView = siliItem && siliItem.tblineView
          ? siliItem.tblineView.getCurTabContentView() : false
          , siliPath = siliContentView ? siliContentView.getPath() : '';
      this.bmModel.setCurrentIdx(listItemData.path, 'content');
      this.bmModel.setCurrentIdx(siliPath, 'list');
      this.showNavigateBtn = true;
    },
    onViewInitialized: function () {
      this.listenTo(this.bmModel, 'change:lastAction', this.onChangeBookmarkLastAction);
    },
    resetRefresh: function () {
      this.bmRefresh = {};
    },
    dialogOptions: function () {
      var actions = _xtsi.getBookmarkActions();
      var options = {
        title: 'BookmarkDialog',
        dialogClass: 'xtsi-bookmark',
        slogxt: {
          dialogId: 'BookmarkDialog',
          restoreState: true,
          btnSubmitShow: false,
          hasPushpin: true,
          tabsData: this.buildTabsData(),
          actions: Drupal.theme.slogxtDropbutton(actions)
        }
      };
      return options;
    },
    buildTabsData: function () {
      var targets = []
          , startT = this.bmModel.hasBookmarks('content') ? 'content' : 'list'
          , tabsData = {startTarget: startT};
      for (var idx in this.bmTypes) {
        var type = this.bmTypes[idx]
            , title = Drupal.t('Bookmarks: ') + this.txtBmType[type];
        targets.push(type);
        tabsData[type] = {
          type: 'checkboxes',
          useActions: true,
          labels: {
            dialogTitle: title,
            tabLabel: type
          },
          targetid: type,
          contentHtmlCallback: this.buildTabContentHtml
        };
      }

      tabsData.targets = targets;
      return tabsData;
    },
    buildTabContentHtml: function (targetid) {
//      slogLog('XtsiBookmarkView::.......................buildTabContentHtml');
      targetid = targetid || this.model.get('activeTab').targetid;
      var defaultRole = _sxt.getUserDefaultRole()
          , bmData = this.bmModel.getData(targetid)
          , ulIcons = false
          , data = [], idx, opts;

      this.bmRefresh[targetid] = false;
      if (bmData.items.length) {
        for (idx in bmData.items) {
          var item = bmData.items[idx]
              , toolbar = _xtsi.getToolbarByData(item)
              , liCls = (idx == bmData.currentIdx) ? 'is-current' : ''
              , titleCls = false;
          if (item.type === 'list' && toolbar) {
            liCls += ' bm-toolbar-icon';
            _sxt.isTbSysId(toolbar) && (liCls += ' tbsys-icon');
            ulIcons = true;
          }
          if (item.pdRole && item.pdRole === defaultRole) {
            var idata = item.data
                , ll = idata.listLabel ? '/' + idata.listLabel : false
                , toolbartab = idata.toolbartab || idata.tbPathData.toolbartab
                , pl = _sxt.getRoleHeaderPath(item.pdRole, toolbartab);
            if (ll) {
              var parts = item.pathLabel.split(ll);
              item.pathLabel = pl + ll + (parts[1] || '');
              idata.pathLabel = pl;
            } else {
              item.pathLabel = idata.pathLabel = pl;
            }
          }
          data.push({
            index: idx,
            liTitle: item.mainLabel,
            liDescription: item.pathLabel,
            titleCls: titleCls,
            liCls: liCls
          });
        }
        opts = {targetid: targetid};
        return Drupal.theme.slogxtListCheckboxes(data, opts, true, ulIcons);
      } else {
        var msg = Drupal.t('There are no bookmarks yet');
        return Drupal.theme.slogxtInfoMessage(msg, 'warning', true);
      }
    },
    onTabContentCreated: function () {
      // set list icons
      if (this.activeTargetId === 'list') {
        this.activeTargetData = this.bmModel.getData('list');
        var thisView = this;
        this.$activeContent.find('.bm-toolbar-icon').each(function () {
          var $el = $(this)
              , newIdx = parseInt($el.attr('index'), 10)
              , itemData = thisView.activeTargetData.items[newIdx]
              , tbView = Drupal.slogtb.getSlogtbView(_xtsi.getToolbarByData(itemData));
          if (tbView && !!tbView.tbIconDarkUrl) {
            $el.find('.li-title').css('backgroundImage', tbView.tbIconDarkUrl);
          }
        });
      }
    },
    onChangedActiveTab: function (model, data) {
      if (!this.actionWizardBaseData) {
        this.activeTargetData = this.bmModel.getData(data.targetid);
        this.$activeContent.find('ul>li.is-current').click();
      }
    },
    onListItemClicked: function (e) {
//      slogLog('_xtsi.XtsiBookmarkView::.....onListItemClicked');
      if (this.$Tabs.hasClass('visually-hidden')  //todo::problem::hard coded
          && _.contains(['open', 'save'], this.curAction)) {
        return;
      }
      var $li = $(e.currentTarget)
          , newIdx = parseInt($li.attr('index'), 10)
          , itemData = this.activeTargetData.items[newIdx]
          , toolbar = _xtsi.getToolbarByData(itemData)
          , isContent = (itemData.type === 'content')
          , targetView = isContent
          ? _xtsi.getActiveContentView()
          : _xtsi.getSiListView(toolbar);
      targetView.show();
      this.bmModel.goTo(targetView, itemData);
      !this._sxtPushpinChecked() && targetView.historySetCurrentItemTO();
      !this.actionWizardBaseData && this.bmModel.setCurrentIdx(itemData.path, itemData.type)

      if (isContent) {
        var lData = targetView.getListItemData()
            , lView = _xtsi.getSiListView(lData.toolbar) || false;
        if (this._sxtPushpinChecked() && lView) {
          lView.show();
          lView.setPathData(lData.tbPathData);
        }

        if (_sxt.isDoNavigateActive() && _sxt.isDonTargetKey('XtsiSlogitem')) {
          var baseData = _sxt.getDonBaseData();
          !!baseData.onDlgListItemClicked //
              && baseData.onDlgListItemClicked($li, lView, lData);
        }
      }
    },
    onChangeBookmarkLastAction: function (m, data) {
      (data.action === 'add' || data.action === 'remove') && (this.bmRefresh[data.type] = true);
    },
    onDonReturn: function () {
      var atabData = this.model.get('activeTab')
          , $item = atabData.$item
          , idx, type;
      for (idx in this.bmTypes) {
        type = this.bmTypes[idx];
        if (this.loaded[type] && this.bmRefresh[type]) {
          this.$Content.find('#' + this.loaded[type]).remove();
          delete this.loaded[type];
        }
      }

      if (this.bmRefresh[atabData.targetid]) {
        this.model.set('activeTab', null, {silent: true});
        $item.click();
      }
    },
    onDialogClosed: function (e) {
      _xtsi.enableSetHistory(this.restoreEnabledHistory);
    },
    executeRemove: function () {
      var awbData = this.actionWizardBaseData
          , indizes = awbData.selected.checked;
      this.bmModel.removeMultiple(awbData.targetid, indizes);
      this._sxtActionsBaseContentRebuild(this.buildTabContentHtml(awbData.targetid));

      var msg = Drupal.t('Bookmarks have been removed.');
      this._sxtActionsWizardReset(msg);
    },
    executeLoaded: function (args) {
      var bmModel = this.bmModel
          , awbData = this.actionWizardBaseData
          , selected = awbData.selected
          , pCount = selected.count || 0
          , pIdxs = selected ? selected.checked : false
          , pItems = pIdxs ? bmModel.getItemsByIndices(args.type, pIdxs) : false
          , lItems = args.items || []
          , lCount = lItems.length
          , newItems, newCount, msg, msgType, existent = '';

      (args.type === 'list') && (lItems = bmModel.fixItemsTbline(lItems));
      newItems = bmModel.getItemsUniqMerge(pItems, lItems);
      newCount = newItems.length;
      if (lCount + pCount > newCount) {
        existent = '; ' + _sxt.txtExistent + '=' + (lCount + pCount - newCount)
      }

      if (newCount > bmModel.maxItems) {
        msgType = 'error';
        msg = _xtsi.bmMaxError + ': ' + newCount + ' (max=' + bmModel.maxItems
            + '; ' + _sxt.txtLoaded + '=' + lCount
            + '; ' + _sxt.txtPreserved + '=' + pCount + existent + ')';
      } else {
        var targetData = bmModel.data[args.type];
        targetData.currentIdx = -1;
        targetData.items = newItems;
        awbData.$content.remove(); // remove for new build

        msg = _xtsi.bmLoaded + ': ' + lCount + ' (' + _sxt.txtPreserved + '=' + pCount + existent + ')';
      }
      this._sxtActionsWizardReset(msg, msgType);
//      slogLog('XtsiBookmarkView.executeLoaded().............');
    },
    getContentType: function () {
      return (this.activeTargetData.isContent ? 'content' : 'list');
    }
  };

  var actionsExt = {
    actionOpen: function () {
      var idxData = this._sxtGetCheckedItemsIdxData()
          , type = this.getContentType()
          , tabData = {
            labels: {
              dialogTitle: Drupal.t('Open'),
              submitLabel: Drupal.t('Next')
            },
            selected: idxData,
            path: _xtsi.getBmstorageActionPath(type, 'open'),
            provider: 'sxt_slogitem'
          };
      this._sxtActionsWizardStart(tabData);
    },
    actionSave: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData && idxData.count > 0) {
        var type = this.getContentType()
            , tabData = {
              labels: {
                dialogTitle: Drupal.t('Save'),
                submitLabel: Drupal.t('Next')
              },
              selected: idxData,
              path: _xtsi.getBmstorageActionPath(type, 'save'),
              provider: 'sxt_slogitem'
            };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    },
    actionRemove: function () {
      var idxData = this._sxtGetCheckedItemsIdxData();
      if (idxData) {
        var $ul = this.$activeContent.find('ul.slogxt-list');
        idxData.message = Drupal.t('Confirm removing bookmarks.');
//        idxData.messageType = 'status';
        var tabData = {
          labels: {
            dialogTitle: Drupal.t('Remove bookmarks'),
            submitLabel: Drupal.t('Remove')
          },
          selected: idxData,
          contentElements: this._sxtDomUlCloneSelected($ul, idxData),
          submitCallback: $.proxy(this.executeRemove, this)
        };
        this._sxtActionsWizardStart(tabData);
      } else {
        this._sxtAlertNotSelected();
      }
    }
  };
  xtsiBookmarkExt.actions.sxt_slogitem = actionsExt;

  /**
   * Backbone View for Bookmark Dialog
   */
  _xtsi.XtsiBookmarkView = _sxt.DialogViewBase.extend(xtsiBookmarkExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
