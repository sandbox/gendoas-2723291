/**
 * @file
 * A Backbone View for ....
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiSurpriseExt = {
    sxtThisClass: 'sxt_slogitem.XtsiSurpriseView',
    actions: {},
    onPreInitialize: function (opts) {
      this.mainView = _xtsi.mainView();
      this.mainModel = this.mainView.model;
      this.mainView.resetBtnItemAction();
      this.restoreEnabledHistory = _xtsi.enableSetHistory(false);
      this.showNavigateBtn = true;
    },
    dialogOptions: function () {
      var actions = _xtsi.getSurpriseActions();
      var options = {
        title: Drupal.t('Surprise'),
        dialogClass: 'xtsi-surprise',
        slogxt: {
          dialogId: 'SurpriseDialog',
          hideTabs: true,
          restoreState: true,
          btnSubmitShow: false,
          hasPushpin: true,
          tabsData: this.buildTabsData(),
          actions: Drupal.theme.slogxtDropbutton(actions)
        }
      };
      return options;
    },
    buildTabsData: function () {
      var tabsData = []
          , type = 'surprise';
      if (this.mainModel.hasSurpriseData()) {
        tabsData[type] = {
          type: 'checkboxes',
          useActions: true,
          labels: {
            dialogTitle: Drupal.t('Surprise'),
            tabLabel: type
          },
          targetid: type,
          contentHtml: this.buildList()
        };
        tabsData.targets = [type];
        return tabsData;
      }
      return false;
    },
    buildList: function () {
      var sData = this.mainModel.getSurpriseData()
          , listItemData = _xtsi.getActiveContentView().getListItemData()
          , cur_path = listItemData.path || false
          , data = [], idx, item;
      for (var idx in sData) {
        item = sData[idx].content;
        data.push({
          index: idx,
          liTitle: item.mainLabel,
          liDescription: _xtsi.contentPathLabel(item.pathLabel, item.listLabel, item.label),
          liCls: (item.path === cur_path) ? 'is-current' : null
        });
      }

      return Drupal.theme.slogxtListCheckboxes(data, null, true);
    },
    onListItemClicked: function (e) {
      var activeContentView = _xtsi.getActiveContentView()
          , $li = $(e.currentTarget)
          , idx = parseInt($li.attr('index'), 10)
          , sData = this.mainModel.getSurpriseData()
          , newData = sData[idx] || {}
      , contentData = newData.content || false
          , targetView = activeContentView;
      contentData && activeContentView.setListItemData(contentData);
      !this._sxtPushpinChecked() && activeContentView.historySetCurrentItemTO();
      this.contentItemClicked = true;

      var lData = targetView.getListItemData()
          , lView = _xtsi.getSiListView(lData.toolbar) || false;
      if (this._sxtPushpinChecked() && lView) {
        lView.show();
        lView.setPathData(lData.tbPathData);
      }

      if (_sxt.isDoNavigateActive()  //
          && _sxt.getDonTargetKey() === 'XtsiSlogitem') {
        var baseData = _sxt.getDonBaseData();
        !!baseData.onDlgListItemClicked //
            && baseData.onDlgListItemClicked($li, lView, lData);
      }
    },
    onDialogClosed: function (e) {
      _xtsi.enableSetHistory(this.restoreEnabledHistory);
    }
  };

  /**
   * Backbone View for ....
   */
  _xtsi.XtsiSurpriseView = _sxt.DialogViewBase.extend(xtsiSurpriseExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
