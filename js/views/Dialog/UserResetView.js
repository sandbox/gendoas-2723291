/**
 * @file
 * A Backbone View for login forms (login, password, register).
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var dlgExt = {
    sxtThisClass: 'sxt_slogitem.UserResetView',
//    titlePrefixes: {
//      info: Drupal.t('Info'),
//      warning: Drupal.t('Warning'),
//      error: Drupal.t('Error')
//    },
    onPreInitialize: function (opts) {
      this.userResetOpts = opts || {};
    },
    dialogOptions: function () {
      var resetOpts = this.userResetOpts || {}
      , sp = resetOpts.splitted || {}
      , pathData = {
        uCmd: sp.command,
        uId: sp.param1,
        uTimestamp: sp.param2,
        uHash: sp.param3,
        uIsLogin: (!!sp.param4 && sp.param4 === 'login')
      }
      , options = {
        dialogClass: 'xtsi-user-reset',
        position: {
          my: 'right top',
          at: 'right-10px top+34px',
          of: _sxt.mainActionsView().$el,
          collision: 'fit'
        },
        slogxt: {
          dialogId: 'UserResetView',
          restoreState: false,
          btnSubmitShow: true,
          btnBackShow: false,
          hasPushpin: false,
          contentData: {
            labels: {
              dialogTitle: '...',
              submitLabel: '...'
            },
            path: resetOpts.path,
            provider: 'sxt_slogitem'
          }
        }
      };

      return options;
    }

  };

  /**
   * Backbone View for message forms
   */
  _xtsi.UserResetView = _sxt.DialogViewBase.extend(dlgExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
