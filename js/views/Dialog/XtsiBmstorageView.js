/**
 * @file
 * A Backbone View for Bookmark Dialog
 */

(function ($, Drupal, drupalSettings, Backbone, _, _tse) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  var xtsiBookmarkExt = {
    sxtThisClass: 'sxt_slogitem.XtsiBmstorageView',
    actions: {},
    onPreInitialize: function (opts) {
      this.showNavigateBtn = true;
      this.bmsModel = _xtsi.bmstorageModel();
    },
    dialogOptions: function () {
      var options = {
        title: 'BmstorageDialog',
        dialogClass: 'xtsi-bmsorage',
        addComponent: ['wizard'],
        slogxt: {
          dialogId: 'BmstorageDialog',
          restoreState: true,
          hasItemAction: true,
          btnSubmitShow: false,
          hasBtnSubmit: false,
          tabsData: this.buildTabsData(),
        }
      };
      return options;
    },
    buildTabsData: function () {
      var tabsData = {
        targets: ['list', 'content'],
        startTarget: 'content',
        list: {
          labels: {
            dialogTitle: Drupal.t('Bookmark storage: List'),
            tabLabel: Drupal.t('List')
          },
          provider: 'sxt_slogitem',
          path: _xtsi.bmstorageViewPath('list'),
          submitCallback: $.proxy(this.executeSubmit, this)
        },
        content: {
          labels: {
            dialogTitle: Drupal.t('Bookmark storage: Content'),
            tabLabel: Drupal.t('Content')
          },
          provider: 'sxt_slogitem',
          path: _xtsi.bmstorageViewPath('content'),
          submitCallback: $.proxy(this.executeSubmit, this)
        },
      };
      return tabsData;
    },
    buildTabContentHtml: function (items, type) {
      var ulIcons = false
          , data = [], idx, opts;
      this.activeTargetItems = items;
      this.activeTargetType = type;
      for (idx in items) {
        var item = items[idx]
            , toolbar = _xtsi.getToolbarByData(item)
            , liCls = ''
            , titleCls = false;
        if (type === 'list' && toolbar) {
          liCls += ' bm-toolbar-icon';
          _sxt.isTbSysId(toolbar) && (liCls += ' tbsys-icon');
          ulIcons = true;
        }
        data.push({
          index: idx,
          liTitle: item.mainLabel,
          liDescription: item.pathLabel,
          titleCls: titleCls,
          liCls: liCls
        });
      }
      opts = {targetid: type};
      return Drupal.theme.slogxtListCheckboxes(data, opts, true, ulIcons);
    },
    onPreAjaxLoadContent: function ($content, data, tabData) {
//      slogLog('_xtsi.XtsiBmstorageView::.....onPreAjaxLoadContent....');
      var storageKey = _xtsi.bmstorageViewStorageKey(tabData.path)
          , result = JSON.parse(sessionStorage.getItem(storageKey));
      if (result && result.slogxtData) {
        var slogxtData = result.slogxtData || {}
        , runCommand = slogxtData.runCommand || 'fault'
            , callback = this._sxtCommandCallback(runCommand);
        if (callback) {
          if (this.isWizard) {
            this.wizardFinished = true;
            var pageData = this._sxtWizardPageData();
            pageData.$content = $content;
            pageData.target = data.targetid;
          }
          callback.apply(this, [$content, result]);
          return true // done
        }
      }

      return false; // not done
    },
    onAjaxResponseDone: function ($content, data) {
//      slogLog('_xtsi.XtsiBmstorageView::.....onAjaxResponseDone....');
      var tabData = this.tabsData[this.activeTargetId]
      , storageKey = _xtsi.bmstorageViewStorageKey(tabData.path);
      sessionStorage.setItem(storageKey, JSON.stringify(data));
    },
    onTabContentCreated: function () {
      // set list icons
      if (this.activeTargetType === 'list') {
        var thisView = this;
        this.$activeContent.find('.bm-toolbar-icon').each(function () {
          var $el = $(this)
              , newIdx = parseInt($el.attr('index'), 10)
              , itemData = thisView.activeTargetItems[newIdx]
              , tbView = Drupal.slogtb.getSlogtbView(_xtsi.getToolbarByData(itemData));
          if (tbView && !!tbView.tbIconDarkUrl) {
            $el.find('.li-title').css('backgroundImage', tbView.tbIconDarkUrl);
          }
        });
      }
    },
    onListItemClicked: function (e) {
      if (this.isWizard) {
//        slogLog('_xtsi.XtsiBmstorageView::.....onListItemClicked.isWizard=TRUE');
        var $li = $(e.currentTarget)
            , newIdx = parseInt($li.attr('index'), 10)
            , itemData = this.activeTargetItems[newIdx]
            , toolbar = _xtsi.getToolbarByData(itemData)
            , isContent = (itemData.type === 'content')
            , targetView = isContent
            ? _xtsi.getActiveContentView()
            : _xtsi.getSiListView(toolbar);
        targetView.show();
        targetView.isContent
            ? targetView.setListItemData(itemData.data)
            : targetView.setPathData(itemData.data);
      } else {
//        slogLog('_xtsi.XtsiBmstorageView::.....onListItemClicked.isWizard=FALSE');
        this.$btnSubmit.click();
      }
    },
    executeSubmit: function () {
//      slogLog('XtsiBmstorageView..........executeSubmit');
      var $item = this.$activeContent.find('li.radio.checked')
          , itemTitle = $item.find('.li-title').sxtGetTopText()
          , path = $item.data('path')
          , labels = (this.tabsData[this.activeTargetId] || {}).labels
          , tabData = {
            labels: {dialogTitle: labels.tabLabel + ': ' + itemTitle},
            path: path,
            provider: 'sxt_slogitem'
          };
//      $item.removeClass('checked is-current');
      this._sxtActionsWizardStart(tabData);
    },
    executeLoaded: function (args) {
      //todo::current::bmstorage::do cache
//      slogLog('XtsiBmstorageView..........executeLoaded');
      var html = this.buildTabContentHtml(args.items, args.type);
      this.$activeContent.html(html);
      this.onTabContentCreated();
      this.preventClose = true;
      this._sxtDialogContentAttach(this.$activeContent);
      this._sxtButtonShow(this.$btnNavigate, true);
      this._sxtDialogAdjust();
    }
  };

  /**
   * Backbone View for Bookmark Dialog
   */
  _xtsi.XtsiBmstorageView = _sxt.DialogViewBase.extend(xtsiBookmarkExt);

})(jQuery, Drupal, drupalSettings, Backbone, _, Drupal.theme.slogxtElement);
