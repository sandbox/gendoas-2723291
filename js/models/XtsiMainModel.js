/**
 * @file
 * A Backbone Model for SlogMain.
 */

(function ($, Backbone, Drupal, drupalSettings, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  /**
   * Backbone model ....
   */
  var xtsiMainModelExt = {
    sxtThisClass: 'sxt_slogitem.XtsiMainModel',
    initializedReady: $.noop,
    defaults: {
      listItemClicked: null,
      activeSiListItem: {}, // ??
      activeContentTarget: null, // ??
      scrollToTopTarget: null,
//      adminDialog: false, // adminDialogView object or false
      promotedData: false,
      surpriseData: false,
      surpriseCurPath: false,
      curSysListView: false
    },
    initialize: function () {
      this.isAdmin = _sxt.isAdmin();
      var promotedData = drupalSettings.sxt_slogitem.promotedData || false
          , surpriseData = drupalSettings.sxt_slogitem.surpriseData || false
          , idx, item;
      if (promotedData) {
        item = promotedData.content;
        item.href = Drupal.url(item.path);
        this.set('promotedData', promotedData);
      }
      if (surpriseData) {
        for (idx in surpriseData) {
          item = surpriseData[idx].content;
          item.href = Drupal.url(item.path);
        }
        this.set('surpriseData', surpriseData);
      }
      delete drupalSettings.sxt_slogitem.promotedData;
      delete drupalSettings.sxt_slogitem.surpriseData;

      var stateModel = _sxt.stateModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);

      this.initializedReady();
      return this;
    },
    getToolbarByTbline: function (tbline, curIdx) {
      var tblRegionData = this.getTblineRegionTbData()
          , toolbar, data;
      for (toolbar in tblRegionData) {
        data = tblRegionData[toolbar];
        if (data.tbline === tbline && data.tblineIndex === curIdx) {
          return toolbar;
        }
      }
      return false;
    },
    getTblineRegionTbData: function (toolbar) {
      if (!this.tblineRegionTbData) {
        this.tblineRegionTbData = {}
        var regionData = this.get('xtRegionData')
            , tblIdx, tblineData, tbIdx, tbView;
        for (tblIdx in regionData) {
          tblineData = regionData[tblIdx];
          if (!tblineData.isContent && tblineData.xtsiViews) {
            for (tbIdx in tblineData.xtsiViews) {
              tbView = tblineData.xtsiViews[tbIdx];
              if (!!tbView.toolbar) {
                this.tblineRegionTbData[tbView.toolbar] = {
                  tbline: tblIdx,
                  tblineIndex: tbView.tblineIndex
                };
              }
            }
          }
        }
      }

      if (!!toolbar) {
        return this.tblineRegionTbData[toolbar];
      }

      return this.tblineRegionTbData;
    },
    hasPromotedData: function () {
      return !!this.get('promotedData');
    },
    getPromotedData: function () {
      return this.get('promotedData');
    },
    hasSurpriseData: function () {
      var data = this.get('surpriseData');
      return (!!data && data.length > 1);
    },
    getSurpriseData: function () {
      return this.get('surpriseData');
    },
    getRandomSurpriseLiData: function () {
      if (this.hasSurpriseData()) {
        var data = this.get('surpriseData')
            , idx = _.random(0, data.length - 1)
            , content = data[idx].content
            , newPath = content.path
            , surpriseCurPath = this.get('surpriseCurPath')
            , changed = (newPath !== surpriseCurPath);
        while (!changed) {
          idx = _.random(0, data.length - 1);
          content = data[idx].content
          newPath = content.path;
          changed = newPath !== surpriseCurPath;
        }
        this.set('surpriseCurPath', newPath);
        return content;
      }
    },
    onChangeStateState: function (stateModel, stateState) {
      function retrieveUpdated() {
        var sids = [], sid;

        $.each(_xtsi.targetsContent, function (idx, target) {
          var cmView = _xtsi.getSiContentView(target)
              , cmData = cmView ? cmView.getListItemData() : {};
          sid = ('' + (cmData.sid || '')) || false;
          !!sid && sids.push(sid);
        });

        if (!_.isEmpty(sids)) {
          var ajax = _sxt.ajaxCreate('mainRetrieveUpdated', $('body'), true)
              , path = _xtsi.baseAjaxPath() + 'retrieve/attachdata';

          ajax.url = ajax.options.url = Drupal.url(path);
          ajax.options.data = {slogitem_ids: _.uniq(sids).join(';')};
          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = ajaxResponse;
//          slogLog('XtsiMainModel.onChangeStateState....ajaxCall');
//          ajax.xttimer.start();
          ajax.$xtelement.trigger('mainRetrieveUpdated');
        }
      }
      function ajaxResponse(ajax, response, status) {
//        ajax.xttimer.stop();
//        slogLog('XtsiMainModel.onChangeStateState....ajaxResponse(attachdata): ' //
//            + Object.keys(response.data).length);
//        slogLog('....ajaxResponse(attachdata).duration: ' + ajax.xttimer.ElapsedMilliseconds);
        !!response.data && _xtsi.mainView().updateDataContent(response.data, true);
      }

      var stateData = stateModel.get('stateData')
          , tblineModels = _xtsi.getTblineModels()
          , thisModel = _xtsi.mainModel()
          , tblData, data, tbData;
      try {
        switch (stateState) {
          case 'loaded':
            // tbline
            data = stateData.TbLineData || {};
            _.each(tblineModels, function (tblineModel) {
              tblData = data[tblineModel.id];
              if (!tblineModel.isContent && tblData) {
                tbData = thisModel.getTblineRegionTbData(tblData.toolbar);
                tblData.currentTabIndex = !!tbData ? tbData.tblineIndex || 0 : 0;
                delete tblData.toolbar;
              }
              tblineModel.setStateData(tblData);
            });

            // autorun
            if (stateData.xtsiAutorunFromPath) {
              drupalSettings.sxt_slogitem.autorun = drupalSettings.sxt_slogitem.autorun || {};
              drupalSettings.sxt_slogitem.autorun.fromPath = stateData.xtsiAutorunFromPath;
              delete stateData.xtsiAutorunFromPath;
            }

            setTimeout($.proxy(retrieveUpdated, this), 50);
            break;
          case 'writing':
            // tbline
            data = {};
            _.each(tblineModels, function (tblineModel) {
              tblData = tblineModel.getStateData();
              if (!tblineModel.isContent && tblData) {
                tblData.toolbar = thisModel.getToolbarByTbline(tblineModel.id, tblData.currentTabIndex);
                if (tblData.toolbar === '_node') {
                  tblData.toolbar = thisModel.getToolbarByTbline(tblineModel.id, 0);
                }
                delete tblData.currentTabIndex;
                data[tblineModel.id] = tblData;
              }
            });
            stateData.TbLineData = data;

            // autorun
            if (drupalSettings.sxt_slogitem.xtsiAutorunFromPath) {
              stateData.xtsiAutorunFromPath = drupalSettings.sxt_slogitem.xtsiAutorunFromPath;
              delete drupalSettings.sxt_slogitem.xtsiAutorunFromPath;
            }

            break;
        }
      } catch (e) {
        slogErr('XtsiMainModel.onChangeStateState: ' + stateState + "\n" + e.message);
      }
    }

  };

  _xtsi.XtsiMainModel = Backbone.Model.extend(xtsiMainModelExt);

})(jQuery, Backbone, Drupal, drupalSettings, _);
