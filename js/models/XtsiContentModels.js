/**
 * @file
 * More Backbone Models for Slogitem.
 */

(function (Backbone, Drupal) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem;
  
  // Backbone model for slogitem content:
  // Content, CHeader, CContent
  _xtsi.XtsiContentModel = Backbone.Model.extend({
    sxtThisClass: 'sxt_slogitem.XtsiContentModel',
    defaults: {
      listItemData: {},
      slogitemContent: {},
      specialData: false,
      tbnodeData: false,
      pagingOpen: false
    },
    getTbnodeId: function() {
      var settings = this.get('slogitemContent').settings || {};
      if (settings.node && settings.node.nodeId) {
        return settings.node.nodeId;
      }
    }
  });

  _xtsi.XtsiCHeaderModel = Backbone.Model.extend({
    sxtThisClass: 'sxt_slogitem.XtsiCHeaderModel',
    defaults: {
      headerData: {},
      dropbuttonIsOpen: false
    }
  });

  _xtsi.XtsiCContentModel = Backbone.Model.extend({
    sxtThisClass: 'sxt_slogitem.XtsiCContentModel',
    defaults: {
      contentData: {}
    }
  });

})(Backbone, Drupal);
