/**
 * @file
 * More Backbone Models for Slogitem.
 */

(function (Backbone, Drupal) {

  "use strict";

  // Backbone model for slogitem list:
  // List only, not splitted in header and content !!
  Drupal.sxt_slogitem.XtsiListModel = Backbone.Model.extend({
    sxtThisClass: 'sxt_slogitem.XtsiListModel',
    defaults: {
      slogtbView: null,
      pathData: null,
      slogitemList: null
    }
  });

})(Backbone, Drupal);
