/**
 * @file
 * A Backbone Model for ???.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem;

  /**
   * Backbone model for .
   */
  var xtsiBmsModelExt = {
    sxtThisClass: 'sxt_slogitem.XtsiBmstorageModel',
    initialize: function () {
      this.mainModel = _xtsi.mainModel();
      var tbmainModel = Drupal.slogtb.mainModel();
      this.listenTo(tbmainModel, 'change:editedTbMenu', this.onChangeEditedTbMenu);
      this.listenTo(this.mainModel, 'change:editedSlogitem', this.onChangeEditedSlogitem);
      this.listenTo(tbmainModel, 'change:movedTbMenu', this.onChangeMovedTbMenu);
      this.listenTo(this.mainModel, 'change:movedSlogitems', this.onChangeMovedSlogitems);
      this.listenTo(this.mainModel, 'change:finishedActionsWizard', this.onChangeFinishedActionsWizard);
      return this;
    },
    onChangeFinishedActionsWizard: function (mm, data) {
      data = data || {};
      if (data.action && data.action === 'bmstorage:save') {
        var bms = drupalSettings.sxt_slogitem.bmstorage_tids || {}
        , bmtype = data.type
            , menuTid = (bms[bmtype] || [])[data.menuTid]
            , rootRemove = false;
        if (!menuTid) {
          rootRemove = true;
        } else {
          try {
            menuTid = parseInt(menuTid, 10);
            var path = _xtsi.bmstorageViewPath(bmtype)
                , storageKey = _xtsi.bmstorageViewStorageKey(path)
                , result = JSON.parse(sessionStorage.getItem(storageKey)) || {}
            , items = result.slogxtData ? result.slogxtData.items || [] : []
                , idx, item;
            for (idx in items) {
              item = items[idx];
              if (item.entityid && item.entityid === menuTid) {
                rootRemove = (item.liTitle != data.menuTitle) || (item.tids != data.ids);
                if (item.tids != data.ids) {
                  path = _xtsi.bmstorageViewPath(bmtype, menuTid);
                  storageKey = _xtsi.bmstorageViewStorageKey(path);
                  sessionStorage.removeItem(storageKey);
                }
                break;
              }
            }
          } catch (e) {
          }
        }

        if (rootRemove) {
          var path = _xtsi.bmstorageViewPath(bmtype)
              , storageKey = _xtsi.bmstorageViewStorageKey(path);
          sessionStorage.removeItem(storageKey);
        }
      }
    },
    onChangeEditedSlogitem: function (mm, data) {
      var bmTids = (drupalSettings.sxt_slogitem.bmstorage_tids || {}).content || []
          , changed_sid = parseInt(data.sid, 10);
      _.each(bmTids, function (tid) {
        try {
          var path = _xtsi.bmstorageViewPath('content', tid)
              , storageKey = _xtsi.bmstorageViewStorageKey(path)
              , result = JSON.parse(sessionStorage.getItem(storageKey)) || {}
          , sids = result.slogxtData ? result.slogxtData.onWizardFinished.args.ids || [] : [];
          _.contains(sids, changed_sid) && sessionStorage.removeItem(storageKey);
        } catch (e) {
        }
      });
    },
    onChangeEditedTbMenu: function (tbmm, data) {
      var bms = drupalSettings.sxt_slogitem.bmstorage_tids || {}
      , bmtype = (data.toolbartab === 'bmstorage_content') ? 'content' : 'list'
          , bmTids = bms[bmtype] || []
          , changed_tid = parseInt(data.tid, 10);
      if (!!bmTids[changed_tid]) {
        var path = _xtsi.bmstorageViewPath(bmtype)
            , storageKey = _xtsi.bmstorageViewStorageKey(path);
        sessionStorage.removeItem(storageKey);
      } else if (bmtype === 'list') {
        _.each(bmTids, function (tid) {
          try {
            var path = _xtsi.bmstorageViewPath('list', tid)
                , storageKey = _xtsi.bmstorageViewStorageKey(path)
                , result = JSON.parse(sessionStorage.getItem(storageKey)) || {}
            , tids = result.slogxtData.onWizardFinished.args.ids || [];
            _.contains(tids, changed_tid) && sessionStorage.removeItem(storageKey);
          } catch (e) {
          }
        });
      }
    },
    onChangeMovedSlogitems: function (mm, data) {
      var bmTids = (drupalSettings.sxt_slogitem.bmstorage_tids || {}).content || []
          , moved_sids = Object.keys(data) || [];
      _.each(bmTids, function (tid) {
        try {
          var path = _xtsi.bmstorageViewPath('content', tid)
              , storageKey = _xtsi.bmstorageViewStorageKey(path)
              , result = JSON.parse(sessionStorage.getItem(storageKey)) || {}
          , sids = result.slogxtData ? result.slogxtData.onWizardFinished.args.ids || [] : []
              , idx, sid;
          for (idx in moved_sids) {
            sid = parseInt(moved_sids[idx], 10);
            if (_.contains(sids, sid)) {
              sessionStorage.removeItem(storageKey);
              break;
            }
          }
        } catch (e) {
        }
      });
    },
    onChangeMovedTbMenu: function (tbmm, data) {
//      slogLog('XtsiBmstorageModel: ................onChangeMovedTbMenu');
      var bmTids = (drupalSettings.sxt_slogitem.bmstorage_tids || {}).list || []
          , moved_tids = data.moved_tids
      _.each(bmTids, function (tid) {
        try {
          var path = _xtsi.bmstorageViewPath('list', tid)
              , storageKey = _xtsi.bmstorageViewStorageKey(path)
              , result = JSON.parse(sessionStorage.getItem(storageKey)) || {}
          , tids = result.slogxtData.onWizardFinished.args.ids || []
              , idx, tid;
          for (idx in moved_tids) {
            tid = parseInt(moved_tids[idx], 10);
            if (_.contains(tids, tid)) {
              sessionStorage.removeItem(storageKey);
              break;
            }
          }
        } catch (e) {
        }
      });
    }

  };

  /**
   * Backbone View for ....
   */
  _xtsi.XtsiBmstorageModel = Backbone.Model.extend(xtsiBmsModelExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
