/**
 * @file
 * A Backbone Model for ???.
 */

(function ($, Backbone, Drupal, drupalSettings, _, undefined) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  /**
   * Backbone model for .
   */
  var xtsiHistoryModelExt = {
    sxtThisClass: 'sxt_slogitem.XtsiHistoryModel',
    defaults: {
      xtRegionData: null,
      checkboxCount: {}
    },
    initialize: function (options) {

      this.mainModel = _xtsi.mainModel();
      this.tDataById = {};
      this.maxItems = _xtsi.getMaxItems('history') || 30;
      this.clearData();

      var stateModel = _sxt.stateModel()
          , tbmainModel = Drupal.slogtb.mainModel()
          , xtsiMainModel = _xtsi.mainModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);
      this.listenTo(tbmainModel, 'change:editedTbMenu', this.onChangeEditedTbMenu);
      this.listenTo(tbmainModel, 'change:editedTbTab', this.onChangeEditedTbTab);
      this.listenTo(tbmainModel, 'change:movedTbMenu', this.onChangeMovedTbMenu);
      this.listenTo(xtsiMainModel, 'change:editedNodeContent', this.onChangeEditedNode);
      this.listenTo(xtsiMainModel, 'change:editedSlogitem', this.onChangeEditedSlogitem);
      this.listenTo(xtsiMainModel, 'change:movedSlogitems', this.onChangeMovedSlogitems);

      return this;
    },
    clearData: function () {
      this.data = {};
    },
    updateDataContent: function (sids) {
      if (!_.isEmpty(sids)) {
        var hModel = this;
        $.each(_xtsi.targetsContent, function (idx, target) {
          var items = hModel.getTargetItemsById(target);
          if (!_.isEmpty(items)) {
            _.each(items, function (item) {
              if (!!sids[item.data.sid]) {
                var cData = sids[item.data.sid].content;
                item.data = cData;
                !!cData.mainLabel && (item.mainLabel = cData.mainLabel);
                item.path = cData.path;
                item.pathLabel = _xtsi.contentPathLabel(cData.pathLabel, cData.listLabel, cData.label);
              }
            });
          }
        });
      }
    },
    onChangeMovedSlogitems: function (xtsimm, done_sids) {
      this.updateDataContent(done_sids);
    },
    onChangeEditedNode: function (xtsimm, data) {
      if (data.labelOld !== data.labelNew) {
        // xtsi-content
        var hModel = this
            , testByEntity = data.etype + '.' + data.eid;
        $.each(_xtsi.targetsContent, function (idx, target) {
          var items = hModel.getTargetItemsById(target);
          if (!_.isEmpty(items)) {
            _.each(items, function (item) {
              var byEntity = (item.data || {}).byEntity || false;
              (byEntity === testByEntity) && (item.mainLabel = data.labelNew);
            });
          }
        });
      }
    },
    onChangeEditedSlogitem: function (xtsimm, data) {
      if (data.labelOld !== data.labelNew) {
        // xtsi-content
        var hModel = this;
        $.each(_xtsi.targetsContent, function (idx, target) {
          var items = hModel.getTargetItemsById(target);
          if (!_.isEmpty(items)) {
            _.each(items, function (item) {
              if (item.data.sid === data.sid) {
                item.pathLabel = item.pathLabel.replace(data.labelOld, data.labelNew);
                item.data.label = item.data.label.replace(data.labelOld, data.labelNew);
              }
            });
          }
        });
      }
    },
    setCurrentIdx: function (path, targetId) {
      var hData = this.getDataById(targetId)
          , idx, item;
      if (hData) {
        hData.currentIdx = -1;
        if (!!path) {
          for (idx in hData.items) {
            item = hData.items[idx];
            if (item.path == path) {
              hData.currentIdx = parseInt(idx, 10);
              break;
            }
          }
        }
      } else if (!!path) {
        var data = '(' + targetId + '/' + path + ')';
        slogErr('XtsiHistoryModel.setCurrentIdx: no hData ' + data);
      }
    },
    getAllData: function () {
      this.data = this.data || {};
      return this.data;
    },
    getAllTargetData: function () {
      return this.tDataById;
    },
    getTargetDataById: function (targetId) {
      return this.tDataById[targetId];
    },
    getTargetItemsById: function (targetId) {
      this.data = this.data || {};
      var info = this.tDataById[targetId] || {}
      , lData = this.data[(info.tbline || '')] || {}
      , items = (lData[info.index] || {}).items || {};
      return items;
    },
    getDataTbline: function (tbline) {
      this.data = this.data || {};
      this.data[tbline] = this.data[tbline] || {};
      return this.data[tbline];
    },
    setDataTbline: function (data, tbline) {
      this.data = this.data || {};
      this.data[tbline] = data;
    },
    getData: function (tData) {
      var tblData = this.getDataTbline(tData.tbline);
      if (!tblData[tData.index]) {
        this.tDataById[tData.id] = tData;
        tblData[tData.index] = {
          id: tData.id,
          tbline: tData.tbline,
          isContent: tData.isContent || false,
          currentIdx: 0,
          length: 0,
          items: [],
          class: tData.class
        };
      }
      return tblData[tData.index];
    },
    getIdxByPath: function (targetId, path) {
      var items = this.getDataById(targetId).items
          , item, idx;
      for (idx in items) {
        item = items[idx];
        if (item.path === path) {
          return idx;
        }
      }

      return -1;
    },
    getIdsByTargetid: function (targetId, selected) {
      var items = this.getDataById(targetId).items
          , result = []
          , item, id;
      _.each(selected, function (idx) {
        item = items[idx];
        id = (item.type === 'list') ? item.data.tid : item.data.sid;
        !!id && result.push(id);
      });
      return result;
    },
    getDataById: function (targetId, tData) {
      tData && (this.tDataById[targetId] = tData);
      if (this.tDataById[targetId]) {
        return this.getData(this.tDataById[targetId]);
      }
    },
    hasData: function (tData) {
//      var data = this.getDataTbline(tData.tbline);
//      (tData.index !== undefined) && (data = data[tData.index] || []);
      var data = this.getData(tData);
      return !_.isEmpty(data);
    },
    hasRecord: function (path, tData) {
      if (path) {
        var items = _sxt.sureArray(this.getData(tData).items)
            , result = items.filter(function (item) {
              return (item.path === path);
            });
        return (result && result.length > 0);
      }
      return false;
    },
    doSyncHistory: function (data, tData) {
      if (this.hasRecord(data.path, tData)) {
        this.setCurrentIdx(data.path, tData.id);
      } else {
        this.setCurrentItem(data, tData);
      }
    },
    setCurrentItem: function (data, tData) {
      if ((_sxt.isTbSysId(tData.id) && !!data.pdRole) //
          || (!!data.pdRole && data.pdRole !== _sxt.getUserDefaultRole())) {
        return this.getData(tData);
      }

      if (!!data.mainLabel && !!data.pathLabel) {
        var maxLen = this.maxItems
            , hData = this.getData(tData);
        if (!hData.moving) {
          hData.items = $.grep(hData.items, function (v) {
            var vPath = v.data.path ? v.data.path : v.data
                , dPath = data.data.path ? data.data.path : data.data;
            return (_.isString(vPath) && _.isString(dPath) && (vPath !== dPath));
          });
          hData.items.unshift(data);
          (hData.items.length > maxLen) && (hData.items.length = maxLen);
          hData.currentIdx = 0;
          hData.length = hData.items.length;
        }
        hData.moving = false;
        tData.tblineView.activateHistoryButtons(tData, hData);
      }

      return this.getData(tData);
    },
    removeCurrentItem: function (targetView, tData) {
      var hData = this.getData(tData)
          , items = hData.items
          , rmIdx = hData.currentIdx
          , goBack = (hData.currentIdx < hData.length - 1)
          , goNext = !goBack && (hData.currentIdx > 0);
      if (items[rmIdx] && (goBack || goNext)) {
        goBack && this.goBack(targetView, tData);
        goNext && this.goNext(targetView, tData);
      }
      delete items[rmIdx];
      hData.items = _.compact(items);
      hData.length = hData.items.length;
      hData.currentIdx = (rmIdx < hData.length) ? rmIdx : rmIdx - 1;
      targetView.activateHistoryButtons();
      (hData.length < 1) && tData.view.setSlogitemListData({noContent: true});
    },
    removeMultiple: function (targetid, indizes, removeCurrent) {
      var hData = this.getDataById(targetid)
          , tData = this.getTargetDataById(targetid);
      if (!hData || !tData) {
        return;
      }

      var newCurrent = hData.currentIdx
          , newPath = false
          , preserveCurrent = !removeCurrent && (_.indexOf(indizes, hData.currentIdx) >= 0)
          , prevIdx = false
          , idx, found = false;

      hData.items = _sxt.sureArray(hData.items);
      if (preserveCurrent) {
        newCurrent = -1;
        for (idx in hData.items) {
          idx = parseInt(idx, 10);
          (idx === hData.currentIdx) && (found = true);
          if (_.indexOf(indizes, idx) < 0) {
            if (found) {
              newCurrent = idx;
              break;
            } else {
              prevIdx = idx;
            }
          }
        }
        (newCurrent < 0) && (newCurrent = prevIdx);
        if (newCurrent < 0) {
          indizes = _.without(indizes, hData.currentIdx);
          newCurrent = hData.currentIdx;
        }
      }

      (newCurrent >= 0) && hData.items[newCurrent] && (newPath = hData.items[newCurrent].path);
      hData.items = hData.items.filter(function (value, index) {
        return (_.indexOf(indizes, index) < 0);
      });
      hData.length = hData.items.length;
      if (hData.length) {
        this.setCurrentIdx(newPath, targetid);
        tData.tblineView.activateHistoryButtons(tData, hData);
      } else if (!hData.isContent && tData.view) {
        tData.view.setSlogitemListData({noContent: true});
      }
    },
    goBack: function (targetView, tData) {
      var hData = this.getData(tData);
      if (hData && hData.currentIdx < hData.length - 1) {
        this.goTo(targetView, hData.currentIdx + 1, tData);
      }
    },
    goNext: function (targetView, tData) {
      var hData = this.getData(tData);
      if (hData && hData.currentIdx > 0) {
        this.goTo(targetView, hData.currentIdx - 1, tData);
      }
    },
    goToCurrent: function (targetView, tData) {
//      slogLog('XtsiHistoryModel...goToCurrent: ' + tData.index);
      var hData = this.getData(tData)
          , index = hData ? hData.currentIdx : 0;
      return this.goTo(targetView, index, tData);
    },
    goTo: function (targetView, index, tData) {
//      slogLog('XtsiHistoryModel.....goTo: ' + index);
      index = parseInt(index, 10);
      var hData = this.getData(tData)
          , itemData = (hData && hData.items) ? hData.items[index] || false : false;
      if (itemData && itemData.data) {
        if (targetView.isContent) {
          targetView.setDepositPathLabel({
            sid: itemData.data.sid,
            cPathLabel: itemData.pathLabel
          });
          targetView.hview.bookmarkRefreshTO();
        }
        hData.currentIdx = index;
        hData.moving = true;
        targetView.isContent
            ? targetView.setListItemData(itemData.data)
            : targetView.setPathData(itemData.data);
        _sxt.isOpenModalDialog() && (hData.moving = false);
        return true;
      }
      return false;
    },
    onChangeEditedTbMenu: function (tbmm, data) {
      // 1. xtsi-list
      var items = this.getTargetItemsById(data.toolbar)
          , hModel = this;
      if (!_.isEmpty(items)) {
        var foundItem = _.find(items, function (item) {
          return (item.data.tid === data.tid);
        });
        foundItem && (foundItem.mainLabel = data.name);
      }

      // 2. xtsi-content
      $.each(_xtsi.targetsContent, function (idx, target) {
        var items = hModel.getTargetItemsById(target);
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            if (item.data.tid === data.tid) {
              var listLabel = item.listLabel || item.data.listLabel
                  , pl = item.data.pathLabel + '/'
                  , re = new RegExp(pl + listLabel);
              item.pathLabel = item.pathLabel.replace(re, pl + data.name);
              item.data.listLabel = item.listLabel = data.name;
            }
          });
        }
      });
    },
    onChangeEditedTbTab: function (tbmm, data) {
      // 1. xtsi-list
      var defaultRole = _sxt.getUserDefaultRole()
          , toolbar = data.toolbar
          , toolbartab = data.toolbartab
          , pathLabel = data.pathLabel
          , items = this.getTargetItemsById(data.toolbar)
          , hModel = this;
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          var id = item.data
              , okTb = !!id.toolbartab && id.toolbartab === toolbartab && id.toolbar === toolbar
              , okRole = item.pdRole ? item.pdRole === defaultRole : true;
          okTb && okRole && (item.pathLabel = pathLabel);
        });
      }

      // 2. xtsi-content
      $.each(_xtsi.targetsContent, function (idx, target) {
        var items = hModel.getTargetItemsById(target);
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            var idata = item.data
                , pd = idata.tbPathData || {}
            , okTb = !!pd.toolbartab && pd.toolbartab === toolbartab && pd.toolbar === toolbar
                , okRole = item.pdRole ? item.pdRole === defaultRole : true;
            if (okTb && okRole) {
              var ll = '/' + idata.listLabel
                  , parts = item.pathLabel.split(ll);
              item.pathLabel = pathLabel + ll + (parts[1] || '');
              idata.pathLabel = pathLabel;
            }
          });
        }
      });
    },
    onChangeMovedTbMenu: function (tbmm, data) {
//      slogLog('XtsiHistoryModel: ................onChangeMovedTbMenu');
      // 1. xtsi-list
      var newPathLabel = data.tPathLabel
          , newToolbar = data.tToolbar
          , newToolbartab = data.tToolbartab
          , newLView = _xtsi.getSiListView(newToolbar) || false
          , isSameTb = (data.sToolbar === data.tToolbar)
          , moved_tids = data.moved_tids
          , donSData = data.sData || {}
      , menuItemPath = donSData.menuItemPath
          , tbPathData = {
            path: menuItemPath,
            tid: donSData.entityId,
            toolbar: newToolbar,
            toolbartab: newToolbartab
          }
      , sourceLView = _xtsi.getSiListView(data.sToolbar)
          , sourceTData = sourceLView.getHistoryTarget()
          , sourceData = this.getData(sourceTData)
          , mItems = []
          , mIndizes = []
          , hModel = this;

      if (menuItemPath) {
        sourceLView.show();
        var idx = this.getIdxByPath(data.sToolbar, menuItemPath);
        if (idx >= 0) {
          var hData = this.getDataById(data.sToolbar) || {};
          this.removeMultiple(data.sToolbar, [idx], true);
          (hData.currentIdx < 0) && !isSameTb && sourceLView.ensureContent();
        }
      }

      if (newLView) {
        var newTData = newLView.getHistoryTarget()
            , newData = this.getData(newTData)
            , newTbline = newTData.tbline
            , newTblineIdx = newTData.index;

        newLView.setPathDataAndShow(tbPathData);
        newLView.historySetCurrentItemTO(true, true);

        if (!_.isEmpty(sourceData.items)) {
          _.each(sourceData.items, function (item, idx) {
            var idata = item.data;
            if (_.contains(moved_tids, idata.tid)) {
              item.pathLabel = newPathLabel;
              item.tbline = newTbline;
              item.tblineIndex = newTblineIdx;
              idata.toolbar = newToolbar;
              idata.toolbartab = newToolbartab;
              if (!isSameTb) {
                mItems.push(item);
                mIndizes.push(idx);
              }
            }
          });

          if (!_.isEmpty(mItems)) {
            newData.items = mItems.concat(newData.items);
            newData.length = newData.items.length;
            this.removeMultiple(data.sToolbar, mIndizes, true);
          }
        }
      } else if (!isSameTb && !_.isEmpty(sourceData.items)) {
        _.each(sourceData.items, function (item, idx) {
          var idata = item.data;
          if (_.contains(moved_tids, idata.tid)) {
            mIndizes.push(idx);
          }
        });
        this.removeMultiple(data.sToolbar, mIndizes, true);
      }


      // 2. xtsi-content
      $.each(_xtsi.targetsContent, function (idx, target) {
        var items = hModel.getTargetItemsById(target);
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            var id = item.data
                , pd = id.tbPathData || {};
            if (_.contains(moved_tids, pd.tid)) {
              var ll = '/' + id.listLabel
                  , re = new RegExp(id.pathLabel + ll);
              item.pathLabel = item.pathLabel.replace(re, newPathLabel + ll);
              id.pathLabel = newPathLabel;
              id.toolbar = newToolbar;
              pd.toolbar = newToolbar;
              pd.toolbartab = newToolbartab;
            }
          });
        }
      });
    },
    onChangeStateState: function (stateModel, stateState) {
      function retrieveUpdated() {
        var hModel = this
            , sids = [], sid;
        $.each(_xtsi.targetsContent, function (idx, target) {
          var items = hModel.getTargetItemsById(target);
          if (!_.isEmpty(items)) {
            _.each(items, function (item) {
              sid = ('' + item.data.sid) || false;
              !!sid && sids.push(sid);
            });
          }
        });

        if (!_.isEmpty(sids)) {
          var ajax = _sxt.ajaxCreate('historyRetrieveUpdated', $('body'), true)
              , path = _xtsi.baseAjaxPath() + 'retrieve/attachdata';

          ajax.url = ajax.options.url = Drupal.url(path);
          ajax.options.data = {slogitem_ids: _.uniq(sids).join(';')};
          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = ajaxResponse;
//          slogLog('XtsiHistoryModel.onChangeStateState....ajaxCall');
//          ajax.xttimer.start();
          ajax.$xtelement.trigger('historyRetrieveUpdated');
        }
      }
      function ajaxResponse(ajax, response, status) {
//        ajax.xttimer.stop();
//        slogLog('XtsiHistoryModel.onChangeStateState....ajaxResponse(attachdata): ' //
//            + Object.keys(response.data).length);
//        slogLog('....ajaxResponse(attachdata).duration: ' + ajax.xttimer.ElapsedMilliseconds);
        this.updateDataContent(response.data);
      }

      var stateData = stateModel.get('stateData');
      try {
        switch (stateState) {
          case 'loaded':
            this.data = this.statePrepareData(stateData.History, stateState);
            this.fixTargetDataById();
            setTimeout($.proxy(retrieveUpdated, this), 50);
            break;
          case 'writing':
            this.data = this.data || {};
            stateData.History = this.statePrepareData(this.data, stateState);
            break;
        }
      } catch (e) {
        slogErr('XtsiHistoryModel.onChangeStateState: ' + stateState + "\n" + e.message);
        this.clearData();
      }
    },
    statePrepareData: function (stateData, stateState) {
      var defaultRole = _sxt.getUserDefaultRole()
          , tblRegionData = this.mainModel.getTblineRegionTbData()
          , role = tblRegionData.role || {}
      , tbl = role.tbline
          , tblIdx = role.tblineIndex
          , tblData = tbl ? stateData[tbl] || {} : {}
      , roleData = tblIdx ? tblData[tblIdx] || false : false;
      if (roleData && roleData.id === 'role') {
        roleData.roleItems = roleData.roleItems || {};
        roleData.roleCurrentIdx = roleData.roleCurrentIdx || {};
        if (stateState === 'writing') {
          var newItems = []
              , idx, item;
          for (idx in roleData.items) {
            item = roleData.items[idx];
            if (item.pdRole && item.pdRole === defaultRole) {
              newItems.push(item);
            }
          }
          roleData.roleItems[defaultRole] = newItems;
          roleData.roleCurrentIdx[defaultRole] = roleData.currentIdx;
          roleData.items = [];
        } else if (stateState === 'loaded') {
          var roleItems = roleData.roleItems || {}
          , roleCurrentIdx = roleData.roleCurrentIdx || {};
          roleData.items = roleItems[defaultRole] || [];
          roleData.length = roleData.items.length;
          roleData.currentIdx = roleCurrentIdx[defaultRole] || (roleData.items.length ? 0 : -1);
        }
      }
      return stateData;
    },
    fixTargetDataById: function () {
      var allData = this.getAllData()
          , tblineViews = _xtsi.getTblineViews()
          , tblIdx, tblData, tblineView, idx, hData, contentView, toolbar, tData;
      for (tblIdx in allData) {
        tblineView = tblineViews[tblIdx];
        if (tblineView.isContent) {
          tblData = allData[tblIdx];
          for (idx in tblData) {
            hData = tblData[idx];
            if (hData && hData.id && !this.tDataById[hData.id]) {
              contentView = tblineView.getContentView(hData.id);
              this.tDataById[hData.id] = contentView.getHistoryTarget();
            }
          }
        } else {
          var tblRegionData = this.mainModel.getTblineRegionTbData();
          for (toolbar in tblRegionData) {
            tblData = tblRegionData[toolbar];
            if (tblData.tbline === tblIdx) {
              contentView = tblineView.getContentView(toolbar);
              tData = contentView.getHistoryTarget();
              hData = this.getDataById(toolbar, tData);
              if (hData && hData.id !== toolbar) {
                hData.id = toolbar;
                hData.class = 'list-' + toolbar;
              }
            }
          }
        }
      }
    }

  };

  /**
   * Backbone View for ....
   */
  _xtsi.XtsiHistoryModel = Backbone.Model.extend(xtsiHistoryModelExt);

})(jQuery, Backbone, Drupal, drupalSettings, _);
