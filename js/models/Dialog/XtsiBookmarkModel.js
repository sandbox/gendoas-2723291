/**
 * @file
 * A Backbone Model for ???.
 */

(function ($, Drupal, drupalSettings, Backbone, _) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;

  /**
   * Backbone model for .
   */
  var xtsiBookmarkModelExt = {
    sxtThisClass: 'sxt_slogitem.XtsiBookmarkModel',
    defaults: {
      lastAction: {},
      checkboxCount: {}
    },
    initialize: function () {
      this.mainModel = _xtsi.mainModel();
      this.maxItems = _xtsi.getMaxItems('bookmark') || 40;
      this.clearData();

      var stateModel = _sxt.stateModel()
          , tbmainModel = Drupal.slogtb.mainModel();
      this.listenTo(stateModel, 'change:stateState', this.onChangeStateState);
      this.listenTo(tbmainModel, 'change:editedTbMenu', this.onChangeEditedTbMenu);
      this.listenTo(tbmainModel, 'change:editedTbTab', this.onChangeEditedTbTab);
      this.listenTo(tbmainModel, 'change:movedTbMenu', this.onChangeMovedTbMenu);
      this.listenTo(this.mainModel, 'change:editedNodeContent', this.onChangeEditedNode);
      this.listenTo(this.mainModel, 'change:editedSlogitem', this.onChangeEditedSlogitem);
      this.listenTo(this.mainModel, 'change:movedSlogitems', this.onChangeMovedSlogitems);
      return this;
    },
    clearData: function () {
      this.data = {content: {isContent: true, items: []}, list: {items: []}};
    },
    updateDataContent: function (sids) {
      if (!_.isEmpty(sids)) {
        var items = (this.data.content || {}).items || {};
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            if (!!sids[item.data.sid]) {
              var cData = sids[item.data.sid].content;
              item.data = cData;
              !!cData.mainLabel && (item.mainLabel = cData.mainLabel);
              item.path = cData.path;
              item.pathLabel = _xtsi.contentPathLabel(cData.pathLabel, cData.listLabel, cData.label);
            }
          });
        }
      }
    },
    onChangeMovedSlogitems: function (mm, done_sids) {
      this.updateDataContent(done_sids);
    },
    onChangeEditedNode: function (xtsimm, data) {
      if (data.labelOld !== data.labelNew) {
        // xtsi-content
        var items = (this.data.content || {}).items || {}
        , testByEntity = data.etype + '.' + data.eid;
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            var byEntity = (item.data || {}).byEntity || false;
            (byEntity === testByEntity) && (item.mainLabel = data.labelNew);
          });
        }
      }
    },
    onChangeEditedSlogitem: function (xtsimm, data) {
      if (data.labelOld !== data.labelNew) {
        // xtsi-content
        var items = (this.data.content || {}).items || {};
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            if (item.data.sid === data.sid && !!item.pathLabel) {
              item.pathLabel = item.pathLabel.replace(data.labelOld, data.labelNew);
            }
          });
        }
      }
    },
    setCurrentIdx: function (path, type) {
      var bmData = this.getData(type)
          , idx, item;
      if (bmData) {
        bmData.currentIdx = -1;
        if (!!path) {
          for (idx in bmData.items) {
            item = bmData.items[idx];
            if (item.path == path) {
              bmData.currentIdx = parseInt(idx, 10);
              break;
            }
          }
        }
      } else if (!!path) {
        var data = '(' + type + '/' + path + ')';
        slogErr('XtsiBookmarkModel.setCurrentIdx: no bmData ' + data);
      }
    },
    getData: function (type) {
      return this.data[type];
    },
    getItemsByIndices: function (type, idxs) {
      var items = this.getData(type).items
          , result = []
          , key, idx;
      for (key in idxs) {
        idx = idxs[key]
        !!items[idx] && result.push(items[idx]);
      }
      return result;
    },
    getItemsUniqMerge: function (first, second) {
      if (!_.isEmpty(second)) {
        if (!first) {
          return second;
        }
        var test = []
            , idx, path;
        for (idx in first) {
          path = first[idx].path;
          test[path] = true;
        }
        for (idx in second) {
          path = second[idx].path;
          if (test[path]) {
            delete second[idx];
          }
        }
        return _.compact(first.concat(second));
      }
      return (first || []);
    },
    getIdsByTargetid: function (targetId, selected) {
      var items = this.getData(targetId).items
          , result = []
          , item, data, id;
      _.each(selected, function (idx) {
        item = items[idx];
        id = (item.type === 'list') ? item.data.tid : item.data.sid;
        !!id && result.push(id)
      });
      return result;
    },
    totalBookmarks: function () {
      return (this.data.content.items.length + this.data.list.items.length);
    },
    isComplete: function () {
      return (this.totalBookmarks() >= this.maxItems);
    },
    hasBookmarks: function (type) {
      var length = type ? this.data[type].items.length : this.totalBookmarks();
      return (length > 0);
    },
    hasBookmark: function (path, type) {
      this.data[type].items = _sxt.sureArray(this.data[type].items);
      var result = this.data[type].items.filter(function (item) {
        return (item.path === path);
      });
      return (result.length > 0);
    },
    addBookmark: function (data) {
//      slogLog('XtsiBookmarkModel.addBookmark');
      if (data.data && data.data.path) {
        var type = data.type;
        this.removeBookmark(data.path, type, true);
        this.data[type].items.unshift(data);
        if (this.totalBookmarks() > this.maxItems) {
          this.data[type].items.length = this.data[type].items.length - 1;
        }
        this.set('lastAction', {action: 'add', type: type, path: data.path});
      } else {
        slogErr('XtsiBookmarkModel.addBookmark: no toolbar: ' + data.path || '?path');
      }
    },
    removeBookmark: function (path, type, silent) {
//      slogLog('XtsiBookmarkModel.removeBookmark');
      this.data[type].items = _sxt.sureArray(this.data[type].items);
      this.data[type].items = this.data[type].items.filter(function (item) {
        return (item.data.path != path);
      });
      if (!silent) {
        this.set('lastAction', {action: 'remove', type: type, path: path});
      }
    },
    removeMultiple: function (type, indizes) {
      var bmData = this.getData(type);
      (_.indexOf(indizes, bmData.currentIdx) >= 0) && (bmData.currentIdx = -1);
      var removed = bmData.items.filter(function (item, index) {
        return (_.indexOf(indizes, index) >= 0);
      });
      bmData.items = bmData.items.filter(function (item, index) {
        return (_.indexOf(indizes, index) < 0);
      });
      this.set('lastAction', {action: 'removeMultiple', type: type, removed: removed});
    },
    goTo: function (targetView, itemData) {
      if (targetView.isContent) {
        targetView.setDepositPathLabel({
          sid: itemData.data.sid,
          cPathLabel: itemData.pathLabel
        });
        targetView.hview.bookmarkRefreshTO();
      }
      targetView.isContent
          ? targetView.setListItemData(itemData.data)
          : targetView.setPathData(itemData.data);
    },
    onChangeEditedTbMenu: function (tbmm, data) {
      // 1. xtsi-list
      var items = (this.data.list || {}).items || {};
      if (!_.isEmpty(items)) {
        var foundItem = _.find(items, function (item) {
          return (item.data.tid === data.tid);
        });
        foundItem && (foundItem.mainLabel = data.name);
      }

      // 2. xtsi-content
      items = (this.data.content || {}).items || {};
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          if (item.data.tid === data.tid) {
            var listLabel = item.listLabel || item.data.listLabel
                , pl = item.data.pathLabel + '/'
                , re = new RegExp(pl + listLabel);
            item.pathLabel = item.pathLabel.replace(re, pl + data.name);
            item.data.listLabel = item.listLabel = data.name;
          }
        });
      }
    },
    onChangeEditedTbTab: function (tbmm, data) {
      // 1. xtsi-list
      var defaultRole = _sxt.getUserDefaultRole()
          , toolbar = data.toolbar
          , toolbartab = data.toolbartab
          , pathLabel = data.pathLabel
          , items = (this.data.list || {}).items || {};
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          var id = item.data
              , okTb = !!id.toolbartab && id.toolbartab === toolbartab && id.toolbar === toolbar
              , okRole = item.pdRole ? item.pdRole === defaultRole : true;
          okTb && okRole && (item.pathLabel = pathLabel);
        });
      }

      // 2. xtsi-content
      items = (this.data.content || {}).items || {};
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          var idata = item.data
              , pd = idata.tbPathData || {}
          , okTb = !!pd.toolbartab && pd.toolbartab === toolbartab && pd.toolbar === toolbar
              , okRole = item.pdRole ? item.pdRole === defaultRole : true;
          if (okTb && okRole) {
            var ll = '/' + idata.listLabel
                , parts = item.pathLabel.split(ll);
            item.pathLabel = pathLabel + ll + (parts[1] || '');
            idata.pathLabel = pathLabel;
          }
        });
      }
    },
    onChangeMovedTbMenu: function (tbmm, data) {
//      slogLog('XtsiBookmarkModel: ................onChangeMovedTbMenu');
      // 1. xtsi-list
      var items = (this.data.list || {}).items || {}
      , moved_tids = data.moved_tids
          , newPathLabel = data.tPathLabel
          , newToolbar = data.tToolbar
          , newToolbartab = data.tToolbartab;
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          var id = item.data;
          if (_.contains(moved_tids, id.tid)) {
            item.pathLabel = newPathLabel;
            id.toolbar = newToolbar;
            id.toolbartab = newToolbartab;
          }
        });
      }

      // 2. xtsi-content
      items = (this.data.content || {}).items || {};
      if (!_.isEmpty(items)) {
        _.each(items, function (item) {
          var id = item.data
              , pd = id.tbPathData || {};
          if (_.contains(moved_tids, id.tid)) {
            var ll = '/' + id.listLabel
                , re = new RegExp(id.pathLabel + ll);
            item.pathLabel = item.pathLabel.replace(re, newPathLabel + ll);
            id.pathLabel = newPathLabel;
            id.toolbar = newToolbar;
            pd.toolbar = newToolbar;
            pd.toolbartab = newToolbartab;
          }
        });
      }
    },
    onChangeStateState: function (stateModel, stateState) {
      function retrieveUpdated() {
        var hModel = this
            , items = this.data.content.items
            , sids = [], sid;
        if (!_.isEmpty(items)) {
          _.each(items, function (item) {
            sid = ('' + item.data.sid) || false;
            !!sid && sids.push(sid);
          });
        }

        if (!_.isEmpty(sids)) {
          var ajax = _sxt.ajaxCreate('bookmarkRetrieveUpdated', $('body'), true)
              , path = _xtsi.baseAjaxPath() + 'retrieve/attachdata';

          ajax.url = ajax.options.url = Drupal.url(path);
          ajax.options.data = {slogitem_ids: _.uniq(sids).join(';')};
          ajax.slogxt.targetThis = this;
          ajax.slogxt.targetCallback = ajaxResponse;
          slogLog('XtsiBookmarkModel.onChangeStateState....ajaxCall');
//          ajax.xttimer.start();
          ajax.$xtelement.trigger('bookmarkRetrieveUpdated');
        }
      }
      function ajaxResponse(ajax, response, status) {
//        ajax.xttimer.stop();
//        slogLog('XtsiBookmarkModel.onChangeStateState....ajaxResponse(attachdata): ' //
//            + Object.keys(response.data).length);
//        slogLog('....ajaxResponse(attachdata).duration: ' + ajax.xttimer.ElapsedMilliseconds);
        this.updateDataContent(response.data);
      }

      var stateData = stateModel.get('stateData');
      try {
        switch (stateState) {
          case 'loaded':
            if (stateData.Bookmarks && !_.isEmpty(stateData.Bookmarks)) {
              this.data = this.statePreLoad(stateData.Bookmarks);
              !!this.data.content && (this.data.content.isContent = true);
            }
            this.set('lastAction', {action: 'stateLoaded'});
            setTimeout($.proxy(retrieveUpdated, this), 50);
            break;
          case 'writing':
            stateData.Bookmarks = this.data;
            break;
        }
      } catch (e) {
        slogErr('XtsiBookmarkModel.onChangeStateState: ' + stateState + "\n" + e.message);
        this.clearData();
      }
    },
    fixItemsTbline: function (items) {
      var tblRegionData = this.mainModel.getTblineRegionTbData()
          , regionData, idx, item, toolbar;
      for (idx in items) {
        item = items[idx];
        toolbar = _xtsi.getToolbarByData(item)
        regionData = tblRegionData[toolbar] || false;
        if (regionData) {
          item.tbline = regionData.tbline;
          item.tblineIndex = regionData.tblineIndex;
        }
      }

      return items;
    },
    statePreLoad: function (stateData) {
      stateData = stateData || {}
      var listItems = stateData.list ? stateData.list.items : false;
      if (listItems && listItems.length) {
        stateData.list.items = this.fixItemsTbline(listItems);
      }

      return stateData;
    }

  };

  /**
   * Backbone View for ....
   */
  _xtsi.XtsiBookmarkModel = Backbone.Model.extend(xtsiBookmarkModelExt);

})(jQuery, Drupal, drupalSettings, Backbone, _);
