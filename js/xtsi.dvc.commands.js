/**
 * @file
 * sxt_slogitem/js/xtsi.dvc.commands.js
 */

(function ($, Drupal, _, undefined) {

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt
      , _stb = Drupal.slogtb
      , _xtg = Drupal.sxt_group;

  var xtsiCommands = {
    reload: function (args) {
      this.$btnClose.button('disable');
      var activeContentView = _xtsi.getActiveContentView();
      activeContentView.historySetCurrentItem(true);
      this._sxtReload(!!args['reloadImmediately'] || !_sxt.localSwitches.reloadPause);
    },
    preSubmitBmstorageSave: function (slogxtData) {
      function getSelectedIds() {
        var awbData = this.actionWizardBaseData || {}
        , targetid = awbData.targetid || false
            , checked = awbData.selected ? awbData.selected.checked || false : false
            , model = this.bmModel || this.historyModel;
        if (targetid && checked) {
          if (model) {
            return (model.getIdsByTargetid(targetid, checked)).join(',');
          }
        }

        slogErr('preSubmitBmstorageSave: invalid selection');
        return false;
      }

      var selectedIds = this.getSelectedIds
          ? this.getSelectedIds()
          : getSelectedIds.call(this);
      this.$activeContent
          .find('input[data-drupal-selector="edit-ids-selected"]')
          .val(selectedIds);
    },
    preSubmitLinkSids: function (slogxtData) {
      function getSelectedSids() {
        var linkSids = '', $form, tabData, $content, $items
            , pdLen = this.pagesData.length;
        $form = (this.pagesData[pdLen - 1].$content || $()).find('form');
        if ($form.length) {
          tabData = this.pagesData[pdLen - 2].tabData || {};
          $content = this.pagesData[pdLen - 2].$content || $();
          $items = $content.find('ul.slogxt-list>li');
          $.each($items, function (iidx, item) {
            var $item = $(item)
                , eid = $item.attr('entityid') || false;
            if (eid) {
              !!linkSids && (linkSids += ',');
              linkSids += eid;
            }
          });
        }
        return linkSids;
      }

      this.$activeContent
          .find('input[data-drupal-selector="edit-link-sids"]')
          .val(getSelectedSids.call(this));
    },
    alterFormAddList: function (args) {
      function getSelIdx(items, selIdx) {
        if (selIdx === 'current' && args.hasItems) {
          var activeContentView = _xtsi.getActiveContentView()
              , curSid = parseInt(activeContentView.curSid, 10)
              , item;
          for (var idx in items) {
            item = items[idx];
            if (item.sid === curSid) {
              selIdx = idx;
              break;
            }
          }
        } else {
          selIdx = (args.selIdx === undefined) ? -1 : args.selIdx
        }
        return parseInt(selIdx, 10);
      }
      function buildList(items, selIdx) {
        var data = [], idx, item;
        for (var idx in items) {
          item = items[idx];
          data.push({
            index: idx,
            liTitle: item.mainLabel,
            liDescription: _xtsi.contentPathLabel(item.pathLabel, item.listLabel, item.label),
            liDetails: item.liDetails || false,
            selected: (idx == selIdx)
          });
        }
        return Drupal.theme.slogxtListRadios(data);
      }
      function attach() {
        $form.find('li')
            .once('slogxt')
            .on('click', $.proxy(function (e) {
              e.stopPropagation();
              var $li = $(e.target).closest('li')
                  , idx = parseInt($li.attr('index'), 10)
                  , contentData = siItems[idx] || false
                  , $field = this.$activeContent.find('input[data-drupal-selector="' + fieldSelector + '"]')
                  , activeContentView = _xtsi.getActiveContentView();
              $li.siblings().removeClass('is-current checked');
              $li.addClass('is-current checked');
              contentData && activeContentView.setListItemData(contentData);
              $field.length && $field.val(contentData.sid);
              this._sxtCalculateCheckedItems();
            }, thisView));
      }

      // 
      var thisView = this
          , $form = this.$activeContent.find('form')
          , siItems = args.items
          , fieldSelector = args.fieldSelector || ''
          , selIdx = getSelIdx(siItems, args.selIdx)
          , showRadios = !!args.radios
          , list = buildList(siItems, selIdx)
          , pageData = this._sxtWizardPageData();
      $form.find('.js-form-wrapper').hide();
      $form.prepend($(list));
      if (showRadios && pageData) {
        pageData.type = 'radios';
        $form.addClass('type-select type-radios');
        this._sxtCalculateCheckedItems();
        pageData.slogxtData.submitPreserve = true;
      }
      !!args.notice && $form.prepend($(args.notice));
      if (!!args.enableSubmit || !!args.onDeactivate) {
        this.$btnSubmit.button('enable');
        pageData.slogxtData.submitPreserve = true;
      } else if (!args.hasItems) {
        this.$btnSubmit.button('disable');
        pageData.slogxtData.submitPreserve = true;
      }
      attach();
      (selIdx >= 0) && $form.find('li').get(selIdx).click();
    },
    resolvePathSlogitemId: function (key, tabData) { // called by _sxtContentForActiveTab
      function validate(data) {
//        slogLog('xtsiCommands.resolvePathSlogitemId.......validate()');
        if (!data.disallow && !!data.targetKey && data.targetKey === 'XtsiSlogitem') {
          donView.resetSelected(baseData, _sxt.invalidSel);

          var valid = true
              , permToolbar = data.toolbar;
          if (_sxt.isTbSysId(permToolbar) //
              && _.contains(['archive', 'trash'], data.actionKey) //
              && !tabData.onClientResolve) {
            if (isMultiple && donView.getInfoCount()) {
              valid = false;
            }

            if (valid) {
              data.targetTb = actionBase.menuTb;
              data.toolbartab = data.actionKey + '_' + actionBase.menuTb;
              data.serverResolve = baseData.resolveKey + '::' + data.actionKey + '~' + data.targetTb;
              data.entityId = -1;
              tabData.serverResolve = data.serverResolve;
              baseData.multiple = false;
              donView.model.slogxtReTrigger('change:baseData');
              thisView.mainView.resetBtnItemAction();

              return true;
            }
          }

          if (valid) {
            var tEntity = data.targetEntity || {}
            , tType = tEntity.type || false
                , nodeId = tEntity.eid || false
                , selCount = donView.getInfoCount()
                , maxCount = baseData.maxCount || 10
                , invalidText = _sxt.invalidSel
                , isEmptyList = data.isEmptyList || false
                , nodePermTb = allowed.nodePermTb;

            valid = (tType === 'node' && !!nodeId);  // for node only !!!
            if (tabData.onClientResolve && allowed.sysInMenuTid) {
              valid = _sxt.isTbSysId(permToolbar) && data.menuTid == allowed.sysInMenuTid;
              !valid && (invalidText = Drupal.t('invalid content list'));
            } else if (!allowed.allowAll) {
              isEmptyList && (valid = !baseData.isSource);
              if (valid) {
                if (_sxt.isTbSysId(permToolbar) && allowed.sysInMenuTid) {
                  valid = true;
                } else if (allowed.allowByTbRank) {
                  valid = true;
                } else if (nodePermTb && data.basePermTb) {
                  permToolbar = data.basePermTb;
                  valid = (permToolbar === nodePermTb.basePermTb && data.menuTid == nodePermTb.inTermId);
                } else {
                  valid = (permToolbar === allowed.menuTb);
                }
              }
            }
          }


          // permissions
          if (valid && !_sxt.isAdmin()) {
            if (baseData.isSource && allowed.allowByTbRank) {
              var tCache = thisView._sxtCacheGet('action', 'target') || {}
              , tToolbar = (tCache.tbPathData || {}).toolbar;
              valid = _sxt.getPermByTbRank(permToolbar, tToolbar);
            } else if (permToolbar === 'role') {
              if (!_xtsi.hasPermRoleContent(perms.role)) {
                valid = false;
              }
            } else if (permToolbar === 'user') {
              if (!perms.user) {  // determined serverside
                valid = false;
              }
            } else if (permToolbar === _sxt.getTbSysId()) {
              //sysInMenuTid
              if (allowed.sysInMenuTid) {
                valid = (allowed.sysInMenuTid == data.menuTid);
              } else if (data.targetTb != _sxt.tokenTargetTb  // allow selection for __targettb__
                  && (!data.rolePerm || !_xtg.hasPermission(data.rolePerm))) {
                valid = false;
              }
            } else if (permToolbar === _sxt.getTbSysNode()) {
              slogErr('Validate for toolbar: ' + permToolbar);
              valid = false;
            } else if (!_sxt.hasPermToolbar(permToolbar)) {
              valid = false;
            }
          }


          if (valid && baseData.isSource) {
            var sCache = thisView._sxtCacheGet('action', 'source') || {}
            , tCache = thisView._sxtCacheGet('action', 'target') || {}
            , ttdHasNids = (tCache.tabData || {}).hasNids;

            if (!!sCache.menuTid && sCache.menuTid !== data.menuTid) {
              valid = false;
              invalidText = Drupal.t('invalid content list');
              sCache.liView.setPathDataAndShow(sCache.tbPathData);
            } else if (selCount >= maxCount) {
              valid = false;
              invalidText = Drupal.t('max. number reached');
            }
            valid && (valid = !!ttdHasNids);
            if (valid && _.contains(ttdHasNids, parseInt(nodeId, 10))) {
              valid = false;
              invalidText = Drupal.t('content already in list');
            }
            if (!baseData.multiple) {
              baseData.multiple = true;
              donView.model.slogxtReTrigger('change:baseData');
            }
          }

          valid ? donView.resetPrepared() : donView.resetSelected(baseData, invalidText);
          data.liView.setItemAction(valid, baseData.multiple);

          return valid;
        }
        return false;
      }

      function onDonItemClick(e) {
//        slogLog('xtsiCommands.resolvePathSlogitemId.......onDonItemClick()');
        var $li = $(e.target).closest('li')
            , entityid = $li.attr('entityid') || false
            , isEmpty = !!entityid && entityid < 0
            , targetid = this.model.get('activeTab').targetid
            , tabData = this.tabsData[targetid] || {}
        , itemData = tabData.siItemsData[entityid] || false
            , tbPathData = tabData.tbPathsData[entityid] || false
            , liView = itemData ? _xtsi.getSiListView(itemData.toolbar) : false
            , cView = _xtsi.getActiveContentView()
            , cancelClass = baseData.multiple ? 'has-cancel' : '';
        if (baseData.multiple) {
          $li.prepend(this.$btnItemAction || $());
          this.$btnItemAction
              .off().on('click', $.proxy(onDonItemRemoveClick, this));
        }
        $li.siblings().removeClass('is-current has-cancel');
        $li.addClass('is-current ' + cancelClass);
        baseData.isSource && (this.$donSourceItem = $li);
        baseData.isTarget && (this.$donTargetItem = $li);

//        slogLog('xtsiC...onDonItemClick().tbPathData: ' + tbPathData);
        if (liView) {
          tbPathData && liView.setPathDataAndShow(tbPathData);
          liView.historySetCurrentItem();
          !isEmpty && itemData && cView && cView.setListItemData(itemData);
        }
      }

      function onDonSelected(data /*donData*/) {
        var curPageIdx = this.getWizardCurPageIdx()
            , itemData = data.itemData
            , tbPathData = data.tbPathData || false;
        this._sxtWizardPagesDestroy(curPageIdx + 1);
        if (!itemData || !tbPathData) {
          return;
        }

        if (!baseData.multiple) {
          tabData.siItemsData = {};
          tabData.tbPathsData = {};
        }
        tabData.siItemsData = tabData.siItemsData || {};
        tabData.siItemsData[data.entityId] = itemData;
        tabData.tbPathsData = tabData.tbPathsData || {};
        tabData.tbPathsData[data.entityId] = tbPathData;

        if (baseData.isTarget) {
          var sils = data.siLiData ? data.siLiData.settings || {} : {}
          , entities = sils.entities || false
              , nids = [];
          if (entities) {
            _.each(entities, function (entity) {
              (entity.type === 'node') && nids.push(parseInt(entity.eid, 10))
            });
          }
          tabData.ownNids = nids;
          tabData.hasNids = _.clone(nids);
          this._sxtCacheSet('action', 'target', {
            menuTid: tbPathData.tid,
            liView: data.liView,
            itemData: itemData,
            tbPathData: tbPathData,
            tabData: tabData
          });
        } else if (baseData.isSource || allowed.allowAll) {
          if (allowed.allowAll) {
//todo::next::   allowed.allowAll
          } else {
            var tCache = this._sxtCacheGet('action', 'target')
                , tEntity = data.targetEntity || {}
            , tEid = parseInt(tEntity.eid, 10);
            !baseData.multiple && (tCache.tabData.hasNids = _.clone(tCache.tabData.ownNids));
            tEid && tCache.tabData.hasNids.push(tEid);
          }

          this._sxtCacheSet('action', 'source', {
            menuTid: tbPathData.tid,
            liView: data.liView,
            itemData: itemData,
            tbPathData: tbPathData,
            tabData: tabData
          });
        }
      }

      function onDonReturn(data /*donData*/) {
        baseData.isTarget && this.$donTargetItem && this.$donTargetItem.click();
        baseData.isSource && this.$donSourceItem && this.$donSourceItem.click();
      }

      function onDonItemRemoveClick(e) {
        e.stopPropagation();
        var $li = $(e.target).closest('li')
            , $nextli = $li.next()
            , sid = $li.attr('entityid') || false
            , nodeId = $li.attr('nodeid') || false
            , targetid = this.model.get('activeTab').targetid
            , curPageIdx = this.getWizardCurPageIdx()
            , wId = 'donWrapper' + curPageIdx
            , $donWrapper = this.$activeContent.find('#' + wId)
            , tabData = this.tabsData[targetid] || {}
        , tCache = this._sxtCacheGet('action', 'target') || {}
        , ttdHasNids = (tCache.tabData || {}).hasNids
            , nidsIdx = ttdHasNids ? _.indexOf(ttdHasNids, parseInt(nodeId, 10)) : -1
            , $liItems, liCount;

        $li.remove();
        $liItems = $donWrapper.find('li');
        liCount = $liItems.length;
        if (liCount === 0) {
          this.$btnSubmit.button('disable');
          this._sxtCacheSet('action', 'source', {});
        }
        this.donView.showSelectedCount(liCount);
        !!sid && !!tabData.siItemsData[sid] && delete tabData.siItemsData[sid];
        if (nidsIdx >= 0) {
          delete ttdHasNids[nidsIdx];
          tCache.tabData.hasNids = _.compact(ttdHasNids);
        }
        baseData.isTarget && (this.$donTargetItem = $());
        baseData.isSource && (this.$donSourceItem = $());
        if ($nextli.length) {
          $nextli.click();
        } else {
          $liItems.last().click();
        }
        if (this.provideWizardPath) {
          var count = donView.getInfoCount()
              , wpLabel = Drupal.t('Items') + ': ' + count;
          this._sxtSetWizardPath(wpLabel);
        }
        this._sxtWizardPagesDestroy(curPageIdx + 1);
        this._sxtDialogAdjust();
      }

      function onDlgListItemClicked($li, lView, lData) {
//        slogLog('xtsiCommands.resolvePathSlogitemId.......onDlgListItemClicked()');
        var mainView = _xtsi.mainView()
            , $itemAction = mainView.$btnItemAction
            , $ul = $li.closest('ul');
        $itemAction.removeClass('documentadd blocked select')
        $li.prepend($itemAction);
        $ul.removeClass('has-action');
        $itemAction.off();
        setTimeout(function () {
          var donData = lView ? lView.getSlogitemDonData() || {} : {}
          , valid = (!!donData.entityId && donData.entityId == lData.sid);
          !valid && (donData.disallow = true);
          _sxt.setDoNavigateRawData(donData, true);
          _sxt.getDoNavigateView().attachItemAction($itemAction);
          valid && $ul.addClass('has-action');
        }, 10);
      }

      function onChangeDonIsActive(isActive, detach) {
        if (detach || !isActive) {
          _xtsi.mainView().detechItemAction();
        }
      }

      function onChangeCurSysListView(data) {
        var liView = data.liView
            , slogitemList = liView.getSlogitemListData()
            , itemData = liView.getCurItemData() || false
            , tbPathData = itemData ? itemData.tbPathData : {
              path: slogitemList.path,
              tid: slogitemList.tid,
              toolbar: slogitemList.toolbar,
              toolbartab: slogitemList.toolbartab
            }
        , donData = {
          toolbar: tbPathData.toolbar,
          toolbartab: tbPathData.toolbartab,
          siLiData: slogitemList,
          itemData: itemData ? itemData : slogitemList,
          tbPathData: tbPathData,
          liView: liView
        };

        onDonSelected.call(thisView, donData);
      }

      // init resolvePathSlogitemId for current wizard page
      var thisView = this
          , donView = this.donView
          , actionBase = this._sxtCacheGet('dialog', 'actionBase') || {}
      , aBaseView = actionBase.view || false
          , donView = this._sxtDonEnsureView()
          , perms = tabData.resolveArgs ? tabData.resolveArgs.perms || {} : {}
      , args = tabData.resolveArgs ? tabData.resolveArgs[key] || {} : {}
      , isMultiple = args.multiple || false
          , btnTitle = isMultiple ? Drupal.t('Select list items') : Drupal.t('Select list item')
          , xtTitle = args.xtTitle || btnTitle
          , xtInfo = args.xtInfo || ''
          , allowed = {
//todo::next::   allowed.allowAll
            allowAll: !!args.allowAll,
            allowByTbRank: !!args.allowByTbRank,
            sysInMenuTid: !!args.sysInMenuTid ? args.sysInMenuTid : false,
            menuTb: actionBase.menuTb,
            nodePermTb: actionBase.nodePermTb
          }
      , info, $doBtn;

      tabData['dialogTitle'] = tabData['actionTitle'] + ': ' + xtTitle;
      if (isMultiple) {
        info = Drupal.t('Click the select button for selecting list items.') + '<br /><br />';
        info += Drupal.t('After clicking the button you may navigate and select some list items.');
      } else {
        info = Drupal.t('Click the select button for selecting a list item.') + '<br /><br />';
        info += Drupal.t('After clicking the button you may navigate and select a list item.');
      }
      $doBtn = Drupal.theme.slogxtDialogDoButton(this.$activeContent, btnTitle, info, xtInfo);

      isMultiple && donView.showSelectedCount(0);
      tabData.preserve = !!args.preserve;
      tabData.siItemsData = {};
      tabData.tbPathsData = {};
      if (isMultiple && tabData.onClientResolve && tabData.serverResolved) {
        tabData.onClientResolve = false;
        tabData.serverResolve = false;
        tabData.serverResolved = false;
      }

      var baseData = {
        perms: perms,
        args: args,
        resolveKey: key,
        infoHint: btnTitle,
        targetKey: 'XtsiSlogitem',
        multiple: isMultiple,
        isSource: !!args.isSource,
        isTarget: !!args.isTarget,
        validate: validate,
        onDonSelected: onDonSelected,
        onDonReturn: onDonReturn,
        onDonItemClick: onDonItemClick,
        onDlgListItemClicked: onDlgListItemClicked,
        onChangeCurSysListView: onChangeCurSysListView,
        onChangeDonIsActive: onChangeDonIsActive
      };
      $doBtn.on('click', $.proxy(this, '_sxtDonStart', baseData));

      if (baseData.isTarget) {
        this._sxtCacheSet('action', 'target', {});
        this.$donTargetItem = $();
        this.$donSourceItem = $();
      } else if (baseData.isSource) {
        if (!this._sxtCacheGet('action', 'target')) {
          this._sxtCacheSet('action', 'target', {
            tabData: {ownNids: [], hasNids: []}
          });
        }
      }

      tabData.baseData = baseData;
      if (aBaseView && !allowed.sysInMenuTid && !baseData.isSource) {
        setTimeout($.proxy(function () {
          aBaseView.historySetCurrentItem();
          var donData = aBaseView.getSlogitemDonData();
          if (donData && donData.siLiData && baseData.validate(donData)) {
            this._sxtDonStartSilent(baseData);
            donView && donView.selectPreparedData(donData, true);
            if (isMultiple) {
              var wId = 'donWrapper' + this.getWizardCurPageIdx()
                  , $donWrapper = this.$activeContent.find('#' + wId)
                  , wpLabel = Drupal.t('Items') + ': ' + $donWrapper.find('li').length;
              this._sxtSetWizardPath(wpLabel);
            }
          }
        }, this), 200);
      }
    },
    resolvePathNodeId: function (key, tabData) { // called by _sxtContentForActiveTab
      function validate(data) {
//        slogLog('xtsiCommands.resolvePathNodeId.......validate()');
        //todo::current::permissions::xtsi
        if (!!data.targetKey && data.targetKey === 'XtsiNodeItem') {
          donView.resetSelected(baseData, _sxt.invalidSel);
          if (data.nodeId != 0) {
            var delimiter = _sxt.getResolveDelim();
            data.entityId = key + delimiter + data.nodeId;
            return true;
          }
        }
        return false;
      }

      var donView = this.donView
          , args = tabData.resolveArgs ? tabData.resolveArgs[key] || {} : {}
      , btnTitle = Drupal.t('Select node')
          , xtTitle = args.xtTitle || btnTitle
          , xtInfo = args.xtInfo || ''
          , info, $doBtn;
      tabData['dialogTitle'] = tabData['actionTitle'] + ': ' + xtTitle;
      info = Drupal.t('Click the select button for selecting a node.') + '<br /><br />';
      info += Drupal.t('After clicking the button you may navigate the contents and select a node.');
      $doBtn = Drupal.theme.slogxtDialogDoButton(this.$activeContent, btnTitle, info, xtInfo);

      var baseData = {
        args: args,
        resolveKey: key,
        infoHint: btnTitle,
        targetKey: 'XtsiNodeItem',
        validate: validate
      };
      $doBtn.on('click', $.proxy(this, '_sxtDonStart', baseData));

      var cmView = _xtsi.getSiContentView('tContentMain');
      if (cmView.hasTbnodeVisible()) {
        setTimeout($.proxy(function () {
          var donData = cmView.hview.getNodeDonData()
              , donView = _sxt.getDoNavigateView();
          if (baseData.validate.call(this, donData)) {
            this._sxtDonStartSilent(baseData);
            donView && donView.selectPreparedData(donData, true);
          }
        }, this), 10);
      }
    },
    attachSysXtsiContentList: function ($content) {
      var pathPart = _xtsi.baseAjaxPath() + 'sid/';
      if (!!pathPart) {
        $content.find('li.radio')
            .once('xtsi-content-list')
            .on('click', $.proxy(function (e) {
              var sid = $(e.target).closest('li').attr('entityid')
                  , href_selected = Drupal.url(pathPart + sid)
                  , sysLiView = _xtsi.getSysListView();
              $.each(sysLiView['$items'], function (iidx, item) {
                var $item = $(item);
                if ($item.attr('href') === href_selected) {
                  $item.click();
                  return false;
                }
              });
            }, this));
        $content.find('li.radio.checked').click();
      }
    },
    tbMenuCheckboxes: function ($content, data) {
//      var callback = this._sxtCommandCallback('radios');
      var callback = this._sxtCommandCallback('checkboxes')
          , defaultRole = _sxt.getUserDefaultRole();
      callback.apply(this, [$content, data]);

      // show the right toolbar icon
//      $content.find('ul.slogxt-list li.radio').each(function () {
      $content.find('ul.slogxt-list li.checkbox').each(function () {
        var $el = $(this)
            , toolbar = $el.data('toolbar')
            , pdRole = $el.data('role') || false
            , tbView = _stb.getSlogtbView(toolbar);
        if (toolbar === 'role' && pdRole && pdRole !== defaultRole) {
          toolbar = _sxt.getTbSysId();
        }
        if (_sxt.isTbSysId(toolbar)) {
          $el.addClass('xt-toolbar-icon tbsys-icon');
        } else if (tbView && !!tbView.tbIconDarkUrl) {
          $el.find('.li-title').css('backgroundImage', tbView.tbIconDarkUrl);
        }
      });
    },
    attachTbmenuCheckboxes: function ($content) {
      $content.find('li.checkbox')
          .once('sxt-tbmenu-list')
          .on('click', $.proxy(function (e) {
            e.stopPropagation();
            var $item = $(e.target).closest('li')
                , toolbar = $item.data('toolbar');
            if (toolbar === 'role') {
              var defaultRole = _sxt.getUserDefaultRole()
                  , pdRole = $item.data('role');
              (defaultRole !== pdRole) && (toolbar = _sxt.getTbSysId());
            }
            var liView = _xtsi.getSiListView(toolbar)
                , pathData = {
                  path: $item.data('path'),
                  tid: $item.attr('entityid'),
                  toolbar: toolbar,
                  toolbartab: $item.data('toolbartab')
                };
            liView && liView.setPathDataAndShow(pathData);
          }, this));
//      $content.find('li.radio.checked').click();
//      $content.find('li.checkbox.checked').click();
    },
    finishedNodeEdit: function (args) {
      var nodeId = args.eid || false
          , closeDelay = args.closeDelay || false;
      nodeId && this.mainView.refreshNodeContents(args.eid, args.goto || false);
      closeDelay && this._sxtCloseWithDelay(closeDelay);
      
      //todo::refresh history, bookmarks, ...      
      args.isNew = false;
      !args.isNew && this.mainView.model.slogxtSetAndTrigger('change:editedNodeContent', null, args);
    },
    finishedSlogitemEdit: function (args) {
      var toolbar = args.toolbar || false
          , lView = !!toolbar ? _xtsi.getSiListView(toolbar) : false
          , curPathData = lView ? lView.curPathData : {}
      , curTid = curPathData.tid || 0;
      lView && curTid == args.tid && lView.refresh();
      !args.isNew && this.mainView.model.slogxtSetAndTrigger('change:editedSlogitem', null, args);
    },
    finishedXtsiListChange: function (args) {
      setTimeout($.proxy(function () {
        this.refreshListContents.call(this, args.toolbar, args.tid);
      }, this.mainView), 500);
    },
    finishedTbnodePrepared: function (args) {
      if (args.nid && args.nid > 0) {
        this.mainView.activeContentMoveToMainView();
        this.mainView.activateContent('tContentMain');
        setTimeout($.proxy(function () {
          this.refreshNodeContents.call(this, args.nid);
        }, this.mainView), 500);
      }
    },
    finishedBookmarksLoaded: function (args) {
      this.executeLoaded(args);
      this._sxtCallFinishedActionsWizard();
    },
    finishedBookmarksSaved: function (args) {
      this._sxtCallFinishedActionsWizard();
    },
    finishedSlogitemNewItems: function (args) {
      var iData = _.first(_.toArray(args));
      if (iData && iData.content && iData.content.toolbar) {
        var targetid = _xtsi.getContentTargetByTb(iData.content.toolbar)
            , content = iData.content
            , cView = _xtsi.getSiContentView(targetid)
            , actionBase = this._sxtCacheGet('dialog', 'actionBase') || {}
        , liView = actionBase.view;
        if (cView) {
          iData.content.reset = true;
          cView.setListItemData(iData.content);
        }
        if (liView) {
          liView.setPathDataAndShow(content.tbPathData || iData.list);
          liView.historySetCurrentItemTO(true);
          liView.refresh();
        }
      }
    },
    finishedSlogitemsMove: function (args) {
      function showView(liView, tbPathData, refresh) {
        liView.setPathDataAndShow(tbPathData);
        liView.historySetCurrentItemTO(true);
        refresh && liView.refresh();
      }
      function enable(target) {
        var se = (target === 'source') ? 'enable' : 'disable'
            , te = (target === 'target') ? 'enable' : 'disable';
        buttons.$source.button(se);
        buttons.$target.button(te);
      }

      var tCache = this._sxtCacheGet('action', 'target')
          , sCache = this._sxtCacheGet('action', 'source')
          , sView = sCache.liView
          , sTbPath = sCache.tbPathData
          , tView = tCache.liView
          , tTbPath = tCache.tbPathData
          , buttons = Drupal.theme.slogxtDialogMoveButtons(this.$activeContent);
      showView(sView, sTbPath, true);
      enable('target');
      setTimeout(function () {
        showView(tView, tTbPath, true); // do refresh
        enable('source');
      }, 1000);

      buttons.$source.on('click', function (e) {
        enable('target');
        sView && showView(sView, sTbPath);
      });
      buttons.$target.on('click', function (e) {
        enable('source');
        tView && showView(tView, tTbPath);
      });
      this.mainView.model.slogxtSetAndTrigger('change:movedSlogitems', null, args);
    },
    devEntitySearch: function (args) {
      var toolbar = args.toolbar || false
          , liView = toolbar ? _xtsi.getSiListView(toolbar) : false
          , tbPathData;
      if (args.entityKey === 'sid') {
        var data = args.data || {};
        tbPathData = data.tbPathData || {};
        if (data && data.path) {
          _xtsi.getActiveContentView().setListItemData(data);
          liView && tbPathData && liView.setPathDataAndShow(tbPathData);
        }
      } else if (args.entityKey === 'tid') {
        tbPathData = args.data;
        liView && tbPathData && liView.setPathDataAndShow(tbPathData);
      }

      this._sxtCloseWithDelay();
    }
  };

  _sxt.dvc.commands.sxt_slogitem = xtsiCommands;

})(jQuery, Drupal, _);
