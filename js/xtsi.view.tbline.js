/**
 * @file
 * ...
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  var _xtsi = Drupal.sxt_slogitem
      , _sxt = Drupal.slogxt;
  
  var xtTbLineProto = _sxt.TbLineView.prototype;
  var xtTbLineViewExt = {
    xtsiInitialized: false,
    xtsiInitialize: function () {
      if (!this.xtsiInitialized) {
        this.xtsiInitialized = true;
        this.mainView = _xtsi.mainView();
        this.mainModel = this.mainView.model;
//        this.historyView = _xtsi.historyView();
        this.historyModel = _xtsi.historyModel();
      }
    },
    xtsiEnsureInitialized: function () {
      !this.xtsiInitialized && this.xtsiInitialize();
    },
    xtsiGetHisoryButtons: function () {
      if (!this.$xtsiHistoryButtons) {
        this.$xtsiHistoryButtons = this.$tblineWrapper.find('.xt-tbline-history >.xt-tbline-item');
      }
      return this.$xtsiHistoryButtons;
    },
    getContentView: function (id) {
      return this.contentViews[id];
    },
    getCurrentContentView: function () {
      if (this.isContent) {
        return this.contentViews[this.mainModel.get('activeContentTarget')];
      } else {
        return this.tabsContentViews[this.model.get('currentTabIndex')];
      }
    },
    activateHistoryButtons: function (target, hData) {
//      slogLog('TbLineView..........activateHistoryButtons.........');
      this.xtsiEnsureInitialized();
      hData = hData || this.historyModel.getData(target);
      this.xtsiGetHisoryButtons().each(function () {
        var $item = $(this)
                , action = $item.attr('value')
                , enabled = false;
        if (hData) {
          switch (action) {
            case 'back':
              enabled = (hData.currentIdx < hData.length - 1);
              break;
            case 'next':
              enabled = (hData.currentIdx > 0);
              break;
            case 'dialog':
              enabled = (hData.length > 1);
              break;
          }
        }
        $item.toggleClass('disabled', !enabled);
      });
    }
  };
  var xtTbLineActions = {
    actionHistoryGo: function ($el, e) {
      var action = $el.attr('value')
              , contentView = this.getCurrentContentView()
              , target = contentView.getHistoryTarget();
      contentView.historyModel['go' + action.capitalize()](contentView, target);
    },
    actionHistoryDialog: function ($el, e) {
      var args = {
        view: 'XtsiHistoryView',
        provider: 'sxt_slogitem',
        options: {
          isForNavigate: _sxt.isDoNavigateActive(),
          startView: this.getCurrentContentView()
        }       
      };
      _sxt.dialogViewOpen(args);
    },
    actionSurpriseDialog: function ($el, e) {
      var args = {
        view: 'XtsiSurpriseView',
        provider: 'sxt_slogitem',
        options: {
          isForNavigate: _sxt.isDoNavigateActive()
        }       
      };
      _sxt.dialogViewOpen(args);
    },
    actionBookmarksDialog: function ($el, e) {
      var args = {
        view: 'XtsiBookmarkView',
        provider: 'sxt_slogitem',
        options: {
          isForNavigate: _sxt.isDoNavigateActive()
        }       
      };
      _sxt.dialogViewOpen(args);
    }
  };

  $.extend(true, xtTbLineProto, xtTbLineViewExt);
  xtTbLineProto.actions.sxt_slogitem = xtTbLineActions;

})(jQuery, Drupal, drupalSettings);

