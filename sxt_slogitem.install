<?php

/**
 * @file
 * Install, update and uninstall functions for the sxt_slogitem module.
 */

/**
 * Implements hook_schema().
 */
function sxt_slogitem_schema() {
  //todo::important::move sxt_slogitem_schema() from slogxt (disabled here)
  $schema['slogitem'] = array(
      'description' => 'Stores slogitem records.',
      'fields' => array(
          'sid' => array(
              'type' => 'serial',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'description' => 'The primary identifier for a slogitem.',
          ),
          'uuid' => array(
              'type' => 'varchar',
              'length' => 128,
              'not null' => FALSE,
              'description' => 'Unique Key: Universally unique identifier for this entity.',
          ),
          'tid' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 0,
              'description' => 'Reference {taxonomy_term_data}.tid.',
          ),
          'title' => array(
              'description' => 'Title of the slogitem.',
              'type' => 'varchar',
              'length' => 255,
              'not null' => TRUE,
              'default' => '',
          ),
          'route_name' => array(
              'type' => 'varchar',
              'length' => 255,
              'not null' => TRUE,
              'default' => 'slogitem.placeholder',
              'description' => 'The machine name of a defined Route this slogitem represents.',
          ),
          'route_parameters' => array(
              'type' => 'blob',
              'size' => 'big',
              'not null' => FALSE,
              'serialize' => TRUE,
              'description' => 'Serialized array of route parameters of this slogitem.',
          ),
          'entity' => array(
              'description' => 'Target Entity, i.e. node (to find in route_parameters).',
              'type' => 'varchar',
              'length' => 32,
              'not null' => TRUE,
              'default' => '',
          ),
          'eid' => array(
              'type' => 'int',
              'unsigned' => TRUE,
              'not null' => TRUE,
              'default' => 0,
              'description' => 'Target Entity ID, i.e. nid for entity node.',
          ),
          'status' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
              'size' => 'small',
              'description' => 'The status of this slogitem (status in progress).',
          ),
          'weight' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
              'description' => 'The weight of this slogitem in relation to other items.',
          ),
          'created' => array(
              'description' => 'The Unix timestamp when the slogitem was created.',
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
          ),
          'changed' => array(
              'type' => 'int',
              'not null' => TRUE,
              'default' => 0,
              'description' => 'The Unix timestamp when the slogitem was most recently saved.',
          ),
      ),
      'indexes' => array(
          'changed' => array('changed'),
          'created' => array('created'),
          'entity' => array('entity', 'eid'),
          'status' => array('status'),
          'tid' => array('tid'),
      ),
      'unique keys' => array(
          'uuid' => array('uuid'),
      ),
      'primary key' => array('sid'),
  );

  return $schema;
}

